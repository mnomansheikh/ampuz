<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    'db' => array(
        'driver'         => 'Pdo',
        'dsn'            => 'mysql:dbname=ampuz_new;host=localhost',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter'
            => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
    'session' => array(
        'config' => array(
            'class' => 'Zend\Session\Config\SessionConfig',
            'options' => array(
                'name' => 'user',
            ),
        ),
        'storage' => 'Zend\Session\Storage\SessionArrayStorage',
        'validators' => array(
            'Zend\Session\Validator\RemoteAddr',
            'Zend\Session\Validator\HttpUserAgent',
        ),
    ),

   // application constants

    'constant'=>array(

    define ('_PROJECT_NAME_', 'ampuz', true),
    define ('_P_', 'Pending', true),
    define ('_A_', 'Approved', true),
    define ('_W_', 'Waiting', true),
    define ('_E_', 'Expired', true),
    define ('_ADMIN_', '1', true),
    define ('_CHAMPION_', '2', true),
    define ('_APPLICATION_', 'Application', true),
    define ('_DASHBOARD_', 'Dashboard', true),
    define ('_EMAIL_', 'Email', true),
    define ('_LOGIN_', 'login', true),
    define ('_ACTIVE_', 'A', true),
    define ('_DEACTIVE_', 'D', true),
    define ('_UPLOAD_', 'upload', true),
    define ('_ATTACHMENT_', 'attachment', true),
    define ('_MANUAL_FIELD_EXTRA_', '3', true),
    define ('_BULK_FIELD_EXTRA_', '10', true)
    )
    );



<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/27/2016
 * Time: 5:40 PM
 */

namespace MeasuringUnit\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class MeasuringUnitTable
{
    protected $tableGateway;

    public function __construct(tableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    public function getMeasuringUnitMaster($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getMeasuringUnit($id)
    {

        $rowset = $this->tableGateway->select(array('id' => $id));


        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveMeasuringUnit(MeasuringUnit $MeasuringUnit, $adapter)
    {
        $sql = new Sql($adapter);
        $id = $MeasuringUnit->id;
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $user_type = $userSession->user_type;


        if ($id == 0) {
         
            $data = array(
                'unit_description' => $MeasuringUnit->unit_description,
                'measure_group_id' => $MeasuringUnit->measure_group_id,
                'is_active' => $MeasuringUnit->is_active,
                );
            $MeasuringUnit_query = $this->tableGateway->insert($data);
            $last_inserted_id = $this->tableGateway->lastInsertValue;

        } 
        else {

            if ($this->getMeasuringUnitMaster($id)) {

                    $data = array(
                        'unit_description' => $MeasuringUnit->unit_description,
                        'measure_group_id' => $MeasuringUnit->measure_group_id,
                        'is_active' => $MeasuringUnit->is_active
                    );
                    $this->tableGateway->update($data, array('id' => $id));


            } else {
                throw new \Exception('Measuring Unit id does not exist');
            }

        }
    }

    /*public function deleteMeasuringUnit($id, $adapter)
    {
        $this->tableGateway->delete(array('milestone_id' => (int)$id, 'status' => _P_));

        $sql = new Sql($adapter);
        $delete = $sql->delete();
        $delete->from('milestone_details');
        $delete->where(array('milestone_id' => (int)$id));
        $statement = $sql->prepareStatementForSqlObject($delete);
        $results = $statement->execute();
    }*/

    public function fetchAll_measuringunit($adapter, $paginated = false)
    {
        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            //        $sql = new Sql($adapter);
            $select->from(array('mu' => 'measure_unit'));
            $select->join(array('mg' => 'measure_group'), 'mu.measure_group_id = mg.id', 'description');
            $select->group('mu.id');

            //$select->from('measure_unit');
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            // create a new result set based on the User entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new MeasuringUnit());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

}



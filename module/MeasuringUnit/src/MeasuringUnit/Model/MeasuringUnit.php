<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/27/2016
 * Time: 5:42 PM
 */

namespace MeasuringUnit\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class MeasuringUnit implements InputFilterAwareInterface
{
    public $id;
    public $unit_description;
    public $value;
    public $measure_group_id;
    public $indicator_id;
    public $is_active;
// public $user_id;
    public $inputFilter;

    protected $dbAdapter;

    public function setDbAdapter($dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->unit_description = (!empty($data['unit_description'])) ? $data['unit_description'] : null;
        $this->measure_group_id = (!empty($data['measure_group_id'])) ? $data['measure_group_id'] : null;
        $this->is_active = (isset($data['is_active'])) ? $data['is_active'] : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $inputFilter->add(array(
                'name' => 'id',
                'required' => false,
                'filters' => array(
                    array('name' => 'Int'),
                ),
            ));

//            $inputFilter->add(array(
//                'name' => 'unit_description', // add second password field
//                /* ... other params ... */
//                'validators' => array(
//                    array(
//                        'name' => '\Zend\Validator\Db\NoRecordExists',
//                        'options' => array(
//                            'table' => 'measure_unit',
//                            'field' => 'unit_description',
//                            'adapter' => $this->dbAdapter,
//                            'messages' => array(
//                                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'Change Measuring Unit Name already exist',
//                            ),
//                        ),
//                    ),
//                ),
//            ));

            $this->inputFilter = $inputFilter;

        }

        return $this->inputFilter;
    }
}
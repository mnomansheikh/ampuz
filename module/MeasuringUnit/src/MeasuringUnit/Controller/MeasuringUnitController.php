<?php

//module/Login/src/Controller/AuthController.php
namespace MeasuringUnit\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use MeasuringUnit\Model\MeasuringUnit;
use MeasuringUnit\Form\MeasuringUnitForm;

class MeasuringUnitController extends AbstractActionController
{
    protected $form;
    protected $userSession;
    protected $storage;
    protected $authservice;
    // protected $currentUserId;
    protected $MeasuringUnitTable;
//    public function getUserProfileTable()
//    {
//        if (!$this->UserProfileTable) {
//            $sm = $this->getServiceLocator();
//            $this->UserProfileTable = $sm->get('Login\Model\UserProfileTable');
//        }
//        return $this->UserProfileTable;
//    }
    public function getMeasuringUnitTable()
    {
        if (!$this->MeasuringUnitTable) {
            $sm = $this->getServiceLocator();
            $this->MeasuringUnitTable = $sm->get('MeasuringUnit\Model\MeasuringUnitTable');
        }
        return $this->MeasuringUnitTable;
    }


    public function getAuthService()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authservice;
    }


    public function ListAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $form = new MeasuringUnitForm($dbAdapter);
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user

            // grab the paginator from the CommunityTable
            $paginator = $this->getMeasuringUnitTable()->fetchAll_measuringunit($dbAdapter, true);
            // set the current page to what has been passed in query string, or to 1 if none set
            $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
            // set the number of items per page to 10
            $paginator->setItemCountPerPage(10);

            $view = new ViewModel(array(
                'paginator' => $paginator,
                'form' => $form

            ));
            $view->setTemplate('measuring-unit/list');
            return $view;
        }

    }

    public function addAction()
    {

        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            //  if ($userSession->user_type == _ADMIN_) {
            $form = new MeasuringUnitForm($dbAdapter);
            $form->get('submit')->setValue('Save');
            $form->get('cancel')->setValue('Cancel');
            $request = $this->getRequest();

            if ($request->isPost()) {
                $MeasuringUnit = new MeasuringUnit();
                $MeasuringUnit->setDbAdapter($dbAdapter);
                $form->setInputFilter($MeasuringUnit->getInputFilter());
                $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(),
                    $this->getRequest()->getFiles()->toArray()
                );

                $form->setData($request->getPost());
                $form->setData($data);

                if ($form->isValid()) {
                    $MeasuringUnit->exchangeArray($form->getData());
                    $this->getMeasuringUnitTable()->saveMeasuringUnit($MeasuringUnit, $dbAdapter);
                    // Redirect to list
                    return $this->redirect()->toRoute('measuring-unit', array(
                        'action' => 'list'
                    ));
                }
            }


            //return array('form' => $form, 'event' => $this->getUserProfileTable()->fetchAll($dbAdapter));
            $view = new ViewModel(array(
                'form' => $form
            ));
            $view->setTemplate('measuring-unit/add');
            return $view;
            // }
            //  if ($userSession->user_type == _CHAMPION_) {
            //      return $this->redirect()->toRoute('application');
            //  }
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function editAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $id = (int)$this->params()->fromRoute('id', 0);
            try {
                $MeasuringUnitTable = $this->getMeasuringUnitTable()->getMeasuringUnit($id, $dbAdapter);
            } catch (\Exception $ex) {
                //return $this->redirect()->toRoute('measuring-unit', array(
                //    'action' => 'list'
                //));
            }
            $form = new MeasuringUnitForm($dbAdapter);
            $MeasuringUnit = new MeasuringUnit();
            $form->bind($MeasuringUnitTable);
            //for session varibles
            $form->get('submit')->setValue('Save');
            $form->get('cancel')->setValue('Cancel');
            $request = $this->getRequest();
            if ($request->isPost()) {
                $MeasuringUnit->setDbAdapter($dbAdapter);
                $form->setInputFilter($MeasuringUnit->getInputFilter());
                $form->setData($request->getPost());
                if ($id > 0) {
                    $form->getInputFilter()->get('unit_description')->setRequired(false);
                }
                if ($form->isValid()) {
                    $MeasuringUnit->exchangeArray($form->getData());
                    $this->getMeasuringUnitTable()->saveMeasuringUnit($MeasuringUnit, $dbAdapter);

                    // Redirect to list
                    return $this->redirect()->toRoute('measuring-unit', array(
                        'action' => 'list'
                    ));
                }
            }
            //  $user = $this->getCurrentUserTable()->fetchAll_event($dbAdapter,$id);
            return array(
                'id' => $id,
                'form' => $form,
                //    'user' => $user,

            );
        } else {
            return $this->redirect()->toRoute('login');
        }

    }

}
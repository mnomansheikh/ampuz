<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/27/2016
 * Time: 5:43 PM
 */

namespace MeasuringUnit\Form;

use Zend\Form\Form;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Sql;
use Zend\Form\Element;

class MeasuringUnitForm extends Form
{

    public function __construct(AdapterInterface $dbAdapter)
    {
        // we want to ignore the name passed
        $this->adapter = $dbAdapter;
        parent::__construct('login');
        $sql = new Sql($dbAdapter);

        $select = $sql->select();
        $select->from('measure_unit');
        $select->where(array('is_active' => 1));
        $select->order('id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $measuringunits = $statement->execute();

        $select = $sql->select();
        $select->from('measure_group');
        //$select->join('company_region', 'company_region.region_id = region.id', 'company_id');
        $select->where(array('measure_group.is_active' => 1));
        $select->order('measure_group.id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $measure_groups = $statement->execute();
        $measure_groups_arr = array();
        $itr = 0;
        foreach ($measure_groups as $measure_group) {
            $measure_groups_arr[$measure_group['id']] = $measure_group['description'];

        }

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'unit_description',
            'attributes' => array(
                'id' => 'unit_description',
                'type' => 'text',
                'required' => 'required',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'measure_group_id',
            'options' => array(
                'label' => 'Measuring Group Name',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $measure_groups_arr,
            ),
            'attributes' => array(
                // 'value' => '', //set selected to '1'
                'class' => 'form-control',
                'id' => 'region_id',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_active',
            'attributes' => array(
                'id' => 'myonoffswitch',
                'class' => 'onoffswitch-checkbox',
                'value' => '1'
            ),
            'options' => array(
                'label' => 'Status',
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0'
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'submitbutton',
                'class' => 'btn8 btn-8 btn-8f pull-right',
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'type' => 'button',
            'options' => array(
                'label' => 'Cancel'
            ),
            'attributes' => array(
                'onclick' => 'javascript:window.location.href = "/' . _PROJECT_NAME_ . '/public/measuring-unit/list";',
                'class' => 'btn8 btn-8 btn-8f pull-right',
            ),
        ));
    }
}
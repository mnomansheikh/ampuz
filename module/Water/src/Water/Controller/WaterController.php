<?php
namespace Water\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Water\Model\Water;
use Water\Form\WaterForm;
use Water\Model\WaterTable;
use Zend\Session\Container;

use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
class WaterController extends AbstractActionController
{
    protected $WaterTable;
    protected $authservice;

    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }
        return $this->authservice;
    }
    public function getMenu()
    {
        if (!$this->MenuTable) {
            $sm = $this->getServiceLocator();
            $this->MenuTable = $sm->get('Application\Model\MenuTable');
        }
        return $this->MenuTable;
    }
    public function indexAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $form = new WaterForm();
            $form ->get('submit')->setValue('Request Indicator');
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $config = $this->getServiceLocator()->get('Config');
            $dsn = $config['db']['dsn'];
            $username = $config['db']['username'];
            $password = $config['db']['password'];
            $controllerClass= __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
           // $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            //$Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
           // $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsIDByGroup($dbAdapter,$moduleNamespace);
            //$frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
           // $record_expiration = $this->getMenu()->RecordExpiration($dbAdapter,$moduleNamespace,$Indicators,$Companies,$Countries,$frequencies);

            if ($userSession->user_type == _ADMIN_) {
                // grab the paginator from the CommunityTable
                $paginator = $this->getMenu()->fetchAll_admin($dbAdapter, true,$moduleNamespace,$dsn,$username,$password,$Indicators);
                //$paginator = $this->getWaterTable()->fetchAll_admin($dbAdapter, true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('water/admin');
                return $view;
            }
            if ($userSession->user_type == _CHAMPION_) {
                // grab the paginator from the CommunityTable
                //$paginator = $this->getWaterTable()->fetchAll_champion($dbAdapter, true);
                $paginator = $this->getMenu()->fetchAll_champion($dbAdapter, true,$moduleNamespace,$dsn,$username,$password,$Indicators);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('water/index');
                return $view;
            }
            /*   if ($userSession->user_type == _CHAMPION_) {
                   $form = new WaterForm();
                   $form->get('submit')->setValue('Add');
                   $request = $this->getRequest();

                   if ($request->isPost()) {
                       $water = new Water();
                       $form->setInputFilter($water->getInputFilter());
                       $form->setData($request->getPost());

                       if ($form->isValid()) {
                           $water->exchangeArray($form->getData());
                           $this->getWaterTable()->saveWater($water, null);

                           // Redirect to list of albums
                           return $this->redirect()->toRoute('water');
                       }
                   }
                   return array('form' => $form, 'water' => $this->getWaterTable()->fetchAll($dbAdapter));
               }
               */
        }
            else {
                return $this->redirect()->toRoute('login');
            }


//        return new ViewModel(array(
//            'water' => $this->getWaterTable()->fetchAll(),
//
//        ));
            //    $form->setVariable('form', $form);

    }

    public function getWaterTable()
    {
        if (!$this->WaterTable) {
            $sm = $this->getServiceLocator();
            $this->WaterTable = $sm->get('Water\Model\WaterTable');
        }
        return $this->WaterTable;
    }

    public function uploadtransportAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $indicatorId = $_GET['id'];
            if ($indicatorId) {
                $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
                $Indicators = $this->getMenu()->fetchIndicatorById($dbAdapter, $indicatorId);
            }
            $view = new ViewModel(array(
                'indicator' => $Indicators
            ));
            $view->setTerminal(true);
            $view->setTemplate('application/file_upload_transport');
            return $view;
            // return array('form' => $form);
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function uploadeventAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $indicatorId = $_GET['id'];
            if ($indicatorId) {
                $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
                $Indicators = $this->getMenu()->fetchIndicatorById($dbAdapter, $indicatorId);
            }
            $view = new ViewModel(array(
                'indicator' => $Indicators
            ));
            $view->setTerminal(true);
            $view->setTemplate('application/file_upload_event');
            return $view;
            // return array('form' => $form);
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function uploaddiversityAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $indicatorId = $_GET['id'];
            if ($indicatorId) {
                $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
                $Indicators = $this->getMenu()->fetchIndicatorById($dbAdapter, $indicatorId);
            }
            $view = new ViewModel(array(
                'indicator' => $Indicators
            ));
            $view->setTerminal(true);
            $view->setTemplate('application/file_upload_diversity');
            return $view;
            // return array('form' => $form);
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function addAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $controllerClass= __NAMESPACE__;
        $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
        $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
        $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
        $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
        $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter,$moduleNamespace);
        $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
        $DateField = $this->getMenu()->DateField();
        $form = new WaterForm($Companies,$Regions,$Countries,$Indicators,$frequencies,$DateField);
        $form->get('submit')->setValue('Save');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $water = new Water();
            $created_date =$this->getRequest()->getPost('create_date');
            $indicator_id =$this->getRequest()->getPost('type');
            $company_id =$this->getRequest()->getPost('company_id');
            $country_id =$this->getRequest()->getPost('country_id');
            $frequency_id =$this->getRequest()->getPost('frequency_id');
            $date_validation = $this->getMenu()->DateFieldValidation($dbAdapter,$moduleNamespace,$created_date,$indicator_id,$company_id,$country_id,$frequency_id);

            $bulk_type = $this->getRequest()->getPost('bulk_type');
            if (empty($bulk_type)) {
                $columns = explode(',', $this->getRequest()->getPost('col'));
                foreach ($columns AS $column) {
                    //$data['col'.$count] = (!empty($column)) ? $column : null;
                    $water->col[] = (!empty($column)) ? $column : '';
                }
            } else {
                for ($count = 1; $count <= _BULK_FIELD_EXTRA_; $count++) {
                    $column = $this->getRequest()->getPost('col' . $count);
                    if (!empty($column)) {
                        ${'col' . $count} = explode(',', $column);
                        foreach (${'col' . $count} AS $col) {

                            $water->{'col' . $count}[] = (!empty($col)) ? $col : '';

                        }
                    }
                }
            }

            $form->setInputFilter($water->getInputFilter($date_validation));

            //                for($i = 1; $i<=_MANUAL_FIELD_EXTRA_;$i++){
//                    //$water->colname.$i =  $data['col'.$i];
//                    $water->columns[] = $form->getInputFilter()->getValue('col'.$i);
//                }

                $form->setData($request->getPost());

                if ($form->isValid()) {
                    if (!empty($_POST["indicator_type"]) && isset($_POST["indicator_type"]) && $_POST["indicator_type"] == 'attachment') {
                        $file_name_tmp = $_FILES["fileupload_attachment"]['tmp_name'];
                        $file_name = $_FILES["fileupload_attachment"]['name'];
                        $temp = explode(".", $file_name);
                        $newfilename = $temp[0] . '.' . end($temp);
                        $add = "$newfilename"; // the path with the file name where the file will be stored
                        $upload = move_uploaded_file($file_name_tmp, $add);
                    }

                    $water->exchangeArray($form->getData());
                    /* Expired entries  marked as normal if the desire entry has been made after intimation.*/
                    $this->getMenu()->StatusChangeApproved($dbAdapter, $moduleNamespace, $indicator_id, $company_id, $country_id, $frequency_id);
                    $expire_date = $this->getMenu()->FrequencyChecker($frequency_id, $created_date);
                    $water->expire_date = $expire_date;
                    $this->getWaterTable()->saveWater($water, $dbAdapter);
                    // Redirect to list of Water
                    return $this->redirect()->toRoute('water');
                }
            }
            $view = new ViewModel(array(
                'form' => $form, 'water' => $this->getWaterTable()->fetchAll($dbAdapter)
            ));
            $view->setTemplate('water/add');
            return $view;
            // return array('form' => $form);
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function editAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {

            $id = (int)$this->params()->fromRoute('id', 0);

            try {
                $water = $this->getWaterTable()->getWater($id);
            } catch (\Exception $ex) {
                return $this->redirect()->toRoute('water', array(
                    'action' => 'index'
                ));
            }
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $controllerClass= __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter,$moduleNamespace);
            $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
            $DateField = $this->getMenu()->DateField();

            $form = new WaterForm($Companies,$Regions,$Countries,$Indicators,$frequencies,$DateField);
            $form->bind($water);
            $userSession = new Container('user');


            if($userSession->user_type == _ADMIN_) {
                $form->get('submit')->setAttribute('value', 'Update & Approval');
            }
            if($userSession->user_type == _CHAMPION_) {
                $form->get('submit')->setAttribute('value', 'Update');
            }


            $request = $this->getRequest();
            if ($request->isPost()) {

                $bulk_type = $this->getRequest()->getPost('bulk_type');
                if (empty($bulk_type) || $bulk_type == "0") {
                    $columns = $this->getRequest()->getPost('col');
                    //$columns = explode(',', $col);
                    foreach ($columns AS $column) {
                        //$data['col'.$count] = (!empty($column)) ? $column : null;
                        $water->col[] = (!empty($column)) ? $column : '';
                    }
                } else {
                    for ($count = 1; $count <= _BULK_FIELD_EXTRA_; $count++) {
                        $column = $this->getRequest()->getPost('col' . $count);
                        if (!empty($column)) {
                            ${'col' . $count} = explode(',', $column);
                            foreach (${'col' . $count} AS $col) {
                                //if (!empty($col)) {
                                    $water->{'col' . $count}[] = (!empty($col)) ? $col : '';
                                //}
                            }
                        }
                    }
                }
                $form->setInputFilter($water->getInputFilter(null));
                $form->setData($request->getPost());

                if ($form->isValid()) {
                    $this->getWaterTable()->saveWater($water, $dbAdapter);

                    // Redirect to list of waters0
                    return $this->redirect()->toRoute('water');
                }
            }
            $water_details = $this->getWaterTable()->fetchAll_water_details($dbAdapter, $id);
            $water = $this->getWaterTable()->fetchAll_water($dbAdapter, $id);
            foreach ($water AS $water_row) {
                $indicatorId = $water_row['type'];
                $uploadedFile['file_name'] = $water_row['file_name'];
                $uploadedFile['org_name'] = $water_row['org_name'];
                for ($a = 1; $a <= _MANUAL_FIELD_EXTRA_; $a++) {
                    $col_value['col_value' . $a] = $water_row['col' . $a];
                }
                $col_value['indicator_type'] = $water_row['indicator_type'];
            }
            $indicator = $this->getMenu()->fetchIndicatorById($dbAdapter, $indicatorId);

            return array(
                'id' => $id,
                'form' => $form,
                'water_details' => $water_details,
                'water' => $water,
                'indicator' => $indicator,
                'column' => $col_value,
                'uploaded_file' => $uploadedFile,
            );
        }
        else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function deleteAction()
    {
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('water');
        }
        if ($id) {
            $this->getWaterTable()->deleteWater($id, $dbAdapter);
            return $this->redirect()->toRoute('water');
        }
    }

    public function downloadAction()
    {
        $str = $this->params()->fromRoute('str', 0);
        $id = $this->params()->fromRoute('id', 0);
        $str = trim($str);
        if ($id == 0) {
            $file = 'excels/'.$str;
        } else {
            $file = $str;
        }

        $response = new \Zend\Http\Response\Stream();
        $response->setStream(fopen($file, 'r'));
        $response->setStatusCode(200);
        $response->setStreamName(basename($file));
        $headers = new \Zend\Http\Headers();
        $headers->addHeaders(array(
            'Content-Disposition' => 'attachment; filename="' . basename($file) .'"',
            'Content-Type' => 'application/octet-stream',
            'Content-Length' => filesize($file),
            'Expires' => '@0', // @0, because zf2 parses date as string to \DateTime() object
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public'
        ));
        $response->setHeaders($headers);
        return $response;

/*

        $str = $this->params()->fromRoute('str', 0);
        $id = $this->params()->fromRoute('id', 0);
        $str = trim($str);

// first, get MIME information from the file
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $str);
        finfo_close($finfo);

// send header information to browser
        header('Content-Type: ' . $mime);
        header('Content-Disposition: attachment;  filename="' . $str . '"');
        header('Content-Length: ' . filesize($str));
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

//stream file
        if ($id = 0) {
            $filename_str = _PROJECT_NAME_.'public/excels/' . $str;
        } else {
            $filename_str = $str;
        }
        ob_get_clean();
        echo file_get_contents($filename_str);
        ob_end_flush();
*/
    }

}
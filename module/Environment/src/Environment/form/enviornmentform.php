<?php
namespace Enviornment\Form;

use Zend\Form\Form;
use zend\Db\Adapter\AdapterInterface;
use zend\Db\ResultSet\ResultSet;
class EnviornmentForm extends Form
{
    //  protected $dbAdapter;
    public function __construct($name = null)
    {
        parent::__construct('enviornment');
        $this->add(array(
            'name' => 'enviornment_id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'company_id',
            'options' => array(
                'label' => 'Company',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => array(
                    '1' => 'MCT FZE',
                    '2' => 'Alied Enterprise'
                ),
            ),
            'attributes' => array(
                'value' => '1', //set selected to '1'
                'class' => 'form-control',

            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'frequency_id',
            'options' => array(
                'label' => 'Frequency',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => array(
                    '1' => 'Monthly',
                ),
            ),
            'attributes' => array(
                'value' => '1', //set selected to '1'
                'class' => 'form-control',

            )
        ));

        $this->add(array(
            'name' => 'no_of_scholarship',
            'attributes' => array(
                'class' => 'form-control',
                'required' =>    'required',
            ),
        ));



        $this->add(array(
            'name' => 'edu_ex_students',
            'attributes' => array(
                'class' => 'form-control',
                'required' =>    'required',
            ),
        ));


        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
                'class' => 'btn btn-primary pull-right',
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'type' => 'button',
            'options'=>array(
                'label'=>'Cancel'
            ),
            'attributes' => array(
                'onclick' => 'javascript:history.back();',
                'class' => 'btn btn-primary pull-right',
            ),

        ));
    }
}
<?php
namespace Energy\Model;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Energy implements InputFilterAwareInterface
{
    public $energy_id;
    public $company_id;
    public $region_id;
    public $country_id;
    public $frequency_id;
    public $type;
    public $value;
    public $id;
    public $inputFilter;
    // for energy detail table
    public $employee_id;
    public $employee_name;
    public $travel_from;
    public $travel_to;
    public $travel_date;
    public $travel_distance;
    public $create_date;
    //Nationality/Diversity
    public $dob;
    public $nationality;
    public $gender;
    public $designation;
    public $bulk_type;
    public $filename;
    public $org_name;
    public $indicator_type;
    //Event
    public $date;

    //protected $inputFilter;                       // <-- Add this variable

    public function exchangeArray($data)
    {
        $this->energy_id = (!empty($data['energy_id'])) ? $data['energy_id'] : null;
        $this->company_id = (!empty($data['company_id'])) ? $data['company_id'] : null;
        $this->region_id = (!empty($data['region_id'])) ? $data['region_id'] : null;
        $this->country_id = (!empty($data['country_id'])) ? $data['country_id'] : null;
        $this->frequency_id = (!empty($data['frequency_id'])) ? $data['frequency_id'] : null;
        $this->type = (!empty($data['type'])) ? $data['type'] : null;
        $this->value = (!empty($data['value'])) ? $data['value'] : null;
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->employee_id = (!empty($data['employee_id'])) ? $data['employee_id'] : null;
        $this->employee_name = (!empty($data['employee_name'])) ? $data['employee_name'] : null;
        $this->travel_from = (!empty($data['travel_from'])) ? $data['travel_from'] : null;
        $this->travel_to = (!empty($data['travel_to'])) ? $data['travel_to'] : null;
        $this->travel_date = (!empty($data['travel_date'])) ? $data['travel_date'] : null;
        $this->travel_distance = (!empty($data['travel_distance'])) ? $data['travel_distance'] : null;
        $this->create_date = (!empty($data['create_date'])) ? $data['create_date'] : null;
        //Nationality/Diversity
        $this->dob                   = (!empty($data['dob'])) ? $data['dob'] : null;
        $this->nationality                   = (!empty($data['nationality'])) ? $data['nationality'] : null;
        $this->gender                   = (!empty($data['gender'])) ? $data['gender'] : null;
        $this->designation                   = (!empty($data['designation'])) ? $data['designation'] : null;
        //Event
        $this->date                 = (isset($data['date']))  ? $data['date']     : null;
        $this->filename             = (isset($data['file_name']))  ? $data['file_name']     : null;
        $this->org_name             = (isset($data['org_name']))  ? $data['org_name']     : null;
        $this->indicator_type             = (isset($data['indicator_type']))  ? $data['indicator_type']     : null;
        $this->bulk_type             = (isset($data['bulk_type']))  ? $data['bulk_type']     : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }


    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter($date_validation=null)
    {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
           if($date_validation != null){
               $inputFilter->add(
                   $date_validation
               );
           }
            $inputFilter->add(array(
                'name'     => 'energy_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'company_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                /*     'validators' => array(
                         array(
                             'name'    => 'StringLength',
                             'options' => array(
                                 'encoding' => 'UTF-8',
                                 'min'      => 1,
                                 'max'      => 100,
                             ),
                         ),
                     ),*/
            ));

            $inputFilter->add(array(
                'name'     => 'frequency_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                /* 'validators' => array(
                     array(
                         'name'    => 'StringLength',
                         'options' => array(
                             'encoding' => 'UTF-8',
                             'min'      => 1,
                             'max'      => 100,
                         ),
                     ),
                 ),*/
            ));

            $inputFilter->add(array(
                'name'     => 'type',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'value',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));


            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
<?php

return array(

    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

    'controllers' => array(
        'invokables' => array(
            'Energy\Controller\Energy' => 'Energy\Controller\EnergyController',
        ),

    ),
    'view_helpers' => array(
        'invokables' => array(
            'distancehelper' => 'Energy\Helper\DistanceHelper',
        ),
    ),
    'service_manager' => array(

    ),

    'router' => array(
// Uncomment below to add routes
        'routes' => array(
            'energy' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/energy[/:action][/:id][/:str]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Energy\Controller',
                        'controller' => 'Energy',
                        'action' => 'index',
                    )
                )
            )
        ),
        'may_terminate' => true,
        'child_routes' => array(
            'default' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/[:controller[/:action]]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    )
                )
            )
        )
    )
);
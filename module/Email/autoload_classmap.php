<?php

return array(
    'di' => array(
        'definition' => array('class' => array(
            'Email\EmailForm' => array(
                '__construct' => array(
                    'required' => true,
                    'captchaAdapter' => array(
                        'required' => true,
                        'type' => 'Zend\Captcha\Adapter',
                    ),
                ),
            ),
            'Zend\Mail\Message' => array(
                'addTo' => array(
                    'emailOrAddressList' => array(
                        'type' => false, 'required' => true),
                    'name' => array('type' => false, 'required' => false),
                ),
                'addFrom' => array(
                    'emailOrAddressList' => array(
                        'type' => false, 'required' => true),
                    'name' => array('type' => false, 'required' => false),
                ),
                'setSender' => array(
                    'emailOrAddressList' => array(
                        'type' => false, 'required' => true),
                    'name' => array('type' => false, 'required' => false),
                ),
            ),


        ),
        ),

        'preferences' => array(
            'Zend\Mail\Transport' => 'Zend\Mail\Transport\Smtp',
        ),
        'instance' => array(
            'Email\EmailForm' => array('parameters' => array(
                'captchaAdapter' => 'Zend\Captcha\Dumb',
            )),
            'Email\Controller\EmailController' => array(
                'parameters' => array(
                    'form' => 'Email\EmailForm',
                    'message' => 'Zend\Mail\Message',
                    'transport' => 'Zend\Mail\Transport',
                )
            ),
            'Zend\View\Resolver\TemplateMapResolver' => array('parameters' => array(
                'map' => array(
                    'email/index' => __DIR__ . '/../view/email/email/index.phtml',
                    'email/thank-you' => __DIR__ . '/../view/email/email/thank-you.phtml',
                ),
            )),
            'Zend\View\Resolver\TemplatePathStack' => array('parameters' => array(
                'paths' => array('email' => __DIR__ . '/../view'),
            )),
        ),
    ),
);

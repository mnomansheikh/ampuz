<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 8/26/2015
* Time: 11:55 AM
*/


// module/PhlyContact/src/PhlyContact/ContactForm.php
namespace Email;

use
    Zend\Form\Form,
    Zend\Validator\Hostname as HostnameValidator;

class EmailForm extends Form
{

    protected $captchaAdapter;
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('email');
        //$this->setAttribute("method", "post");


        $this->add(array(
            'name' => 'to',
            'type' => 'email',
            'options' => array(
                'label' => 'To',
                'attributes' => array(
                    'value'  => ' ',
                    'class' => 'control-label'
                ),
            ),
            'attributes' => array(
                'class' => 'form-control',
            'required' =>    'required',
            ),

        ));
        $this->add(array(
            'name' => 'cc',
            'type' => 'email',
            'options' => array(
                'label' => 'cc',
                'attributes' => array(
                    'value'  => ' ',
                    'class' => 'control-label'
                ),
            ),
            'attributes' => array(

                'value' => 'apptest.comstar@gmail.com',
                'class' => 'form-control',
                
            ),

        ));
        $this->add(array(
            'name' => 'subject',
            'type' => 'Text',
            'options' => array(
                'label' => 'Subject',
                'attributes' => array(
                    'value'  => ' ',
                    'class' => 'control-label'
                ),

            ),
                'attributes'    =>  array(
                'class'         => 'form-control',
                'required'      => 'required',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'message',
            'options' => array(
                'label' => 'Message',
                //'value_options' => $this->getOptionsForSelect(),

                'attributes' => array(
                    'class' => 'control-label'
                )
            ),
            'attributes' => array(
                'value' => 'Your previous month\'s data requirement has not been submitted. The request will be escalated in 2 days if the data is not received.  ',
                'class' => 'form-control',
                'required' =>    'required',
            )
        ));


        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
                'class' => 'btn btn-primary pull-right',
            ),
        ));
    }
}

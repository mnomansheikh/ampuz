<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 8/26/2015
 * Time: 12:00 PM
 */

namespace Email\Controller;

use Email\EmailForm,
    Email\Model\Email,
    Zend\Mail\Transport,
    Zend\Mail\Message as Message,
    Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel;

class EmailController extends AbstractActionController
{
    protected $form;
    protected $message;
    protected $transport;

    public function setMessage(Message $message)
    {
        $this->message = $message;
    }

    public function setMailTransport(Transport $transport)
    {
        $this->transport = $transport;
    }

    public function setEmailForm(EmailForm $form)
    {
        $this->form = $form;
    }

    public function indexAction()
    {
         $form = new EmailForm();
		//request indicator
		
		$to  ="";
		if((isset($_POST['title']))){
			$title = $_POST['title'];
			$indicator = $_POST['indicator'];
		    $description = $_POST['description'];
			$form->get('message')->setValue($title."\n".$indicator."\n".$description);
			$form->get('cc')->setValue("");

		}
		else{

		/*$id =  $this->params()->fromRoute('id', 0);*/
        $to =  $this->params()->fromRoute('email_id', 0);

		}

       // $to = 'demo.comstar@gmail.com';
        $form->get('submit')->setValue('Send');
        $form->get('to')->setValue($to);
        return array('form' => $form);
    }

   public function processAction()
    {
        $form = new EmailForm();

        $request = $this->getRequest();


        if ($request->isPost()) {
            $education = new Email();
            $form->setInputFilter($education->getInputFilter());
            $form->setData($request->getPost());


            if((isset($_POST['title']))){
                $title = $_POST['title'];
                $indicator = $_POST['indicator'];
                $description = $_POST['description'];
                $form->get('message')->setValue($title."\n".$indicator."\n".$description);
                $form->get('cc')->setValue("muzair@comstar.com.pk");
                $form->get('subject')->setValue("Email");
                $form->get('to')->setValue("muzair@comstar.com.pk");
                $form->get('submit')->setValue('Send');
            }

            //print_r($form->getMessages()); //error messages

            if ($form->isValid()) {
                //   $education->exchangeArray($form->getData());
               // $this->getEducationTable()->saveEducation($education, null);
              // send email...
                $this->sendEmail($form->getData());

                $view = new ViewModel(array(

                ));
                $view->setTemplate('email/thank-you');
                return $view;
            }

        }
       // return array('form' => $form);
        $view = new ViewModel(array(
            'form' => $form
        ));
        $view->setTemplate('email/index');
        return $view;
    }


    protected function sendEmail(array $data)
    {
        $from = $data['to'];
        $cc = $data['cc'];
        $subject =  $data['subject'];
        $body = $data['message'];

        $message = new \Zend\Mail\Message();

        $message->setBody($body);
        $message->setFrom($from);
        $message->addTo($from);
        $message->addCc($cc);
        $message->setSubject($subject);

        $smtpOptions = new \Zend\Mail\Transport\SmtpOptions();

        $smtpOptions->setHost('smtp.gmail.com')
            ->setConnectionClass('login')
            ->setName('smtp.gmail.com')
            ->setConnectionConfig(array(
                    'username' => 'ampuz.app@gmail.com',
                    'password' => 'ampuz@123',
                    'ssl' => 'tls',
                )
            );
        $transport = new \Zend\Mail\Transport\Smtp($smtpOptions);
        $transport->send($message);
    }

    public function thankYouAction()
    {
        $headers = $this->request->headers();
        if (!$headers->has('Referer')
            || !preg_match('#/email$#',
                $headers->get('Referer')->getFieldValue())
        ) {
            $this->redirect->toRoute('contact');
            return $this->response;
        }

        // do nothing...
        return array();
    }
}
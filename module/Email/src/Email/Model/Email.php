<?php
namespace Email\Model;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Email implements InputFilterAwareInterface
{
    public $education_id;
    public $company_id;
    public $frequency_id;
    public $no_of_scholarship;
    public $edu_ex_students;
    public $id;
    public $inputFilter;
    //protected $inputFilter;                       // <-- Add this variable

    public function exchangeArray($data)
    {
        $this->education_id         = (!empty($data['education_id'])) ? $data['education_id'] : null;
        $this->company_id           = (!empty($data['company_id'])) ? $data['company_id'] : null;
        $this->frequency_id         = (!empty($data['frequency_id'])) ? $data['frequency_id'] : null;
        $this->no_of_scholarship    = (!empty($data['no_of_scholarship'])) ? $data['no_of_scholarship'] : null;
        $this->edu_ex_students      = (!empty($data['edu_ex_students'])) ? $data['edu_ex_students'] : null;
        $this->id                   = (!empty($data['id'])) ? $data['id'] : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }


    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();


            $inputFilter->add(array(
                'name'     => 'to',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
               /* 'validators' => array(
                    array(
                        'name'    => 'StringLenght',
    ),
    new \Zend\Validator\EmailAddress()
)*/
            ));

            $inputFilter->add(array(
                'name'     => 'subject',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'message',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 250,
                        ),
                    ),
                ),
            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
<?php
namespace Wmanagement\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Wmanagement\Model\Wmanagement;
use Wmanagement\Form\WmanagementForm;
use Wmanagement\Model\WmanagementTable;
use Zend\Session\Container;

use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class WmanagementController extends AbstractActionController
{
    protected $WmanagementTable;
    protected $authservice;

    public function getAuthService()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }
        return $this->authservice;
    }

    public function getMenu()
    {
        if (!$this->MenuTable) {
            $sm = $this->getServiceLocator();
            $this->MenuTable = $sm->get('Application\Model\MenuTable');
        }
        return $this->MenuTable;
    }

    public function indexAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $form = new WmanagementForm();
            $form->get('submit')->setValue('Request Indicator');
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $config = $this->getServiceLocator()->get('Config');
            $dsn = $config['db']['dsn'];
            $username = $config['db']['username'];
            $password = $config['db']['password'];
            $controllerClass = __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            //$Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            //$Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            //$Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsIDByGroup($dbAdapter, $moduleNamespace);
            //$frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
            //$record_expiration = $this->getMenu()->RecordExpiration($dbAdapter,$moduleNamespace,$Indicators,$Companies,$Countries,$frequencies);

            if ($userSession->user_type == _ADMIN_) {
                // grab the paginator from the CommunityTable
                $paginator = $this->getMenu()->fetchAll_admin($dbAdapter, true, $moduleNamespace, $dsn, $username, $password, $Indicators);
                //$paginator = $this->getWmanagementTable()->fetchAll_admin($dbAdapter, true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('wmanagement/admin');
                return $view;
            }
            if ($userSession->user_type == _CHAMPION_) {
                // grab the paginator from the CommunityTable
                //$paginator = $this->getWmanagementTable()->fetchAll_champion($dbAdapter, true);
                $paginator = $this->getMenu()->fetchAll_champion($dbAdapter, true, $moduleNamespace, $dsn, $username, $password, $Indicators);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('wmanagement/index');
                return $view;
            }
            /*   if ($userSession->user_type == _CHAMPION_) {
                   $form = new WmanagementForm($dbAdapter);
                   $form->get('submit')->setValue('Add');
                   $request = $this->getRequest();

                   if ($request->isPost()) {
                       $wmanagement = new Wmanagement();
                       $form->setInputFilter($wmanagement->getInputFilter());
                       $form->setData($request->getPost());

                       if ($form->isValid()) {
                           $wmanagement->exchangeArray($form->getData());
                           $this->getWmanagementTable()->saveWmanagement($wmanagement, null);

                           // Redirect to list of albums
                           return $this->redirect()->toRoute('wmanagement');
                       }
                   }
                   return array('form' => $form, 'wmanagement' => $this->getWmanagementTable()->fetchAll($dbAdapter));
               }
               */
        } else {
            return $this->redirect()->toRoute('login');
        }


//        return new ViewModel(array(
//            'wmanagement' => $this->getWmanagementTable()->fetchAll(),
//
//        ));
        //    $form->setVariable('form', $form);

    }

    public function getWmanagementTable()
    {
        if (!$this->WmanagementTable) {
            $sm = $this->getServiceLocator();
            $this->WmanagementTable = $sm->get('Wmanagement\Model\WmanagementTable');
        }
        return $this->WmanagementTable;
    }

    public function uploadtransportAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $indicatorId = $_GET['id'];
            if ($indicatorId) {
                $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
                $Indicators = $this->getMenu()->fetchIndicatorById($dbAdapter, $indicatorId);
            }
            $view = new ViewModel(array(
                'indicator' => $Indicators
            ));
            $view->setTerminal(true);
            $view->setTemplate('application/file_upload_transport');
            return $view;
            // return array('form' => $form);
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function uploadeventAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $indicatorId = $_GET['id'];
            if ($indicatorId) {
                $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
                $Indicators = $this->getMenu()->fetchIndicatorById($dbAdapter, $indicatorId);
            }
            $view = new ViewModel(array(
                'indicator' => $Indicators
            ));
            $view->setTerminal(true);
            $view->setTemplate('application/file_upload_event');
            return $view;
            // return array('form' => $form);
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function uploaddiversityAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $indicatorId = $_GET['id'];
            if ($indicatorId) {
                $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
                $Indicators = $this->getMenu()->fetchIndicatorById($dbAdapter, $indicatorId);
            }
            $view = new ViewModel(array(
                'indicator' => $Indicators
            ));
            $view->setTerminal(true);
            $view->setTemplate('application/file_upload_diversity');
            return $view;
            // return array('form' => $form);
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function addAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $controllerClass = __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter, $moduleNamespace);
            $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter, $moduleNamespace);
            $DateField = $this->getMenu()->DateField();
            $form = new WmanagementForm($Companies, $Regions, $Countries, $Indicators, $frequencies, $DateField);
            $form->get('submit')->setValue('Save');
            $request = $this->getRequest();

            if ($request->isPost()) {
                $wmanagement = new Wmanagement();
                $created_date = $this->getRequest()->getPost('create_date');
                $indicator_id = $this->getRequest()->getPost('type');
                $company_id = $this->getRequest()->getPost('company_id');
                $country_id = $this->getRequest()->getPost('country_id');
                $frequency_id = $this->getRequest()->getPost('frequency_id');
                $date_validation = $this->getMenu()->DateFieldValidation($dbAdapter, $moduleNamespace, $created_date, $indicator_id, $company_id, $country_id, $frequency_id);

                $bulk_type = $this->getRequest()->getPost('bulk_type');
                if (empty($bulk_type)) {
                    $columns = explode(',', $this->getRequest()->getPost('col'));
                    foreach ($columns AS $column) {
                        //$data['col'.$count] = (!empty($column)) ? $column : null;
                        $wmanagement->col[] = (!empty($column)) ? $column : '';
                    }
                } else {
                    for ($count = 1; $count <= _BULK_FIELD_EXTRA_; $count++) {
                        $column = $this->getRequest()->getPost('col' . $count);
                        if (!empty($column)) {
                            ${'col' . $count} = explode(',', $column);
                            foreach (${'col' . $count} AS $col) {

                                $wmanagement->{'col' . $count}[] = (!empty($col)) ? $col : '';

                            }
                        }
                    }
                }


                $form->setInputFilter($wmanagement->getInputFilter($date_validation));

//                for($i = 1; $i<=_MANUAL_FIELD_EXTRA_;$i++){
//                    //$wmanagement->colname.$i =  $data['col'.$i];
//                    $wmanagement->columns[] = $form->getInputFilter()->getValue('col'.$i);
//                }

                $form->setData($request->getPost());

                if ($form->isValid()) {
                    if (!empty($_POST["indicator_type"]) && isset($_POST["indicator_type"]) && $_POST["indicator_type"] == 'attachment') {
                        $file_name_tmp = $_FILES["fileupload_attachment"]['tmp_name'];
                        $file_name = $_FILES["fileupload_attachment"]['name'];
                        $temp = explode(".", $file_name);
                        $newfilename = $temp[0] . '.' . end($temp);
                        $add = "$newfilename"; // the path with the file name where the file will be stored
                        $upload = move_uploaded_file($file_name_tmp, $add);
                    }

                    $wmanagement->exchangeArray($form->getData());
                    /* Expired entries  marked as normal if the desire entry has been made after intimation.*/
                    $this->getMenu()->StatusChangeApproved($dbAdapter, $moduleNamespace, $indicator_id, $company_id, $country_id, $frequency_id);
                    $expire_date = $this->getMenu()->FrequencyChecker($frequency_id, $created_date);
                    $wmanagement->expire_date = $expire_date;
                    $this->getWmanagementTable()->saveWmanagement($wmanagement, $dbAdapter);
                    // Redirect to list of Wmanagement
                    return $this->redirect()->toRoute('wmanagement');
                }
            }
            $view = new ViewModel(array(
                'form' => $form, 'wmanagement' => $this->getWmanagementTable()->fetchAll($dbAdapter)
            ));
            $view->setTemplate('wmanagement/add');
            return $view;
            // return array('form' => $form);
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function editAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {

            $id = (int)$this->params()->fromRoute('id', 0);

            try {
                $wmanagement = $this->getWmanagementTable()->getWmanagement($id);
            } catch (\Exception $ex) {
                return $this->redirect()->toRoute('wmanagement', array(
                    'action' => 'index'
                ));
            }
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $controllerClass = __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter, $moduleNamespace);
            $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter, $moduleNamespace);
            $DateField = $this->getMenu()->DateField();

            $form = new WmanagementForm($Companies, $Regions, $Countries, $Indicators, $frequencies, $DateField);
            $form->bind($wmanagement);
            $userSession = new Container('user');


            if ($userSession->user_type == _ADMIN_) {
                $form->get('submit')->setAttribute('value', 'Update & Approval');
            }
            if ($userSession->user_type == _CHAMPION_) {
                $form->get('submit')->setAttribute('value', 'Update');
            }


            $request = $this->getRequest();
            if ($request->isPost()) {

                $bulk_type = $this->getRequest()->getPost('bulk_type');
                if (empty($bulk_type) || $bulk_type == "0") {
                    $columns = $this->getRequest()->getPost('col');
                    //$columns = explode(',', $col);
                    foreach ($columns AS $column) {
                        //$data['col'.$count] = (!empty($column)) ? $column : null;
                        $wmanagement->col[] = (!empty($column)) ? $column : '';
                    }
                } else {
                    for ($count = 1; $count <= _BULK_FIELD_EXTRA_; $count++) {
                        $column = $this->getRequest()->getPost('col' . $count);
                        if (!empty($column)) {
                            ${'col' . $count} = explode(',', $column);
                            foreach (${'col' . $count} AS $col) {
                                //if (!empty($col)) {
                                $wmanagement->{'col' . $count}[] = (!empty($col)) ? $col : '';
                                //}
                            }
                        }
                    }
                }
                $form->setInputFilter($wmanagement->getInputFilter(null));
                $form->setData($request->getPost());

                if ($form->isValid()) {
                    $this->getWmanagementTable()->saveWmanagement($wmanagement, $dbAdapter);

                    // Redirect to list of wmanagements0
                    return $this->redirect()->toRoute('wmanagement');
                }
            }
            $wmanagement_details = $this->getWmanagementTable()->fetchAll_wmanagement_details($dbAdapter, $id);
            $wmanagement = $this->getWmanagementTable()->fetchAll_wmanagement($dbAdapter, $id);
            foreach ($wmanagement AS $wmanagement_row) {
                $indicatorId = $wmanagement_row['type'];
                $uploadedFile['file_name'] = $wmanagement_row['file_name'];
                $uploadedFile['org_name'] = $wmanagement_row['org_name'];
                for ($a = 1; $a <= _MANUAL_FIELD_EXTRA_; $a++) {
                    $col_value['col_value' . $a] = $wmanagement_row['col' . $a];
                }
                $col_value['indicator_type'] = $wmanagement_row['indicator_type'];
            }
            $indicator = $this->getMenu()->fetchIndicatorById($dbAdapter, $indicatorId);

            return array(
                'id' => $id,
                'form' => $form,
                'wmanagement_details' => $wmanagement_details,
                'wmanagement' => $wmanagement,
                'indicator' => $indicator,
                'column' => $col_value,
                'uploaded_file' => $uploadedFile,
            );
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function deleteAction()
    {
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('wmanagement');
        }
        if ($id) {
            $this->getWmanagementTable()->deleteWmanagement($id, $dbAdapter);
            return $this->redirect()->toRoute('wmanagement');
        }
    }

    public function downloadAction()
    {
        $str = $this->params()->fromRoute('str', 0);
        $id = $this->params()->fromRoute('id', 0);
        $str = trim($str);
        if ($id == 0) {
            $file = 'excels/'.$str;
        } else {
            $file = $str;
        }

        $response = new \Zend\Http\Response\Stream();
        $response->setStream(fopen($file, 'r'));
        $response->setStatusCode(200);
        $response->setStreamName(basename($file));
        $headers = new \Zend\Http\Headers();
        $headers->addHeaders(array(
            'Content-Disposition' => 'attachment; filename="' . basename($file) .'"',
            'Content-Type' => 'application/octet-stream',
            'Content-Length' => filesize($file),
            'Expires' => '@0', // @0, because zf2 parses date as string to \DateTime() object
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public'
        ));
        $response->setHeaders($headers);
        return $response;
    }

}
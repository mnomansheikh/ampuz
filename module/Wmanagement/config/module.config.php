<?php

return array(

    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

    'controllers' => array(
        'invokables' => array(
            'Wmanagement\Controller\Wmanagement' => 'Wmanagement\Controller\WmanagementController',
        ),

    ),
    'view_helpers' => array(
        'invokables' => array(
            'distancehelper' => 'Wmanagement\Helper\DistanceHelper',
        ),
    ),
    'service_manager' => array(

    ),

    'router' => array(
// Uncomment below to add routes
        'routes' => array(
            'wmanagement' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/wmanagement[/:action][/:id][/:str]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Wmanagement\Controller',
                        'controller' => 'Wmanagement',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
            'default' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/[:controller[/:action]]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    )
                )
            )
        )
    )
);
<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/27/2016
 * Time: 5:43 PM
 */

namespace Country\Form;
use Zend\Form\Form;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Sql;
use Zend\Form\Element;

class CountryForm extends Form
{

    public function __construct(AdapterInterface $dbAdapter)
    {
        // we want to ignore the name passed
        $this->adapter = $dbAdapter;
        parent::__construct('login');
        $sql = new Sql($dbAdapter);

        $select = $sql->select();
        $select->from('country');
        $select->where(array('is_active' => 1));
        $select->order('id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $countries = $statement->execute();

        $select = $sql->select();
        $select->from('region');
        //$select->join('company_region', 'company_region.region_id = region.id', 'company_id');
        $select->where(array('region.is_active' => 1));
        $select->order('region.id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $regions = $statement->execute();
        $regions_arr = array();
        $itr = 0 ;
        foreach ($regions as $region) {
            $regions_arr[$region['id']] = $region['region_description'];

        }


        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'country_name',
            'attributes' => array(
                'id' => 'country_name',
                'type' => 'text',
                'required' => 'required',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'region_id',
            'options' => array(
                'label' => 'Region',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $regions_arr,
            ),
            'attributes' => array(
                // 'value' => '', //set selected to '1'
                'class' => 'form-control',
                'id' => 'region_id',
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_active',
            'attributes' => array(
                'id' => 'myonoffswitch',
                'class' => 'onoffswitch-checkbox',
                'value' => '1'
            ),
            'options' => array(
                'label' => 'Status',
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0'
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'submitbutton',
                'class' => 'btn8 btn-8 btn-8f pull-right',
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'type' => 'button',
            'options' => array(
                'label' => 'Cancel'
            ),
            'attributes' => array(
                'onclick' => 'javascript:window.location.href = "/' . _PROJECT_NAME_ . '/public/country/list";',
                'class' => 'btn8 btn-8 btn-8f pull-right',
            ),
        ));
    }
}
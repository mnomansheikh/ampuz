<?php

//module/Login/src/Controller/AuthController.php
namespace Country\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Country\Model\Country;
use Country\Form\CountryForm;

class CountryController extends AbstractActionController
{
    protected $form;
    protected $userSession;
    protected $storage;
    protected $authservice;
    // protected $currentUserId;
    protected $CountryTable;
//    public function getUserProfileTable()
//    {
//        if (!$this->UserProfileTable) {
//            $sm = $this->getServiceLocator();
//            $this->UserProfileTable = $sm->get('Login\Model\UserProfileTable');
//        }
//        return $this->UserProfileTable;
//    }
    public function getCountryTable()
    {
        if (!$this->CountryTable) {
            $sm = $this->getServiceLocator();
            $this->CountryTable = $sm->get('Country\Model\CountryTable');
        }
        return $this->CountryTable;
    }


    public function getAuthService()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authservice;
    }


    public function ListAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $form = new CountryForm($dbAdapter);
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user

            // grab the paginator from the CommunityTable
            $paginator = $this->getCountryTable()->fetchAll_country($dbAdapter, true);
            // set the current page to what has been passed in query string, or to 1 if none set
            $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
            // set the number of items per page to 10
            $paginator->setItemCountPerPage(10);

            $view = new ViewModel(array(
                'paginator' => $paginator,
                'form' => $form

            ));
            $view->setTemplate('country/list');
            return $view;
        }

    }

    public function addAction()
    {

        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            //  if ($userSession->user_type == _ADMIN_) {
            $form = new CountryForm($dbAdapter);
            $form->get('submit')->setValue('Save');
            $form->get('cancel')->setValue('Cancel');
            $request = $this->getRequest();

            if ($request->isPost()) {
                $Country = new Country();
                $Country->setDbAdapter($dbAdapter);
                $form->setInputFilter($Country->getInputFilter());
                $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(),
                    $this->getRequest()->getFiles()->toArray()
                );

                $form->setData($request->getPost());
                $form->setData($data);

                if ($form->isValid()) {
                    $Country->exchangeArray($form->getData());
                    $this->getCountryTable()->saveCountry($Country, $dbAdapter);
                    // Redirect to list
                    return $this->redirect()->toRoute('country', array(
                        'action' => 'list'
                    ));
                }
            }


            //return array('form' => $form, 'event' => $this->getUserProfileTable()->fetchAll($dbAdapter));
            $view = new ViewModel(array(
                'form' => $form
            ));
            $view->setTemplate('country/add');
            return $view;
            // }
            //  if ($userSession->user_type == _CHAMPION_) {
            //      return $this->redirect()->toRoute('application');
            //  }
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function editAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $id = (int)$this->params()->fromRoute('id', 0);
            try {
                $CountryTable = $this->getCountryTable()->getCountry($id, $dbAdapter);
            } catch (\Exception $ex) {
                //return $this->redirect()->toRoute('country', array(
                //    'action' => 'list'
                //));
            }
            $form = new CountryForm($dbAdapter);
            $Country = new Country();
            $form->bind($CountryTable);
            //for session varibles
            $form->get('submit')->setValue('Save');
            $form->get('cancel')->setValue('Cancel');
            $request = $this->getRequest();
            if ($request->isPost()) {
                $Country->setDbAdapter($dbAdapter);
                $form->setInputFilter($Country->getInputFilter());
                $form->setData($request->getPost());
                if ($id > 0) {
                    $form->getInputFilter()->get('country_name')->setRequired(false);
                }
                if ($form->isValid()) {
                    $Country->exchangeArray($form->getData());
                    $this->getCountryTable()->saveCountry($Country, $dbAdapter);

                    // Redirect to list
                    return $this->redirect()->toRoute('country', array(
                        'action' => 'list'
                    ));
                }
            }
            //  $user = $this->getCurrentUserTable()->fetchAll_event($dbAdapter,$id);
            return array(
                'id' => $id,
                'form' => $form,
                //    'user' => $user,

            );
        } else {
            return $this->redirect()->toRoute('login');
        }

    }

}
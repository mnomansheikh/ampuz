<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/27/2016
 * Time: 5:42 PM
 */

namespace Country\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Country implements InputFilterAwareInterface
{
    public $id;
    public $country_name;
    public $is_active;
    public $region_id;
    public $inputFilter;

    protected $dbAdapter;

    public function setDbAdapter($dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->country_name = (!empty($data['country_name'])) ? $data['country_name'] : null;
        $this->region_id = (!empty($data['region_id'])) ? $data['region_id'] : null;
        $this->is_active = (isset($data['is_active'])) ? $data['is_active'] : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $inputFilter->add(array(
                'name' => 'id',
                'required' => false,
                'filters' => array(
                    array('name' => 'Int'),
                ),
            ));

//            $inputFilter->add(array(
//                'name' => 'country_name', // add second password field
//                /* ... other params ... */
//                'validators' => array(
//                    array(
//                        'name' => '\Zend\Validator\Db\NoRecordExists',
//                        'options' => array(
//                            'table' => 'country',
//                            'field' => 'country_name',
//                            'adapter' => $this->dbAdapter,
//                            'messages' => array(
//                                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'Change Country Name already exist',
//                            ),
//                        ),
//                    ),
//                ),
//            ));

            $this->inputFilter = $inputFilter;

        }

        return $this->inputFilter;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/27/2016
 * Time: 5:40 PM
 */

namespace Country\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class CountryTable
{
    protected $tableGateway;

    public function __construct(tableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getCountryMaster($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getCountry($id)
    {

        //$rowset = $this->tableGateway->select(array('id' => $id));

        $rowset = $this->tableGateway->select(function (Select $select) use ($id) {
            $select
                ->columns(array(
                    '*'
                ))
                ->join(array('cr' => 'country_region'), 'cr.country_id = country.id', array('country_id' => new Expression('GROUP_CONCAT(DISTINCT cr.country_id)'), 'region_id' => 'region_id'))
                ->join(array('r' => 'region'), 'r.id = cr.region_id', 'region_description')
                ->where(array('country.id ' => $id));
        });


        $row = $rowset->current();
        $row->region_id = $row['region_id'];
        //$row = (object) $row;
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveCountry(Country $Country, $adapter)
    {
        $sql = new Sql($adapter);
        $id = $Country->id;
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $user_type = $userSession->user_type;


        if ($id == 0) {

            $data = array(
                'country_name' => $Country->country_name,
                'is_active' => $Country->is_active,
            );
            $Country_query = $this->tableGateway->insert($data);
            $last_inserted_id = $this->tableGateway->lastInsertValue;
            $data_region = array(
                'country_id' => $last_inserted_id,
                'region_id' => $Country->region_id
            );

            $update = $sql->insert('country_region');
            $update->values($data_region);
            $statement = $sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();

        } else {

            if ($this->getCountryMaster($id)) {

                $data = array(
                    'country_name' => $Country->country_name,
                    'is_active' => $Country->is_active
                );
                $this->tableGateway->update($data, array('id' => $id));

                $data_region = array(
                    'country_id' => $Country->id,
                    'region_id' => $Country->region_id
                );

                $update = $sql->update('country_region');
                $update->set($data_region);
                $update->where(array('country_id' => $id));
                $statement = $sql->prepareStatementForSqlObject($update);
                $results = $statement->execute();
            } else {
                throw new \Exception('Country id does not exist');
            }

        }
    }

    /*public function deleteCountry($id, $adapter)
    {
        $this->tableGateway->delete(array('milestone_id' => (int)$id, 'status' => _P_));

        $sql = new Sql($adapter);
        $delete = $sql->delete();
        $delete->from('milestone_details');
        $delete->where(array('milestone_id' => (int)$id));
        $statement = $sql->prepareStatementForSqlObject($delete);
        $results = $statement->execute();
    }*/

    public function fetchAll_country($adapter, $paginated = false)
    {
        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            //        $sql = new Sql($adapter);
            $select->from(array('c' => 'country'));
            $select->join(array('cr' => 'country_region'), 'cr.country_id = c.id', array('country_id' => new Expression('GROUP_CONCAT(DISTINCT cr.country_id)'), 'region_id' => 'region_id'));
            $select->join(array('r' => 'region'), 'r.id = cr.region_id', 'region_description');
            $select->group('c.id');
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            // create a new result set based on the User entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Country());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

}



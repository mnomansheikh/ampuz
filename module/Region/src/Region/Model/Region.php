<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/27/2016
 * Time: 5:42 PM
 */

namespace Region\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Region implements InputFilterAwareInterface
{
    public $id;
    public $region_description;
    public $is_active;
// public $user_id;
    public $inputFilter;

    protected $dbAdapter;

    public function setDbAdapter($dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->region_description = (!empty($data['region_description'])) ? $data['region_description'] : null;
        $this->is_active = (isset($data['is_active'])) ? $data['is_active'] : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $inputFilter->add(array(
                'name' => 'id',
                'required' => false,
                'filters' => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name' => 'region_description', // add second password field
                /* ... other params ... */
                'validators' => array(
                    array(
                        'name' => '\Zend\Validator\Db\NoRecordExists',
                        'options' => array(
                            'table' => 'region',
                            'field' => 'region_description',
                            'adapter' => $this->dbAdapter,
                            'messages' => array(
                                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'Change Region Name already exist',
                            ),
                        ),
                    ),
                ),
            ));

            $this->inputFilter = $inputFilter;

        }

        return $this->inputFilter;
    }
}
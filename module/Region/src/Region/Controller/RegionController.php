<?php

//module/Login/src/Controller/AuthController.php
namespace Region\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Region\Model\Region;
use Region\Form\RegionForm;

class RegionController extends AbstractActionController
{
    protected $form;
    protected $userSession;
    protected $storage;
    protected $authservice;
    // protected $currentUserId;
    protected $RegionTable;
//    public function getUserProfileTable()
//    {
//        if (!$this->UserProfileTable) {
//            $sm = $this->getServiceLocator();
//            $this->UserProfileTable = $sm->get('Login\Model\UserProfileTable');
//        }
//        return $this->UserProfileTable;
//    }
    public function getRegionTable()
    {
        if (!$this->RegionTable) {
            $sm = $this->getServiceLocator();
            $this->RegionTable = $sm->get('Region\Model\RegionTable');
        }
        return $this->RegionTable;
    }


    public function getAuthService()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authservice;
    }


    public function ListAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $form = new RegionForm($dbAdapter);
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user

            // grab the paginator from the CommunityTable
            $paginator = $this->getRegionTable()->fetchAll_region($dbAdapter, true);
            // set the current page to what has been passed in query string, or to 1 if none set
            $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
            // set the number of items per page to 10
            $paginator->setItemCountPerPage(10);

            $view = new ViewModel(array(
                'paginator' => $paginator,
                'form' => $form

            ));
            $view->setTemplate('region/list');
            return $view;
        }

    }

    public function addAction()
    {

        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            //  if ($userSession->user_type == _ADMIN_) {
            $form = new RegionForm($dbAdapter);
            $form->get('submit')->setValue('Save');
            $form->get('cancel')->setValue('Cancel');
            $request = $this->getRequest();

            if ($request->isPost()) {
                $Region = new Region();
                $Region->setDbAdapter($dbAdapter);
                $form->setInputFilter($Region->getInputFilter());
                $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(),
                    $this->getRequest()->getFiles()->toArray()
                );

                $form->setData($request->getPost());
                $form->setData($data);

                if ($form->isValid()) {
                    $Region->exchangeArray($form->getData());
                    $this->getRegionTable()->saveRegion($Region, $dbAdapter);
                    // Redirect to list
                    return $this->redirect()->toRoute('region', array(
                        'action' => 'list'
                    ));
                }
            }


            //return array('form' => $form, 'event' => $this->getUserProfileTable()->fetchAll($dbAdapter));
            $view = new ViewModel(array(
                'form' => $form
            ));
            $view->setTemplate('region/add');
            return $view;
            // }
            //  if ($userSession->user_type == _CHAMPION_) {
            //      return $this->redirect()->toRoute('application');
            //  }
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function editAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $id = (int)$this->params()->fromRoute('id', 0);
            try {
                $RegionTable = $this->getRegionTable()->getRegion($id, $dbAdapter);
            } catch (\Exception $ex) {
                //return $this->redirect()->toRoute('region', array(
                //    'action' => 'list'
                //));
            }
            $form = new RegionForm($dbAdapter);
            $Region = new Region();
            $form->bind($RegionTable);
            //for session varibles
            $form->get('submit')->setValue('Save');
            $form->get('cancel')->setValue('Cancel');
            $request = $this->getRequest();
            if ($request->isPost()) {
                $Region->setDbAdapter($dbAdapter);
                $form->setInputFilter($Region->getInputFilter());
                $form->setData($request->getPost());
                if ($id > 0) {
                    $form->getInputFilter()->get('region_description')->setRequired(false);
                }
                if ($form->isValid()) {
                    $Region->exchangeArray($form->getData());
                    $this->getRegionTable()->saveRegion($Region, $dbAdapter);

                    // Redirect to list
                    return $this->redirect()->toRoute('region', array(
                        'action' => 'list'
                    ));
                }
            }
            //  $user = $this->getCurrentUserTable()->fetchAll_event($dbAdapter,$id);
            return array(
                'id' => $id,
                'form' => $form,
                //    'user' => $user,

            );
        } else {
            return $this->redirect()->toRoute('login');
        }

    }

}
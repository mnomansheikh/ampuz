<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Dashboard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class DashboardController extends AbstractActionController
{
    protected $authservice;
    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authservice;
    }
    public function getMenu()
    {
        if (!$this->MenuTable) {
            $sm = $this->getServiceLocator();
            $this->MenuTable = $sm->get('Application\Model\MenuTable');
        }
        return $this->MenuTable;
    }
    public function indexAction()
    {

        //Check user is logged in

        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()){
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $company_id ='';
            $region_id ='';
            $country_id ='';
            $group ='';
            $frequency ='';
            $indicator ='';
            $toyear ='';
            $fromyear ='' ;
            if($_REQUEST){
                extract($_REQUEST);
                $company_id;
                $region_id;
                $country_id;
                $indicator;
                $toyear;
                $fromyear ;
                $group ;
                $frequency;
            }

            //for session varibles
            $userSession = new Container('user');

            // $form = new MilestoneForm($dbAdapter);
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            if($userSession->user_type == _ADMIN_){
                // grab the paginator from the CommunityTable
                //   $paginator = $this->getMilestoneTable()->fetchAll_admin($dbAdapter,true);
                // set the current page to what has been passed in query string, or to 1 if none set
                //   $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                //   $paginator->setItemCountPerPage(10);
                //  $GData = $this->getMilestoneTable()->fetchGraphData($dbAdapter,$fromyear,$toyear,$company_id,$event_desc);
                $Groups = $this->getMenu()->fetchAllGroupForEHS($dbAdapter);
                $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
                $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
                $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
                $Indicators = $this->getMenu()->fetchIndicator($dbAdapter);
                $Frequencies = $this->getMenu()->fetchFrequency($dbAdapter);
                //$group_name = $this->getMenu()->fetchGroupById($dbAdapter,$group);
                if($company_id == '' &&   $region_id == '' &&   $country_id == ''){
                    foreach ($Companies as $key=>$value) {
                        $company_id = $key;
                    }
                    foreach ($Regions as $region) {
                       $reg_com = isset($region['attributes']['data-company-id']);
                       if( $company_id == $reg_com){
                         $region_id =  $region['value'];
                       }
                    }
                    foreach ($Countries as $country) {
                        $country_com = isset($country['attributes']['data-company-id']) ;
                        $country_reg = isset($country['attributes']['data-region-id']);
                        if( $company_id == $country_com && $region_id == $country_reg){
                            $country_id =  $country['value'];
                        }
                    }
                    $company_name = $this->getMenu()->fetchCompanyById($dbAdapter,$company_id);
                    $country_name = $this->getMenu()->fetchCountryById($dbAdapter,$country_id);
                    $region_name = $this->getMenu()->fetchRegionById($dbAdapter,$region_id);
                    $GData_Energy_mix = $this->getMenu()->fetchEnergyMixGraphData($dbAdapter,$company_id,$group,$frequency,$indicator,date('Y-01-01'),date('Y-12-31'),$country_id);
                    $GData_Diversity = $this->getMenu()->fetchGraphData($dbAdapter,$company_id,12,1,array(2),date('Y-01-01'),date('Y-12-31'),$country_id);
                    $GData_waste = $this->getMenu()->fetchGraphData($dbAdapter,$company_id,2,1,array(3,4,5,6,7,8,9),date('Y-01-01'),date('Y-12-31'),$country_id);
                    $GData_Training = $this->getMenu()->fetchGraphData($dbAdapter,$company_id,10,1,null,date('Y-01-01'),date('Y-12-31'),$country_id);

                    $fromyear =date('Y-01-01') ;
                    $toyear =date('Y-12-31');
                }
                else{
                    $company_name = $this->getMenu()->fetchCompanyById($dbAdapter,$company_id);
                    $country_name = $this->getMenu()->fetchCountryById($dbAdapter,$country_id);
                    $region_name = $this->getMenu()->fetchRegionById($dbAdapter,$region_id);
                    $GData_Energy_mix = $this->getMenu()->fetchEnergyMixGraphData($dbAdapter,$company_id,$group,$frequency,$indicator,$fromyear,$toyear,$country_id);
                    $GData_Diversity = $this->getMenu()->fetchGraphData($dbAdapter,$company_id,12,1,array(2),$fromyear,$toyear,$country_id);
                    $GData_waste = $this->getMenu()->fetchGraphData($dbAdapter,$company_id,2,1,array(3,4,5,6,7,8,9),$fromyear,$toyear,$country_id);
                    $GData_Training = $this->getMenu()->fetchGraphData($dbAdapter,$company_id,10,1,null,$fromyear,$toyear,$country_id);

                }
                $view = new ViewModel(array(
                    'GData_Energy_mix' => $GData_Energy_mix,
                    'GData_Diversity' => $GData_Diversity,
                    'GData_waste' => $GData_waste,
                    'GData_Training' => $GData_Training,
                    'Groups' => $Groups,
                    'Companies' => $Companies,
                    'Regions' => $Regions,
                    'Countries' => $Countries,
                    'Indicators' => $Indicators,
                    'Frequencies' => $Frequencies,
                    'company_name' => $company_name,
                    'country_name' => $country_name,
                    'region_name' => $region_name,
                    //'group_name' => $group_name

                ));

                $view->setTemplate('dashboard/index');
                return $view;
            }
            if($userSession->user_type == _CHAMPION_) {
                return $this->redirect()->toRoute('application');
            }
        } else {
            return $this->redirect()->toRoute('login');
        }

    }

}


<?php
namespace Health\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Http\Header\Date;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
class HealthTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    public function fetchAll($adapter)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;
        // $resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('health');
        $select->where(array('health.user_id' => $user_id,'health.status'=>_P_));
        $select->join('user_profile', 'user_profile.user_id = health.user_id','user_name');
        $select->join('company', 'company.company_id = health.company_id','company_name');
        $select->join('frequency', 'frequency.frequency_id = health.frequency_id','frequency_name');
        $select->join('indicator', 'indicator.id = health.type','name');
        $select->order('health_id DESC');


        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function fetchAll_health($adapter, $id)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id = $userSession->user_id;
        //$resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        // create a new Select object for the table community
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('health');
        $select->where(array('health_id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function fetchAll_health_details($adapter, $id)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id = $userSession->user_id;
        //$resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        // create a new Select object for the table community
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bulk_detail');
        $select->where(array('section_name' => 'health', 'parent_id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function fetchAll_admin($adapter, $paginated = false)
    {

        if ($paginated) {
            // create a new Select object for the table community
            $userSession = new Container('user');

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('health');
            $select->join('user_profile', 'user_profile.user_id = health.user_id',array('user_name','user_email'));
            $select->join('company', 'company.company_id = health.company_id','company_name');
            $select->join('country', 'country.id = health.country_id','country_name');
            $select->join('region', 'region.id = health.region_id','region_description');
            $select->join('frequency', 'frequency.frequency_id = health.frequency_id','frequency_name');
            $select->join('indicator', 'indicator.id = health.type','name');
            $select->where(array('health.company_id'=>$userSession->user_companies,'health.country_id'=>$userSession->user_countries,'health.region_id'=>$userSession->user_regions));
            $select->order('health_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Health());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function fetchAll_champion($adapter,$paginated=false)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('health');
            $select->where(array('health.user_id'=>$user_id));
            $select->join('user_profile', 'user_profile.user_id = health.user_id','user_name');
            $select->join('company', 'company.company_id = health.company_id','company_name');
            $select->join('frequency', 'frequency.frequency_id = health.frequency_id','frequency_name');
            $select->join('indicator', 'indicator.id = health.type','name');
            $select->order('health_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Health());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function getHealth($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('health_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveHealth(Health $health, $adapter)
    {
        {
            $sql = new Sql($adapter);
            $id = $health->health_id;
            $colname1 = '';
            $colname2 = '';
            $colname3 = '';

            $col1 = "";
            $col2 = "";
            $col3 = "";
            $col4 = "";
            $col5 = "";
            $col6 = "";
            $col7 = "";
            $col8 = "";
            $col9 = "";
            $col10 = "";
            if (empty($health->bulk_type) || $health->bulk_type == 0) {
                $count = 1;
                foreach ($health->col AS $columns) {
                    ${'colname' . $count} = $columns;
                    $count++;
                }
            } else {
                for($count=1; $count<=_BULK_FIELD_EXTRA_; $count++){
                    if(!empty($health->{'col'.$count})){
                        foreach($health->{'col'.$count} AS $item){
                            //if(!empty($item)){
                            ${'col'.$count}[] = $item;
                            //}
                        }
                        //${'col'.$count} = $health->{'col'.$count};
                    }else{
                        ${'col'.$count} = '';
                    }
                }
            }

            $userSession = new Container('user');
            $user_id = $userSession->user_id;
            $user_type = $userSession->user_type;
            $oneYearOn = date('Y-m-d', strtotime(date("Y-m-d", time()) . " + 365 day"));
            //health detail table
            $id_trim = ltrim($health->employee_id, ',');
            $name_trim = ltrim($health->employee_name, ',');
            $travel_from_trim = ltrim($health->travel_from, ',');
            $travel_to_trim = ltrim($health->travel_to, ',');
            $travel_date_trim = ltrim($health->travel_date, ',');
            $travel_distance_trim = ltrim($health->travel_distance, ',');
            $dob_trim = ltrim($health->dob, ',');
            $nationality_trim = ltrim($health->nationality, ',');
            $gender_trim = ltrim($health->gender, ',');
            $designation_trim = ltrim($health->designation, ',');
            $date = ltrim($health->date, ',');
            $emp_ids = explode(',', $id_trim);
            $names = explode(',', $name_trim);
            $travel_from = explode(',', $travel_from_trim);
            $travel_to = explode(',', $travel_to_trim);
            $travel_date = explode(',', $travel_date_trim);
            $travel_distance = explode(',', $travel_distance_trim);
            $dobs = explode(',', $dob_trim);
            $nationalities = explode(',', $nationality_trim);
            $genders = explode(',', $gender_trim);
            $designations = explode(',', $designation_trim);
            $date = explode(',', $date);
            if ($id == 0) {
                if ($health->indicator_type == _ATTACHMENT_ || $health->indicator_type == _UPLOAD_) {
                    $data = array(
                        'health_id' => $health->health_id,
                        'company_id' => $health->company_id,
                        'region_id' => $health->region_id,
                        'country_id' => $health->country_id,
                        'frequency_id' => $health->frequency_id,
                        'type' => $health->type,
                        'value' => $health->value,
                        'status' => _P_,
                        'user_id' => $user_id,
                        'create_date' => $health->create_date,
                        'expiry_date' => $health->expire_date,
                        'file_name' => $health->filename,
                        'org_name' => str_replace('excels/', '', $health->org_name),
                        'indicator_type' => $health->indicator_type,
                        'col1' => $colname1,
                        'col2' => $colname2,
                        'col3' => $colname3
                    );
                } else {
                    $data = array(
                        'health_id' => $health->health_id,
                        'company_id' => $health->company_id,
                        'region_id' => $health->region_id,
                        'country_id' => $health->country_id,
                        'frequency_id' => $health->frequency_id,
                        'type' => $health->type,
                        'value' => $health->value,
                        'status' => _P_,
                        'user_id' => $user_id,
                        'create_date' => $health->create_date,
                        'expiry_date' => $health->expire_date,
                        'col1' => $colname1,
                        'col2' => $colname2,
                        'col3' => $colname3
                    );
                }

                $health_query = $this->tableGateway->insert($data);

                if ($health->indicator_type == _UPLOAD_) {
                    $health_id = $this->tableGateway->lastInsertValue;
                    if ($health_query == true) {
                        foreach ($emp_ids as $index => $name) {
                            if ($health->bulk_type == 1) {
                                $data1 = array(
                                    'employee_id' => $emp_ids[$index],
                                    'employee_name' => $names[$index],
                                    'employee_designation' => $designations[$index],
                                    'create_date' => $date[$index],
                                    'bulk_type' => $health->bulk_type,
                                    'parent_id' => $health_id,
                                    'section_name' => 'health',
                                    'col1' => $col1[$index],
                                    'col2' => $col2[$index],
                                    'col3' => $col3[$index],
                                    'col4' => $col4[$index],
                                    'col5' => $col5[$index],
                                    'col6' => $col6[$index],
                                    'col7' => $col7[$index],
                                    'col8' => $col8[$index],
                                    'col9' => $col9[$index],
                                    'col10' => $col10[$index],
                                );
                            } else if ($health->bulk_type == 2) {
                                $data1 = array(
                                    'employee_id' => $emp_ids[$index],
                                    'employee_name' => $names[$index],
                                    'employee_dob' => date('Y-m-d', strtotime($dobs[$index])),
                                    'employee_age' => date_diff(date_create($dobs[$index]), date_create('today'))->y,
                                    'employee_nationality' => $nationalities[$index],
                                    'gender' => $genders[$index],
                                    'employee_designation' => $designations[$index],
                                    'bulk_type' => $health->bulk_type,
                                    'parent_id' => $health_id,
                                    'section_name' => 'health',
                                    'col1' => $col1[$index],
                                    'col2' => $col2[$index],
                                    'col3' => $col3[$index],
                                    'col4' => $col4[$index],
                                    'col5' => $col5[$index],
                                    'col6' => $col6[$index],
                                    'col7' => $col7[$index],
                                    'col8' => $col8[$index],
                                    'col9' => $col9[$index],
                                    'col10' => $col10[$index],
                                );
                            } else if ($health->bulk_type == 3) {
                                $data1 = array(
                                    'employee_id' => $emp_ids[$index],
                                    'employee_name' => $names[$index],
                                    'travel_from' => $travel_from[$index],
                                    'travel_to' => $travel_to[$index],
                                    'travel_distance' => $travel_distance[$index],
                                    'travel_date' => date_format(date_create($travel_date[$index]), "Y-m-d"),
                                    'bulk_type' => $health->bulk_type,
                                    'parent_id' => $health_id,
                                    'section_name' => 'health',
                                    'col1' => $col1[$index],
                                    'col2' => $col2[$index],
                                    'col3' => $col3[$index],
                                    'col4' => $col4[$index],
                                    'col5' => $col5[$index],
                                    'col6' => $col6[$index],
                                    'col7' => $col7[$index],
                                    'col8' => $col8[$index],
                                    'col9' => $col9[$index],
                                    'col10' => $col10[$index],
                                );
                            }

                            $insert = $sql->insert('bulk_detail');
                            //  $insert->columns($columns);
                            $insert->values($data1);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }

                    }
                }
            } else {
                if ($this->getHealth($id)) {
                    if ($user_type == _ADMIN_) {
                        $data = array(
                            /*'company_id' => $health->company_id,
                            'frequency_id' => $health->frequency_id,
                            'region_id'        => $health->region_id,
                            'country_id'        => $health->country_id,
                            'type'      => $health->type,*/
                            'value' => $health->value,
                            'status' => _A_,
                            'col1' => $colname1,
                            'col2' => $colname2,
                            'col3' => $colname3

                        );
                        if ($health->indicator_type == _UPLOAD_) {
                            // if file re uploaded
                            if ($health->emp_id != NULL) {
                                $delete_row = $sql->delete('bulk_detail');
                                $delete_row->where(array('section_name' => 'health', 'parent_id' => (int)$id));
                                $statement = $sql->prepareStatementForSqlObject($delete_row);
                                $results = $statement->execute();
                                foreach ($emp_ids as $index => $name) {
                                    if ($health->bulk_type == 1) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'employee_designation' => $designations[$index],
                                            'create_date' => $date[$index],
                                            'bulk_type' => $health->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'health',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    } else if ($health->bulk_type == 2) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'employee_dob' => date('Y-m-d', strtotime($dobs[$index])),
                                            'employee_age' => date_diff(date_create($dobs[$index]), date_create('today'))->y,
                                            'employee_nationality' => $nationalities[$index],
                                            'gender' => $genders[$index],
                                            'employee_designation' => $designations[$index],
                                            'bulk_type' => $health->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'health',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    } else if ($health->bulk_type == 3) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'travel_from' => $travel_from[$index],
                                            'travel_to' => $travel_to[$index],
                                            'travel_distance' => $travel_distance[$index],
                                            'travel_date' => date_format(date_create($travel_date[$index]), "Y-m-d"),
                                            'bulk_type' => $health->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'health',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    }
                                    $insert = $sql->insert('bulk_detail');
                                    //  $insert->columns($columns);
                                    $insert->values($data1);
                                    $statement = $sql->prepareStatementForSqlObject($insert);
                                    $results = $statement->execute();
                                }
                            }
                        }
                    }
                    if ($user_type == _CHAMPION_) {
                        $data = array(
                            /*  'company_id' => $health->company_id,
                              'region_id'        => $health->region_id,
                              'country_id'        => $health->country_id,
                              'frequency_id' => $health->frequency_id,
                              'type'      => $health->type,*/
                            'value' => $health->value,
                            'status' => _P_,
                            'col1' => $colname1,
                            'col2' => $colname2,
                            'col3' => $colname3
                        );
                        if ($health->indicator_type == _UPLOAD_) {
                            // if file re uploaded
                            if ($health->employee_id != NULL) {
                                $delete_row = $sql->delete('bulk_detail');
                                $delete_row->where(array('section_name' => 'health', 'parent_id' => (int)$id));
                                $statement = $sql->prepareStatementForSqlObject($delete_row);
                                $results = $statement->execute();
                                foreach ($emp_ids as $index => $name) {
                                    if ($health->bulk_type == 1) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'employee_designation' => $designations[$index],
                                            'create_date' => $date[$index],
                                            'bulk_type' => $health->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'health',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    } else if ($health->bulk_type == 2) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'employee_dob' => date('Y-m-d', strtotime($dobs[$index])),
                                            'employee_age' => date_diff(date_create($dobs[$index]), date_create('today'))->y,
                                            'employee_nationality' => $nationalities[$index],
                                            'gender' => $genders[$index],
                                            'employee_designation' => $designations[$index],
                                            'bulk_type' => $health->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'health',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    } else if ($health->bulk_type == 3) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'travel_from' => $travel_from[$index],
                                            'travel_to' => $travel_to[$index],
                                            'travel_distance' => $travel_distance[$index],
                                            'travel_date' => date_format(date_create($travel_date[$index]), "Y-m-d"),
                                            'bulk_type' => $health->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'health',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    }
                                    $insert = $sql->insert('bulk_detail');
                                    //  $insert->columns($columns);
                                    $insert->values($data1);
                                    $statement = $sql->prepareStatementForSqlObject($insert);
                                    $results = $statement->execute();
                                }
                            }
                        }
                    }
                    $this->tableGateway->update($data, array('health_id' => $id));
                } else {
                    throw new \Exception('Health id does not exist');
                }

            }
        }
    }

    public function deleteHealth($id, $adapter)
    {
        $sql = new Sql($adapter);
        $delete = $sql->delete();
        $delete->from('bulk_detail');
        $delete->where(array('section_name' => 'health', 'parent_id' => (int)$id));
        $statement = $sql->prepareStatementForSqlObject($delete);
        $results = $statement->execute();
        $this->tableGateway->delete(array('health_id' => (int)$id));

    }
}
<?php
namespace Education\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Education\Model\Education;
use Education\Form\EducationForm;
use Education\Model\EducationTable;
use Zend\Session\Container;

use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
class EducationController extends AbstractActionController
{
    protected $EducationTable;
    protected $authservice;
    protected $EventTable;
    protected $TrainingTable;

    public function getEventTable()
    {
        if (!$this->EventTable) {
            $sm = $this->getServiceLocator();
            $this->EventTable = $sm->get('Event\Model\EventTable');
        }
        return $this->EventTable;
    }

    public function getTrainingTable()
    {
        if (!$this->TrainingTable) {
            $sm = $this->getServiceLocator();
            $this->TrainingTable = $sm->get('Training\Model\TrainingTable');
        }
        return $this->TrainingTable;
    }

    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }
        return $this->authservice;
    }
    public function getMenu()
    {
        if (!$this->MenuTable) {
            $sm = $this->getServiceLocator();
            $this->MenuTable = $sm->get('Application\Model\MenuTable');
        }
        return $this->MenuTable;
    }
    public function indexAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $form = new EducationForm();
            $form->get('submit')->setValue('Request Indicator');
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $config = $this->getServiceLocator()->get('Config');
            $dsn = $config['db']['dsn'];
            $username = $config['db']['username'];
            $password = $config['db']['password'];
            $controllerClass= __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            //$Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            //$Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            //$Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsIDByGroup($dbAdapter,$moduleNamespace);
            //$frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
            //$record_expiration = $this->getMenu()->RecordExpiration($dbAdapter,$moduleNamespace,$Indicators,$Companies,$Countries,$frequencies);

            $_SESSION['category']   = "community";
            $_SESSION['area']       = "education";

            if ($userSession->user_type == _ADMIN_) {
                // grab the paginator from the CommunityTable
                $paginator = $this->getMenu()->fetchAll_admin($dbAdapter, true,$moduleNamespace,$dsn,$username,$password,$Indicators);
                //$paginator = $this->getEducationTable()->fetchAll_admin($dbAdapter, true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('education/admin');
                return $view;
            }
            if ($userSession->user_type == _CHAMPION_) {
                // grab the paginator from the CommunityTable
                $paginator = $this->getMenu()->fetchAll_champion($dbAdapter, true,$moduleNamespace,$dsn,$username,$password,$Indicators);
                //$paginator = $this->getEducationTable()->fetchAll_champion($dbAdapter, true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('education/index');
                return $view;
            }
            /* if ($userSession->user_type == _CHAMPION_) {
                $form = new EducationForm();
                $form->get('submit')->setValue('Add');
                $request = $this->getRequest();

                if ($request->isPost()) {
                    $education = new Education();
                    $form->setInputFilter($education->getInputFilter());
                    $form->setData($request->getPost());

                    if ($form->isValid()) {
                        $education->exchangeArray($form->getData());
                        $this->getEducationTable()->saveEducation($education, null);

                        // Redirect to list of albums
                        return $this->redirect()->toRoute('education');
                    }
                }
                return array('form' => $form, 'education' => $this->getEducationTable()->fetchAll($dbAdapter));
            }
            */
        }
            else {
                return $this->redirect()->toRoute('login');
            }


//        return new ViewModel(array(
//            'education' => $this->getEducationTable()->fetchAll(),
//
//        ));
            //    $form->setVariable('form', $form);

    }
    public function getEducationTable()
    {
        if (!$this->EducationTable) {
            $sm = $this->getServiceLocator();
            $this->EducationTable = $sm->get('Education\Model\EducationTable');
        }
        return $this->EducationTable;
    }

    public function addAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $controllerClass= __NAMESPACE__;
        $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
        $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
        $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
        $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
        $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter,$moduleNamespace);
        $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
        $DateField = $this->getMenu()->DateField();
        $form = new EducationForm($Companies,$Regions,$Countries,$Indicators,$frequencies,$DateField);
        $form->get('submit')->setValue('Save');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $education = new Education();
            $created_date =$this->getRequest()->getPost('create_date');
            $indicator_id =$this->getRequest()->getPost('type');
            $company_id =$this->getRequest()->getPost('company_id');
            $country_id =$this->getRequest()->getPost('country_id');
            $frequency_id =$this->getRequest()->getPost('frequency_id');
            $date_validation = $this->getMenu()->DateFieldValidation($dbAdapter,$moduleNamespace,$created_date,$indicator_id,$company_id,$country_id,$frequency_id);
            $form->setInputFilter($education->getInputFilter($date_validation));
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $education->exchangeArray($form->getData());
               /* Expired entries  marked as normal if the desire entry has been made after intimation.*/
                $this->getMenu()->StatusChangeApproved($dbAdapter,$moduleNamespace,$indicator_id,$company_id,$country_id,$frequency_id);
                $expire_date = $this->getMenu()->FrequencyChecker($frequency_id,$created_date);
                $education->expire_date =$expire_date;
                $this->getEducationTable()->saveEducation($education);
                // Redirect to list of Education
                return $this->redirect()->toRoute('education');
            }
        }
        $view = new ViewModel(array(
            'form' => $form, 'education' => $this->getEducationTable()->fetchAll($dbAdapter)
        ));
        $view->setTemplate('education/add');
        return $view;
        // return array('form' => $form);
        }
        else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function editAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {

            $id = (int)$this->params()->fromRoute('id', 0);

            try {
                $education = $this->getEducationTable()->getEducation($id);
            } catch (\Exception $ex) {
                return $this->redirect()->toRoute('education', array(
                    'action' => 'index'
                ));
            }

            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $controllerClass= __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter,$moduleNamespace);
            $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
            $DateField = $this->getMenu()->DateField();

            $form = new EducationForm($Companies,$Regions,$Countries,$Indicators,$frequencies,$DateField);
            $form->bind($education);
            $userSession = new Container('user');

            if($userSession->user_type == _ADMIN_) {
                $form->get('submit')->setAttribute('value', 'Update & Approval');
            }
            if($userSession->user_type == _CHAMPION_) {
                $form->get('submit')->setAttribute('value', 'Update');
            }

            $request = $this->getRequest();
            if ($request->isPost()) {
                $form->setInputFilter($education->getInputFilter(null));
                $form->setData($request->getPost());

                if ($form->isValid()) {
                    $this->getEducationTable()->saveEducation($education,$id);

                    // Redirect to list of educations0
                    return $this->redirect()->toRoute('education');
                }
            }

            return array(
                'id' => $id,
                'form' => $form,
            );
        }
        else {
                return $this->redirect()->toRoute('login');
            }
        }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('education');
        }
        if($id){
            $this->getEducationTable()->deleteEducation($id);
            return $this->redirect()->toRoute('education');
        }
    }
 public function Graph1Action(){
        $fromyear ='';
        $toyear ='';
        $indicator ='';
        $company_id ='';
        if($_REQUEST){
            extract($_REQUEST);
            $fromyear;
            $toyear;
            $indicator;
            $company_id;
        }

        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()){
            //for session varibles
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            if($userSession->user_type == _ADMIN_){
                $GData = $this->getEducationTable()->fetchGraphData($dbAdapter,$fromyear,$toyear,$company_id);


                // grab the paginator from the CommunityTable
                $paginator = $this->getEventTable()->fetchAll_admin($dbAdapter,true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);


                //------------------------------ BAR Chart --------------------------------------
                // grab the paginator from the CommunityTable
                $principle = $this->getTrainingTable()->fetchAll_principle($dbAdapter,true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $principle->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $principle->setItemCountPerPage(10);


                //------------------------------ PIE Chart --------------------------------------
                // grab the paginator from the CommunityTable
                $principle_pie = $this->getTrainingTable()->fetchAll_principle($dbAdapter,true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $principle_pie->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $principle_pie->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'GData'     => $GData,
                    'paginator' => $paginator,
                    'indicator' => $indicator,
                    'principle' => $principle,
                    'principle_pie' => $principle_pie,
                ));
                $view->setTemplate('education/analytic');
                return $view;
            }
            if($userSession->user_type == _CHAMPION_) {
                return $this->redirect()->toRoute('application');
            }
        } else {
            return $this->redirect()->toRoute('login');
        }

    }
}
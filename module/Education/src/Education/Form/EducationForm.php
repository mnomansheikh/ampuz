<?php
namespace Education\Form;

use Zend\Form\Form;
use zend\Db\Adapter\AdapterInterface;
use zend\Db\ResultSet\ResultSet;
class EducationForm extends Form
{
    //  protected $dbAdapter;
    public function __construct($Companies = null,$Regions=null,$Countries= null,$Indicators=null,$Frequencies = null,$DateField=null)
    {
        // we want to ignore the name passed
        parent::__construct('education');
        //$this->setAttribute("method", "post");

        $this->add(array(
            'name' => 'education_id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'category',
            'type' => 'text',
            'attributes' => array(
                'value' => 'community'
            )
        ));

        $this->add(array(
            'name' => 'area',
            'type' => 'text',
            'attributes' => array(
                'value' => 'education'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'company_id',
            'options' => array(
                'label' => 'Company',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Companies,
            ),
            'attributes' => array(
               // 'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'company_form_id',

            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'region_id',
            'options' => array(
                'label' => 'Country',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Regions,
            ),
            'attributes' => array(
                // 'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'region_form_id',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country_id',
            'options' => array(
                'label' => 'Country',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Countries,
            ),
            'attributes' => array(
                // 'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'country_form_id',

            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'frequency_ids',
            'options' => array(
                'label' => 'Frequency',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Frequencies,
            ),
            'attributes' => array(
             //   'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'frequency_form_ids',
            )
        ));
        $this->add(array(
            'name' => 'frequency_name',
            'type' => 'text',
            'attributes' => array(
                'readonly' => 'readonly',
                 'class' => 'form-control',
                'id' => 'frequency_form_name',
            )
        ));
        $this->add(array(
            'name' => 'frequency_id',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'frequency_form_id',
            )
        ));
        if($DateField != null){
            $this->add(
                $DateField
            );
       }
        $this->add(array(
            'name' => 'month',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'month',
            )
        ));
        $this->add(array(
            'name' => 'year',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'year',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'type',
            'options' => array(
                'label' => 'Type',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Indicators,
            ),
            'attributes' => array(
              /*  'value' => '1', //set selected to '1'*/
                'class' => 'form-control',
                'id' => 'indicator_form_id',
            )
        ));

        $this->add(array(
            'name' => 'value',
            'attributes' => array(
                'class' => 'form-control',
                'required' =>    'required',
            ),
        ));


        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
                'class' => 'btn8 btn-8f btn-8 pull-right',
            ),
        ));
  $this->add(array(
            'name' => 'cancel',
            'type' => 'button',
            'options'=>array(
                'label'=>'Cancel'
            ),
            'attributes' => array(
                'onclick' => 'javascript:window.location.href = "/'._PROJECT_NAME_.'/public/education";',
                'class' => 'btn8 btn-8f btn-8 pull-right',
            ),

        ));
    }
}
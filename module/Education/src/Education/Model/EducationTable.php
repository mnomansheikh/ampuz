<?php
namespace Education\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Http\Header\Date;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
class EducationTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($adapter)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;
      // $resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('education');
        $select->where(array('education.user_id' => $user_id,'education.status'=>_P_));
        $select->join('user_profile', 'user_profile.user_id = education.user_id','user_name');
        $select->join('company', 'company.company_id = education.company_id','company_name');
        $select->join('indicator', 'indicator.id = education.type','name');
        $select->join('frequency', 'frequency.frequency_id = education.frequency_id','frequency_name');
        $select->order('education_id DESC');


        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function fetchAll_admin($adapter,$paginated=false)
    {

        if ($paginated) {
            // create a new Select object for the table community
            $userSession = new Container('user');

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('education');
            $select->join('user_profile', 'user_profile.user_id = education.user_id',array('user_name','user_email'));
            $select->join('company', 'company.company_id = education.company_id','company_name');
            $select->join('country', 'country.id = education.country_id','country_name');
            $select->join('region', 'region.id = education.region_id','region_description');
            $select->join('indicator', 'indicator.id = education.type','name');
            $select->join('frequency', 'frequency.frequency_id = education.frequency_id','frequency_name');
            $select->where(array('education.company_id'=>$userSession->user_companies,'education.country_id'=>$userSession->user_countries,'education.region_id'=>$userSession->user_regions));
            $select->order('education_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Education());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function fetchAll_champion($adapter,$paginated=false)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('education');
            $select->where(array('education.user_id'=>$user_id));
            $select->join('user_profile', 'user_profile.user_id = education.user_id','user_name');
            $select->join('company', 'company.company_id = education.company_id','company_name');
            $select->join('indicator', 'indicator.id = education.type','name');
            $select->join('frequency', 'frequency.frequency_id = education.frequency_id','frequency_name');
            $select->order('education_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Education());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }


   public function getEducation($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('education_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveEducation(Education $education)
    {
//        $data = array(
//            'education_id'      => $education->education_id,
//            'company_id'        => $education->company_id,
//            'frequency_id'      => $education->frequency_id,
//            'no_of_scholarship' => $education->no_of_scholarship,
//            'edu_ex_students'   => $education->edu_ex_students,
//        );
//
//      //  $id = (int) $education->id;
//        if ($id == 0) {
//            $this->tableGateway->insert($data);
//        } else {
//            if ($this->getEducation($id)) {
//                $this->tableGateway->update($data, array('education_id' => $id));
//            } else {
//                throw new \Exception('Education id does not exist');
//            }
//        }

        {
            $id = $education->education_id;
            $userSession = new Container('user');
            $user_id = $userSession->user_id;
            $user_type = $userSession->user_type;
            $oneYearOn = date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day"));
            if($id == 0) {
                        $data = array(
                        'education_id'  => $education->education_id,
                        'company_id'    => $education->company_id,
                        'region_id'        => $education->region_id,
                        'country_id'        => $education->country_id,
                        'frequency_id'  => $education->frequency_id,
                        'type'          => $education->type,
                        'value'         => $education->value,
                        'status'  => _P_,
                        'user_id'       => $user_id,
                        'create_date'       => $education->create_date,
                        'expiry_date'       => $education->expire_date
                    );
                    $this->tableGateway->insert($data);
                }

            else {
                if ($this->getEducation($id)) {
                    if($user_type == _ADMIN_) {
                        $data = array(
                            /*'company_id'    => $education->company_id,
                            'region_id'        => $education->region_id,
                            'country_id'        => $education->country_id,
                            'frequency_id'  => $education->frequency_id,
                            'type'          => $education->type,*/
                            'value'         => $education->value,
                            'status'  => _A_

                        );
                    }
                    if($user_type == _CHAMPION_) {
                        $data = array(
                            /*'company_id' => $education->company_id,
                            'region_id'        => $education->region_id,
                            'country_id'        => $education->country_id,
                            'frequency_id' => $education->frequency_id,
                            'type'          => $education->type,*/
                            'value'         => $education->value,
                            'status' => _P_,

                        );
                    }
                    $this->tableGateway->update($data, array('education_id' => $id));
                } else {
                    throw new \Exception('Education id does not exist');
                }

            }
        }
    }

    public function deleteEducation($id)
    {
        $this->tableGateway->delete(array('education_id' => (int) $id));
    }
 public function fetchGraphData($adapter,$fromyear=NULL,$toyear=NULL,$company_id =NULL)
    {
        $sql = new Sql($adapter);

        $select = $sql->select();
        $expression = new Expression("YEAR(create_date)");
        $select->columns(array( new Expression("YEAR(create_date) AS YEAR"), new Expression("SUM(value) AS no_student")));
        $select->from('education');

        $select->group(array(new Expression("YEAR(create_date)")));
        $select->order(array(new Expression("YEAR(create_date)")));
        if($fromyear== NULL){

            $fromyear = date('Y');
            $toyear = date('Y');
            $year1 =(int)$fromyear;
            $year2 =(int)$toyear;
        }
        else{

            $year1 = (int)$fromyear;
            $year2 = (int)$toyear;
        }

        $yearexp = (new Expression("YEAR(create_date)"));
        // $subSelect = new Select('community_partnership');
        $select->where(array('status'=>_A_));
        if($company_id != NULL){
            $company_id = (int)$company_id;
            $select->where(array('status'=>_A_,'company_id'=>$company_id));
        }
        else{

            $select->where(array('status'=>_A_));
        }
        $select->where->between($yearexp, $year1, $year2);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
}
<?php
namespace Workplace\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Http\Header\Date;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
class WorkplaceTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    public function fetchAll($adapter)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;
        // $resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('workplace');
        $select->where(array('workplace.user_id' => $user_id,'workplace.status'=>_P_));
        $select->join('user_profile', 'user_profile.user_id = workplace.user_id','user_name');
        $select->join('company', 'company.company_id = workplace.company_id','company_name');
        $select->join('frequency', 'frequency.frequency_id = workplace.frequency_id','frequency_name');
        $select->join('indicator', 'indicator.id = workplace.type','name');
        $select->order('workplace_id DESC');


        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function fetchAll_workplace($adapter, $id)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id = $userSession->user_id;
        //$resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        // create a new Select object for the table community
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('workplace');
        $select->where(array('workplace_id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function fetchAll_workplace_details($adapter, $id)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id = $userSession->user_id;
        //$resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        // create a new Select object for the table community
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bulk_detail');
        $select->where(array('section_name' => 'workplace', 'parent_id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function fetchAll_admin($adapter, $paginated = false)
    {

        if ($paginated) {
            // create a new Select object for the table community
            $userSession = new Container('user');

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('workplace');
            $select->join('user_profile', 'user_profile.user_id = workplace.user_id',array('user_name','user_email'));
            $select->join('company', 'company.company_id = workplace.company_id','company_name');
            $select->join('country', 'country.id = workplace.country_id','country_name');
            $select->join('region', 'region.id = workplace.region_id','region_description');
            $select->join('frequency', 'frequency.frequency_id = workplace.frequency_id','frequency_name');
            $select->join('indicator', 'indicator.id = workplace.type','name');
            $select->where(array('workplace.company_id'=>$userSession->user_companies,'workplace.country_id'=>$userSession->user_countries,'workplace.region_id'=>$userSession->user_regions));
            $select->order('workplace_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Workplace());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function fetchAll_champion($adapter,$paginated=false)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('workplace');
            $select->where(array('workplace.user_id'=>$user_id));
            $select->join('user_profile', 'user_profile.user_id = workplace.user_id','user_name');
            $select->join('company', 'company.company_id = workplace.company_id','company_name');
            $select->join('frequency', 'frequency.frequency_id = workplace.frequency_id','frequency_name');
            $select->join('indicator', 'indicator.id = workplace.type','name');
            $select->order('workplace_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Workplace());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function getWorkplace($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('workplace_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveWorkplace(Workplace $workplace, $adapter)
    {
        {
            $sql = new Sql($adapter);
            $id = $workplace->workplace_id;
            $colname1 = '';
            $colname2 = '';
            $colname3 = '';

            $col1 = "";
            $col2 = "";
            $col3 = "";
            $col4 = "";
            $col5 = "";
            $col6 = "";
            $col7 = "";
            $col8 = "";
            $col9 = "";
            $col10 = "";
            if (empty($workplace->bulk_type) || $workplace->bulk_type == 0) {
                $count = 1;
                foreach ($workplace->col AS $columns) {
                    ${'colname' . $count} = $columns;
                    $count++;
                }
            } else {
                for($count=1; $count<=_BULK_FIELD_EXTRA_; $count++){
                    if(!empty($workplace->{'col'.$count})){
                        foreach($workplace->{'col'.$count} AS $item){
                            //if(!empty($item)){
                            ${'col'.$count}[] = $item;
                            //}
                        }
                        //${'col'.$count} = $workplace->{'col'.$count};
                    }else{
                        ${'col'.$count} = '';
                    }
                }
            }

            $userSession = new Container('user');
            $user_id = $userSession->user_id;
            $user_type = $userSession->user_type;
            $oneYearOn = date('Y-m-d', strtotime(date("Y-m-d", time()) . " + 365 day"));
            //workplace detail table
            $id_trim = ltrim($workplace->employee_id, ',');
            $name_trim = ltrim($workplace->employee_name, ',');
            $travel_from_trim = ltrim($workplace->travel_from, ',');
            $travel_to_trim = ltrim($workplace->travel_to, ',');
            $travel_date_trim = ltrim($workplace->travel_date, ',');
            $travel_distance_trim = ltrim($workplace->travel_distance, ',');
            $dob_trim = ltrim($workplace->dob, ',');
            $nationality_trim = ltrim($workplace->nationality, ',');
            $gender_trim = ltrim($workplace->gender, ',');
            $designation_trim = ltrim($workplace->designation, ',');
            $date = ltrim($workplace->date, ',');
            $emp_ids = explode(',', $id_trim);
            $names = explode(',', $name_trim);
            $travel_from = explode(',', $travel_from_trim);
            $travel_to = explode(',', $travel_to_trim);
            $travel_date = explode(',', $travel_date_trim);
            $travel_distance = explode(',', $travel_distance_trim);
            $dobs = explode(',', $dob_trim);
            $nationalities = explode(',', $nationality_trim);
            $genders = explode(',', $gender_trim);
            $designations = explode(',', $designation_trim);
            $date = explode(',', $date);
            if ($id == 0) {
                if ($workplace->indicator_type == _ATTACHMENT_ || $workplace->indicator_type == _UPLOAD_) {
                    $data = array(
                        'workplace_id' => $workplace->workplace_id,
                        'company_id' => $workplace->company_id,
                        'region_id' => $workplace->region_id,
                        'country_id' => $workplace->country_id,
                        'frequency_id' => $workplace->frequency_id,
                        'type' => $workplace->type,
                        'value' => $workplace->value,
                        'status' => _P_,
                        'user_id' => $user_id,
                        'create_date' => $workplace->create_date,
                        'expiry_date' => $workplace->expire_date,
                        'file_name' => $workplace->filename,
                        'org_name' => str_replace('excels/', '', $workplace->org_name),
                        'indicator_type' => $workplace->indicator_type,
                        'col1' => $colname1,
                        'col2' => $colname2,
                        'col3' => $colname3
                    );
                } else {
                    $data = array(
                        'workplace_id' => $workplace->workplace_id,
                        'company_id' => $workplace->company_id,
                        'region_id' => $workplace->region_id,
                        'country_id' => $workplace->country_id,
                        'frequency_id' => $workplace->frequency_id,
                        'type' => $workplace->type,
                        'value' => $workplace->value,
                        'status' => _P_,
                        'user_id' => $user_id,
                        'create_date' => $workplace->create_date,
                        'expiry_date' => $workplace->expire_date,
                        'col1' => $colname1,
                        'col2' => $colname2,
                        'col3' => $colname3
                    );
                }

                $workplace_query = $this->tableGateway->insert($data);

                if ($workplace->indicator_type == _UPLOAD_) {
                    $workplace_id = $this->tableGateway->lastInsertValue;
                    if ($workplace_query == true) {
                        foreach ($emp_ids as $index => $name) {
                            if ($workplace->bulk_type == 1) {
                                $data1 = array(
                                    'employee_id' => $emp_ids[$index],
                                    'employee_name' => $names[$index],
                                    'employee_designation' => $designations[$index],
                                    'create_date' => $date[$index],
                                    'bulk_type' => $workplace->bulk_type,
                                    'parent_id' => $workplace_id,
                                    'section_name' => 'workplace',
                                    'col1' => $col1[$index],
                                    'col2' => $col2[$index],
                                    'col3' => $col3[$index],
                                    'col4' => $col4[$index],
                                    'col5' => $col5[$index],
                                    'col6' => $col6[$index],
                                    'col7' => $col7[$index],
                                    'col8' => $col8[$index],
                                    'col9' => $col9[$index],
                                    'col10' => $col10[$index],
                                );
                            } else if ($workplace->bulk_type == 2) {
                                $data1 = array(
                                    'employee_id' => $emp_ids[$index],
                                    'employee_name' => $names[$index],
                                    'employee_dob' => date('Y-m-d', strtotime($dobs[$index])),
                                    'employee_age' => date_diff(date_create($dobs[$index]), date_create('today'))->y,
                                    'employee_nationality' => $nationalities[$index],
                                    'gender' => $genders[$index],
                                    'employee_designation' => $designations[$index],
                                    'bulk_type' => $workplace->bulk_type,
                                    'parent_id' => $workplace_id,
                                    'section_name' => 'workplace',
                                    'col1' => $col1[$index],
                                    'col2' => $col2[$index],
                                    'col3' => $col3[$index],
                                    'col4' => $col4[$index],
                                    'col5' => $col5[$index],
                                    'col6' => $col6[$index],
                                    'col7' => $col7[$index],
                                    'col8' => $col8[$index],
                                    'col9' => $col9[$index],
                                    'col10' => $col10[$index],
                                );
                            } else if ($workplace->bulk_type == 3) {
                                $data1 = array(
                                    'employee_id' => $emp_ids[$index],
                                    'employee_name' => $names[$index],
                                    'travel_from' => $travel_from[$index],
                                    'travel_to' => $travel_to[$index],
                                    'travel_distance' => $travel_distance[$index],
                                    'travel_date' => date_format(date_create($travel_date[$index]), "Y-m-d"),
                                    'bulk_type' => $workplace->bulk_type,
                                    'parent_id' => $workplace_id,
                                    'section_name' => 'workplace',
                                    'col1' => $col1[$index],
                                    'col2' => $col2[$index],
                                    'col3' => $col3[$index],
                                    'col4' => $col4[$index],
                                    'col5' => $col5[$index],
                                    'col6' => $col6[$index],
                                    'col7' => $col7[$index],
                                    'col8' => $col8[$index],
                                    'col9' => $col9[$index],
                                    'col10' => $col10[$index],
                                );
                            }

                            $insert = $sql->insert('bulk_detail');
                            //  $insert->columns($columns);
                            $insert->values($data1);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }

                    }
                }
            } else {
                if ($this->getWorkplace($id)) {
                    if ($user_type == _ADMIN_) {
                        $data = array(
                            /*'company_id' => $workplace->company_id,
                            'frequency_id' => $workplace->frequency_id,
                            'region_id'        => $workplace->region_id,
                            'country_id'        => $workplace->country_id,
                            'type'      => $workplace->type,*/
                            'value' => $workplace->value,
                            'status' => _A_,
                            'col1' => $colname1,
                            'col2' => $colname2,
                            'col3' => $colname3

                        );
                        if ($workplace->indicator_type == _UPLOAD_) {
                            // if file re uploaded
                            if ($workplace->emp_id != NULL) {
                                $delete_row = $sql->delete('bulk_detail');
                                $delete_row->where(array('section_name' => 'workplace', 'parent_id' => (int)$id));
                                $statement = $sql->prepareStatementForSqlObject($delete_row);
                                $results = $statement->execute();
                                foreach ($emp_ids as $index => $name) {
                                    if ($workplace->bulk_type == 1) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'employee_designation' => $designations[$index],
                                            'create_date' => $date[$index],
                                            'bulk_type' => $workplace->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'workplace',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    } else if ($workplace->bulk_type == 2) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'employee_dob' => date('Y-m-d', strtotime($dobs[$index])),
                                            'employee_age' => date_diff(date_create($dobs[$index]), date_create('today'))->y,
                                            'employee_nationality' => $nationalities[$index],
                                            'gender' => $genders[$index],
                                            'employee_designation' => $designations[$index],
                                            'bulk_type' => $workplace->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'workplace',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    } else if ($workplace->bulk_type == 3) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'travel_from' => $travel_from[$index],
                                            'travel_to' => $travel_to[$index],
                                            'travel_distance' => $travel_distance[$index],
                                            'travel_date' => date_format(date_create($travel_date[$index]), "Y-m-d"),
                                            'bulk_type' => $workplace->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'workplace',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    }
                                    $insert = $sql->insert('bulk_detail');
                                    //  $insert->columns($columns);
                                    $insert->values($data1);
                                    $statement = $sql->prepareStatementForSqlObject($insert);
                                    $results = $statement->execute();
                                }
                            }
                        }
                    }
                    if ($user_type == _CHAMPION_) {
                        $data = array(
                            /*  'company_id' => $workplace->company_id,
                              'region_id'        => $workplace->region_id,
                              'country_id'        => $workplace->country_id,
                              'frequency_id' => $workplace->frequency_id,
                              'type'      => $workplace->type,*/
                            'value' => $workplace->value,
                            'status' => _P_,
                            'col1' => $colname1,
                            'col2' => $colname2,
                            'col3' => $colname3
                        );
                        if ($workplace->indicator_type == _UPLOAD_) {
                            // if file re uploaded
                            if ($workplace->employee_id != NULL) {
                                $delete_row = $sql->delete('bulk_detail');
                                $delete_row->where(array('section_name' => 'workplace', 'parent_id' => (int)$id));
                                $statement = $sql->prepareStatementForSqlObject($delete_row);
                                $results = $statement->execute();
                                foreach ($emp_ids as $index => $name) {
                                    if ($workplace->bulk_type == 1) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'employee_designation' => $designations[$index],
                                            'create_date' => $date[$index],
                                            'bulk_type' => $workplace->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'workplace',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    } else if ($workplace->bulk_type == 2) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'employee_dob' => date('Y-m-d', strtotime($dobs[$index])),
                                            'employee_age' => date_diff(date_create($dobs[$index]), date_create('today'))->y,
                                            'employee_nationality' => $nationalities[$index],
                                            'gender' => $genders[$index],
                                            'employee_designation' => $designations[$index],
                                            'bulk_type' => $workplace->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'workplace',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    } else if ($workplace->bulk_type == 3) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'travel_from' => $travel_from[$index],
                                            'travel_to' => $travel_to[$index],
                                            'travel_distance' => $travel_distance[$index],
                                            'travel_date' => date_format(date_create($travel_date[$index]), "Y-m-d"),
                                            'bulk_type' => $workplace->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'workplace',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    }
                                    $insert = $sql->insert('bulk_detail');
                                    //  $insert->columns($columns);
                                    $insert->values($data1);
                                    $statement = $sql->prepareStatementForSqlObject($insert);
                                    $results = $statement->execute();
                                }
                            }
                        }
                    }
                    $this->tableGateway->update($data, array('workplace_id' => $id));
                } else {
                    throw new \Exception('Workplace id does not exist');
                }

            }
        }
    }

    public function deleteWorkplace($id, $adapter)
    {
        $sql = new Sql($adapter);
        $delete = $sql->delete();
        $delete->from('bulk_detail');
        $delete->where(array('section_name' => 'workplace', 'parent_id' => (int)$id));
        $statement = $sql->prepareStatementForSqlObject($delete);
        $results = $statement->execute();
        $this->tableGateway->delete(array('workplace_id' => (int)$id));

    }
}
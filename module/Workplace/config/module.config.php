<?php

return array(

    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

    'controllers' => array(
        'invokables' => array(
            'Workplace\Controller\Workplace' => 'Workplace\Controller\WorkplaceController',
        ),

    ),
    'view_helpers' => array(
        'invokables' => array(
            'distancehelper' => 'Workplace\Helper\DistanceHelper',
        ),
    ),
    'service_manager' => array(

    ),

    'router' => array(
// Uncomment below to add routes
        'routes' => array(
            'workplace' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/workplace[/:action][/:id][/:str]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Workplace\Controller',
                        'controller' => 'Workplace',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
            'default' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/[:controller[/:action]]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    )
                )
            )
        )
    )
);
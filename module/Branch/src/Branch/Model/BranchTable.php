<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/27/2016
 * Time: 5:40 PM
 */

namespace Branch\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class BranchTable
{
    protected $tableGateway;

    public function __construct(tableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;

        $username = $userSession->username;
        $password = md5($userSession->password);
        $resultSet = $this->tableGateway->select(array('user_name' => $username, 'user_pwd' => $password));
        return $resultSet;
    }

    public function getUserProfile($id, $adapter)
    {
        $id = (int)$id;

//  SELECT u.*,u.`user_id` AS userid
//, GROUP_CONCAT(DISTINCT uc.`company_id`) AS company_ids
//,GROUP_CONCAT(DISTINCT uco.`country_id`) AS country_ids
//,GROUP_CONCAT(DISTINCT ur.`region_id`) AS region_ids
//,GROUP_CONCAT(DISTINCT umr.`sub_menu_id`) AS sub_menu_ids
//FROM `user_profile` u
//INNER JOIN `user_company` uc ON uc.`user_id` = u.`user_id`
// INNER JOIN `user_country` uco ON uco.`user_id` = u.`user_id`
//INNER JOIN `user_region` ur ON ur.`user_id` = u.`user_id`
// INNER JOIN `user_menu_rights` umr ON umr.`user_id` = u.`user_id`
//WHERE u.user_id = 7
//GROUP BY `userid`;

//        $sql = new Sql($adapter);
//        $select = $sql->select();
//        $select->from(array('u' => 'user_profile'));
//        $select->join(array('uc' => 'user_company'), 'uc.user_id = u.user_id ',array( 'company_ids' => new Expression('GROUP_CONCAT(DISTINCT uc.company_id)')));
//        $select->join(array('uco' => 'user_country'), 'uco.user_id = u.user_id ',array( 'country_ids'=> new Expression('GROUP_CONCAT(DISTINCT uco.country_id)')));
//        $select->join(array('ur' => 'user_region'), 'ur.user_id = u.user_id ',array('region_ids' =>  new Expression('GROUP_CONCAT(DISTINCT ur.region_id)')));
//        $select->join(array('umr' => 'user_menu_rights'), 'umr.user_id = u.user_id ',array( 'sub_menu_ids' =>  new Expression('GROUP_CONCAT(DISTINCT umr.sub_menu_id)')));
//        $select->where(array('u.user_id '=>$id));
//        //$select->group('u.userid');
//        $statement = $sql->prepareStatementForSqlObject($select);
//        $results = $statement->execute();
//        //  $rowset = $this->tableGateway->select(array('user_id' => $id));
//        $row = $results->current();
//
//        $company_ids = explode(',',$row['company_ids']);
//        $country_ids = explode(',',$row['country_ids']);
//        $region_ids = explode(',',$row['region_ids']);
//        $sub_menu_ids = explode(',',$row['sub_menu_ids']);
//        $row['company_id']=  (object) $company_ids;
//        $row['country_id']=  (object) $country_ids;
//        $row['region_id']=  (object) $region_ids;
//        $row['sub_menu_id']=  (object) $sub_menu_ids;

        $rowset = $this->tableGateway->select(function (Select $select) use ($id) {
            $select
                ->columns(array(
                    '*'
                ))
                ->join(array('uc' => 'user_company'), 'uc.user_id = user_profile.user_id ', array('company_ids' => new Expression('GROUP_CONCAT(DISTINCT uc.company_id)')))
                ->join(array('uco' => 'user_country'), 'uco.user_id = user_profile.user_id ', array('country_ids' => new Expression('GROUP_CONCAT(DISTINCT uco.country_id)')))
                ->join(array('ur' => 'user_region'), 'ur.user_id = user_profile.user_id ', array('region_ids' => new Expression('GROUP_CONCAT(DISTINCT ur.region_id)')))
                ->join(array('umr' => 'user_menu_rights'), 'umr.user_id = user_profile.user_id ', array('menu_ids' => new Expression('GROUP_CONCAT(DISTINCT umr.menu_id)'),'sub_menu_ids' => new Expression('GROUP_CONCAT(DISTINCT umr.sub_menu_id)')))
                ->where(array('user_profile.user_id ' => $id));
            // ->group('user_profile.userid');
        });


        $row = $rowset->current();
        $company_ids = explode(',', $row['company_ids']);
        $country_ids = explode(',', $row['country_ids']);
        $region_ids = explode(',', $row['region_ids']);
        $menu_ids = explode(',', $row['menu_ids']);
        $sub_menu_ids = explode(',', $row['sub_menu_ids']);
        $row['company_id'] = $company_ids;
        $row['country_id'] = $country_ids;
        $row['region_id'] = $region_ids;
        $row['menu_id'] = $menu_ids;
        $row['sub_menu_id'] = $sub_menu_ids;
        $row['user_password'] = $row['user_pwd'];
        // $id  = (int) $id;
        // $rowset = $this->tableGateway->select(array('user_id' => $id));
        // $row = $rowset->current();


//$row = (object) $row;
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getUser($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(array('user_id' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveBranch(Branch $Branch, $adapter)
    {
        $sql = new Sql($adapter);
        $id = $Branch->company_id;
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $user_type = $userSession->user_type;


        if ($id == 0) {

            $data = array(
                'user_name' => $Branch->user_name,
                'user_pwd' => md5($Branch->user_password),
                'user_email' => $Branch->user_email,
                'gender_id' => $Branch->gender_id,
                'city_id' => $Branch->city_id,
                /* 'country_id' => $Branch->country_id,
                 'company_id' => $Branch->company_id,*/
                'user_role_id' => $Branch->user_role_id,
                'user_type' => $Branch->user_type,
                'status' => $Branch->status,
                'create_date' => date('Y-m-d'),
                'expiry_date' => $Branch->expiry_date,
                'create_by' => $user_id,

            );
            $Branch_query = $this->tableGateway->insert($data);
            $last_inserted_id = $this->tableGateway->lastInsertValue;
            if ($Branch_query == true) {
                /*insert companies*/
                foreach ($Branch->company_id as $company_id) {
                    $data_user_company = array(
                        'company_id' => $company_id,
                        'user_id' => $last_inserted_id,
                    );
                    $insert = $sql->insert('user_company');
                    $insert->values($data_user_company);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }
                /*insert companies*/
                /*insert regions*/
                foreach ($Branch->region_id as $region_id) {
                    $data_user_region = array(
                        'region_id' => $region_id,
                        'user_id' => $last_inserted_id,
                    );
                    $insert = $sql->insert('user_region');
                    $insert->values($data_user_region);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }
                /*insert regions*/
                /*insert countries*/
                foreach ($Branch->country_id as $country_id) {
                    $data_user_region = array(
                        'country_id' => $country_id,
                        'user_id' => $last_inserted_id,
                    );
                    $insert = $sql->insert('user_country');
                    $insert->values($data_user_region);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }
                /*insert countries*/
                /*insert user_menu_rights*/
                foreach ($Branch->menu_id as $menu_id) {
                    $data_user_menu = array(
                        'menu_id' => $menu_id,
                        'user_id' => $last_inserted_id,
                    );
                    $insert = $sql->insert('user_menu_rights');
                    $insert->values($data_user_menu);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }
                foreach ($Branch->sub_menu_id as $sub_menu_id) {
                    $data_user_menu = array(
                        'sub_menu_id' => $sub_menu_id,
                        'user_id' => $last_inserted_id,
                    );
                    $insert = $sql->insert('user_menu_rights');
                    $insert->values($data_user_menu);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }
                /*insert user_menu_rights*/
            }

        }
        else {

            if ($this->getUser($id)) {
                if ($user_type == _ADMIN_) {
                    $data = array(
                        /* 'user_name' => $Branch->user_name,*/
                        /*'user_pwd' => md5($Branch->user_password),*/
                        'user_email' => $Branch->user_email,
                        'gender_id' => $Branch->gender_id,
                        'city_id' => $Branch->city_id,
                        /* 'country_id' => $Branch->country_id,
                         'company_id' => $Branch->company_id,*/
                        'user_role_id' => $Branch->user_role_id,
                        'user_type' => $Branch->user_type,
                        'status' => $Branch->status,
                        'modified_date' => date('Y-m-d'),
                        'expiry_date' => $Branch->expiry_date,
                        'create_by' => $user_id,

                    );
                    /*change password */
                    if($Branch->user_password != NULL){
                        $data[ 'user_pwd' ] =md5($Branch->user_password);
                    }
                    /*change password */
                    //status
                    /*
                                if($Branch->status == 1 ){
                                    $status  = _ACTIVE_;

                                }
                                else{
                                    $status  = _DEACTIVE_;
                                }*/

                    // if file re uploaded
                    if ($Branch->user_id != NULL) {

                        /*delete companies*/
                        $delete_row = $sql->delete('user_company');
                        $delete_row->where(array('user_id' => (int)$id));
                        $statement = $sql->prepareStatementForSqlObject($delete_row);
                        $statement->execute();
                        /*delete regions*/
                        $delete_row = $sql->delete('user_region');
                        $delete_row->where(array('user_id' => (int)$id));
                        $statement = $sql->prepareStatementForSqlObject($delete_row);
                        $statement->execute();
                        /*delete countries*/
                        $delete_row = $sql->delete('user_country');
                        $delete_row->where(array('user_id' => (int)$id));
                        $statement = $sql->prepareStatementForSqlObject($delete_row);
                        $statement->execute();
                        /*delete user_menu_rights*/
                        $delete_row = $sql->delete('user_menu_rights');
                        $delete_row->where(array('user_id' => (int)$id));
                        $statement = $sql->prepareStatementForSqlObject($delete_row);
                        $statement->execute();

                        /*insert companies*/
                        foreach ($Branch->company_id as $company_id) {
                            $data_user_company = array(
                                'company_id' => $company_id,
                                'user_id' => $id,
                            );
                            $insert = $sql->insert('user_company');
                            $insert->values($data_user_company);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                        /*insert companies*/
                        /*insert regions*/
                        foreach ($Branch->region_id as $region_id) {
                            $data_user_region = array(
                                'region_id' => $region_id,
                                'user_id' => $id,
                            );
                            $insert = $sql->insert('user_region');
                            $insert->values($data_user_region);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                        /*insert regions*/
                        /*insert countries*/
                        foreach ($Branch->country_id as $country_id) {
                            $data_user_region = array(
                                'country_id' => $country_id,
                                'user_id' => $id,
                            );
                            $insert = $sql->insert('user_country');
                            $insert->values($data_user_region);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                        /*insert countries*/
                        /*insert user_menu_rights*/
                        foreach ($Branch->menu_id as $menu_id) {
                            $data_user_menu = array(
                                'menu_id' => $menu_id,
                                'user_id' => $id,
                            );
                            $insert = $sql->insert('user_menu_rights');
                            $insert->values($data_user_menu);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                        foreach ($Branch->sub_menu_id as $sub_menu_id) {
                            $data_user_menu = array(
                                'sub_menu_id' => $sub_menu_id,
                                'user_id' => $id,
                            );
                            $insert = $sql->insert('user_menu_rights');
                            $insert->values($data_user_menu);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                        /*insert user_menu_rights*/

                    } else {

                    }
                    $this->tableGateway->update($data, array('user_id' => $id));
                }

            } else {
                throw new \Exception('User id does not exist');
            }

        }
    }

    public function deleteMilestone($id, $adapter)
    {
        $this->tableGateway->delete(array('milestone_id' => (int)$id, 'status' => _P_));

        $sql = new Sql($adapter);
        $delete = $sql->delete();
        $delete->from('milestone_details');
        $delete->where(array('milestone_id' => (int)$id));
        $statement = $sql->prepareStatementForSqlObject($delete);
        $results = $statement->execute();
    }

    public function fetchAll_user($adapter, $paginated = false)
    {
        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            //        $sql = new Sql($adapter);
            $select->from(array('u' => 'user_profile'));
            $select->join(array('uc' => 'user_company'), 'uc.user_id = u.user_id ', array('company_ids' => new Expression('GROUP_CONCAT(DISTINCT uc.company_id)')));
            $select->join(array('uco' => 'user_country'), 'uco.user_id = u.user_id ', array('country_ids' => new Expression('GROUP_CONCAT(DISTINCT uco.country_id)')));
            $select->join(array('ur' => 'user_region'), 'ur.user_id = u.user_id ', array('region_ids' => new Expression('GROUP_CONCAT(DISTINCT ur.region_id)')));
            $select->join(array('umr' => 'user_menu_rights'), 'umr.user_id = u.user_id ', array('sub_menu_ids' => new Expression('GROUP_CONCAT(DISTINCT umr.sub_menu_id)')));
            $select->group('u.user_id');
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            // create a new result set based on the User entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new UserProfile());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );

//            $statement = $sql->prepareStatementForSqlObject($select);
//            $selectquery = $statement->execute();
//            foreach ($selectquery as $selectquery1) {
//                $date = $selectquery1['create_date'];
//                //echo $selectquery1['create_date'] . '<br>';
//
//                if ((strtotime($date) < strtotime('30 days ago')) && $selectquery1['company_id'] && $selectquery1['user_id']) {
//                    //  echo 'true' . '<br>';
//
//                }
//            }
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

}



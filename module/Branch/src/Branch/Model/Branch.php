<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/27/2016
 * Time: 5:42 PM
 */

namespace Branch\Model;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Branch implements InputFilterAwareInterface
{
    public $company_id;
    public $company_name;
    public $address;
    public $email;
    public $region_id;
    public $country_id;
    public $city_id;
    public $contact_no;
    public $status;
// public $user_id;
    public $inputFilter;

    protected $dbAdapter;

    public function setDbAdapter($dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }
    public function exchangeArray($data)
    {
        $this->company_id             = (!empty($data['company_id'])) ? $data['company_id'] : null;
        $this->company_name             = (!empty($data['company_name'])) ? $data['company_name'] : null;
        $this->address    = (!empty($data['address'])) ? $data['address'] : null;
        $this->email            = (!empty($data['email'])) ? $data['email'] : null;
        $this->region_id         = (!empty($data['region_id'])) ? $data['region_id'] : null;
        $this->country_id         = (!empty($data['country_id'])) ? $data['country_id'] : null;
        $this->city_id             = (isset($data['city_id']))  ? $data['city_id']     : null;
        $this->contact_no             = (isset($data['contact_no']))  ? $data['contact_no']     : null;
        $this->status           = (isset($data['status']))  ? $data['status']     : null;
    }
    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
//            $inputFilter->add(array(
//                'name'     => 'user_id',
//                'required' => false,
//                'filters'  => array(
//                    array('name' => 'Int'),
//                ),
//                /*  'validators' => array(
//                     array(
//                         'name'    => 'StringLength',
//                         'options' => array(
//                             'encoding' => 'UTF-8',
//                             'min'      => 1,
//                             'max'      => 100,
//                         ),
//                     ),
//                 ),*/
//            ));
//            $inputFilter->add(array(
//                'name' => 'user_name', // add second password field
//                /* ... other params ... */
//                'validators' => array(
//                    array(
//                        'name' => '\Zend\Validator\Db\NoRecordExists',
//                        'options' => array(
//                            'table' => 'user_profile',
//                            'field' => 'user_name',
//                            'adapter' => $this->dbAdapter,
//                            'messages' => array(
//                                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'Change Username already exist',
//                            ),
//                        ),
//                    ),
//                ),
//            ));
//            $inputFilter->add(array(
//                'name' => 'confirm_password', // add second password field
//                /* ... other params ... */
//                'validators' => array(
//                    array(
//                        'name' => 'Identical',
//                        'options' => array(
//                            'message' => 'Password and Re-type Password do not match',
//                            'token' => 'user_password', // name of first password field
//                        ),
//                    ),
//                ),
//            ));
//            $inputFilter->add(array(
//                'name'     => 'expiry_date',
//                'required' => false,
//                'filters'  => array(
//                    array('name' => 'StripTags'),
//                    array('name' => 'StringTrim'),
//                ),
//                /*  'validators' => array(
//                      array(
//                          'name'    => 'StringLength',
//                          'options' => array(
//                              'encoding' => 'UTF-8',
//                              'min'      => 1,
//                              'max'      => 100,
//                          ),
//                      ),
//                  ),*/
//            ));

            $this->inputFilter = $inputFilter;

        }

        return $this->inputFilter;
    }
}
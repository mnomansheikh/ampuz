<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/27/2016
 * Time: 5:43 PM
 */

namespace Branch\Form;
use Zend\Form\Form;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Sql;
use Zend\Form\Element;

class BranchForm extends Form
{

    public function __construct(AdapterInterface $dbAdapter)
    {
        // we want to ignore the name passed
        $this->adapter = $dbAdapter;
        parent::__construct('login');
        $sql = new Sql($dbAdapter);

        $select = $sql->select();
        $select->from('region');
        //$select->join('company_region', 'company_region.region_id = region.id', 'company_id');
        $select->where(array('region.is_active' => 1));
        $select->order('region.id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $regions = $statement->execute();
        $regions_arr = array();
        $itr = 0 ;
        foreach ($regions as $region) {
            $regions_arr[$itr]['value'] = $region['id'];
            $regions_arr[$itr]['label'] = $region['region_description'];
            $itr++;
        }

        $select = $sql->select();
        $select->from('city');
        $select->join('country_city', 'country_city.city_id = city.id', 'country_id');
        $select->where(array('city.is_active' => 1));
        $select->order('city.id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $cities = $statement->execute();
        $cities_arr = array();
        $itr = 0;
        foreach ($cities as $city) {
            $cities_arr[$itr]['value'] = $city['id'];
            $cities_arr[$itr]['label'] = $city['city_name'];
            $cities_arr[$itr]['attributes']['data-country-id'] = $city['country_id'];
            $itr++;
        }


        $select = $sql->select();
        $select->from('country');
        $select->join('country_region', 'country_region.country_id = country.id', 'region_id');
        $select->where(array('country.is_active' => 1));
        $select->order('country.id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $countries = $statement->execute();
        $countries_arr = array();
        $itr = 0;
        foreach ($countries as $country) {
            // $countries_arr[$country['id']] = $country['country_name'];
            $countries_arr[$itr]['value'] = $country['id'];
            $countries_arr[$itr]['label'] = $country['country_name'];
            $countries_arr[$itr]['attributes']['data-region-id'] = $country['region_id'];
            $itr++;
        }


        $this->add(array(
            'name' => 'company_id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'company_name',
            'attributes' => array(
                'id' => 'company_name',
                'type' => 'text',
                'required' => 'required',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'address',
            'attributes' => array(
                'id' => 'address',
                'type' => 'textarea',
                'required' => 'required',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'id' => 'email',
                'type' => 'email',
                'required' => 'required',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'region_id',
            'options' => array(
                'label' => 'Region',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $regions_arr,
            ),
            'attributes' => array(
                // 'value' => '', //set selected to '1'
                'class' => 'form-control',
                'id' => 'region_id',
                'multiple' => 'multiple',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country_id',
            'options' => array(
                'label' => 'Country',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $countries_arr,
            ),
            'attributes' => array(
                // 'value' => '', //set selected to '1'
                'class' => 'form-control',
                'id' => 'country_id',
                'multiple' => 'multiple',

            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'city_id',
            'options' => array(
                'label' => 'City',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $cities_arr,
            ),
            'attributes' => array(
                // 'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'city_id',
                'multiple' => 'multiple',
            )
        ));

        $this->add(array(
            'name' => 'contact_no',
            'attributes' => array(
                'id' => 'contact_no',
                'type' => 'text',
                'required' => 'required',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'status',
            'attributes' => array(
                'id' => 'status',
                'value' => '1'
            ),
            'options' => array(
                'label' => 'Status',
                'use_hidden_element' => true,
                'checked_value' => 'A',
                'unchecked_value' => 'D'
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'submitbutton',
                'class' => 'btn8 btn-8 btn-8f pull-right',
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'type' => 'button',
            'options' => array(
                'label' => 'Cancel'
            ),
            'attributes' => array(
                'onclick' => 'javascript:window.location.href = "/' . _PROJECT_NAME_ . '/public/company/list";',
                'class' => 'btn8 btn-8 btn-8f pull-right',
            ),
        ));
    }
}
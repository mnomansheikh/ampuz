<?php
namespace National\Form;

use Zend\Form\Form;

class NationalForm extends Form
{
    //  protected $dbAdapter;
    public function __construct($Companies = null,$Regions=null,$Countries= null,$Indicators=null,$Frequencies = null,$DateField=null)
    {
        // we want to ignore the name passed
        parent::__construct('national');
        //$this->setAttribute("method", "post");

        $this->add(array(
            'name' => 'national_id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'category',
            'type' => 'text',
            'attributes' => array(
                'value' => 'our people'
            )
        ));

        $this->add(array(
            'name' => 'area',
            'type' => 'text',
            'attributes' => array(
                'value' => 'nationalization'
            )
        ));

        $this->add(array(
            'name' => 'employee_id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'name',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'dob',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'nationality',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'gender',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'designation',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'file_name',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'org_name',
            'type' => 'Hidden',
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'company_id',
            'options' => array(
                'label' => 'Company',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Companies,
            ),
            'attributes' => array(
               // 'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'company_form_id',

            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'region_id',
            'options' => array(
                'label' => 'Country',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Regions,
            ),
            'attributes' => array(
                // 'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'region_form_id',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country_id',
            'options' => array(
                'label' => 'Country',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Countries,
            ),
            'attributes' => array(
                // 'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'country_form_id',

            )
        ));
/*        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'frequency_ids',
            'options' => array(
                'label' => 'Frequency',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Frequencies,
            ),
            'attributes' => array(
             //   'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'frequency_form_ids',
            )
        ));*/
        $this->add(array(
            'name' => 'frequency_name',
            'type' => 'text',
            'attributes' => array(
                'readonly' => 'readonly',
                 'class' => 'form-control',
               // 'id' => 'frequency_form_name',
                'value' => 'Monthly'
            )
        ));
        $this->add(array(
            'name' => 'frequency_id',
            'type' => 'Hidden',
            'attributes' => array(
           //     'id' => 'frequency_form_id',
                 'value' => '1'
            )
        ));
        if($DateField != null){
            $this->add(
                $DateField
            );
       }
        $this->add(array(
            'name' => 'month',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'month',
            )
        ));
        $this->add(array(
            'name' => 'year',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'year',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'type',
            'options' => array(
                'label' => 'Type',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Indicators,
            ),
            'attributes' => array(
              /*  'value' => '1', //set selected to '1'*/
                'class' => 'form-control',
                'id' => 'indicator_form_id',
            )
        ));

        $this->add(array(
            'name' => 'value',
            'attributes' => array(
                'class' => 'form-control',
                'required' =>    'required',
                'readonly' =>    'readonly',
            ),
        ));


        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
                'class' => 'btn8 btn-8 btn-8f pull-right',
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'type' => 'button',
            'options'=>array(
                'label'=>'Cancel'
            ),
            'attributes' => array(
                'onclick' => 'javascript:window.location.href = "/'._PROJECT_NAME_.'/public/national";',
                'class' => 'btn8 btn-8f btn-8 pull-right',
            ),

        ));

        $this->add(array(
            'name' => 'upload',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'File Upload',
                'id' => 'submitbutton1',
                'class' => 'btn btn-primary btn-xs',
            ),
        ));

        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');

        $this->add(array(
            'name' => 'fileupload',
            'attributes' => array(
                'type'  => 'file',
                'required'  => 'required',
            ),
            'options' => array(
                'label' => '',
                'class' => 'form-control',
            ),
        ));
    }
}
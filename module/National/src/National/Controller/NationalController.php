<?php
namespace National\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use National\Model\National;
use National\Form\NationalForm;
use National\Model\NationalTable;
use Zend\Session\Container;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class NationalController extends AbstractActionController
{
    protected $NationalTable;
    protected $authservice;

    public function getAuthService()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }
        return $this->authservice;
    }

    public function getMenu()
    {
        if (!$this->MenuTable) {
            $sm = $this->getServiceLocator();
            $this->MenuTable = $sm->get('Application\Model\MenuTable');
        }
        return $this->MenuTable;
    }

    public function indexAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $form = new NationalForm();
            $form->get('submit')->setValue('Request Indicator');
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $config = $this->getServiceLocator()->get('Config');
            $dsn = $config['db']['dsn'];
            $username = $config['db']['username'];
            $password = $config['db']['password'];
            $controllerClass= __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
           // $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            //$Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            //$Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsIDByGroup($dbAdapter,$moduleNamespace);
            //$frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
           // $record_expiration = $this->getMenu()->RecordExpiration($dbAdapter,$moduleNamespace,$Indicators,$Companies,$Countries,$frequencies);

            if ($userSession->user_type == _ADMIN_) {
                // grab the paginator from the CommunityTable
                //$paginator = $this->getNationalTable()->fetchAll_admin($dbAdapter, true);
                $paginator = $this->getMenu()->fetchAll_admin($dbAdapter, true,$moduleNamespace,$dsn,$username,$password,$Indicators);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('national/admin');
                return $view;
            }
            if ($userSession->user_type == _CHAMPION_) {
                // grab the paginator from the CommunityTable
               // $paginator = $this->getNationalTable()->fetchAll_champion($dbAdapter, true);
                $paginator = $this->getMenu()->fetchAll_champion($dbAdapter, true,$moduleNamespace,$dsn,$username,$password,$Indicators);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('national/index');
                return $view;
            }
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function getNationalTable()
    {
        if (!$this->NationalTable) {
            $sm = $this->getServiceLocator();
            $this->NationalTable = $sm->get('National\Model\NationalTable');
        }
        return $this->NationalTable;
    }

    public function addAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $controllerClass = __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter, $moduleNamespace);
            $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter, $moduleNamespace);
            $DateField = $this->getMenu()->DateField();
            $form = new NationalForm($Companies, $Regions, $Countries, $Indicators, $frequencies, $DateField);
            $form->get('submit')->setValue('Save');
            $request = $this->getRequest();
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            if ($request->isPost()) {
                $national = new National();
                $created_date = $this->getRequest()->getPost('create_date');
                $indicator_id = $this->getRequest()->getPost('type');
                $company_id = $this->getRequest()->getPost('company_id');
                $country_id = $this->getRequest()->getPost('country_id');
                $frequency_id = $this->getRequest()->getPost('frequency_id');
                $date_validation = $this->getMenu()->DateFieldValidation($dbAdapter, $moduleNamespace, $created_date, $indicator_id, $company_id, $country_id, $frequency_id);
                $form->setInputFilter($national->getInputFilter($date_validation));
                $form->setData($request->getPost());

                if ($form->isValid()) {
                    $national->exchangeArray($form->getData());
                    /* Expired entries  marked as normal if the desire entry has been made after intimation.*/
                    $this->getMenu()->StatusChangeApproved($dbAdapter, $moduleNamespace, $indicator_id, $company_id, $country_id, $frequency_id);
                    $expire_date = $this->getMenu()->FrequencyChecker($frequency_id, $created_date);
                    $national->expire_date = $expire_date;
                    $this->getNationalTable()->saveNational($national, $dbAdapter);
                    // Redirect to list of National
                    return $this->redirect()->toRoute('national');
                }
            }
            $view = new ViewModel(array(
                'form' => $form, 'national' => $this->getNationalTable()->fetchAll($dbAdapter)
            ));
            $view->setTemplate('national/add');
            return $view;
            // return array('form' => $form);
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function editAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {

            $id = (int)$this->params()->fromRoute('id', 0);

            try {
                $national = $this->getNationalTable()->getNational($id);
            } catch (\Exception $ex) {
                return $this->redirect()->toRoute('national', array(
                    'action' => 'index'
                ));
            }

            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $controllerClass = __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter, $moduleNamespace);
            $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter, $moduleNamespace);
            $DateField = $this->getMenu()->DateField();

            $form = new NationalForm($Companies, $Regions, $Countries, $Indicators, $frequencies, $DateField);
            $form->bind($national);
            $userSession = new Container('user');

            if ($userSession->user_type == _ADMIN_) {
                $form->get('submit')->setAttribute('value', 'Update & Approval');
            }
            if ($userSession->user_type == _CHAMPION_) {
                $form->get('submit')->setAttribute('value', 'Update');
            }

            $request = $this->getRequest();
            if ($request->isPost()) {
                $form->setInputFilter($national->getInputFilter());
                $form->setData($request->getPost());

                if ($form->isValid()) {
                    $this->getNationalTable()->saveNational($national, $dbAdapter);

                    // Redirect to list of nationals0
                    return $this->redirect()->toRoute('national');
                }
            }
            $national_details = $this->getNationalTable()->fetchAll_national_details($dbAdapter, $id);
            $national = $this->getNationalTable()->fetchAll_national($dbAdapter, $id);
            return array(
                'id' => $id,
                'form' => $form,
                'national_details' => $national_details,
                'national' => $national,
            );
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function deleteAction()
    {
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('national');
        }
        if ($id) {
            $this->getNationalTable()->deleteNational($id, $dbAdapter);
            return $this->redirect()->toRoute('national');
        }
    }
    public function Graph1Action(){
        $company_id ='';
        $region_id ='';
        $country_id ='';
        $group ='';
        $frequency ='';
        $indicator ='';
        $toyear ='';
        $fromyear ='' ;
        $gender ='' ;
        $nationality ='' ;
        $designation ='' ;
        if($_REQUEST){
            extract($_REQUEST);
            $company_id;
            $region_id;
            $country_id;
            $indicator;
            $toyear;
            $fromyear ;
            $group ;
            $frequency;
            $nationality;
            $designation;
        }
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()){
            //for session varibles
            $userSession = new Container('user');
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            // $form = new MilestoneForm($dbAdapter);
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            if($userSession->user_type == _ADMIN_){
                $controllerClass = __NAMESPACE__;
                $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
                // grab the paginator from the CommunityTable
                //   $paginator = $this->getMilestoneTable()->fetchAll_admin($dbAdapter,true);
                // set the current page to what has been passed in query string, or to 1 if none set
                //   $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                //   $paginator->setItemCountPerPage(10);
                //  $GData = $this->getMilestoneTable()->fetchGraphData($dbAdapter,$fromyear,$toyear,$company_id,$event_desc);
                $GData = $this->getMenu()->fetchGraphData($dbAdapter,$company_id,$group,$frequency,$indicator,$fromyear,$toyear,$country_id);
                $Groups = $this->getMenu()->fetchAllGroupForOurPeople($dbAdapter);
                $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
                $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
                $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
                $Indicators = $this->getMenu()->fetchIndicator($dbAdapter);
                $Frequencies = $this->getMenu()->fetchFrequency($dbAdapter);
                $company_name = $this->getMenu()->fetchCompanyById($dbAdapter,$company_id);
                $country_name = $this->getMenu()->fetchCountryById($dbAdapter,$country_id);
                $region_name = $this->getMenu()->fetchRegionById($dbAdapter,$region_id);
                $group_name = $this->getMenu()->fetchGroupById($dbAdapter,$group);
                /*diversity*/
                $genders = $this->getMenu()->fetchAllGenderForDiversity($dbAdapter);
                $nationalities = $this->getMenu()->fetchAllNationalityForDiversity($dbAdapter);
                $designations = $this->getMenu()->fetchAllDesignationsForDiversity($dbAdapter);
              if($group ==12){
                  $GData_Venn = $this->getMenu()->fetchGraphDataForDiversityVenn($dbAdapter,$company_id,$group,$frequency,$indicator,$fromyear,$toyear,$country_id,$gender,$nationality,$designation);

              }
                 /*diversity*/
                $view = new ViewModel(array(
                    'GData' => $GData,
                    'Groups' => $Groups,
                    'Companies' => $Companies,
                    'Regions' => $Regions,
                    'Countries' => $Countries,
                    'Indicators' => $Indicators,
                    'Frequencies' => $Frequencies,
                    'company_name' => $company_name,
                    'country_name' => $country_name,
                    'region_name' => $region_name,
                    'group_name' => $group_name,
                    'moduleNamespace' => $moduleNamespace,
                    'genders' => $genders,
                    'nationalities' => $nationalities,
                    'designations' => $designations,
                    'GData_Venn' => $GData_Venn,

                ));
                $view->setTemplate('national/analytic');
                return $view;
            }
            if($userSession->user_type == _CHAMPION_) {
                return $this->redirect()->toRoute('application');
            }
        } else {
            return $this->redirect()->toRoute('login');
        }

    }
}
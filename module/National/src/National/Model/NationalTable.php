<?php
namespace National\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Http\Header\Date;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
class NationalTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($adapter)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;
        // $resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('national');
        $select->where(array('national.user_id' => $user_id,'national.status'=>_P_));
        $select->join('user_profile', 'user_profile.user_id = national.user_id','user_name');
        $select->join('company', 'company.company_id = national.company_id','company_name');
       // $select->join('indicator', 'indicator.id = national.type','name');
        $select->join('frequency', 'frequency.frequency_id = national.frequency_id','frequency_name');
        $select->order('national_id DESC');


        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    public function fetchAll_national($adapter,$id)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;
        //$resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        // create a new Select object for the table community
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('national');
        $select->where(array('national_id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    public function fetchAll_national_details($adapter,$id)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;
        //$resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        // create a new Select object for the table community
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bulk_details');
        $select->where(array('section_name' => 'national' ,'parent_id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    public function fetchAll_admin($adapter,$paginated=false)
    {

        if ($paginated) {
            // create a new Select object for the table community
            $userSession = new Container('user');

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('national');
            $select->join('user_profile', 'user_profile.user_id = national.user_id',array('user_name','user_email'));
            $select->join('company', 'company.company_id = national.company_id','company_name');
            $select->join('country', 'country.id = national.country_id','country_name');
            $select->join('region', 'region.id = national.region_id','region_description');
           // $select->join('indicator', 'indicator.id = national.type','name');
            $select->join('frequency', 'frequency.frequency_id = national.frequency_id','frequency_name');
            $select->where(array('national.company_id'=>$userSession->user_companies,'national.country_id'=>$userSession->user_countries,'national.region_id'=>$userSession->user_regions));
            $select->order('national_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new National());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function fetchAll_champion($adapter,$paginated=false)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('national');
            $select->where(array('national.user_id'=>$user_id));
            $select->join('user_profile', 'user_profile.user_id = national.user_id','user_name');
            $select->join('company', 'company.company_id = national.company_id','company_name');
            //$select->join('indicator', 'indicator.id = national.type','name');
            $select->join('frequency', 'frequency.frequency_id = national.frequency_id','frequency_name');
            $select->order('national_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new National());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function getNational($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('national_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveNational(National $national,$adapter)
    {
        $sql = new Sql($adapter);
        {
            $id = $national->national_id;
            $userSession = new Container('user');
            $user_id = $userSession->user_id;
            $user_type = $userSession->user_type;
            //national detail
            $employee_id_trim=  ltrim($national->employee_id, ',');
            $name_trim=    ltrim($national->name, ',');
            $dob_trim=   ltrim($national->dob, ',');
            $nationality_trim=   ltrim($national->nationality, ',');
            $gender_trim=   ltrim($national->gender, ',');
            $designation_trim=   ltrim($national->designation, ',');
            $employee_ids = explode(',', $employee_id_trim);
            $names = explode(',',$name_trim);
            $dobs = explode(',', $dob_trim);
            $nationalities = explode(',', $nationality_trim);
            $genders = explode(',', $gender_trim);
            $designations = explode(',', $designation_trim);
            if($id == 0) {
                $data = array(
                    'national_id'       => $national->national_id,
                    'company_id'        => $national->company_id,
                    'region_id'        => $national->region_id,
                    'country_id'        => $national->country_id,
                    'frequency_id'      => $national->frequency_id,
                    'type'              => $national->type,
                    'value'             => $national->value,
                    'status'            => _P_,
                    'user_id'           => $user_id,
                    'create_date'       => $national->create_date,
                    'expiry_date'       => $national->expire_date
                );
                $national_query=   $this->tableGateway->insert($data);
                $id = $this->tableGateway->lastInsertValue;
                if($national_query == true){
                    foreach ($employee_ids as $index => $emp_id) {
                        $data1 = array(
                            'employee_id' =>  $employee_ids[$index],
                            'employee_name' =>  $names[$index],
                            'employee_dob' =>  date('Y-m-d',strtotime($dobs[$index])),
                            'employee_age' =>  date_diff(date_create($dobs[$index]), date_create('today'))->y,
                            'employee_nationality' =>  $nationalities[$index],
                            'gender' =>  $genders[$index],
                            'employee_designation' =>  $designations[$index],
                            'parent_id' => $id,
                            'section_name' => 'national' ,
                            'company_id'        => $national->company_id,
                            'region_id'        => $national->region_id,
                            'country_id'        => $national->country_id,
                        );

                        /*check if employ already exist*/
//                        $sql = new Sql($adapter);
//                        $select = $sql->select();
//                        $select->from('national_details');
//                        $select->where(array('company_id' => $national->company_id, 'region_id'  => $national->region_id, 'country_id'  => $national->country_id,'employee_id' =>  $employee_ids[$index]));
//                        $statement = $sql->prepareStatementForSqlObject($select);
//                        $results = $statement->execute();
//                        $row = $results->current();
//                        if($row){
//                            $update= $sql->update('national_details');
//                            //  $insert->columns($columns);
//                            $update->set($data1);
//                            $update->where(array( 'employee_id' =>  $employee_ids[$index]));
//                            $statement = $sql->prepareStatementForSqlObject($update);
//                            $results = $statement->execute();
//                        }
//                        else{
                            $insert= $sql->insert('bulk_detail');
                            //  $insert->columns($columns);
                            $insert->values($data1);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        //}
                        /*check if employ already exist*/
                    }

                }
            }

            else {
                if ($this->getNational($id)) {
                    if($user_type == _ADMIN_) {
                        $data = array(
                           /* 'company_id' => $national->company_id,
                            'frequency_id' => $national->frequency_id,
                            'type'              => $national->type,*/
                            'value'             => $national->value,
                            'status'  => _A_

                        );
                        if($national->employee_id !=NULL) {
                            $delete_row = $sql->delete('bulk_detail');
                            $delete_row->where(array('section_name' => 'national' ,'parent_id' => (int)$id));
                            $statement = $sql->prepareStatementForSqlObject($delete_row);
                            foreach ($employee_ids as $index => $emp_id) {
                                $data1 = array(
                                    'employee_id' =>  $employee_ids[$index],
                                    'employee_name' =>  $names[$index],
                                    'employee_dob' =>  date('Y-m-d',strtotime($dobs[$index])),
                                    'employee_age' =>  date_diff(date_create($dobs[$index]), date_create('today'))->y,
                                    'employee_nationality' =>  $nationalities[$index],
                                    'gender' =>  $genders[$index],
                                    'employee_designation' =>  $designations[$index],
                                    'parent_id' => $id,
                                    'section_name' => 'national' ,
                                    'company_id'        => $national->company_id,
                                    'region_id'        => $national->region_id,
                                    'country_id'        => $national->country_id,
                                );

                                /*check if employ already exist*/
//                                $sql = new Sql($adapter);
//                                $select = $sql->select();
//                                $select->from('national_details');
//                                $select->where(array('company_id' => $national->company_id, 'region_id'  => $national->region_id, 'country_id'  => $national->country_id,'employee_id' =>  $employee_ids[$index]));
//                                $statement = $sql->prepareStatementForSqlObject($select);
//                                $results = $statement->execute();
//                                $row = $results->current();
//                                if($row){
//                                    $update= $sql->update('national_details');
//                                    //  $insert->columns($columns);
//                                    $update->set($data1);
//                                    $update->where(array( 'employee_id' =>  $employee_ids[$index]));
//                                    $statement = $sql->prepareStatementForSqlObject($update);
//                                    $results = $statement->execute();
//                                }
//                                else{
                                    $insert= $sql->insert('bulk_detail');
                                    //  $insert->columns($columns);
                                    $insert->values($data1);
                                    $statement = $sql->prepareStatementForSqlObject($insert);
                                    $results = $statement->execute();
                              //  }
                                /*check if employ already exist*/
                            }
                        }
                    }
                    if($user_type == _CHAMPION_) {
                        $data = array(
                          /*  'company_id' => $national->company_id,
                            'frequency_id' => $national->frequency_id,
                            'type'              => $national->type,*/
                            'value'             => $national->value,
                            'status' => _P_,

                        );
                        if($national->employee_id !=NULL) {
                            $delete_row = $sql->delete('bulk_detail');
                            $delete_row->where(array('section_name' => 'national' ,'parent_id' => (int)$id));
                            $statement = $sql->prepareStatementForSqlObject($delete_row);
                            foreach ($employee_ids as $index => $emp_id) {
                                $data1 = array(
                                    'employee_id' =>  $employee_ids[$index],
                                    'employee_name' =>  $names[$index],
                                    'employee_dob' =>  date('Y-m-d',strtotime($dobs[$index])),
                                    'employee_age' =>  date_diff(date_create($dobs[$index]), date_create('today'))->y,
                                    'employee_nationality' =>  $nationalities[$index],
                                    'gender' =>  $genders[$index],
                                    'employee_designation' =>  $designations[$index],
                                    'parent_id' => $id,
                                    'section_name' => 'national' ,
                                    'company_id'        => $national->company_id,
                                    'region_id'        => $national->region_id,
                                    'country_id'        => $national->country_id,
                                );

//                                /*check if employ already exist*/
//                                $sql = new Sql($adapter);
//                                $select = $sql->select();
//                                $select->from('national_details');
//                                $select->where(array('company_id' => $national->company_id, 'region_id'  => $national->region_id, 'country_id'  => $national->country_id,'employee_id' =>  $employee_ids[$index]));
//                                $statement = $sql->prepareStatementForSqlObject($select);
//                                $results = $statement->execute();
//                                $row = $results->current();
//                                if($row){
//                                    $update= $sql->update('national_details');
//                                    //  $insert->columns($columns);
//                                    $update->set($data1);
//                                    $update->where(array( 'employee_id' =>  $employee_ids[$index]));
//                                    $statement = $sql->prepareStatementForSqlObject($update);
//                                    $results = $statement->execute();
//                                }
//                                else{
                                    $insert= $sql->insert('bulk_detail');
                                    //  $insert->columns($columns);
                                    $insert->values($data1);
                                    $statement = $sql->prepareStatementForSqlObject($insert);
                                    $results = $statement->execute();
                                //}
                                /*check if employ already exist*/
                            }
                        }
                    }
                    $this->tableGateway->update($data, array('section_name' => 'national' ,'parent_id' => $id));
                } else {
                    throw new \Exception('National id does not exist');
                }

            }
        }
    }

    public function deleteNational($id,$adapter)
    {
        $this->tableGateway->delete(array('section_name' => 'national' ,'parent_id' => (int) $id));
        $sql = new Sql($adapter);
        $delete = $sql->delete();
        $delete->from('bulk_detail');
        $delete->where (array('section_name' => 'national' ,'parent_id'  => (int) $id ));
        $statement = $sql->prepareStatementForSqlObject($delete);
        $results = $statement->execute();
    }
}
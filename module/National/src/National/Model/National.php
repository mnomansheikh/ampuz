<?php
namespace National\Model;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class National implements InputFilterAwareInterface
{
    public $national_id;
    public $company_id;
    public $region_id;
    public $country_id;
    public $frequency_id;
   // public $type;
    public $value;
    public $id;
    public $inputFilter;
    public $create_date;
    public $employee_id ;
    public $name ;
    public $dob ;
    public $nationality ;
    public $gender;
    public $designation;
    public $filename;
    public $org_name;
    //protected $inputFilter;                       // <-- Add this variable

    public function exchangeArray($data)
    {
        $this->national_id          = (!empty($data['national_id'])) ? $data['national_id'] : null;
        $this->company_id           = (!empty($data['company_id'])) ? $data['company_id'] : null;
        $this->region_id           = (!empty($data['region_id'])) ? $data['region_id'] : null;
        $this->country_id           = (!empty($data['country_id'])) ? $data['country_id'] : null;
        $this->frequency_id         = (!empty($data['frequency_id'])) ? $data['frequency_id'] : null;
        //$this->type                 = (!empty($data['type'])) ? $data['type'] : null;
        $this->value                = (!empty($data['value'])) ? $data['value'] : null;
        $this->id                   = (!empty($data['id'])) ? $data['id'] : null;
        $this->create_date                   = (!empty($data['create_date'])) ? $data['create_date'] : null;
        $this->employee_id                   = (!empty($data['employee_id'])) ? $data['employee_id'] : null;
        $this->name                   = (!empty($data['name'])) ? $data['name'] : null;
        $this->dob                   = (!empty($data['dob'])) ? $data['dob'] : null;
        $this->nationality                   = (!empty($data['nationality'])) ? $data['nationality'] : null;
        $this->gender                   = (!empty($data['gender'])) ? $data['gender'] : null;
        $this->designation                   = (!empty($data['designation'])) ? $data['designation'] : null;
        $this->filename             = (isset($data['file_name']))  ? $data['file_name']     : null;
        $this->org_name             = (isset($data['org_name']))  ? $data['org_name']     : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }


    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter($date_validation=null)
    {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
           if($date_validation != null){
               $inputFilter->add(
                   $date_validation
               );
           }
            $inputFilter->add(array(
                'name'     => 'national_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'company_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ));
            $inputFilter->add(array(
                'name'     => 'frequency_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'value',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));

//             $inputFilter->add(array(
//                 'name'     => 'type',
//                 'required' => true,
//                 'filters'  => array(
//                     array('name' => 'StripTags'),
//                     array('name' => 'StringTrim'),
//                 ),
//                 'validators' => array(
//                     array(
//                         'name'    => 'StringLength',
//                         'options' => array(
//                             'encoding' => 'UTF-8',
//                             'min'      => 1,
//                            'max'      => 100,
//                        ),
//                    ),
//                ),
//            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
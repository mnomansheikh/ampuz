<?php
namespace Request\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Request\Model\Request;          // <-- Add this import
use Request\Form\RequestForm;       // <-- Add this import
use Request\Model;
//use Request\Model\RequestIndicator;
use Zend\Session\Container;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class RequestController extends AbstractActionController
{
    protected $requestTable;

    public function indexAction()
    {


        $id =  $this->params()->fromRoute('id', 0);
//        $to =  $this->params()->fromRoute('email_id', 0);
        $to = 'demo.comstar@gmail.com';
        $form = new EmailForm();
        $form->get('submit')->setValue('Send');
        $form->get('to')->setValue($to);
        return array(
            'form' => $form


        );
    }


    public function addAction()
    {
        $category = $_POST['category'];
        $area =     $_POST['area'];

        $form = new RequestForm();
        $form->get('submit')->setValue('Send');

        $request = $this->getRequest();
//
//        if ($request->isPost()) {
//            $album = new Request();
//            $form->setInputFilter($album->getInputFilter());
//            $form->setData($request->getPost());
//            RequestIndicator::saveData();
//
//            if ($form->isValid()) {
//
//                $album->exchangeArray($form->getData());
//                $this->getRequestTable()->saveAlbum($album);
//
//
//
//                // Redirect to list of albums
//                return $this->redirect()->toRoute('dashboard');
//            }
//        }
        return array(
            'form' => $form,
            'area' => $area,
            'category' => $category,
        );
    }
}
<?php
namespace Request\Form;

use Zend\Form\Form;

class RequestForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('request');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'title',
            'type' => 'Text',
            'options' => array(
                'label' => 'Category',
                'attributes' => array(
                    'value'  => ' ',
                    'class' => 'control-label'
                ),
            ),
            'attributes' => array(
                'class'  => 'form-control',
                'required' =>    'required',
                'readonly' => 'readonly'
            ),

        ));

        $this->add(array(
            'name' => 'indicator',
            'type' => 'Text',
            'options' => array(
                'label' => 'Area',
                'attributes' => array(
                    'value'  => ' ',
                    'class' => 'control-label'
                ),
            ),
            'attributes' => array(
                'class'  =>     'form-control',
                'required' =>    'required',
                'readonly' => 'readonly'
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'description',
            'options' => array(
                'label' => 'Message',
                //'value_options' => $this->getOptionsForSelect(),
                'attributes' => array(
                    'class' => 'control-label'
                )
            ),
            'attributes' => array(
                'value' =>      '',
                'class' =>      'form-control',
                'required' =>   'required',
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
                'class' => 'btn8 btn-8 btn-8f pull-right',
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'type' => 'button',
            'options'=>array(
                'label'=>'Cancel'
            ),
            'attributes' => array(
                'onclick' => 'javascript:history.back();',
                'class' => 'btn8 btn-8 btn-8f pull-right',
            ),

        ));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 1/5/2016
 * Time: 4:10 PM
 */

namespace Request\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Session\Container;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

class RequestIndicator
{


    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($adapter)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('area');
        $select->order('name DESC');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

//    public function saveData($data)
//    {
//
//        $data = array(
//            'created_at'      => '2007-03-22',
//            'description' => 'Something wrong',
//            'area_id'      => 1,
//            'category_id' => 1
//        );
//
//        ('request_indicator');
//
//    }
    public function saveData()
    {
        $form = new RequestForm();
        $form->get('submit')->setValue('Save');
        $request = $this->getRequest();
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        if ($request->isPost()) {
            $education = new Request();
            $form->setInputFilter($education->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $education->exchangeArray($form->getData());
                $this->getEducationTable()->saveRequest($education);
                // Redirect to list of Education
                return $this->redirect()->toRoute('request');
            }
        }

        // return array('form' => $form);
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/4/2016
 * Time: 1:16 PM
 */

namespace CarbonCalculator\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Session\Container;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

class CarbonCalculatorTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }


    public function fetchGraphData($adapter, $company_id = NULL, $group = NULL, $frequency = NULL, $indicator = NULL, $fromyear = NULL, $toyear = NULL, $country_id = NULL)
    {
        //$indicator_str = implode('","',$indicator);
        if ($indicator[0] == "") {
            $indicator = NULL;
        }
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('sub_menu');
        // $select->join('menu', 'menu.id = sub_menu.menu_id');
        $select->where(array('id' => $group));
        $statement = $sql->prepareStatementForSqlObject($select);
        $sub_menu = $statement->execute();
        $Gdata_arr = array();
        foreach ($sub_menu as $sub_menus) {
            $route = $sub_menus['route'];
            $label = $sub_menus['label'];
            $select = $sql->select();
            $select->from($route);
            if ($frequency != NULL) {
                $select->where(array($route . '.frequency_id' => $frequency));
            }
            if ($company_id != NULL) {
                $select->where(array($route . '.company_id' => $company_id));
            }
            if ($country_id != NULL) {
                $select->where(array($route . '.country_id' => $country_id));
            }
            if ($indicator != NULL) {
                $select->join('indicator', 'indicator.id = ' . $route . '.type', array('indicator_name' => 'name', 'carbon_value' => 'measuring_unit_value'));
                $select->join('measure_unit', 'measure_unit.id = indicator.measuring_unit_id', 'unit_description');
                $select->where(array($route . '.type' => $indicator));
            }
            $select->where->between($route . '.create_date', $fromyear, $toyear);
            $select->where(array($route . '.status' => _A_));
            $select->order(array($route . '.type ASC ', $route . '.create_date ASC'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $table_results = $statement->execute();
            $itr = 0;

            foreach ($table_results as $table_result) {

                if ($frequency == NULL || $indicator == NULL) {
                    $select = $sql->select();
                    $select->from('indicator');
                    $select->columns(array('indicator_name' => 'name', 'carbon_value' => 'measuring_unit_value'));
                    $select->join('measure_unit', 'measure_unit.id = indicator.measuring_unit_id', 'unit_description');
                    $select->where(array('indicator.id' => $table_result['type']));
                    $statement = $sql->prepareStatementForSqlObject($select);
                    $measure_units = $statement->execute();
                    foreach ($measure_units as $measure_unit) {
                        $measure_unit_carbon = $measure_unit['carbon_value'];
                        $indicator_name = $measure_unit['indicator_name'];
                    }
                    //$measure_unit_carbon = $table_result['carbon_value'];
                }

                // $select->join('measure_unit', 'measure_unit.indicator_id = '.$route.'.type',array('unit_description','carbon_value' => 'value'));
                // $select->where(array($route.'.company_id' => $company_id, $route.'.status' => _A_,$route.'.type'=> $indicator));
                $Gdata_arr[$itr] = $table_result;
                $Gdata_arr[$itr]['label'] = $label;
                $Gdata_arr[$itr]['value'] = $table_result['value'];
                $Gdata_arr[$itr]['date'] = $table_result['create_date'];
                if ($frequency == NULL || $indicator == NULL) {
                    $Gdata_arr[$itr]['carbon_value'] = $measure_unit_carbon;
                    $Gdata_arr[$itr]['indicator_name'] = $indicator_name;
                } else if ($indicator != NULL) {
                    $Gdata_arr[$itr]['carbon_value'] = $table_result['carbon_value'];
                }

                if ($indicator == NULL) {
                    $Gdata_arr[$itr]['indicator_status'] = 'noindicator';
                }
                $itr++;
            }
        }
        return $Gdata_arr;
    }
}
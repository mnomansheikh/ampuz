<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/4/2016
 * Time: 1:14 PM
 */
namespace CarbonCalculator\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use CarbonCalculator\Model\CarbonCalculatorTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Milestone\Model\Milestone;
use Milestone\Form\MilestoneForm;
use Zend\Validator\File\Size;
use Zend\Session\Container;

class CarbonCalculatorController extends AbstractActionController
{
    protected $eventTable;
    protected $authservice;
    protected $TrainingTable;


    public function getAuthService()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authservice;
    }

    public function getCarbonCalculatorTable()
    {
        if (!$this->CarbonCalculatorTable) {
            $sm = $this->getServiceLocator();
            $this->CarbonCalculatorTable = $sm->get('CarbonCalculator\Model\CarbonCalculatorTable');
        }
        return $this->CarbonCalculatorTable;
    }

    public function getMenu()
    {
        if (!$this->MenuTable) {
            $sm = $this->getServiceLocator();
            $this->MenuTable = $sm->get('Application\Model\MenuTable');
        }
        return $this->MenuTable;
    }

    public function Graph1Action()
    {
        $company_id = '';
        $region_id = '';
        $country_id = '';
        $group = '';
        $frequency = '';
        $indicator = '';
        $toyear = '';
        $fromyear = '';
        if ($_REQUEST) {
            extract($_REQUEST);
            $company_id;
            $region_id;
            $country_id;
            $indicator;
            $toyear;
            $fromyear;
            $group;
            $frequency;
        }
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            //for session varibles
            $userSession = new Container('user');
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            // $form = new MilestoneForm($dbAdapter);
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            if ($userSession->user_type == _ADMIN_) {
                // grab the paginator from the CommunityTable
                //   $paginator = $this->getMilestoneTable()->fetchAll_admin($dbAdapter,true);
                // set the current page to what has been passed in query string, or to 1 if none set
                //   $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                //   $paginator->setItemCountPerPage(10);
                //  $GData = $this->getMilestoneTable()->fetchGraphData($dbAdapter,$fromyear,$toyear,$company_id,$event_desc);
                $GData = $this->getCarbonCalculatorTable()->fetchGraphData($dbAdapter, $company_id, $group, $frequency, $indicator, $fromyear, $toyear, $country_id);

                $Groups = $this->getMenu()->fetchGroup($dbAdapter);
                $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
                $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
                $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
                $Indicators = $this->getMenu()->fetchIndicator($dbAdapter);
                $Frequencies = $this->getMenu()->fetchFrequency($dbAdapter);
                $company_name = $this->getMenu()->fetchCompanyById($dbAdapter, $company_id);
                $country_name = $this->getMenu()->fetchCountryById($dbAdapter, $country_id);
                $region_name = $this->getMenu()->fetchRegionById($dbAdapter, $region_id);
                $group_name = $this->getMenu()->fetchGroupById($dbAdapter, $group);
                $view = new ViewModel(array(
                    'GData' => $GData,
                    'Groups' => $Groups,
                    'Companies' => $Companies,
                    'Regions' => $Regions,
                    'Countries' => $Countries,
                    'Indicators' => $Indicators,
                    'Frequencies' => $Frequencies,
                    'company_name' => $company_name,
                    'country_name' => $country_name,
                    'region_name' => $region_name,
                    'group_name' => $group_name,
                    //'form' => $form,
                    //'indicator' => $indicator,
                    // 'paginator' => $paginator,
                ));

                $view->setTemplate('carboncalculator/analytic');
                return $view;
            }
            if ($userSession->user_type == _CHAMPION_) {
                return $this->redirect()->toRoute('application');
            }
        } else {
            return $this->redirect()->toRoute('login');
        }

    }
}
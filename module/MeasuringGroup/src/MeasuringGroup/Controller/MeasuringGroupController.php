<?php

//module/Login/src/Controller/AuthController.php
namespace MeasuringGroup\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use MeasuringGroup\Model\MeasuringGroup;
use MeasuringGroup\Form\MeasuringGroupForm;

class MeasuringGroupController extends AbstractActionController
{
    protected $form;
    protected $userSession;
    protected $storage;
    protected $authservice;
    // protected $currentUserId;
    protected $MeasuringGroupTable;
//    public function getUserProfileTable()
//    {
//        if (!$this->UserProfileTable) {
//            $sm = $this->getServiceLocator();
//            $this->UserProfileTable = $sm->get('Login\Model\UserProfileTable');
//        }
//        return $this->UserProfileTable;
//    }
    public function getMeasuringGroupTable()
    {
        if (!$this->MeasuringGroupTable) {
            $sm = $this->getServiceLocator();
            $this->MeasuringGroupTable = $sm->get('MeasuringGroup\Model\MeasuringGroupTable');
        }
        return $this->MeasuringGroupTable;
    }


    public function getAuthService()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authservice;
    }


    public function ListAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $form = new MeasuringGroupForm($dbAdapter);
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user

            // grab the paginator from the CommunityTable
            $paginator = $this->getMeasuringGroupTable()->fetchAll_measuringgroup($dbAdapter, true);
            // set the current page to what has been passed in query string, or to 1 if none set
            $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
            // set the number of items per page to 10
            $paginator->setItemCountPerPage(10);

            $view = new ViewModel(array(
                'paginator' => $paginator,
                'form' => $form

            ));
            $view->setTemplate('measuring-group/list');
            return $view;
        }

    }

    public function addAction()
    {

        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            //  if ($userSession->user_type == _ADMIN_) {
            $form = new MeasuringGroupForm($dbAdapter);
            $form->get('submit')->setValue('Save');
            $form->get('cancel')->setValue('Cancel');
            $request = $this->getRequest();

            if ($request->isPost()) {
                $MeasuringGroup = new MeasuringGroup();
                $MeasuringGroup->setDbAdapter($dbAdapter);
                $form->setInputFilter($MeasuringGroup->getInputFilter());
                $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(),
                    $this->getRequest()->getFiles()->toArray()
                );

                $form->setData($request->getPost());
                $form->setData($data);

                if ($form->isValid()) {
                    $MeasuringGroup->exchangeArray($form->getData());
                    $this->getMeasuringGroupTable()->saveMeasuringGroup($MeasuringGroup, $dbAdapter);
                    // Redirect to list
                    return $this->redirect()->toRoute('measuring-group', array(
                        'action' => 'list'
                    ));
                }
            }


            //return array('form' => $form, 'event' => $this->getUserProfileTable()->fetchAll($dbAdapter));
            $view = new ViewModel(array(
                'form' => $form
            ));
            $view->setTemplate('measuring-group/add');
            return $view;
            // }
            //  if ($userSession->user_type == _CHAMPION_) {
            //      return $this->redirect()->toRoute('application');
            //  }
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function editAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $id = (int)$this->params()->fromRoute('id', 0);
            try {
                $MeasuringGroupTable = $this->getMeasuringGroupTable()->getMeasuringGroup($id, $dbAdapter);
            } catch (\Exception $ex) {
                //return $this->redirect()->toRoute('measuring-group', array(
                //    'action' => 'list'
                //));
            }
            $form = new MeasuringGroupForm($dbAdapter);
            $MeasuringGroup = new MeasuringGroup();
            $form->bind($MeasuringGroupTable);
            //for session varibles
            $form->get('submit')->setValue('Save');
            $form->get('cancel')->setValue('Cancel');
            $request = $this->getRequest();
            if ($request->isPost()) {
                $MeasuringGroup->setDbAdapter($dbAdapter);
                $form->setInputFilter($MeasuringGroup->getInputFilter());
                $form->setData($request->getPost());
                if ($id > 0) {
                    $form->getInputFilter()->get('description')->setRequired(false);
                }
                if ($form->isValid()) {
                    $MeasuringGroup->exchangeArray($form->getData());
                    $this->getMeasuringGroupTable()->saveMeasuringGroup($MeasuringGroup, $dbAdapter);

                    // Redirect to list
                    return $this->redirect()->toRoute('measuring-group', array(
                        'action' => 'list'
                    ));
                }
            }
            //  $user = $this->getCurrentUserTable()->fetchAll_event($dbAdapter,$id);
            return array(
                'id' => $id,
                'form' => $form,
                //    'user' => $user,

            );
        } else {
            return $this->redirect()->toRoute('login');
        }

    }

}
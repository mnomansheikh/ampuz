<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/27/2016
 * Time: 5:40 PM
 */

namespace Company\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class CompanyTable
{
    protected $tableGateway;

    public function __construct(tableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    public function getCompanyMaster($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(array('company_id' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getCompany($id)
    {
//        SELECT c.* ,
// GROUP_CONCAT(DISTINCT cr.region_id) AS region_ids ,
// GROUP_CONCAT(DISTINCT cco.country_id) AS country_ids ,
// GROUP_CONCAT(DISTINCT cci.city_id) AS city_ids
//FROM `company` c
//INNER JOIN `company_region` cr ON cr.`company_id` = c.`company_id`
//INNER JOIN `company_country` cco ON cco.`company_id` = c.`company_id`
//INNER JOIN `company_city` cci ON cci.`company_id` = c.`company_id`
//WHERE c.`company_id` = 4
//GROUP BY `company_id`;

        $rowset = $this->tableGateway->select(function (Select $select) use ($id) {
            $select
                ->columns(array(
                    '*'
                ))
                ->join(array('cr' => 'company_region'), 'cr.company_id = company.company_id', array('region_id' => new Expression('GROUP_CONCAT(DISTINCT cr.region_id)')))
                ->join(array('cco' => 'company_country'), 'cco.company_id = company.company_id ', array('country_id' => new Expression('GROUP_CONCAT(DISTINCT cco.country_id)')))
                ->join(array('cci' => 'company_city'), 'cci.company_id = company.company_id', array('city_id' => new Expression('GROUP_CONCAT(DISTINCT cci.city_id)')))
                ->where(array('company.company_id ' => $id));
        });


        $row = $rowset->current();
        $region_ids = explode(',', $row['region_id']);
        $country_ids = explode(',', $row['country_id']);
        $city_ids = explode(',', $row['city_id']);
        $row->region_id = $region_ids;
        $row->country_id = $country_ids;
        $row->city_id = $city_ids;
        //$row = (object) $row;
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveCompany(Company $Company, $adapter)
    {
        $sql = new Sql($adapter);
        $id = $Company->company_id;
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $user_type = $userSession->user_type;


        if ($id == 0) {
         
            $data = array(
                'company_name' => $Company->company_name,
                'address' => $Company->address,
                'email' => $Company->email,
                'contact_no' => $Company->contact_no,
                'is_active' => $Company->is_active,
                'created_date' => date('Y-m-d'),
                'created_by' => $user_id
                );
            $Company_query = $this->tableGateway->insert($data);
            $last_inserted_id = $this->tableGateway->lastInsertValue;
            if ($Company_query == true) {
                /*insert regions*/
                foreach ($Company->region_id as $region_id) {
                    $data_user_region = array(
                        'region_id' => $region_id,
                        'company_id' => $last_inserted_id,
                    );
                    $insert = $sql->insert('company_region');
                    $insert->values($data_user_region);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }
                /*insert regions*/
                /*insert countries*/
                foreach ($Company->country_id as $country_id) {
                    $data_user_country = array(
                        'country_id' => $country_id,
                        'company_id' => $last_inserted_id,
                    );
                    $insert = $sql->insert('company_country');
                    $insert->values($data_user_country);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }
                /*insert countries*/
                /*insert cities*/
                foreach ($Company->city_id as $city_id) {
                    $data_user_country = array(
                        'city_id' => $city_id,
                        'company_id' => $last_inserted_id,
                    );
                    $insert = $sql->insert('company_city');
                    $insert->values($data_user_country);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }
                /*insert cities*/
            }

        } 
        else {

            if ($this->getCompanyMaster($id)) {

                    $data = array(
                        /*'company_name' => $Company->company_name,*/
                        'address' => $Company->address,
                        'email' => $Company->email,
                        'contact_no' => $Company->contact_no,
                        'is_active' => $Company->is_active,
                        'modified_date' => date('Y-m-d'),
                        'modified_by' => $user_id
                    );
                    if ($Company->company_id != NULL) {


                        /*delete regions*/
                        $delete_row = $sql->delete('company_region');
                        $delete_row->where(array('company_id' => (int)$id));
                        $statement = $sql->prepareStatementForSqlObject($delete_row);
                        $statement->execute();
                        /*delete countries*/
                        $delete_row = $sql->delete('company_country');
                        $delete_row->where(array('company_id' => (int)$id));
                        $statement = $sql->prepareStatementForSqlObject($delete_row);
                        $statement->execute();
                        /*delete cities*/
                        $delete_row = $sql->delete('company_city');
                        $delete_row->where(array('company_id' => (int)$id));
                        $statement = $sql->prepareStatementForSqlObject($delete_row);
                        $statement->execute();

                        /*insert regions*/
                        foreach ($Company->region_id as $region_id) {
                            $data_user_region = array(
                                'region_id' => $region_id,
                                'company_id' => $id,
                            );
                            $insert = $sql->insert('company_region');
                            $insert->values($data_user_region);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                        /*insert regions*/
                        /*insert countries*/
                        foreach ($Company->country_id as $country_id) {
                            $data_user_country = array(
                                'country_id' => $country_id,
                                'company_id' => $id,
                            );
                            $insert = $sql->insert('company_country');
                            $insert->values($data_user_country);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                        /*insert countries*/
                        /*insert cities*/
                        foreach ($Company->city_id as $city_id) {
                            $data_user_country = array(
                                'city_id' => $city_id,
                                'company_id' => $id,
                            );
                            $insert = $sql->insert('company_city');
                            $insert->values($data_user_country);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                        /*insert cities*/

                    } else {

                    }
                    $this->tableGateway->update($data, array('company_id' => $id));


            } else {
                throw new \Exception('Company id does not exist');
            }

        }
    }

    /*public function deleteCompany($id, $adapter)
    {
        $this->tableGateway->delete(array('milestone_id' => (int)$id, 'status' => _P_));

        $sql = new Sql($adapter);
        $delete = $sql->delete();
        $delete->from('milestone_details');
        $delete->where(array('milestone_id' => (int)$id));
        $statement = $sql->prepareStatementForSqlObject($delete);
        $results = $statement->execute();
    }*/

    public function fetchAll_company($adapter, $paginated = false)
    {
        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            //        $sql = new Sql($adapter);
            $select->from(array('c' => 'company'));
            $select->join(array('cr' => 'company_region'), 'cr.company_id = c.company_id', array('region_id' => new Expression('GROUP_CONCAT(DISTINCT cr.region_id)')));
            $select->join(array('r' => 'region'), 'r.id = cr.region_id', array('region_description' => new Expression('GROUP_CONCAT(DISTINCT r.region_description)')));
            $select->join(array('cco' => 'company_country'), 'cco.company_id = c.company_id ', array('country_id' => new Expression('GROUP_CONCAT(DISTINCT cco.country_id)')));
            $select->join(array('co' => 'country'), 'co.id = cco.country_id ', array('country_name' => new Expression('GROUP_CONCAT(DISTINCT co.country_name)')));
            $select->join(array('cci' => 'company_city'), 'cci.company_id = c.company_id', array('city_id' => new Expression('GROUP_CONCAT(DISTINCT cci.city_id)')));
            $select->join(array('ci' => 'city'), 'ci.id = cci.city_id', array('city_name' => new Expression('GROUP_CONCAT(DISTINCT ci.city_name)')));
            $select->group('c.company_id');
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            // create a new result set based on the User entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Company());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );

//            $statement = $sql->prepareStatementForSqlObject($select);
//            $selectquery = $statement->execute();
//            foreach ($selectquery as $selectquery1) {
//                $date = $selectquery1['create_date'];
//                //echo $selectquery1['create_date'] . '<br>';
//
//                if ((strtotime($date) < strtotime('30 days ago')) && $selectquery1['company_id'] && $selectquery1['user_id']) {
//                    //  echo 'true' . '<br>';
//
//                }
//            }
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

}



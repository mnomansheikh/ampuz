<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/27/2016
 * Time: 5:42 PM
 */

namespace Company\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Company implements InputFilterAwareInterface
{
    public $company_id;
    public $company_name;
    public $address;
    public $email;
    public $region_id;
    public $country_id;
    public $city_id;
    public $contact_no;
    public $is_active;
// public $user_id;
    public $inputFilter;

    protected $dbAdapter;

    public function setDbAdapter($dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    public function exchangeArray($data)
    {
        $this->company_id = (!empty($data['company_id'])) ? $data['company_id'] : null;
        $this->company_name = (!empty($data['company_name'])) ? $data['company_name'] : null;
        $this->address = (!empty($data['address'])) ? $data['address'] : null;
        $this->email = (!empty($data['email'])) ? $data['email'] : null;
        $this->region_id = (!empty($data['region_id'])) ? $data['region_id'] : null;
        $this->country_id = (!empty($data['country_id'])) ? $data['country_id'] : null;
        $this->city_id = (isset($data['city_id'])) ? $data['city_id'] : null;
        $this->contact_no = (isset($data['contact_no'])) ? $data['contact_no'] : null;
        $this->is_active = (isset($data['is_active'])) ? $data['is_active'] : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $inputFilter->add(array(
                'name' => 'company_id',
                'required' => false,
                'filters' => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name' => 'company_name', // add second password field
                /* ... other params ... */
                'validators' => array(
                    array(
                        'name' => '\Zend\Validator\Db\NoRecordExists',
                        'options' => array(
                            'table' => 'company',
                            'field' => 'company_name',
                            'adapter' => $this->dbAdapter,
                            'messages' => array(
                                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'Change Company Name already exist',
                            ),
                        ),
                    ),
                ),
            ));
            $inputFilter->add(array(
                'name' => 'contact_no', // add second password field
                /* ... other params ... */
                'validators' => array(
                    array(
                        'name' => '\Zend\Validator\Digits',
                        'options' => array(
                            'messages' => array(
                                'notDigits' => 'Please enter a valid contact number ',
                            )
                        ),
                    ),
                    array(
                        'name' => 'string_length',
                        'options' => array(
                            'min' => '13',
                            'max' => '20',
                            'messages' => array(
                                'stringLengthTooShort' => 'The input is less than %min% characters long in contact number',
                                'stringLengthTooLong' => 'The input is more than %max% characters long in contact number'
                            )
                        ),
                    ),
                ),
            ));

            $this->inputFilter = $inputFilter;

        }

        return $this->inputFilter;
    }
}
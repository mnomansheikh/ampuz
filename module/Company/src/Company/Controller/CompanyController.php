<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/27/2016
 * Time: 5:30 PM
 */

//module/Login/src/Controller/AuthController.php
namespace Company\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Company\Model\Company;
use Company\Form\CompanyForm;

class CompanyController extends AbstractActionController
{
    protected $form;
    protected $userSession;
    protected $storage;
    protected $authservice;
    // protected $currentUserId;
    protected $CompanyTable;
//    public function getUserProfileTable()
//    {
//        if (!$this->UserProfileTable) {
//            $sm = $this->getServiceLocator();
//            $this->UserProfileTable = $sm->get('Login\Model\UserProfileTable');
//        }
//        return $this->UserProfileTable;
//    }
    public function getCompanyTable()
    {
        if (!$this->CompanyTable) {
            $sm = $this->getServiceLocator();
            $this->CompanyTable = $sm->get('Company\Model\CompanyTable');
        }
        return $this->CompanyTable;
    }


    public function getAuthService()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authservice;
    }


    public function ListAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $form = new CompanyForm($dbAdapter);
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user

            // grab the paginator from the CommunityTable
            $paginator = $this->getCompanyTable()->fetchAll_company($dbAdapter, true);
            // set the current page to what has been passed in query string, or to 1 if none set
            $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
            // set the number of items per page to 10
            $paginator->setItemCountPerPage(10);

            $view = new ViewModel(array(
                'paginator' => $paginator,
                'form' => $form

            ));
            $view->setTemplate('company/list');
            return $view;
        } /*   if ($userSession->user_type == _CHAMPION_) {
                   $form = new EnergyForm();
                   $form->get('submit')->setValue('Add');
                   $request = $this->getRequest();

                   if ($request->isPost()) {
                       $energy = new Energy();
                       $form->setInputFilter($energy->getInputFilter());
                       $form->setData($request->getPost());

                       if ($form->isValid()) {
                           $energy->exchangeArray($form->getData());
                           $this->getEnergyTable()->saveEnergy($energy, null);

                           // Redirect to list of albums
                           return $this->redirect()->toRoute('energy');
                       }
                   }
                   return array('form' => $form, 'energy' => $this->getEnergyTable()->fetchAll($dbAdapter));
               }
               */
        else {
            return $this->redirect()->toRoute('login');
        }


//        return new ViewModel(array(
//            'energy' => $this->getEnergyTable()->fetchAll(),
//
//        ));
        //    $form->setVariable('form', $form);

    }

    public function addAction()
    {

        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
          //  if ($userSession->user_type == _ADMIN_) {
                $form = new CompanyForm($dbAdapter);
                $form->get('submit')->setValue('Save');
                $form->get('cancel')->setValue('Cancel');
                $request = $this->getRequest();

                if ($request->isPost()) {
                    $Company = new Company();
                    $Company->setDbAdapter($dbAdapter);
                    $form->setInputFilter($Company->getInputFilter());
                    $data = array_merge_recursive(
                        $this->getRequest()->getPost()->toArray(),
                        $this->getRequest()->getFiles()->toArray()
                    );

                    $form->setData($request->getPost());
                    $form->setData($data);

                    if ($form->isValid()) {
                        $Company->exchangeArray($form->getData());
                        $this->getCompanyTable()->saveCompany($Company, $dbAdapter);
                        // Redirect to list
                        return $this->redirect()->toRoute('company', array(
                            'action' => 'list'
                        ));
                    }
                }


                //return array('form' => $form, 'event' => $this->getUserProfileTable()->fetchAll($dbAdapter));
                $view = new ViewModel(array(
                    'form' => $form
                ));
                $view->setTemplate('company/add');
                return $view;
           // }
          //  if ($userSession->user_type == _CHAMPION_) {
          //      return $this->redirect()->toRoute('application');
          //  }
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function editAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $id = (int)$this->params()->fromRoute('id', 0);
            try {
                $CompanyTable = $this->getCompanyTable()->getCompany($id, $dbAdapter);
            } catch (\Exception $ex) {
                return $this->redirect()->toRoute('company', array(
                    'action' => 'list'
                ));
            }
            $form = new CompanyForm($dbAdapter);
            $Company = new Company();
            $form->bind($CompanyTable);
            //for session varibles
            $form->get('submit')->setValue('Save');
            $form->get('cancel')->setValue('Cancel');
            $request = $this->getRequest();
            if ($request->isPost()) {
                $Company->setDbAdapter($dbAdapter);
                $form->setInputFilter($Company->getInputFilter());
                $form->setData($request->getPost());
                if ($id > 0) {
                    $form->getInputFilter()->get('company_name')->setRequired(false);
                }
                if ($form->isValid()) {
                    $Company->exchangeArray($form->getData());
                    $this->getCompanyTable()->saveCompany($Company, $dbAdapter);

                    // Redirect to list
                    return $this->redirect()->toRoute('company', array(
                        'action' => 'list'
                    ));
                }
            }
            //  $user = $this->getCurrentUserTable()->fetchAll_event($dbAdapter,$id);
            return array(
                'id' => $id,
                'form' => $form,
                //    'user' => $user,

            );
        } else {
            return $this->redirect()->toRoute('login');
        }

    }

}
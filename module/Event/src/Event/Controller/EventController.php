<?php
namespace Event\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Event\Model\EventTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Event\Model\Event;
use Event\Form\EventForm;
use Zend\Validator\File\Size;
use Zend\Session\Container;

class EventController extends AbstractActionController
{
    protected $eventTable;
    protected $authservice;
    protected $TrainingTable;


    public function getTrainingTable()
    {
        if (!$this->TrainingTable) {
            $sm = $this->getServiceLocator();
            $this->TrainingTable = $sm->get('Training\Model\TrainingTable');
        }
        return $this->TrainingTable;
    }


    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authservice;
    }
    public function getMenu()
    {
        if (!$this->MenuTable) {
            $sm = $this->getServiceLocator();
            $this->MenuTable = $sm->get('Application\Model\MenuTable');
        }
        return $this->MenuTable;
    }
    public function indexAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()){
            $form = new EventForm();
            $form->get('submit')->setValue('Request Indicator');
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $config = $this->getServiceLocator()->get('Config');
            $dsn = $config['db']['dsn'];
            $username = $config['db']['username'];
            $password = $config['db']['password'];
            $controllerClass= __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
           // $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            //$Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            //$Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsIDByGroup($dbAdapter,$moduleNamespace);
            //$frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
            //$record_expiration = $this->getMenu()->RecordExpiration($dbAdapter,$moduleNamespace,$Indicators,$Companies,$Countries,$frequencies);

            $_SESSION['category']   = "community";
            $_SESSION['area']       = "event";

            if($userSession->user_type == _ADMIN_){
                // grab the paginator from the CommunityTable
               // $paginator = $this->getEventTable()->fetchAll_admin($dbAdapter,true);
                $paginator = $this->getMenu()->fetchAll_admin($dbAdapter, true,$moduleNamespace,$dsn,$username,$password,$Indicators);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('event/admin');
                return $view;
            }

            if($userSession->user_type == _CHAMPION_) {

                // grab the paginator from the CommunityTable
                //$paginator = $this->getEventTable()->fetchAll_champion($dbAdapter,true);
                $paginator = $this->getMenu()->fetchAll_champion($dbAdapter, true,$moduleNamespace,$dsn,$username,$password,$Indicators);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form

                ));
                $view->setTemplate('event/index');
                return $view;

                //  return array('form' => $form, 'event' => $this->getEventTable()->fetchAll($dbAdapter));
            }
//              return new ViewModel(array(
//
//              'event' => $this->getEventTable()->fetchAll(),
////
//              ));
        } else {
            return $this->redirect()->toRoute('login');
        }
    }
    public function getEventTable()
    {
        if (!$this->eventTable) {
            $sm = $this->getServiceLocator();
            $this->eventTable = $sm->get('Event\Model\EventTable');
        }
        return $this->eventTable;
    }

    public function addAction()
    {

        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            if ($userSession->user_type == _CHAMPION_) {
                $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
                $controllerClass= __NAMESPACE__;
                $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
                $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
                $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
                $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
                $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter,$moduleNamespace);
                $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
                $DateField = $this->getMenu()->DateField();
                $form = new EventForm($Companies,$Regions,$Countries,$Indicators,$frequencies,$DateField);
                $form->get('submit')->setValue('Save');
                $form->get('cancel')->setValue('Cancel');
                $request = $this->getRequest();

                if ($request->isPost()) {
                    $event = new Event();
                    $created_date =$this->getRequest()->getPost('create_date');
                    $indicator_id =$this->getRequest()->getPost('type');
                    $company_id =$this->getRequest()->getPost('company_id');
                    $country_id =$this->getRequest()->getPost('country_id');
                    $frequency_id =$this->getRequest()->getPost('frequency_id');
                    $date_validation = $this->getMenu()->DateFieldValidation($dbAdapter,$moduleNamespace,$created_date,$indicator_id,$company_id,$country_id,$frequency_id);
                    $form->setInputFilter($event->getInputFilter($date_validation));
                    $form->setData($request->getPost());

//          File upload
                    $nonFile = $request->getPost()->toArray();
                    $File = $this->params()->fromFiles('fileupload');
                    $data = array_merge(
                        $nonFile, //POST
                        array('fileupload' => $File['name']) //FILE...
                    );

                    $data = array_merge_recursive(
                        $this->getRequest()->getPost()->toArray(),
                        $this->getRequest()->getFiles()->toArray()
                    );

                    $form->setData($request->getPost());
                    $form->setData($data);

                    if ($form->isValid()) {
                        $event->exchangeArray($form->getData());
                       
                        // Redirect to list of albums
                        $expire_date = $this->getMenu()->FrequencyChecker($frequency_id,$created_date);
                        $event->expire_date =$expire_date;
                        $this->getEventTable()->saveEvent($event,$dbAdapter);
                        return $this->redirect()->toRoute('event');
                    }
                }


                //return array('form' => $form, 'event' => $this->getEventTable()->fetchAll($dbAdapter));
                $view = new ViewModel(array(
                    'form' => $form, 'event' => $this->getEventTable()->fetchAll($dbAdapter)
                ));
                $view->setTemplate('event/add');
                return $view;
            }
            if ($userSession->user_type == _ADMIN_) {
                return $this->redirect()->toRoute('event');
            }
        }
        else {
            return $this->redirect()->toRoute('login');
        }
    }
    public function editAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()){
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $id = (int) $this->params()->fromRoute('id', 0);
            try {
                $event = $this->getEventTable()->getEvent($id);
            }
            catch (\Exception $ex) {
                return $this->redirect()->toRoute('event', array(
                    'action' => 'index'
                ));
            }
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $controllerClass= __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter,$moduleNamespace);
            $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
            $DateField = $this->getMenu()->DateField();

            $form = new EventForm($Companies,$Regions,$Countries,$Indicators,$frequencies,$DateField);
            $form->bind($event);
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $form->get('cancel')->setValue('Cancel');
            if($userSession->user_type == _ADMIN_) {
                $form->get('submit')->setAttribute('value', 'Update & Approval');
            }
            if($userSession->user_type == _CHAMPION_) {
                $form->get('submit')->setAttribute('value', 'Update');
            }
            $request = $this->getRequest();
            if ($request->isPost()) {
                $form->setInputFilter($event->getInputFilter(null));
                $form->setData($request->getPost());

                if ($form->isValid()) {
                    $this->getEventTable()->saveEvent($event,$dbAdapter);

                    // Redirect to list of Community
                    return $this->redirect()->toRoute('event');
                }
            }
            $event_details = $this->getEventTable()->fetchAll_event_details($dbAdapter,$id);
            $event = $this->getEventTable()->fetchAll_event($dbAdapter,$id);
            return array(
                'id' => $id,
                'form' => $form,
                'event_details' => $event_details,
                'event' => $event,

            );
        } else {
            return $this->redirect()->toRoute('login');
        }

    }


    public function deleteAction()
    {
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('event');
        }
        if($id){
            $this->getEventTable()->deleteEvent($id, $dbAdapter);
            return $this->redirect()->toRoute('event');
        }
    }


    public function downloadAction()
    {

        $str = $this->params()->fromRoute('str', 0);
        $id = $this->params()->fromRoute('id', 0);
        $str = trim($str);


// first, get MIME information from the file
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime =  finfo_file($finfo, $str);
        finfo_close($finfo);

// send header information to browser
        header('Content-Type: '.$mime);
        header('Content-Disposition: attachment;  filename="'.$str.'"');
        header('Content-Length: ' . filesize($str));
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

//stream file
        ob_get_clean();
        echo file_get_contents($str);
        ob_end_flush();


    }

    public function Graph1Action(){
        $fromyear ='';
        $toyear ='';
        $event_desc='';
        $indicator ='';
        $company_id ='';
        if($_REQUEST){
            extract($_REQUEST);
            $fromyear;
            $toyear;
            $indicator;
            $company_id;
        }
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()){
            //for session varibles
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            if($userSession->user_type == _ADMIN_){
                // grab the paginator from the CommunityTable
                $paginator = $this->getEventTable()->fetchAll_admin($dbAdapter,true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);
                $GData = $this->getEventTable()->fetchGraphData($dbAdapter,$fromyear,$toyear,$company_id,$event_desc);

                //------------------------------ BAR Chart --------------------------------------
                // grab the paginator from the CommunityTable
                $principle = $this->getTrainingTable()->fetchAll_principle($dbAdapter,true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $principle->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $principle->setItemCountPerPage(10);

                //------------------------------ PIE Chart --------------------------------------
                // grab the paginator from the CommunityTable
                $principle_pie = $this->getTrainingTable()->fetchAll_principle($dbAdapter,true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $principle_pie->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $principle_pie->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'GData' => $GData,
                    'indicator' => $indicator,
                    'paginator' => $paginator,
                    'event_desc' => $event_desc,
                    'principle' => $principle,
                    'principle_pie' => $principle_pie,
                ));

                $view->setTemplate('event/analytic');
                return $view;
            }
            if($userSession->user_type == _CHAMPION_) {
                return $this->redirect()->toRoute('application');
            }
        } else {
            return $this->redirect()->toRoute('login');
        }

    }
}
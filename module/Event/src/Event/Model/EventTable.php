<?php
namespace Event\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Session\Container;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
class EventTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($adapter)
{
    //for session varibles
    $userSession = new Container('user');
    //  echo 'Logged in as ' . $userSession->username;
    $user_id=  $userSession->user_id;
    //$resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
    // create a new Select object for the table community
    $sql = new Sql($adapter);
    $select = $sql->select();
    $select->from('event');
    $select->where(array('event.user_id' => $user_id,'event.status'=>_P_));
    $select->join('user_profile', 'user_profile.user_id = event.user_id','user_name');
    $select->join('company', 'company.company_id = event.company_id','company_name');
    $select->join('indicator', 'indicator.id = event.type','name');
 /*   $select->join('department', 'department.department_id = event.department_id','department_name');*/
    $select->join('frequency', 'frequency.frequency_id = event.frequency_id','frequency_name');
    $select->order('event_id DESC');

    $statement = $sql->prepareStatementForSqlObject($select);
    $results = $statement->execute();
    return $results;
}

    public function fetchAll_event($adapter,$id)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;
        //$resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        // create a new Select object for the table community
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('event');
        $select->where(array('event_id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function fetchAll_event_details($adapter,$id)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;
        //$resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        // create a new Select object for the table community
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('event_details');
        $select->where(array('event_id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    public function fetchAll_admin($adapter,$paginated=false)
        {
        if ($paginated) {
            // create a new Select object for the table community
            $userSession = new Container('user');

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('event');
            $select->join('user_profile', 'user_profile.user_id = event.user_id',array('user_name','user_email'));
            $select->join('company', 'company.company_id = event.company_id','company_name');
            $select->join('country', 'country.id = event.country_id','country_name');
            $select->join('region', 'region.id = event.region_id','region_description');
            $select->join('indicator', 'indicator.id = event.type','name');
          /*  $select->join('department', 'department.department_id = event.department_id','department_name');*/
            $select->join('frequency', 'frequency.frequency_id = event.frequency_id','frequency_name');
            $select->where(array('event.company_id'=>$userSession->user_companies,'event.country_id'=>$userSession->user_countries,'event.region_id'=>$userSession->user_regions));
            $select->order('event_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Event());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );

 $statement = $sql->prepareStatementForSqlObject($select);
            $selectquery = $statement->execute();
            foreach ($selectquery as $selectquery1) {
                $date = $selectquery1['create_date'];
                //echo $selectquery1['create_date'] . '<br>';

                if ((strtotime($date) < strtotime('30 days ago')) && $selectquery1['company_id'] && $selectquery1['user_id']) {
                  //  echo 'true' . '<br>';

                }
            }
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function fetchAll_champion($adapter,$paginated=false)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
           // $select = $sql->where(array('user_id' => $user_id));
            $select->from('event');
            $select->where(array('event.user_id' => $user_id));
            $select->join('user_profile', 'user_profile.user_id = event.user_id','user_name');
            $select->join('company', 'company.company_id = event.company_id','company_name');
            $select->join('indicator', 'indicator.id = event.type','name');
           /* $select->join('department', 'department.department_id = event.department_id','department_name');*/
            $select->join('frequency', 'frequency.frequency_id = event.frequency_id','frequency_name');
            $select->order('event_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Event());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function getEvent($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('event_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveEvent(Event $event,$adapter)
    {

        $sql = new Sql($adapter);
        $id = $event->event_id;
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $user_type = $userSession->user_type;
        $oneYearOn = date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day"));
        //event detail
        $id_trim=  ltrim($event->emp_id, ',');
        $name_trim=    ltrim($event->name, ',');
        $design_trim=   ltrim($event->designation, ',');
        $date_trim=   ltrim($event->date, ',');
        $ids = explode(',', $id_trim);
        $names = explode(',',$name_trim);
        $designations = explode(',', $design_trim);
        $dates = explode(',', $date_trim);
        if($id == 0) {


            $data = array(
                'type' => $event->type,
                'value' => $event->value,
                'company_id' => $event->company_id,
                'region_id'        => $event->region_id,
                'country_id'        => $event->country_id,
                'department_id' => $event->department_id,
                'frequency_id' => $event->frequency_id,
                'file_name' => $event->filename,
                'org_name' => $event->org_name,
                'create_date' => date("Y-m-d"),
                'status' => _P_,
                'user_id'  => $user_id,
                'create_date'       => $event->create_date,
                'expiry_date'       => $event->expire_date


            );
            $event_query= $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
            if($event_query == true){
                foreach ($names as $index => $name) {
                $columns = array(
                    'employee_id',
                    'employee_name',
                    'event_atten_designation',
                    'create_date',
                    'event_id',
                );
                $data1 = array(
                        'employee_id' =>  $ids[$index],
                        'employee_name' =>  $names[$index],
                        'event_atten_designation' =>  $designations[$index],
                        'create_date' =>  $dates[$index],
                        'event_id' => $id,

                    );

                $insert= $sql->insert('event_details');
              //  $insert->columns($columns);
                $insert->values($data1);
                $statement = $sql->prepareStatementForSqlObject($insert);
                $results = $statement->execute();
                }

            }
        }


        else {
            if ($this->getEvent($id)) {
                if($user_type == _ADMIN_) {

                    $data = array(
                       /* 'company_id' => $event->company_id,
                        'region_id'        => $event->region_id,
                        'country_id'        => $event->country_id,
                        'department_id' => $event->department_id,
                        'file_name' => $event->filename,
                        'org_name' => $event->org_name,
                        'frequency_id' => $event->frequency_id,
                        'type' => $event->type,*/
                        'value' => $event->value,
                        'status'  => _A_,

                    );
                    /*if( $event->filename != NULL) {
                        $data = array(
                            'file_name' => $event->filename,
                            'org_name' => $event->org_name,
                        );
                    }*/
                  // if file re uploaded
                    if($event->emp_id !=NULL){
                       $delete_row= $sql->delete('event_details');
                        $delete_row->where(array('event_id' => (int) $id));
                        $statement = $sql->prepareStatementForSqlObject($delete_row);
                        $results = $statement->execute();
                        foreach ($names as $index => $name) {
                            $columns = array(
                                'employee_id',
                                'employee_name',
                                'event_atten_designation',
                                'create_date',
                                'event_id',
                            );
                            $data1 = array(
                                'employee_id' =>  $ids[$index],
                                'employee_name' =>  $names[$index],
                                'event_atten_designation' =>  $designations[$index],
                                'create_date' =>  $dates[$index],
                                'event_id' => $id,

                            );

                            $insert= $sql->insert('event_details');
                            //  $insert->columns($columns);
                            $insert->values($data1);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }

                    }
                    else{

                    }
                }
                if($user_type == _CHAMPION_) {
                    $data = array(

                     /*   'company_id' => $event->company_id,
                        'region_id'        => $event->region_id,
                        'country_id'        => $event->country_id,
                        'department_id' => $event->department_id,
                        'file_name' => $event->filename,
                        'org_name' => $event->org_name,
                        'frequency_id' => $event->frequency_id,*/
                       /* 'type' => $event->type,*/
                        'value' => $event->value,
                        'status' => _P_,
                    );
                    // if file re uploaded
                    if($event->emp_id !=NULL){
                        $delete_row= $sql->delete('event_details');
                        $delete_row->where(array('event_id' => (int) $id));
                        $statement = $sql->prepareStatementForSqlObject($delete_row);
                        $results = $statement->execute();
                        foreach ($names as $index => $name) {
                            $columns = array(
                                'employee_id',
                                'employee_name',
                                'event_atten_designation',
                                'create_date',
                                'event_id',
                            );
                            $data1 = array(
                                'employee_id' =>  $ids[$index],
                                'employee_name' =>  $names[$index],
                                'event_atten_designation' =>  $designations[$index],
                                'create_date' =>  $dates[$index],
                                'event_id' => $id,
                            );
                            $insert= $sql->insert('event_details');
                            //  $insert->columns($columns);
                            $insert->values($data1);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                    }
                }
                $this->tableGateway->update($data, array('event_id' => $id));
            }
            else {
                throw new \Exception('Event id does not exist');
            }

        }
    }

    public function deleteEvent($id, $adapter )
    {
        $this->tableGateway->delete(array('event_id' => (int) $id,'status' => _P_));

        $sql = new Sql($adapter);
        $delete = $sql->delete();
        $delete->from('event_details');
        $delete->where (array('event_id'  => (int) $id ));
        $statement = $sql->prepareStatementForSqlObject($delete);
        $results = $statement->execute();
    }

 public function fetchGraphData($adapter,$fromyear=NULL,$toyear=NULL,$company_id =NULL,$event_desc =NULL)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $expression = new Expression("YEAR(event.create_date)");
        $select->columns(array( new Expression('COUNT(*) AS Total'),new Expression("MONTH(event.create_date) AS MONTH"),'event_id'));
        $select->from('event_details');
        $select->join('event', 'event_details.event_id = event.event_id','type');
        $select->group(array(new Expression("MONTH(event.create_date)"),'event_details.event_id'));
        $select->order(array(new Expression("MONTH(event.create_date)"),'event_details.event_id'));
        if($fromyear== NULL){
            $fromyear = date('Y');
            $toyear = date('Y');
            $year1 =(int)$fromyear;
            $year2 =(int)$toyear;
        }
        else{
            $year1 = (int)$fromyear;
            $year2 = (int)$toyear;
        }
        $yearexp = (new Expression("MONTH(event.create_date)"));
        // $subSelect = new Select('community_partnership');
        $select->where(array('event.status'=>_A_));
        if($company_id != NULL ){
            $company_id = (int)$company_id;

            $select->where(array('event.status'=>_A_,'event.company_id'=>$company_id));
        }
        if($event_desc != NULL ){

            $select->where(array('event.status'=>_A_,'event.type'=>$event_desc));
        }
        else{

            $select->where(array('event.status'=>_A_));
        }
        $select->where->between($yearexp, $year1, $year2);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
}
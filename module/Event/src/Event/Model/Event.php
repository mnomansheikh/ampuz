<?php
namespace Event\Model;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Event implements InputFilterAwareInterface
{
    public $event_id;
    public $type;
    public $value;
    public $user_name;
    public $event_atten_designation;
    public $user_id;
    public $company_id;
    public $region_id;
    public $country_id;
    public $department_id;
    public $frequency_id;
    public $filename;
    public $org_name;
    public $emp_id;
    public $name;
    public $id;
   // public $event;
    public $event_name;
    public $designation;
    public $date;
    public $inputFilter;
    public $create_date;
    //protected $inputFilter;                       // <-- Add this variable

    public function exchangeArray($data)
    {
        $this->event_id             = (!empty($data['event_id'])) ? $data['event_id'] : null;
        $this->value                = (!empty($data['value'])) ? $data['value'] : null;
        $this->type                 = (!empty($data['type'])) ? $data['type'] : null;
        $this->user_name            = (!empty($data['user_name'])) ? $data['user_name'] : null;
        $this->event_atten_designation            = (!empty($data['event_atten_designation'])) ? $data['event_atten_designation'] : null;
        $this->user_id              = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->company_id           = (!empty($data['company_id'])) ? $data['company_id'] : null;
        $this->region_id           = (!empty($data['region_id'])) ? $data['region_id'] : null;
        $this->country_id           = (!empty($data['country_id'])) ? $data['country_id'] : null;
        $this->department_id        = (!empty($data['department_id'])) ? $data['department_id'] : null;
        $this->frequency_id         = (!empty($data['frequency_id'])) ? $data['frequency_id'] : null;
        $this->filename             = (isset($data['file_name']))  ? $data['file_name']     : null;
        $this->org_name             = (isset($data['org_name']))  ? $data['org_name']     : null;
        $this->create_date          = (isset($data['create_date']))  ? $data['create_date']     : null;
        $this->emp_id               = (isset($data['emp_id']))  ? $data['emp_id']     : null;
        $this->name                 = (isset($data['name']))  ? $data['name']     : null;
        $this->event_name           = (isset($data['event_name']))  ? $data['event_name']     : null;
        $this->designation          = (isset($data['designation']))  ? $data['designation']     : null;
        $this->date                 = (isset($data['date']))  ? $data['date']     : null;
        $this->id                   = (!empty($data['id'])) ? $data['id'] : null;

    }
    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter($date_validation=null)
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            if($date_validation != null){
                $inputFilter->add(
                    $date_validation
                );
            }
            $inputFilter->add(array(
                'name'     => 'event_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'company_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
              /*  'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),*/
            ));
            $inputFilter->add(array(
                'name'     => 'frequency_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
              /*  'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),*/
            ));


            $inputFilter->add(array(
                'name'     => 'type',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'value',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
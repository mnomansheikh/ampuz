<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/30/2016
 * Time: 11:51 AM
 */

namespace Milestone\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Session\Container;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
class MilestoneTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($adapter)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;
        //$resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        // create a new Select object for the table community
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('milestone');
        $select->where(array('milestone.user_id' => $user_id,'milestone.status'=>_P_));
        $select->join('user_profile', 'user_profile.user_id = milestone.user_id','user_name');
        $select->join('company', 'company.company_id = milestone.company_id','company_name');
        $select->join('department', 'department.department_id = user_profile.department_id','department_name');
        $select->join('frequency', 'frequency.frequency_id = milestone.frequency_id','frequency_name');
        $select->order('milestone_id DESC');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function fetchAll_milestone($adapter,$id)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;
        //$resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        // create a new Select object for the table community
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('milestone');
        $select->where(array('milestone_id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function fetchAll_milestone_details($adapter,$id)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;
        //$resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        // create a new Select object for the table community
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('milestone_details');
        $select->where(array('milestone_id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    public function fetchAll_admin($adapter,$paginated=false)
    {
        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('milestone');
            $select->join('company', 'company.company_id = milestone.company_id','company_name');
            $select->join('frequency', 'frequency.frequency_id = milestone.milestone_duration_type_id','frequency_name');
           // $select->join('indicator', 'indicator.id = milestone.indicator_id','name','left');
            $select->join('indicator', new Expression('FIND_IN_SET(indicator.id, milestone.indicator_id)'),array('IndicatorName'=> new Expression('GROUP_CONCAT(indicator.name ORDER BY indicator.id)')),'left');
           // $select->join('gender', 'gender.gender_id = milestone.gender_id','gender_description');
            $select->join('sub_menu', 'sub_menu.id = milestone.sub_menu_id','label');
            $select->join('milestone_nature', 'milestone_nature.id = milestone.milestone_nature_id', array('nature_description' => 'description'));
            $select->join('milestone_position', 'milestone_position.id = milestone.milestone_position_id',array('position_description' => 'description'));
            $select->join('milestone_slot', 'milestone_slot.milestone_duration_type_id = milestone.milestone_duration_type_id','milestone_duration_type');
            $select->join('user_profile', 'user_profile.user_id = milestone.created_by','user_name');
            $select->group('milestone.milestone_id');
            $select->order('milestone.milestone_id DESC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Milestone());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );

//            $statement = $sql->prepareStatementForSqlObject($select);
//            $selectquery = $statement->execute();
//            foreach ($selectquery as $selectquery1) {
//                $date = $selectquery1['create_date'];
//                //echo $selectquery1['create_date'] . '<br>';
//
//                if ((strtotime($date) < strtotime('30 days ago')) && $selectquery1['company_id'] && $selectquery1['user_id']) {
//                    //  echo 'true' . '<br>';
//
//                }
//            }
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function fetchAll_champion($adapter,$paginated=false)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            // $select = $sql->where(array('user_id' => $user_id));
            $select->from('milestone');
            $select->where(array('milestone.user_id' => $user_id));
            $select->join('user_profile', 'user_profile.user_id = milestone.user_id','user_name');
            $select->join('company', 'company.company_id = milestone.company_id','company_name');
            $select->join('department', 'department.department_id = user_profile.department_id','department_name');
            $select->join('frequency', 'frequency.frequency_id = milestone.frequency_id','frequency_name');
            $select->order('milestone_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Milestone());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function getMilestone($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('milestone_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveMilestone(Milestone $milestone,$adapter)
    {
        $sql = new Sql($adapter);
        $id = $milestone->milestone_id;
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $user_type = $userSession->user_type;
        /*indicators*/
        $indicators = implode(',',$milestone->indicator_id);
        /*indicators*/
        if($id == 0) {
            $data = array(
                'milestone_description' => $milestone->description,
                'short_description' => $milestone->short_description,
                'company_id' => $milestone->company_id,
                'region_id'        => $milestone->region_id,
                'country_id'        => $milestone->country_id,
                'milestone_duration_type_id' => $milestone->frequency_id,
                'gender_id' => $milestone->gender_id,
                'sub_menu_id' => $milestone->group_id,
                'indicator_id' => $indicators,
                'milestone_nature_id' => $milestone->nature_id,
                'milestone_position_id' => $milestone->position_id,
                'target_value' => $milestone->tar_value,
                'is_active' => $milestone->status,
                'created_by'  => $user_id,
                'milestone_create_date'  => $milestone->start_date,
                'milestone_end_date'  => $milestone->end_date,
                'calculation_unit'  => $milestone->cal_unit,
                'percentage'  => $milestone->percent,
                'last_update_on'  => date('Y-m-d')

            );
            $milestone_query= $this->tableGateway->insert($data);
           // $id = $this->tableGateway->lastInsertValue;
           /* if($milestone_query == true){
                foreach ($names as $index => $name) {
                    $columns = array(
                        'employee_id',
                        'employee_name',
                        'milestone_atten_designation',
                        'create_date',
                        'milestone_id',
                    );
                    $data1 = array(
                        'employee_id' =>  $ids[$index],
                        'employee_name' =>  $names[$index],
                        'milestone_atten_designation' =>  $designations[$index],
                        'create_date' =>  $dates[$index],
                        'milestone_id' => $id,

                    );

                    $insert= $sql->insert('milestone_details');
                    //  $insert->columns($columns);
                    $insert->values($data1);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }

            }*/
        }
//        else {
//            if ($this->getMilestone($id)) {
//                if($user_type == _ADMIN_) {
//                    $data = array(
//                        'milestone_description' => $milestone->milestone_description,
//                        'company_id' => $milestone->short_description,
//                        'department_id' => $milestone->department_id,
//                        'file_name' => $milestone->filename,
//                        'org_name' => $milestone->org_name,
//                        'frequency_id' => $milestone->frequency_id,
//                        'status'  => _A_,
//
//                    );
//                    /*if( $milestone->filename != NULL) {
//                        $data = array(
//                            'file_name' => $milestone->filename,
//                            'org_name' => $milestone->org_name,
//                        );
//                    }*/
//                    // if file re uploaded
//                    if($milestone->emp_id !=NULL){
//                        $delete_row= $sql->delete('milestone_details');
//                        $delete_row->where(array('milestone_id' => (int) $id));
//                        $statement = $sql->prepareStatementForSqlObject($delete_row);
//                        $results = $statement->execute();
//                        foreach ($names as $index => $name) {
//                            $columns = array(
//                                'employee_id',
//                                'employee_name',
//                                'milestone_atten_designation',
//                                'create_date',
//                                'milestone_id',
//                            );
//                            $data1 = array(
//                                'employee_id' =>  $ids[$index],
//                                'employee_name' =>  $names[$index],
//                                'milestone_atten_designation' =>  $designations[$index],
//                                'create_date' =>  $dates[$index],
//                                'milestone_id' => $id,
//
//                            );
//
//                            $insert= $sql->insert('milestone_details');
//                            //  $insert->columns($columns);
//                            $insert->values($data1);
//                            $statement = $sql->prepareStatementForSqlObject($insert);
//                            $results = $statement->execute();
//                        }
//
//                    }
//                    else{
//
//                    }
//                }
//                $this->tableGateway->update($data, array('milestone_id' => $id));
//            }
//            else {
//                throw new \Exception('Milestone id does not exist');
//            }
//
//        }
    }

    public function deleteMilestone($id, $adapter )
{
    $this->tableGateway->delete(array('milestone_id' => (int) $id,'status' => _P_));

    $sql = new Sql($adapter);
    $delete = $sql->delete();
    $delete->from('milestone_details');
    $delete->where (array('milestone_id'  => (int) $id ));
    $statement = $sql->prepareStatementForSqlObject($delete);
    $results = $statement->execute();
}
    public function statusMilestone($id,$status)
    {
        $data = array(
            'is_active' => $status,
        );
        $this->tableGateway->update($data, array('milestone_id' => $id));
    }

    public function fetchGraphData($adapter,$milestone_id=NULL)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('milestone');
        $select->join('milestone_nature', 'milestone_nature.id = milestone.milestone_nature_id', array('nature_description' => 'description'));
        $select->join('frequency', 'frequency.frequency_id = milestone.milestone_duration_type_id','frequency_name');
        $select->join('milestone_position', 'milestone_position.id = milestone.milestone_position_id',array('position_description' => 'description'));
        $select->join('indicator', new Expression('FIND_IN_SET(indicator.id, milestone.indicator_id)'),array('sub_menu_id'=>'sub_menu_id','indicator_name'=> new Expression('GROUP_CONCAT(indicator.name ORDER BY indicator.id)')),'left');
      // $select->join('indicator', 'indicator.id = milestone.indicator_id',array('sub_menu_id'=>'sub_menu_id','indicator_name' => 'name'),'left');
        $select->join('sub_menu', 'sub_menu.id = milestone.sub_menu_id','route');
       // $select->join('menu', 'menu.id = sub_menu.menu_id');
        $select->where(array('milestone.milestone_id' => $milestone_id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $milestone_results = $statement->execute();
        $Gdata_arr = array();
        foreach ($milestone_results as $milestone_result) {
            $position = $milestone_result['position_description'];
            $nature = $milestone_result['nature_description'];
            $old_value = $milestone_result['old_value'];
            $target_value = $milestone_result['target_value'];
            $indicator = $milestone_result['indicator_name'];
            $start_date = $milestone_result['milestone_create_date'];
            $end_date = $milestone_result['milestone_end_date'];
            $frequency = $milestone_result['frequency_name'];
            $percentage = $milestone_result['percentage'];
            $Gdata_arr['position'] = $position;
            $Gdata_arr['nature'] = $nature;
            $Gdata_arr['old_value'] = $old_value;
            $Gdata_arr['target_value'] = $target_value;
            $Gdata_arr['percentage'] = $percentage;
            $Gdata_arr['indicator'] = $indicator;
            $Gdata_arr['frequency'] = $frequency;
            $Gdata_arr['start_date'] = $start_date;
            $Gdata_arr['end_date'] = $end_date;
            $indicator_ids = $milestone_result['indicator_id'];
            $indicator_ids_array = explode(",", $indicator_ids);

            $table = $milestone_result['route'];
            if ($table != NULL) {
                $Gdata_arr['group'] = ucfirst($table);
                $select = $sql->select();
               // if ($indicator == NULL) {
                    $select = $select->columns(array('*', new Expression('SUM(value) AS total_value')));
               // }
                $select->from($table);
                $select->where(array('company_id' => $milestone_result['company_id'],'region_id' => $milestone_result['region_id'],'country_id' => $milestone_result['country_id'], 'frequency_id' => $milestone_result['milestone_duration_type_id'], 'status' => _A_));
                if ($indicator != NULL) {
                    $select->where(array('type' => $indicator_ids_array));
                }

                $select->where->between('create_date', $milestone_result['milestone_create_date'], $milestone_result['milestone_end_date']);
              //  if ($indicator == NULL) {
                    $select->group(new Expression('MONTH(create_date)'));
               // }
                $statement = $sql->prepareStatementForSqlObject($select);
                $table_results = $statement->execute();
                $itr = 0;
                foreach ($table_results as $table_result) {
                        $Gdata_arr['actual_result'][$itr]['value'] = $table_result['total_value'];

                    $Gdata_arr['actual_result'][$itr]['date'] = $table_result['create_date'];
                    $itr++;
                }
            }
        }

        return $Gdata_arr;
    }
}
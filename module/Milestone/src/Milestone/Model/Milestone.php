<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/30/2016
 * Time: 11:50 AM
 */

namespace Milestone\Model;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Milestone implements InputFilterAwareInterface
{
    public $milestone_id;
    public $company_id;
    public $region_id;
    public $country_id;
    public $description;
    public $short_description;
    public $frequency_id;
    public $group_id;
    public $indicator_id;
    public $start_date;
    public $end_date;
    public $gender_id;
    public $position_id;
    public $nature_id;
    public $tar_value;
    public $cal_unit;
    public $status;
    public $percent;
   // public $user_id;
    public $inputFilter;


    public function exchangeArray($data)
    {
        $this->region_id           = (!empty($data['region_id'])) ? $data['region_id'] : null;
        $this->country_id           = (!empty($data['country_id'])) ? $data['country_id'] : null;
        $this->milestone_id             = (!empty($data['milestone_id'])) ? $data['milestone_id'] : null;
        $this->cal_unit             = (!empty($data['cal_unit'])) ? $data['cal_unit'] : null;
        $this->description    = (!empty($data['description'])) ? $data['description'] : null;
        $this->short_description            = (!empty($data['short_description'])) ? $data['short_description'] : null;
        $this->frequency_id            = (!empty($data['frequency_id'])) ? $data['frequency_id'] : null;
        //$this->user_id              = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->company_id           = (!empty($data['company_id'])) ? $data['company_id'] : null;
        $this->group_id      = (!empty($data['group_id'])) ? $data['group_id'] : null;
        $this->indicator_id        = (!empty($data['indicator_id'])) ? $data['indicator_id'] : null;
        $this->position_id         = (!empty($data['position_id'])) ? $data['position_id'] : null;
        $this->start_date             = (isset($data['start_date']))  ? $data['start_date']     : null;
        $this->end_date             = (isset($data['end_date']))  ? $data['end_date']     : null;
        $this->gender_id          = (isset($data['gender_id']))  ? $data['gender_id']     : null;
        $this->nature_id               = (isset($data['nature_id']))  ? $data['nature_id']     : null;
        $this->tar_value                 = (isset($data['tar_value']))  ? $data['tar_value']     : null;
        $this->status           = (isset($data['status']))  ? $data['status']     : null;
        $this->percent           = (isset($data['percent']))  ? $data['percent']     : null;

    }
    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $milestone= new Milestone();

            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $inputFilter->add(array(
                'name'     => 'milestone_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
                /*  'validators' => array(
                     array(
                         'name'    => 'StringLength',
                         'options' => array(
                             'encoding' => 'UTF-8',
                             'min'      => 1,
                             'max'      => 100,
                         ),
                     ),
                 ),*/
            ));
            $inputFilter->add(array(
                'name'     => 'tar_value',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Between',
                        'options' => array(
                            'min' => 1,
                            'max' => 100000000,

                        ),
                    ),
                )
            ));
            $inputFilter->add(array(
                'name'     => 'percent',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Between',
                        'options' => array(
                            'min' => 1,
                            'max' => 100,

                        ),
                    ),
                )
            ));
            $inputFilter->add(array(
                'name'     => 'description',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                /*  'validators' => array(
                      array(
                          'name'    => 'StringLength',
                          'options' => array(
                              'encoding' => 'UTF-8',
                              'min'      => 1,
                              'max'      => 100,
                          ),
                      ),
                  ),*/
            ));
            $inputFilter->add(array(
                'name'     => 'short_description',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                /*  'validators' => array(
                      array(
                          'name'    => 'StringLength',
                          'options' => array(
                              'encoding' => 'UTF-8',
                              'min'      => 1,
                              'max'      => 100,
                          ),
                      ),
                  ),*/
            ));
            $inputFilter->add(array(
                'name'     => 'frequency_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),

            ));
            $inputFilter->add(array(
                'name'     => 'company_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));
            $inputFilter->add(array(
                'name'     => 'indicator_id',
                'required' => false,
              /*  'filters'  => array(
                    array('name' => 'Int'),
                ),*/
            ));
           $inputFilter->add(array(
                'name'     => 'group_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));
            $inputFilter->add(array(
                'name'     => 'gender_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));
            $inputFilter->add(array(
                'name'     => 'nature_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));
            $inputFilter->add(array(
                'name'     => 'cal_unit',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));
            $inputFilter->add(array(
                'name'     => 'status',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));
            $inputFilter->add(array(
                'name'     => 'start_date',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                /*  'validators' => array(
                      array(
                          'name'    => 'StringLength',
                          'options' => array(
                              'encoding' => 'UTF-8',
                              'min'      => 1,
                              'max'      => 100,
                          ),
                      ),
                  ),*/
            ));
            $inputFilter->add(array(
                'name'     => 'end_date',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                /*  'validators' => array(
                      array(
                          'name'    => 'StringLength',
                          'options' => array(
                              'encoding' => 'UTF-8',
                              'min'      => 1,
                              'max'      => 100,
                          ),
                      ),
                  ),*/
            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
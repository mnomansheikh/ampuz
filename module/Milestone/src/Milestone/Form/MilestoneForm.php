<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/30/2016
 * Time: 11:47 AM
 */

namespace Milestone\Form;

use Zend\Form\Form;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
class MilestoneForm extends Form
{

    public function __construct(AdapterInterface $dbAdapter,$Companies = null,$Regions=null,$Countries= null)
    {
        // we want to ignore the name passed
        $this->adapter =$dbAdapter;
        parent::__construct('milestone');
        $sql = new Sql($dbAdapter);

        $select = $sql->select();
        $select->from('frequency');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->order('frequency_id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $frequencies = $statement->execute();
        $frequencies_arr =array();
        foreach ($frequencies as $frequency) {
            $frequencies_arr[$frequency['frequency_id']] = $frequency['frequency_name'];
        }
        $select = $sql->select();
        $select->from('sub_menu');
         $select->join('indicator', 'sub_menu.id = indicator.sub_menu_id','name');
        $select->order('id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $groups = $statement->execute();
        $groups_arr =array(''=>'');
        foreach ($groups as $group) {
            $groups_arr[$group['id']] = $group['label'];
        }
        $select = $sql->select();
        $select->from('sub_menu');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->order('id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $sub_menus = $statement->execute();
        //$indicators_arr =array(''=>'');
        $indicators_arr =array();
       foreach ($sub_menus as $sub_menu) {
           $select = $sql->select();
           $select->from('indicator');
           $select->where(array('sub_menu_id' => $sub_menu['id']));
           // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
           $select->order('id asc');
           $statement = $sql->prepareStatementForSqlObject($select);
           $indicators = $statement->execute();
           foreach ($indicators as $indicator) {
               $indicators_arr[$sub_menu['label']]['label']= $sub_menu['label'];
               $indicators_arr[$sub_menu['label']]['options'][$indicator['id']] = $indicator['name'];
           }
       }

        $select = $sql->select();
        $select->from('gender');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->order('gender_id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $genders = $statement->execute();
        $genders_arr =array();
        foreach ($genders as $gender) {
            $genders_arr[$gender['gender_id']] = $gender['gender_description'];
        }

        $select = $sql->select();
        $select->from('milestone_position');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->order('id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $positions = $statement->execute();
        $positions_arr =array();
        foreach ($positions as $position) {
            $positions_arr[$position['id']] = $position['description'];
        }

        $select = $sql->select();
        $select->from('milestone_nature');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->order('id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $natures = $statement->execute();
        $natures_arr =array();
        foreach ($natures as $nature) {
            $natures_arr[$nature['id']] = $nature['description'];
        }

        $select = $sql->select();
        $select->from('measure_unit');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->order('id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $measure_units= $statement->execute();
        $measure_units_arr =array();
        foreach ($measure_units as $measure_unit) {
            $measure_units_arr[$measure_unit['id']] = $measure_unit['unit_description'];
        }

        $select = $sql->select();
        $select->from('company');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->order('company_id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $companies= $statement->execute();
        $companies_arr =array();
        foreach ($companies as $company) {
            $companies_arr[$company['company_id']] = $company['company_name'];
        }


        $select = $sql->select();
        $select->from('milestone');
        $select->where(array('is_active'=>'1'));
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->order('milestone_id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $milestones = $statement->execute();
        $milestones_arr =array();
        foreach ($milestones as $milestone) {
            $milestones_arr[$milestone['milestone_id']] = $milestone['short_description'];
        }
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'milestone_id',
            'options' => array(
                'label' => 'Milestone',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $milestones_arr,
            ),
            'attributes' => array(
                'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'milestone_id'

            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'company_id',
            'options' => array(
                'label' => 'Company',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Companies,
            ),
            'attributes' => array(
                // 'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'company_form_id',

            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'region_id',
            'options' => array(
                'label' => 'Country',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Regions,
            ),
            'attributes' => array(
                // 'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'region_form_id',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country_id',
            'options' => array(
                'label' => 'Country',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Countries,
            ),
            'attributes' => array(
                // 'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'country_form_id',

            )
        ));
        $this->add(array(
            'name'      => 'description',
            'attributes'=> array(
                'id'    => 'description',
                'type'  => 'textarea',
                'required' =>    'required',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'short_description',
            'attributes' => array(
                'class' => 'form-control',
                'required' =>    'required',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'frequency_id',
            'options' => array(
                'label' => 'Frequency',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $frequencies_arr,
            ),
            'attributes' => array(
                'value' => '1', //set selected to '1'
                'class' => 'form-control',

            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'group_id',
            'options' => array(
                'label' => 'Group',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $groups_arr,
            ),
            'attributes' => array(
                'value' => '', //set selected to '1'
                'class' => 'form-control',
                'id'    => 'group',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'indicator_id',
            'options' => array(
                'label' => 'Indicator',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $indicators_arr,
            ),
            'attributes' => array(
                'value' => '', //set selected to '1'
               /* 'class' => 'form-control',*/
                'id'    => 'indicator',
                'multiple' => 'multiple',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'gender_id',
            'options' => array(
                'label' => 'Gender',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $genders_arr,
            ),
            'attributes' => array(
                'value' => '1', //set selected to '1'
                'class' => 'form-control',

            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'position_id',
            'options' => array(
                'label' => 'Position',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $positions_arr,
            ),
            'attributes' => array(
                'value' => '1', //set selected to '1'
                'class' => 'form-control',

            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'nature_id',
            'options' => array(
                'label' => 'Nature',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $natures_arr,
            ),
            'attributes' => array(
                'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'nature_id',
            )
        ));
        $this->add(array(
        'name' => 'tar_value',
        'type' => 'text',
        'attributes' => array(
            'class' => 'form-control',
            'id' => 'tar_value'
        )
    ));
        $this->add(array(
            'name' => 'percent',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control num_percent',
                'id' => 'percent',

            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'status',
            'attributes' => array(
                'id' => 'myonoffswitch',
                'class' => 'onoffswitch-checkbox',
                'value' => '1'
            ),
            'options' => array(
                'label' => 'Status',
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'cal_unit',
            'options' => array(
                'label' => 'Calculation Unit',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $measure_units_arr,
            ),
            'attributes' => array(
                'value' => '1', //set selected to '1'
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
                'class' => 'btn8 btn-8 btn-8f pull-right',
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'type' => 'button',
            'options'=>array(
                'label'=>'Cancel'
            ),
            'attributes' => array(
                'onclick' => 'javascript:history.back();',
                'class' => 'btn8 btn-8 btn-8f pull-right',
            ),
        ));
    }
}
<?php
namespace Procurement\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Http\Header\Date;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
class ProcurementTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($adapter)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;
        // $resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('procurement');
        $select->where(array('procurement.user_id' => $user_id,'procurement.status'=>_P_));
        $select->join('user_profile', 'user_profile.user_id = procurement.user_id','user_name');
        $select->join('company', 'company.company_id = procurement.company_id','company_name');
        $select->join('frequency', 'frequency.frequency_id = procurement.frequency_id','frequency_name');
        $select->join('indicator', 'indicator.id = procurement.type','name');
        $select->order('procurement_id DESC');


        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    public function fetchAll_admin($adapter,$paginated=false)
    {

        if ($paginated) {
            // create a new Select object for the table community
            $userSession = new Container('user');

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('procurement');
            $select->join('user_profile', 'user_profile.user_id = procurement.user_id',array('user_name','user_email'));
            $select->join('company', 'company.company_id = procurement.company_id','company_name');
            $select->join('country', 'country.id = procurement.country_id','country_name');
            $select->join('region', 'region.id = procurement.region_id','region_description');
            $select->join('frequency', 'frequency.frequency_id = procurement.frequency_id','frequency_name');
            $select->join('indicator', 'indicator.id = procurement.type','name');
            $select->where(array('procurement.company_id'=>$userSession->user_companies,'procurement.country_id'=>$userSession->user_countries,'procurement.region_id'=>$userSession->user_regions));
            $select->order('procurement_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Procurement());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function fetchAll_champion($adapter,$paginated=false)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('procurement');
            $select->where(array('procurement.user_id'=>$user_id));
            $select->join('user_profile', 'user_profile.user_id = procurement.user_id','user_name');
            $select->join('company', 'company.company_id = procurement.company_id','company_name');
            $select->join('frequency', 'frequency.frequency_id = procurement.frequency_id','frequency_name');
            $select->join('indicator', 'indicator.id = procurement.type','name');
            $select->order('procurement_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Procurement());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function getProcurement($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('procurement_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveProcurement(Procurement $procurement)
    {
        {
            $id = $procurement->procurement_id;
            $userSession = new Container('user');
            $user_id = $userSession->user_id;
            $user_type = $userSession->user_type;
            if($id == 0) {
                $data = array(
                    'procurement_id'    => $procurement->procurement_id,
                    'company_id'        => $procurement->company_id,
                    'region_id'        => $procurement->region_id,
                    'country_id'        => $procurement->country_id,
                    'frequency_id'      => $procurement->frequency_id,
                    'type'              => $procurement->type,
                    'value'             => $procurement->value,
                    'status'            => _P_,
                    'user_id'           => $user_id,
                    'create_date'       => $procurement->create_date,
                    'expiry_date'       => $procurement->expire_date
                );
                $this->tableGateway->insert($data);
            }

            else {
                if ($this->getProcurement($id)) {
                    if($user_type == _ADMIN_) {
                        $data = array(
                           /* 'company_id' => $procurement->company_id,
                            'frequency_id' => $procurement->frequency_id,
                            'region_id'        => $procurement->region_id,
                            'country_id'        => $procurement->country_id,
                            'type'          => $procurement->type,*/
                            'value'         => $procurement->value,
                            'status'  => _A_

                        );
                    }
                    if($user_type == _CHAMPION_) {
                        $data = array(
                        /*    'company_id' => $procurement->company_id,
                            'region_id'        => $procurement->region_id,
                            'country_id'        => $procurement->country_id,
                            'frequency_id' => $procurement->frequency_id,
                            'type'          => $procurement->type,*/
                            'value'         => $procurement->value,
                            'status' => _P_,

                        );
                    }
                    $this->tableGateway->update($data, array('procurement_id' => $id));
                } else {
                    throw new \Exception('Procurement id does not exist');
                }

            }
        }
    }

    public function deleteProcurement($id)
    {
        $this->tableGateway->delete(array('procurement_id' => (int) $id));
    }
}
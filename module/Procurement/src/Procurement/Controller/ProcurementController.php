<?php
namespace Procurement\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Procurement\Model\Procurement;
use Procurement\Form\ProcurementForm;
use Procurement\Model\ProcurementTable;
use Zend\Session\Container;

use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
class ProcurementController extends AbstractActionController
{
    protected $ProcurementTable;
    protected $authservice;

    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }
        return $this->authservice;
    }
    public function getMenu()
    {
        if (!$this->MenuTable) {
            $sm = $this->getServiceLocator();
            $this->MenuTable = $sm->get('Application\Model\MenuTable');
        }
        return $this->MenuTable;
    }
    public function indexAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $form = new ProcurementForm();
            $form->get('submit')->setValue('Request Indicator');
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $config = $this->getServiceLocator()->get('Config');
            $dsn = $config['db']['dsn'];
            $username = $config['db']['username'];
            $password = $config['db']['password'];
            $controllerClass= __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            $Indicators = $this->getMenu()->fetchIndicatorsIDByGroup($dbAdapter,$moduleNamespace);
            if ($userSession->user_type == _ADMIN_) {
                // grab the paginator from the CommunityTable
                $paginator = $this->getMenu()->fetchAll_admin($dbAdapter, true,$moduleNamespace,$dsn,$username,$password,$Indicators);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form,
                ));
                $view->setTemplate('procurement/admin');
                return $view;
            }
            if ($userSession->user_type == _CHAMPION_) {
                // grab the paginator from the CommunityTable
                $paginator = $this->getMenu()->fetchAll_champion($dbAdapter, true,$moduleNamespace,$dsn,$username,$password,$Indicators);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('procurement/index');
                return $view;
            }
            /*   if ($userSession->user_type == _CHAMPION_) {
                   $form = new ProcurementForm();
                   $form->get('submit')->setValue('Add');
                   $request = $this->getRequest();

                   if ($request->isPost()) {
                       $procurement = new Procurement();
                       $form->setInputFilter($procurement->getInputFilter());
                       $form->setData($request->getPost());

                       if ($form->isValid()) {
                           $procurement->exchangeArray($form->getData());
                           $this->getProcurementTable()->saveProcurement($procurement, null);

                           // Redirect to list of albums
                           return $this->redirect()->toRoute('procurement');
                       }
                   }
                   return array('form' => $form, 'procurement' => $this->getProcurementTable()->fetchAll($dbAdapter));
               }
               */
        }
            else {
                return $this->redirect()->toRoute('login');
            }


//        return new ViewModel(array(
//            'procurement' => $this->getProcurementTable()->fetchAll(),
//
//        ));
            //    $form->setVariable('form', $form);

    }
    public function getProcurementTable()
    {
        if (!$this->ProcurementTable) {
            $sm = $this->getServiceLocator();
            $this->ProcurementTable = $sm->get('Procurement\Model\ProcurementTable');
        }
        return $this->ProcurementTable;
    }

    public function addAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $controllerClass= __NAMESPACE__;
        $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
        $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
        $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
        $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
        $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter,$moduleNamespace);
        $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
        $DateField = $this->getMenu()->DateField();
        $form = new ProcurementForm($Companies,$Regions,$Countries,$Indicators,$frequencies,$DateField);
        $form->get('submit')->setValue('Save');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $procurement = new Procurement();
            $created_date =$this->getRequest()->getPost('create_date');
            $indicator_id =$this->getRequest()->getPost('type');
            $company_id =$this->getRequest()->getPost('company_id');
            $country_id =$this->getRequest()->getPost('country_id');
            $frequency_id =$this->getRequest()->getPost('frequency_id');
            $date_validation = $this->getMenu()->DateFieldValidation($dbAdapter,$moduleNamespace,$created_date,$indicator_id,$company_id,$country_id,$frequency_id);
            $form->setInputFilter($procurement->getInputFilter($date_validation));
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $procurement->exchangeArray($form->getData());
               /* Expired entries  marked as normal if the desire entry has been made after intimation.*/
                $this->getMenu()->StatusChangeApproved($dbAdapter,$moduleNamespace,$indicator_id,$company_id,$country_id,$frequency_id);
                $expire_date = $this->getMenu()->FrequencyChecker($frequency_id,$created_date);
                $procurement->expire_date =$expire_date;
                $this->getProcurementTable()->saveProcurement($procurement);
                // Redirect to list of Procurement
                return $this->redirect()->toRoute('procurement');
            }
        }
        $view = new ViewModel(array(
            'form' => $form, 'procurement' => $this->getProcurementTable()->fetchAll($dbAdapter)
        ));
        $view->setTemplate('procurement/add');
        return $view;
        // return array('form' => $form);
        }
        else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function editAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {

            $id = (int)$this->params()->fromRoute('id', 0);

            try {
                $procurement = $this->getProcurementTable()->getProcurement($id);
            } catch (\Exception $ex) {
                return $this->redirect()->toRoute('procurement', array(
                    'action' => 'index'
                ));
            }

            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $controllerClass= __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter,$moduleNamespace);
            $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
            $DateField = $this->getMenu()->DateField();

            $form = new ProcurementForm($Companies,$Regions,$Countries,$Indicators,$frequencies,$DateField);
            $form->bind($procurement);
            $userSession = new Container('user');

            if($userSession->user_type == _ADMIN_) {
                $form->get('submit')->setAttribute('value', 'Update & Approval');
            }
            if($userSession->user_type == _CHAMPION_) {
                $form->get('submit')->setAttribute('value', 'Update');
            }

            $request = $this->getRequest();
            if ($request->isPost()) {
                $form->setInputFilter($procurement->getInputFilter(null));
                $form->setData($request->getPost());

                if ($form->isValid()) {
                    $this->getProcurementTable()->saveProcurement($procurement,$id);

                    // Redirect to list of procurements0
                    return $this->redirect()->toRoute('procurement');
                }
            }

            return array(
                'id' => $id,
                'form' => $form,
            );
        }
        else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('procurement');
        }
        if($id){
            $this->getProcurementTable()->deleteProcurement($id);
            return $this->redirect()->toRoute('procurement');
        }
    }
    public function Graph1Action(){
        $company_id ='';
        $region_id ='';
        $country_id ='';
        $group ='';
        $frequency ='';
        $indicator ='';
        $toyear ='';
        $fromyear ='' ;
        if($_REQUEST){
            extract($_REQUEST);
            $company_id;
            $region_id;
            $country_id;
            $indicator;
            $toyear;
            $fromyear ;
            $group ;
            $frequency;
        }
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()){
            //for session varibles
            $userSession = new Container('user');
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $controllerClass = __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            // $form = new MilestoneForm($dbAdapter);
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            if($userSession->user_type == _ADMIN_){
                // grab the paginator from the CommunityTable
                //   $paginator = $this->getMilestoneTable()->fetchAll_admin($dbAdapter,true);
                // set the current page to what has been passed in query string, or to 1 if none set
                //   $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                //   $paginator->setItemCountPerPage(10);
                //  $GData = $this->getMilestoneTable()->fetchGraphData($dbAdapter,$fromyear,$toyear,$company_id,$event_desc);
                $GData = $this->getMenu()->fetchGraphData($dbAdapter,$company_id,$group,$frequency,$indicator,$fromyear,$toyear,$country_id);

                $Groups = $this->getMenu()->fetchAllGroupForEHS($dbAdapter);
                $Companies = $this->getMenu()->fetchCompany($dbAdapter);
                $Regions = $this->getMenu()->fetchRegion($dbAdapter);
                $Countries = $this->getMenu()->fetchCountry($dbAdapter);
                $Indicators = $this->getMenu()->fetchIndicator($dbAdapter);
                $Frequencies = $this->getMenu()->fetchFrequency($dbAdapter);
                $company_name = $this->getMenu()->fetchCompanyById($dbAdapter,$company_id);
                $country_name = $this->getMenu()->fetchCountryById($dbAdapter,$country_id);
                $region_name = $this->getMenu()->fetchRegionById($dbAdapter,$region_id);
                $group_name = $this->getMenu()->fetchGroupById($dbAdapter,$group);
                $view = new ViewModel(array(
                    'GData' => $GData,
                    'Groups' => $Groups,
                    'Companies' => $Companies,
                    'Regions' => $Regions,
                    'Countries' => $Countries,
                    'Indicators' => $Indicators,
                    'Frequencies' => $Frequencies,
                    'company_name' => $company_name,
                    'country_name' => $country_name,
                    'region_name' => $region_name,
                    'group_name' => $group_name,
                    'moduleNamespace' => $moduleNamespace

                ));

                $view->setTemplate('procurement/analytic');
                return $view;
            }
            if($userSession->user_type == _CHAMPION_) {
                return $this->redirect()->toRoute('application');
            }
        } else {
            return $this->redirect()->toRoute('login');
        }

    }
}
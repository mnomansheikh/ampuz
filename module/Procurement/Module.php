<?php

/**
 * Generated by ZF2ModuleCreator
 */

namespace Procurement;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Procurement\Model\Procurement;
use Procurement\Model\ProcurementTable;
use Zend\Db\ResultSet\ResultSet;
use Event\Model\Event;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\ModuleRouteListener;
use Training\Model\Training;
class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{

    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Return an array for passing to Zend\Loader\AutoloaderFactory.
     *
     * @return array
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Procurement\Model\ProcurementTable' =>  function($sm) {
                    $tableGateway = $sm->get('ProcurementTableGateway');
                    $table = new ProcurementTable($tableGateway);
                    return $table;
                },

                'ProcurementTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Procurement());
                    return new TableGateway('procurement', $dbAdapter, null, $resultSetPrototype);
                },
                'MenuTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    /// $resultSetPrototype->setArrayObjectPrototype(new CarbonCalculator());
                    return new TableGateway('', $dbAdapter, null, $resultSetPrototype);
                }

            ),
        );
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // Autoload all classes from namespace 'Procurement' from '/module/Procurement/src/Procurement'
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                )
            )
        );
    }
}
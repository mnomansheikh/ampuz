<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/28/2016
 * Time: 3:04 PM
 */
namespace Application\Navigation;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MainNavigationFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $navigation =  new MainNavigation();
        return $navigation->createService($serviceLocator);
    }
}
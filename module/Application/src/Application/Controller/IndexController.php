<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class IndexController extends AbstractActionController
{
    protected $authservice;

    public function indexAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $userSession = new Container('user');
            $username = $userSession->username;
            return new ViewModel();
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function getAuthService()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }
        return $this->authservice;
    }

    public function dashboardAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $userSession = new Container('user');
            $username = $userSession->username;
            $view = new ViewModel(array());
            $view->setTemplate('application/dashboard');
            return $view;
        } else {
            return $this->redirect()->toRoute('login');
        }
    }
}


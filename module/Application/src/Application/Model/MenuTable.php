<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/28/2016
 * Time: 3:03 PM
 */
namespace Application\Model;

use    Zend\Mvc\MvcEvent;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Session\Container;
use Zend\Db\Sql\Expression;
use Zend\Validator\Db\NoRecordExists;
use Zend\Validator\Callback;
use Zend\InputFilter\Input;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use PDO;
use Zend\Db\ResultSet\ResultSet;

class MenuTable extends AbstractTableGateway
    implements AdapterAwareInterface
{
    protected $table = 'menu';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new HydratingResultSet();

        $this->initialize();
    }

    public function fetchAll()
    {
        //for session varibles
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array('m' => 'menu'));
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->join(array('umr' => 'user_menu_rights'), 'umr.menu_id = m.id', 'user_id');
        $select->where(array('umr.user_id' => $user_id));
        $select->order('m.id asc');

        $statement = $sql->prepareStatementForSqlObject($select);
        $menus = $statement->execute();

        //  $resultSet = $this->select(function (Select $select){
        //      $select->order(array('id asc'));
        //  });

        // $resultSet = $results->toArray();
        $resultSet = array();
        $itr = 0;
        foreach ($menus as $menu):
            $select = $sql->select();
            $resultSet[$itr]['label'] = $menu['label'];
            if ($menu['href'] == '#') {
                $resultSet[$itr]['uri'] = $menu['href'];
            } else if ($menu['uri'] != '' || $menu['uri'] != NULL) {
                $resultSet[$itr]['uri'] = $menu['uri'];
            } else {
                $resultSet[$itr]['route'] = $menu['route'];
            }
            $resultSet[$itr]['class'] = $menu['class'];
            $resultSet[$itr]['icon'] = $menu['icon'];
//            SELECT
//  sm.*
//FROM
//  `sub_menu` sm
//  INNER JOIN `user_menu_rights` umr ON umr.`sub_menu_id` = sm.`id`
//WHERE sm.`menu_id` = "3" AND umr.`user_id` = 7
//ORDER BY sm.`id` ASC
//
            $select->from(array('sm' => 'sub_menu'));
            $select->join(array('umr' => 'user_menu_rights'), 'umr.sub_menu_id = sm.id', 'user_id');
            $select->where(array('sm.menu_id' => $menu['id'], 'umr.user_id' => $user_id));
            $select->order('sm.sequence_id asc');
            $statement = $sql->prepareStatementForSqlObject($select);
            $sub_menus = $statement->execute();
            if ($sub_menus) :

                $itr1 = 0;
                foreach ($sub_menus as $sub_menu) :

                    $resultSet[$itr]['pages'][$itr1]['label'] = $sub_menu['label'];
                    $resultSet[$itr]['pages'][$itr1]['icon'] = $sub_menu['icon'];
                    if ($sub_menu['uri'] != '' || $sub_menu['uri'] != NULL) {
                        $resultSet[$itr]['pages'][$itr1]['uri'] = $sub_menu['uri'];
                    } else {
                        $resultSet[$itr]['pages'][$itr1]['route'] = $sub_menu['route'];
                    }

                    $itr1++;
                endforeach;
            endif;
            $itr++;
        endforeach;

//      $resultSet1 = array(
//
//            [
//                'label' => 'Google',
//                'uri' => 'https://www.google.com',
//                'target' => '_blank'
//            ],
//            [
//                'label' => 'Home',
//                'route' => 'home'
//            ],
//            [
//                'label' => 'Modules',
//                'uri' => '#',
//                'pages' => [
//                    [
//                        'label' => 'LearnZF2Ajax',
//                        'route' => 'application'
//                    ],
//                    [
//                        'label' => 'LearnZF2FormUsage',
//                        'route' => 'application'
//                    ],
//                    [
//                        'label' => 'LearnZF2Barcode',
//                        'route' => 'application'
//                    ],
//                    [
//                        'label' => 'LearnZF2Pagination',
//                        'route' => 'application'
//                    ],
//                    [
//                        'label' => 'LearnZF2Log',
//                        'route' => 'application'
//                    ]
//                ]
//            ]
//    );
//
//       echo '<pre>',print_r($resultSet,1),'</pre>';
//       echo '<pre>',print_r($resultSet1,1),'</pre>';

        return $resultSet;
    }

    public function fetchGroup($adapter)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('sub_menu');
        //SELECT * FROM `sub_menu` WHERE menu_id= '2' AND id IN(3,4,6)
        // $select->join('menu', 'menu.id = sub_menu.menu_id');
        $select->where(array('menu_id' => 2, 'id' => array(3, 4, 6)));
        $statement = $sql->prepareStatementForSqlObject($select);
        $Group = $statement->execute();


        return $Group;
    }

    public function fetchCompany($adapter)
    {
//        $sql = new Sql($adapter);
//        $select = $sql->select();
//        $select->from('company');
//        //SELECT * FROM `sub_menu` WHERE menu_id= '2' AND id IN(3,4,6)
//        // $select->join('menu', 'menu.id = sub_menu.menu_id');
//        // $select->where(array('menu_id' => 2, 'id' => array(3,4,6)));
//        $statement = $sql->prepareStatementForSqlObject($select);
//        $company = $statement->execute();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('company');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->where(array('is_active' => 1));
        $select->order('company_id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $companies = $statement->execute();
        $companies_arr = array();
        foreach ($companies as $company) {
            $companies_arr[$company['company_id']] = $company['company_name'];
        }

        return $companies_arr;
    }

    public function fetchCountry($adapter)
    {
        $sql = new Sql($adapter);
        // $select = $sql->select();
        //  $select->from('country');
        //SELECT * FROM `sub_menu` WHERE menu_id= '2' AND id IN(3,4,6)
        // $select->join('menu', 'menu.id = sub_menu.menu_id');
        // $select->where(array('menu_id' => 2, 'id' => array(3,4,6)));
        //  $statement = $sql->prepareStatementForSqlObject($select);
        //   $country = $statement->execute();
        $select = $sql->select();
        $select->from('country');
        $select->join('country_region', 'country_region.country_id = country.id', 'region_id');
        $select->join('company_country', 'company_country.country_id = country.id', 'company_id');
        $select->where(array('country.is_active' => 1));
        $select->order('country.id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $countries = $statement->execute();
        $countries_arr = array();
        $itr = 0;
        foreach ($countries as $country) {
            // $countries_arr[$country['id']] = $country['country_name'];
            $countries_arr[$itr]['value'] = $country['id'];
            $countries_arr[$itr]['label'] = $country['country_name'];
            $countries_arr[$itr]['attributes']['data-region-id'] = $country['region_id'];
            $countries_arr[$itr]['attributes']['data-company-id'] = $country['company_id'];
            $itr++;
        }

        return $countries_arr;
    }

    public function fetchRegion($adapter)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('region');
        $select->join('company_region', 'company_region.region_id = region.id', 'company_id');
        $select->where(array('region.is_active' => 1));
        $select->order('region.id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $regions = $statement->execute();
        $regions_arr = array();
        $itr = 0;
        foreach ($regions as $region) {
            //$regions_arr[$region['id']] = $region['region_description'];
            $regions_arr[$itr]['value'] = $region['id'];
            $regions_arr[$itr]['label'] = $region['region_description'];
            $regions_arr[$itr]['attributes']['data-company-id'] = $region['company_id'];
            $itr++;
        }
        return $regions_arr;
    }

    public function fetchIndicator($adapter)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('indicator');
        //SELECT * FROM `sub_menu` WHERE menu_id= '2' AND id IN(3,4,6)
        // $select->join('menu', 'menu.id = sub_menu.menu_id');
        // $select->where(array('menu_id' => 2, 'id' => array(3,4,6)));
        $statement = $sql->prepareStatementForSqlObject($select);
        $indicator = $statement->execute();


        return $indicator;
    }

    public function fetchFrequency($adapter)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('frequency');
        //SELECT * FROM `sub_menu` WHERE menu_id= '2' AND id IN(3,4,6)
        // $select->join('menu', 'menu.id = sub_menu.menu_id');
        // $select->where(array('menu_id' => 2, 'id' => array(3,4,6)));
        $statement = $sql->prepareStatementForSqlObject($select);
        $frequency = $statement->execute();


        return $frequency;
    }

    public function fetchIndicatorsByGroup($adapter, $id)
    {
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('indicator');
        $select->join('sub_menu', 'sub_menu.id = indicator.sub_menu_id', 'route');
        $select->join('user_indicator', 'user_indicator.indicator_id = indicator.id');
        $select->where(array('indicator.is_active' => 1, 'sub_menu.route' => $id, 'user_indicator.user_id' => $user_id));
        $select->order('indicator.id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $indicators = $statement->execute();
        $indicators_arr = array('' => '');
        $itr = 1;
        foreach ($indicators as $indicator) {
            //$indicators_arr[$indicator['id']] = $indicator['name'];
            $indicators_arr[$itr]['value'] = $indicator['id'];
            $indicators_arr[$itr]['label'] = $indicator['name'];
            $indicators_arr[$itr]['attributes']['data-uploading'] = $indicator['is_uploading'];
            $indicators_arr[$itr]['attributes']['data-attachment'] = $indicator['is_attachment'];
            $indicators_arr[$itr]['attributes']['data-bulk-type'] = $indicator['bulk_type'];
            for ($a = 1; $a <= _MANUAL_FIELD_EXTRA_; $a++) {
                $indicators_arr[$itr]['attributes']['data-is_col' . $a] = ucfirst(strtolower($indicator['is_col' . $a]));
            }
            $itr++;
        }
        return $indicators_arr;
    }

    public function fetchIndicatorsIDByGroup($adapter, $id)
    {
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('indicator');
        $select->join('sub_menu', 'sub_menu.id = indicator.sub_menu_id', 'route');
        $select->join('user_indicator', 'user_indicator.indicator_id = indicator.id');
        $select->where(array('indicator.is_active' => 1, 'sub_menu.route' => $id, 'user_indicator.user_id' => $user_id));
        $select->order('indicator.id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $indicators = $statement->execute();
        $indicators_arr = array('' => '');
        foreach ($indicators as $indicator) {
            $indicators_arr[] = $indicator['id'];
        }
        return $indicators_arr;
    }

    public function fetchCompaniesByUser($adapter)
    {
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('company');
        $select->join('user_company', 'user_company.company_id = company.company_id', 'user_id');
        $select->where(array('company.is_active' => 1, 'user_company.user_id' => $user_id));
        $select->order('company.company_id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $companies = $statement->execute();
        $companies_arr = array(0 => ' ');
        foreach ($companies as $company) {
            $companies_arr[$company['company_id']] = $company['company_name'];
        }
        return $companies_arr;
    }

    public function fetchRegionsByUser($adapter)
    {
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('region');
        $select->join('company_region', 'company_region.region_id = region.id', 'company_id');
        $select->join('user_region', 'user_region.region_id = region.id', 'user_id');
        $select->where(array('region.is_active' => 1, 'user_region.user_id' => $user_id));
        $select->order('region.id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $regions = $statement->execute();
        $regions_arr = array('' => '');
        $itr = 0;
        foreach ($regions as $region) {
            $regions_arr[$itr]['value'] = $region['id'];
            $regions_arr[$itr]['label'] = $region['region_description'];
            $regions_arr[$itr]['attributes']['data-company-id'] = $region['company_id'];
            $itr++;
        }


        return $regions_arr;
    }

    public function fetchCountriesByUser($adapter)
    {
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('country');
        $select->join('country_region', 'country_region.country_id = country.id', 'region_id');
        $select->join('company_country', 'company_country.country_id = country.id', 'company_id');
        $select->join('user_country', 'user_country.country_id = country.id', 'user_id');
        $select->where(array('country.is_active' => 1, 'user_country.user_id' => $user_id));
        $select->order('country.id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $countries = $statement->execute();
        $countries_arr = array('' => '');
        $itr = 0;
        foreach ($countries as $country) {
            $countries_arr[$itr]['value'] = $country['id'];
            $countries_arr[$itr]['label'] = $country['country_name'];
            $countries_arr[$itr]['attributes']['data-region-id'] = $country['region_id'];
            $countries_arr[$itr]['attributes']['data-company-id'] = $country['company_id'];
            $itr++;
        }
        return $countries_arr;
    }

    public function fetchFrequenciesByIndicator($adapter, $id)
    {
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from(array('f' => 'frequency'));
        $select->join(array('i' => 'indicator'), 'i.frequency_id = f.frequency_id', array('indicator_id' => 'id'));
        $select->join(array('sm' => 'sub_menu'), 'sm.id = i.sub_menu_id', 'route');
        $select->where(array('i.is_active' => 1, 'sm.route' => $id));
        $select->order('f.frequency_id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $frequencies = $statement->execute();
        $frequencies_arr = array();
        $itr = 0;
        foreach ($frequencies as $frequency) {
            $frequencies_arr[$itr]['value'] = $frequency['frequency_id'];
            $frequencies_arr[$itr]['label'] = $frequency['frequency_name'];
            $frequencies_arr[$itr]['attributes']['data-indicator-id'] = $frequency['indicator_id'];
            $itr++;
        }
        return $frequencies_arr;
    }

    public function DateField()
    {
        return array(
            'type' => 'text',
            'name' => 'create_date',
            'options' => array(
                'label' => 'Date',
            ),
            'attributes' => array(
                /*  'value' => '1', //set selected to '1'*/
                'class' => 'form-control',
                'id' => 'create_date',
                'required' => 'required'
            )
        );
    }

    public function RecordExpiration($adapter, $table, $indicators, $companies, $countries, $frequencies)
    {
        $mons = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec");
        $month = date("m", strtotime(date('Y-m-d')));
        $year = date("Y", strtotime(date('Y-m-d')));
        $quarter = ceil($month / 3);
        foreach ($indicators as $key => $value) {
            $indicator_id = $key;
            foreach ($companies as $key1 => $value1) {
                $company_id = $key1;
                $sql = new Sql($adapter);
                $select1 = $sql->select();
                $select1->columns(array('last_month' => new Expression("MONTH(create_date)")));
                $select1->from($table);
                $select1->where->EqualTo('MONTH(create_date)', $month);
                $select1->where->EqualTo('YEAR(create_date)', $year);
                $select1->where(array('type' => $indicator_id, 'company_id' => $company_id, 'frequency_id' => 1));
                $select1->order('create_date DESC');
                $select1->limit(1);
                $statement1 = $sql->prepareStatementForSqlObject($select1);
                $query1 = $statement1->getSql();
                $query1 = str_replace('`', '', $query1);
                $statement1->setSql($query1);
                $result1 = $statement1->execute();
                $row1 = $result1->current();
                if (!$row1) {
                    // $select1->columns(array('last_month'=>new Expression("MONTH(create_date)")));
                    $sql = new Sql($adapter);
                    $update = $sql->update();
                    $update->table($table);
                    $update->set(array('status' => _W_));
                    // $select1->where->EqualTo('MONTH(create_date)', $month);
                    // $select1->where->EqualTo('YEAR(create_date)', $year);
                    $update->where(array('type' => $indicator_id, 'company_id' => $company_id, 'frequency_id' => 1));
                    $sql = new Sql($adapter);
                    $select2 = $sql->select();
                    $select2->columns(array('last_month' => new Expression("MONTH(create_date)")));
                    $select2->from($table);
                    $select2->where->lessThanOrEqualTo('MONTH(create_date)', $month);
                    $select2->where->lessThanOrEqualTo('YEAR(create_date)', $year);
                    $select2->where(array('type' => $indicator_id, 'company_id' => $company_id, 'frequency_id' => 1));
                    $select2->order('create_date DESC');
                    $select2->limit(1);
                    $statement2 = $sql->prepareStatementForSqlObject($select2);
                    $query2 = $statement2->getSql();
                    $query2 = str_replace('`', '', $query2);
                    $statement2->setSql($query2);
                    $result2 = $statement2->execute();

                    $row2 = $result2->current();
                    $update->where(array('type' => $indicator_id, 'frequency_id' => 1, 'MONTH(create_date)' => $row2['last_month']));
                    // $update->order('create_date DESC');
                    // $update->limit(1);
                    $statement_update = $sql->prepareStatementForSqlObject($update);
                    $query_update = $statement_update->getSql();
                    $query_update = str_replace('`', '', $query_update);
                    $statement_update->setSql($query_update);
                    $result_update = $statement_update->execute();
                    //$row1 = $result_update->current();
                }
            }
        }
        /*add dummy rows with expiration*/
        $sql_query = 'SELECT *
        FROM    (
            SELECT 1 AS MonthNumber

        UNION ALL SELECT 2
        UNION ALL SELECT 3
        UNION ALL SELECT 4
        UNION ALL SELECT 5
        UNION ALL SELECT 6
        UNION ALL SELECT 7
        UNION ALL SELECT 8
        UNION ALL SELECT 9
        UNION ALL SELECT 10
           UNION ALL SELECT 11
              UNION ALL SELECT 12

        ) AS months
 LEFT OUTER JOIN ' . $table . ' MM
ON MonthNumber = MONTH(MM.create_date)
    AND YEAR(MM.create_date) = 2016
WHERE MonthNumber <= (SELECT MONTH(CURDATE()) - 1)';

        $statement = $this->adapter->query($sql_query);
        $statement_entry_check_execute = $statement->execute();
        $data = array();

        foreach ($statement_entry_check_execute as $item) {
            $item['status'] = _W_;
            $data[] = $item;

        }

        return $data;
        /*add dummy rows with expiration*/
    }

    public function DateFieldValidation($adapter, $table, $create_date, $indicator_id, $company_id, $country_id, $frequency_id)
    {
        $mons = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec");
        $month = date("m", strtotime($create_date));
        $year = date("Y", strtotime($create_date));
        $quarter = ceil($month / 3);
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select1 = $sql->select();
        switch ($frequency_id) {
            /*monthly*/
            case 1:
                $select1->columns(array('last_month' => new Expression("MONTH(create_date)")));
                $select1->from($table);
                $select1->where->lessThanOrEqualTo('MONTH(create_date)', $month);
                $select1->where->lessThanOrEqualTo('YEAR(create_date)', $year);
                $select1->where(array('type' => $indicator_id, 'company_id' => $company_id, 'country_id' => $country_id, 'frequency_id' => $frequency_id));
                $select1->order('create_date DESC');
                $select1->limit(1);
                $statement1 = $sql->prepareStatementForSqlObject($select1);
                //$result1 = $statement1->execute();
                $query1 = $statement1->getSql();
                $query1 = str_replace('`', '', $query1);
                $statement1->setSql($query1);
                $result1 = $statement1->execute();
                $row1 = $result1->current();
                $last_month = (int)$row1['last_month'];
                $cur_month = (int)$month;

                $check_month = $last_month + 1;
                $cur_month_name = $mons[$check_month];
                $select->columns(array('*', 'month' => new Expression("MONTH(create_date)"), 'year' => new Expression("YEAR(create_date)")));
                $select->from($table);
                $select->where(array('MONTH(create_date)' => $month,
                    'YEAR(create_date)' => $year, 'type' => $indicator_id, 'company_id' => $company_id, 'country_id' => $country_id, 'frequency_id' => $frequency_id));

                if ($check_month != $cur_month && $check_month < $cur_month) {
                    $validator = new NoRecordExists($select1);

                } else {
                    $validator = new NoRecordExists($select);
                }


                $validator->setAdapter($adapter);
                if ($validator->isValid($month)) {
                } else {
                    if ($check_month != $cur_month && $check_month < $cur_month) {
                        $validator->setMessage('Please insert missing record of "' . $cur_month_name . '" ');
                    } else {
                        $validator->setMessage('Record Already inserted of this month');

                    }
                }
                return array(
                    'name' => 'month',
                    'validators' => array($validator)
                );
                break;
            /*yearly*/
            case 2:
                $select1->columns(array('last_year' => new Expression("YEAR(create_date)")));
                $select1->from($table);
                $select1->where->lessThanOrEqualTo('YEAR(create_date)', $year);
                $select1->where(array('type' => $indicator_id, 'company_id' => $company_id, 'country_id' => $country_id, 'frequency_id' => $frequency_id));
                $select1->order('create_date DESC');
                $select1->limit(1);
                $statement1 = $sql->prepareStatementForSqlObject($select1);
                //$result1 = $statement1->execute();
                $query1 = $statement1->getSql();
                $query1 = str_replace('`', '', $query1);
                $statement1->setSql($query1);
                $result1 = $statement1->execute();
                $row1 = $result1->current();
                $last_year = (int)$row1['last_year'];
                $cur_year = (int)$year;
                $check_year = $last_year + 1;

                $select->columns(array('*', 'month' => new Expression("MONTH(create_date)"), 'year' => new Expression("YEAR(create_date)")));
                $select->from($table);
                $select->where(array('YEAR(create_date)' => $year, 'type' => $indicator_id, 'company_id' => $company_id, 'country_id' => $country_id, 'frequency_id' => $frequency_id));
                if ($check_year != $cur_year && $check_year < $cur_year) {
                    $validator = new NoRecordExists($select1);

                } else {
                    $validator = new NoRecordExists($select);
                }
                $validator->setAdapter($adapter);
                if ($validator->isValid($year)) {
                } else {
                    if ($check_year != $cur_year && $check_year < $cur_year) {
                        $validator->setMessage('Please insert missing record of "' . $check_year . '" ');
                    } else {
                        $validator->setMessage('Record Already inserted of this year');
                    }
                }
                return array(
                    'name' => 'year',
                    'validators' => array($validator)
                );
                break;
            /*bi annual*/
            case 3:
                return date("Y-m-d", strtotime("+6 month", strtotime($create_date)));
                break;
            /*quarterly*/
            case 4:
                $select->columns(array('*', 'month' => new Expression("MONTH(create_date)"), 'year' => new Expression("YEAR(create_date)")));
                $select->from($table);
                $select->where(array('QUARTER(create_date)' => $quarter,
                    'YEAR(create_date)' => $year, 'type' => $indicator_id, 'company_id' => $company_id, 'country_id' => $country_id, 'frequency_id' => $frequency_id));

                $validator = new NoRecordExists($select);
                $validator->setAdapter($adapter);
                if ($validator->isValid($quarter)) {
                } else {

                    $validator->setMessage('Record Already inserted of this month');
                }
                return array(
                    'name' => 'quarter',
                    'validators' => array($validator)
                );
                break;

            default:
                /** do stuff for other failure **/
                break;
        }


    }

    public function StatusChangeApproved($adapter, $table, $indicator_id, $company_id, $country_id, $frequency_id)
    {

        $sql = new Sql($adapter);
        $select = $sql->update();
        $select->table($table);
        $select->set(array('status' => _P_));
        $select->where(array('type' => $indicator_id, 'company_id' => $company_id, 'country_id' => $country_id, 'frequency_id' => $frequency_id, 'status' => _W_));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

    }

    public function FrequencyChecker($frequency, $create_date)
    {

        switch ($frequency) {
            /*monthly*/
            case 1:
                return date("Y-m-d", strtotime("+1 month", strtotime($create_date)));
                break;
            /*yearly*/
            case 2:
                return date("Y-m-d", strtotime("+1 year", strtotime($create_date)));
                break;
            /*bi annual*/
            case 3:
                return date("Y-m-d", strtotime("+6 month", strtotime($create_date)));
                break;
            /*quarterly*/
            case 4:
                return date("Y-m-d", strtotime("+3 month", strtotime($create_date)));
                break;

            default:
                /** do stuff for other failure **/
                break;
        }

    }

    public function fetchCompanyById($adapter, $id)
    {

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->columns(array('company_name' => 'company_name'));
        $select->from('company');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->where(array('is_active' => 1, 'company_id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $company_name = null;
        foreach ($result as $item) {
            $company_name = $item['company_name'];
        }
        return $company_name;

    }

    public function fetchCountryById($adapter, $id)
    {

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->columns(array('country_name' => 'country_name'));
        $select->from('country');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->where(array('is_active' => 1, 'id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $country_name = null;
        foreach ($result as $item) {
            $country_name = $item['country_name'];
        }
        return $country_name;
    }

    public function fetchRegionById($adapter, $id)
    {

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->columns(array('region_description' => 'region_description'));
        $select->from('region');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->where(array('is_active' => 1, 'id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $region_name = null;
        foreach ($result as $item) {
            $region_name = $item['region_description'];
        }
        return $region_name;
    }

    public function fetchGroupById($adapter, $id)
    {

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->columns(array('label' => 'label'));
        $select->from('sub_menu');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->where(array('id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $group_name = null;
        foreach ($result as $item) {
            $group_name = $item['label'];
        }
        return $group_name;
    }

    public function fetchAllGroupForEHS($adapter)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('sub_menu');
        //SELECT * FROM `sub_menu` WHERE menu_id= '2' AND id IN(3,4,6)
        // $select->join('menu', 'menu.id = sub_menu.menu_id');
        $select->where(array('menu_id' => 2));
        $statement = $sql->prepareStatementForSqlObject($select);
        $Group = $statement->execute();


        return $Group;
    }

    public function fetchAllGenderForDiversity($adapter)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->columns(array('gender' => new Expression('DISTINCT gender')));
        $select->from('national_details');
        $statement = $sql->prepareStatementForSqlObject($select);
        $gender = $statement->execute();
        return $gender;
    }

    public function fetchAllNationalityForDiversity($adapter)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->columns(array('nationality' => new Expression('DISTINCT nationality')));
        $select->from('national_details');
        $statement = $sql->prepareStatementForSqlObject($select);
        $nationality = $statement->execute();
        return $nationality;
    }

    public function fetchAllDesignationsForDiversity($adapter)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->columns(array('designation' => new Expression('DISTINCT designation')));
        $select->from('national_details');
        $statement = $sql->prepareStatementForSqlObject($select);
        $designation = $statement->execute();
        return $designation;
    }

    public function fetchAllGroupForCommunity($adapter)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('sub_menu');
        //SELECT * FROM `sub_menu` WHERE menu_id= '2' AND id IN(3,4,6)
        // $select->join('menu', 'menu.id = sub_menu.menu_id');
        $select->where(array('menu_id' => 3));
        $statement = $sql->prepareStatementForSqlObject($select);
        $Group = $statement->execute();


        return $Group;
    }

    public function fetchGraphData($adapter, $company_id = NULL, $group = NULL, $frequency = NULL, $indicator = NULL, $fromyear = NULL, $toyear = NULL, $country_id = NULL)
    {
        //$indicator_str = implode('","',$indicator);
        if ($indicator[0] == "") {
            $indicator = NULL;
        }
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('sub_menu');
        // $select->join('menu', 'menu.id = sub_menu.menu_id');
        $select->where(array('id' => $group));
        $statement = $sql->prepareStatementForSqlObject($select);
        $sub_menu = $statement->execute();
        $Gdata_arr = array();
        foreach ($sub_menu as $sub_menus) {
            $route = $sub_menus['route'];
            $label = $sub_menus['label'];
            $select = $sql->select();
            $select->from($route);

            if ($group != 12) {
                if ($frequency != NULL) {
                    $select->where(array($route . '.frequency_id' => $frequency, $route . '.company_id' => $company_id, $route . '.country_id' => $country_id, $route . '.status' => _A_));
                }
                if ($frequency == NULL && $indicator == NULL) {
                    $select->where(array($route . '.company_id' => $company_id/*,'frequency_id' => $milestone_result['milestone_duration_type_id']*/, $route . '.country_id' => $country_id, $route . '.status' => _A_/*,'type'=> $milestone_result['indicator_id']*/));
                }

                $select->join('indicator', 'indicator.id = ' . $route . '.type', array('indicator_name' => 'name', 'group_id' => 'group_id'));
                $select->where->between($route . '.create_date', $fromyear, $toyear);
                $select->where(array($route . '.status' => _A_));
                $select->order(array($route . '.type ASC ', $route . '.create_date ASC'));
            } /*diversity*/
            else {
                // $select->join('national_details', $route . '.national_id = national_details.national_id', array('value' => new Expression('Count(*)'), 'gender' => 'gender', 'nationality' => 'nationality', 'age' => 'age', 'designation' => 'designation'));
                $select->join('national_details', $route . '.national_id = national_details.national_id', array('value' => new Expression(('Count(DISTINCT(employee_id))')), 'gender' => 'gender', 'nationality' => 'nationality', 'age' => 'age', 'designation' => 'designation'));

                $select->where->between($route . '.create_date', $fromyear, $toyear);
                $select->where(array($route . '.frequency_id' => $frequency, $route . '.company_id' => $company_id, $route . '.country_id' => $country_id, $route . '.status' => _A_));

                /*Gender*/
                if ($indicator[0] == 1) {
                    $select->group(array('national_details.gender'));
                    //$select->group(array($route . '.create_date '));
                    // $select->order(array('national_details.gender'));
                }
                /*Nationality*/
                if ($indicator[0] == 2) {
                    $select->group(array('national_details.nationality'));
                    // $select->group(array($route . '.create_date '));
                    // $select->order(array('national_details.nationality'));
                }
                /*Nationality*/
                if ($indicator[0] == 3) {
                    $select->group(array('national_details.age'));
                    // $select->group(array($route . '.create_date '));
                    //$select->order(array('national_details.age'));
                }
                /*Designation*/
                if ($indicator[0] == 4) {
                    $select->group(array('national_details.designation'));
                    //  $select->group(array($route . '.create_date '));
                    //$select->order(array('national_details.designation'));
                }
                $select->order(array($route . '.create_date ASC'));
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            $table_results = $statement->execute();
            $itr = 0;

            foreach ($table_results as $table_result) {
                /*diversity*/
                if ($indicator[0] == 1 && $group == 12) {
                    $Gdata_arr[$itr]['label'] = $table_result['gender'];
                    $Gdata_arr[$itr]['indicator_name'] = $table_result['gender'];
                    $Gdata_arr[$itr]['type'] = $table_result['gender'];
                    $Gdata_arr[$itr]['date'] = $toyear;
                } else if ($indicator[0] == 2 && $group == 12) {
                    $Gdata_arr[$itr]['label'] = $table_result['nationality'];
                    $Gdata_arr[$itr]['indicator_name'] = $table_result['nationality'];
                    $Gdata_arr[$itr]['type'] = $table_result['nationality'];
                    $Gdata_arr[$itr]['date'] = $toyear;
                } else if ($indicator[0] == 3 && $group == 12) {
                    $Gdata_arr[$itr]['label'] = $table_result['age'];
                    $Gdata_arr[$itr]['indicator_name'] = $table_result['age'];
                    $Gdata_arr[$itr]['type'] = $table_result['age'];
                    $Gdata_arr[$itr]['date'] = $toyear;
                } else if ($indicator[0] == 4 && $group == 12) {
                    $Gdata_arr[$itr]['label'] = $table_result['designation'];
                    $Gdata_arr[$itr]['indicator_name'] = $table_result['designation'];
                    $Gdata_arr[$itr]['type'] = $table_result['designation'];
                    $Gdata_arr[$itr]['date'] = $toyear;
                } else {
                    $Gdata_arr[$itr]['label'] = $label;
                    $Gdata_arr[$itr] = $table_result;
                    $Gdata_arr[$itr]['date'] = $table_result['create_date'];
                }
                // $select->join('measure_unit', 'measure_unit.indicator_id = '.$route.'.type',array('unit_description','carbon_value' => 'value'));
                // $select->where(array($route.'.company_id' => $company_id, $route.'.status' => _A_,$route.'.type'=> $indicator));


                $Gdata_arr[$itr]['value'] = $table_result['value'];


                if ($indicator == NULL) {
                    $Gdata_arr[$itr]['indicator_status'] = 'noindicator';
                }
                $itr++;
            }
        }
        return $Gdata_arr;
    }

    public function fetchGraphDataForDiversityVenn($adapter, $company_id = NULL, $group = NULL, $frequency = NULL, $indicator = NULL, $fromyear = NULL, $toyear = NULL, $country_id = NULL, $gender = NULL, $nationality = NULL, $designation = NULL)
    {
        //$indicator_str = implode('","',$indicator);
        $i1 = null;
        $i2 = null;
        $i3 = null;
        if ($indicator[0] == "") {
            $indicator = NULL;
        }
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('sub_menu');
        // $select->join('menu', 'menu.id = sub_menu.menu_id');
        $select->where(array('id' => $group));
        $statement = $sql->prepareStatementForSqlObject($select);
        $sub_menu = $statement->execute();
        $sets = array();
//        var sets = [
//            {"sets": [0], "label": "Pakistani", "size": 10},
//            {"sets": [1], "label": "manager", "size": 5},
//            {"sets": [2], "label": "female", "size": 5},
//            {"sets": [0, 1], "size": 5},
//            {"sets": [1, 2], "size":5}
//        ];
        foreach ($sub_menu as $sub_menus) {
            $route = $sub_menus['route'];
            $label = $sub_menus['label'];
            $select = $sql->select();
            $select->from($route);
            /*gender*/
            $select->join('national_details', $route . '.national_id = national_details.national_id', array('val' => new Expression('Count(*)')));
            $select->where->between($route . '.create_date', $fromyear, $toyear);
            $select->where(array($route . '.frequency_id' => $frequency, $route . '.company_id' => $company_id, $route . '.country_id' => $country_id, $route . '.status' => _A_));
            $select->where(array('national_details.gender' => $gender));

            $statement = $sql->prepareStatementForSqlObject($select);
            $gender_results = $statement->execute();
            $itr = 0;
            foreach ($gender_results as $gender_result) {
                $i1 = $itr;
                $sets[$itr]["sets"] = [$itr];
                $sets[$itr]["label"] = $gender;
                $sets[$itr]["size"] = (int)$gender_result['val'];


                $itr++;
            }
            $select = $sql->select();
            $select->from($route);
            /*nationality*/
            $select->join('national_details', $route . '.national_id = national_details.national_id', array('val' => new Expression('Count(*)')));
            $select->where->between($route . '.create_date', $fromyear, $toyear);
            $select->where(array($route . '.frequency_id' => $frequency, $route . '.company_id' => $company_id, $route . '.country_id' => $country_id, $route . '.status' => _A_));
            $select->where(array('national_details.nationality' => $nationality));

            $statement = $sql->prepareStatementForSqlObject($select);
            $nationality_results = $statement->execute();
            foreach ($nationality_results as $nationality_result) {
                $i2 = $itr;
                $sets[$itr]["sets"] = [$itr];
                $sets[$itr]["label"] = $nationality;
                $sets[$itr]["size"] = (int)$nationality_result['val'];
                $itr++;
            }
            $select = $sql->select();
            $select->from($route);
            /*designation*/
            $select->join('national_details', $route . '.national_id = national_details.national_id', array('val' => new Expression('Count(*)')));
            $select->where->between($route . '.create_date', $fromyear, $toyear);
            $select->where(array($route . '.frequency_id' => $frequency, $route . '.company_id' => $company_id, $route . '.country_id' => $country_id, $route . '.status' => _A_));
            $select->where(array('national_details.designation' => $designation));

            $statement = $sql->prepareStatementForSqlObject($select);
            $designation_results = $statement->execute();
            foreach ($designation_results as $designation_result) {
                $i3 = $itr;
                $sets[$itr]["sets"] = [$itr];
                $sets[$itr]["label"] = $designation;
                $sets[$itr]["size"] = (int)$designation_result['val'];
                $itr++;
            }
            $select = $sql->select();
            $select->from($route);
            /*gender and nationality*/
            $select->join('national_details', $route . '.national_id = national_details.national_id', array('val' => new Expression('Count(*)')));
            $select->where->between($route . '.create_date', $fromyear, $toyear);
            $select->where(array($route . '.frequency_id' => $frequency, $route . '.company_id' => $company_id, $route . '.country_id' => $country_id, $route . '.status' => _A_));
            $select->where(array('national_details.gender' => $gender, 'national_details.nationality' => $nationality));

            $statement = $sql->prepareStatementForSqlObject($select);
            $gender_nationality_results = $statement->execute();
            foreach ($gender_nationality_results as $gender_nationality_result) {
                $sets[$itr]["sets"] = [$i1, $i2];
                //  $sets[$itr]["label"] = $nationality;
                $sets[$itr]["size"] = (int)$gender_nationality_result['val'];
                $itr++;
            }
            $select = $sql->select();
            $select->from($route);
            /*nationality and designation*/
            $select->join('national_details', $route . '.national_id = national_details.national_id', array('val' => new Expression('Count(*)')));
            $select->where->between($route . '.create_date', $fromyear, $toyear);
            $select->where(array($route . '.frequency_id' => $frequency, $route . '.company_id' => $company_id, $route . '.country_id' => $country_id, $route . '.status' => _A_));
            $select->where(array('national_details.designation' => $designation, 'national_details.nationality' => $nationality));

            $statement = $sql->prepareStatementForSqlObject($select);
            $designation_nationality_results = $statement->execute();
            foreach ($designation_nationality_results as $designation_nationality_result) {
                $sets[$itr]["sets"] = [$i2, $i3];
                //  $sets[$itr]["label"] = $nationality;
                $sets[$itr]["size"] = (int)$designation_nationality_result['val'];
                $itr++;
            }
        }
        return $sets;
    }

    public function fetchEnergyMixGraphData($adapter, $company_id = NULL, $group = NULL, $frequency = NULL, $indicator = NULL, $from = NULL, $to = NULL, $country_id = NULL)
    {
        //$indicator_str = implode('","',$indicator);
        /* if ($indicator[0] == "") {
             $indicator = NULL;
         }*/
//        SELECT i.id,sm.route FROM `indicator` i
//INNER JOIN `sub_menu` sm ON i.`sub_menu_id` = sm.id
//WHERE
//
//i.energy_mix_id = 1
        if ($from == NULL) {
            $from = date('Y-01-01');
        }
        if ($to == NULL) {
            $to = date('Y-12-01');
        }
//     SELECT
// GROUP_CONCAT( DISTINCT(`sm`.`route`))AS route,
// GROUP_CONCAT( DISTINCT(`sm`.`label`))AS label,
// GROUP_CONCAT( `i`.`id`)AS ids
// -- `sm`.`route` AS `route`,
// -- `sm`.`label` AS `label` ,
//FROM
//  `indicator` AS `i`
//  INNER JOIN `sub_menu` AS `sm`
//    ON `i`.`sub_menu_id` = `sm`.`id`
//WHERE `i`.`energy_mix_id` = 1
//GROUP BY  `sm`.`route`
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->columns(array('ids' => new Expression('GROUP_CONCAT(DISTINCT i.id)')));
        $select->from(array('i' => 'indicator'));
        $select->join(array('sm' => 'sub_menu'), 'i.sub_menu_id = sm.id', array('route' => new Expression('GROUP_CONCAT(DISTINCT sm.route)'), 'label' => new Expression('GROUP_CONCAT(DISTINCT sm.label)')));
        $select->where(array('i.energy_mix_id' => 1));
        $select->group('sm.route');
        $statement = $sql->prepareStatementForSqlObject($select);
        $indicators = $statement->execute();
        $Gdata_arr = array();
        $itr = 0;
        foreach ($indicators as $indicator) {
            $route = $indicator['route'];
            $label = $indicator['label'];
            $ids = explode(',', $indicator['ids']);
            $select = $sql->select();
            $select->from($route);
//            if ($frequency != NULL) {
//                $select->where(array($route . '.frequency_id' => $frequency, $route . '.company_id' => $company_id,$route . '.country_id' => $country_id, $route . '.status' => _A_));
//            }
//            if ($frequency == NULL && $indicator == NULL) {
//                $select->where(array($route . '.company_id' => $company_id/*,'frequency_id' => $milestone_result['milestone_duration_type_id']*/,$route . '.country_id' => $country_id, $route . '.status' => _A_/*,'type'=> $milestone_result['indicator_id']*/));
//            }
            $select->join('indicator', 'indicator.id = ' . $route . '.type', array('indicator_name' => 'name'));
            if ($company_id != NULL && $country_id != NULL) {
                $select->where(array($route . '.company_id' => $company_id, $route . '.country_id' => $country_id));
            }
            $select->where->between($route . '.create_date', $from, $to);
            $select->where(array($route . '.type' => $ids, $route . '.status' => _A_));
            $select->order(array($route . '.type ASC ', $route . '.create_date ASC'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $table_results = $statement->execute();
            foreach ($table_results as $table_result) {

                // $select->join('measure_unit', 'measure_unit.indicator_id = '.$route.'.type',array('unit_description','carbon_value' => 'value'));
                // $select->where(array($route.'.company_id' => $company_id, $route.'.status' => _A_,$route.'.type'=> $indicator));
                $Gdata_arr[$itr] = $table_result;
                $Gdata_arr[$itr]['label'] = $label;
                $Gdata_arr[$itr]['value'] = $table_result['value'];
                $Gdata_arr[$itr]['date'] = $table_result['create_date'];


                if ($indicator == NULL) {
                    $Gdata_arr[$itr]['indicator_status'] = 'noindicator';
                }
                $itr++;
            }
        }
//        if(!empty($Gdata_arr)){
//            foreach ($Gdata_arr as $key => $row) {
//                $indicator_name[$key] = $row['indicator_name'];
//            }
//
//            array_multisort($indicator_name, SORT_ASC, $Gdata_arr);
//        }

        return $Gdata_arr;
    }

    public function fetchAllGroupForOurPeople($adapter)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('sub_menu');
        //SELECT * FROM `sub_menu` WHERE menu_id= '2' AND id IN(3,4,6)
        // $select->join('menu', 'menu.id = sub_menu.menu_id');
        $select->where(array('menu_id' => 4));
        $statement = $sql->prepareStatementForSqlObject($select);
        $Group = $statement->execute();
        return $Group;
    }

    public function fetchAll_admin($adapter, $paginated = false, $table_name, $dsn, $username, $password, $types)
    {

        if ($paginated) {
            $userSession = new Container('user');
            $sql = new Sql($adapter);
            $select_year = $sql->select();
            $select_year->columns(array('year' => new expression('distinct(YEAR(create_date))')));
            $select_year->from($table_name);
            $select_year->order(array('year' => 'DESC'));
            $statement_year = $sql->prepareStatementForSqlObject($select_year);
            $years = $statement_year->execute();
            $data = array();

            $companies = $userSession->user_companies;
            $countries = $userSession->user_countries;
            $regions = $userSession->user_regions;
            $j = 0;
            foreach ($years as $year) {
                for ($i = 0; $i < count($types); $i++) {
                    for ($m = 0; $m < count($companies); $m++) {
                        for ($k = 0; $k < count($countries); $k++) {
                            for ($l = 0; $l < count($regions); $l++) {
                                $pdo = new PDO($dsn, $username, $password);
                                $uresult = $pdo->prepare('CALL sp_check_dummy_rows_admin("' . $table_name . '","' . $companies[$m] . '","' . $countries[$k] . '","' . $regions[$l] . '","' . $types[$i] . '","' . $year['year'] . '")');
                                $uresult->execute();
                                $row = $uresult->fetchAll(PDO::FETCH_ASSOC);

                                foreach ($row as $result) {
                                    if ($result['user_id'] == NULL && ((int)$data[$j - 1]['type'] != $types[$i] || (int)$data[$j - 1]['company_id'] != $companies[$m] || (int)$data[$j - 1]['region_id'] != $regions[$l] || (int)$data[$j - 1]['country_id'] != $countries[$k])) {

                                    } else if ($result['user_id'] == NULL) {

                                        $data[$j] = $data[$j - 1];
                                        $create_date = strtotime($data[$j]['create_date']);
                                        $create_date = date("Y-m-d", strtotime("+1 month", $create_date));
                                        $data[$j]['create_date'] = $create_date;

                                        $expiry_date = strtotime($data[$j]['expiry_date']);
                                        $expiry_date = date("Y-m-d", strtotime("+1 month", $expiry_date));
                                        $data[$j]['expiry_date'] = $expiry_date;
                                        $data[$j]['status'] = _W_;
                                        $data[$j]['value'] = '-';
                                    } else {
                                        $data[$j] = $result;
                                    }
                                    $j++;
                                }
                            }
                        }
                    }
                }
            }
            // }

            // create a new pagination adapter object
            $paginatorAdapter = new ArrayAdapter(
            // our configured select object
                $data
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function fetchAll_champion($adapter, $paginated = false, $table_name, $dsn, $username, $password, $types)
    {

        if ($paginated) {
            $userSession = new Container('user');
            $user_id = $userSession->user_id;
            $sql = new Sql($adapter);
            $select_year = $sql->select();
            $select_year->columns(array('year' => new expression('distinct(YEAR(create_date))')));
            $select_year->from($table_name);
            $select_year->order(array('year' => 'DESC'));
            $statement_year = $sql->prepareStatementForSqlObject($select_year);
            $years = $statement_year->execute();
            $data = array();

            $companies = $userSession->user_companies;
            $countries = $userSession->user_countries;
            $regions = $userSession->user_regions;;
            $j = 0;
            foreach ($years as $year) {


                for ($i = 0; $i < count($types); $i++) {
                    for ($m = 0; $m < count($companies); $m++) {
                        for ($k = 0; $k < count($countries); $k++) {
                            for ($l = 0; $l < count($regions); $l++) {
                                $pdo = new PDO($dsn, $username, $password);
                                $uresult = $pdo->prepare('CALL sp_check_dummy_rows_champion("' . $table_name . '","' . $companies[$m] . '","' . $countries[$k] . '","' . $regions[$l] . '","' . $types[$i] . '","' . $year["year"] . '","' . $user_id . '")');
                                $uresult->execute();
                                $row = $uresult->fetchAll(PDO::FETCH_ASSOC);

                                foreach ($row as $result) {

                                    if ($result['user_id'] == NULL && ((int)$data[$j - 1]['type'] != $types[$i] || (int)$data[$j - 1]['company_id'] != $companies[$m] || (int)$data[$j - 1]['region_id'] != $regions[$l] || (int)$data[$j - 1]['country_id'] != $countries[$k])) {

                                    } else if ($result['user_id'] == NULL) {

                                        $data[$j] = $data[$j - 1];
                                        $create_date = strtotime($data[$j]['create_date']);
                                        $create_date = date("Y-m-d", strtotime("+1 month", $create_date));
                                        $data[$j]['create_date'] = $create_date;

                                        $expiry_date = strtotime($data[$j]['expiry_date']);
                                        $expiry_date = date("Y-m-d", strtotime("+1 month", $expiry_date));
                                        $data[$j]['expiry_date'] = $expiry_date;
                                        $data[$j]['status'] = _W_;
                                        $data[$j]['value'] = '-';
                                    } else {
                                        $data[$j] = $result;
                                    }
                                    $j++;
                                }
                            }
                        }
                    }
                }
            }

            // create a new pagination adapter object
            $paginatorAdapter = new ArrayAdapter(
            // our configured select object
                $data
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function fetchBulkDetails($adapter, $section, $parent_id)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bulk_detail');
        if ($section == 'event') {
            $select->where(array('section_name' => $section, 'parent_id' => $parent_id));
            $statement = $sql->prepareStatementForSqlObject($select);
            $bulk_details = $statement->execute();
        } else if ($section == 'transport') {
            $select->where(array('section_name' => $section, 'parent_id' => $parent_id));
            $statement = $sql->prepareStatementForSqlObject($select);
            $bulk_details = $statement->execute();
        } else if ($section == 'national') {
            $select->where(array('section_name' => $section, 'parent_id' => $parent_id));
            $statement = $sql->prepareStatementForSqlObject($select);
            $bulk_details = $statement->execute();
        }
        return $bulk_details;
    }

    public function fetchIndicatorById($adapter, $id)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('indicator');
        $select->where(array('id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

}


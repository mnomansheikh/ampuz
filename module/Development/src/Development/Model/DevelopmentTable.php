<?php
namespace Energy\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Http\Header\Date;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
class EnergyTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    public function fetchAll($adapter)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;
        // $resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('development');
        $select->where(array('development.user_id' => $user_id,'development.status'=>_P_));
        $select->join('user_profile', 'user_profile.user_id = development.user_id','user_name');
        $select->join('company', 'company.company_id = development.company_id','company_name');
        $select->join('frequency', 'frequency.frequency_id = development.frequency_id','frequency_name');
        $select->join('indicator', 'indicator.id = development.type','name');
        $select->order('development_id DESC');


        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function fetchAll_development($adapter, $id)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id = $userSession->user_id;
        //$resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        // create a new Select object for the table community
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('development');
        $select->where(array('development_id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function fetchAll_development_details($adapter, $id)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id = $userSession->user_id;
        //$resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        // create a new Select object for the table community
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bulk_detail');
        $select->where(array('section_name' => 'development', 'parent_id' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function fetchAll_admin($adapter, $paginated = false)
    {

        if ($paginated) {
            // create a new Select object for the table community
            $userSession = new Container('user');

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('development');
            $select->join('user_profile', 'user_profile.user_id = development.user_id',array('user_name','user_email'));
            $select->join('company', 'company.company_id = development.company_id','company_name');
            $select->join('country', 'country.id = development.country_id','country_name');
            $select->join('region', 'region.id = development.region_id','region_description');
            $select->join('frequency', 'frequency.frequency_id = development.frequency_id','frequency_name');
            $select->join('indicator', 'indicator.id = development.type','name');
            $select->where(array('development.company_id'=>$userSession->user_companies,'development.country_id'=>$userSession->user_countries,'development.region_id'=>$userSession->user_regions));
            $select->order('development_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Energy());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function fetchAll_champion($adapter,$paginated=false)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('development');
            $select->where(array('development.user_id'=>$user_id));
            $select->join('user_profile', 'user_profile.user_id = development.user_id','user_name');
            $select->join('company', 'company.company_id = development.company_id','company_name');
            $select->join('frequency', 'frequency.frequency_id = development.frequency_id','frequency_name');
            $select->join('indicator', 'indicator.id = development.type','name');
            $select->order('development_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Energy());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function getEnergy($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('development_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveEnergy(Energy $development, $adapter)
    {
        {
            $sql = new Sql($adapter);
            $id = $development->development_id;
            $colname1 = '';
            $colname2 = '';
            $colname3 = '';

            $col1 = "";
            $col2 = "";
            $col3 = "";
            $col4 = "";
            $col5 = "";
            $col6 = "";
            $col7 = "";
            $col8 = "";
            $col9 = "";
            $col10 = "";
            if (empty($development->bulk_type) || $development->bulk_type == 0) {
                $count = 1;
                foreach ($development->col AS $columns) {
                    ${'colname' . $count} = $columns;
                    $count++;
                }
            } else {
                for($count=1; $count<=_BULK_FIELD_EXTRA_; $count++){
                    if(!empty($development->{'col'.$count})){
                        foreach($development->{'col'.$count} AS $item){
                            //if(!empty($item)){
                            ${'col'.$count}[] = $item;
                            //}
                        }
                        //${'col'.$count} = $development->{'col'.$count};
                    }else{
                        ${'col'.$count} = '';
                    }
                }
            }

            $userSession = new Container('user');
            $user_id = $userSession->user_id;
            $user_type = $userSession->user_type;
            $oneYearOn = date('Y-m-d', strtotime(date("Y-m-d", time()) . " + 365 day"));
            //development detail table
            $id_trim = ltrim($development->employee_id, ',');
            $name_trim = ltrim($development->employee_name, ',');
            $travel_from_trim = ltrim($development->travel_from, ',');
            $travel_to_trim = ltrim($development->travel_to, ',');
            $travel_date_trim = ltrim($development->travel_date, ',');
            $travel_distance_trim = ltrim($development->travel_distance, ',');
            $dob_trim = ltrim($development->dob, ',');
            $nationality_trim = ltrim($development->nationality, ',');
            $gender_trim = ltrim($development->gender, ',');
            $designation_trim = ltrim($development->designation, ',');
            $date = ltrim($development->date, ',');
            $emp_ids = explode(',', $id_trim);
            $names = explode(',', $name_trim);
            $travel_from = explode(',', $travel_from_trim);
            $travel_to = explode(',', $travel_to_trim);
            $travel_date = explode(',', $travel_date_trim);
            $travel_distance = explode(',', $travel_distance_trim);
            $dobs = explode(',', $dob_trim);
            $nationalities = explode(',', $nationality_trim);
            $genders = explode(',', $gender_trim);
            $designations = explode(',', $designation_trim);
            $date = explode(',', $date);
            if ($id == 0) {
                if ($development->indicator_type == _ATTACHMENT_ || $development->indicator_type == _UPLOAD_) {
                    $data = array(
                        'development_id' => $development->development_id,
                        'company_id' => $development->company_id,
                        'region_id' => $development->region_id,
                        'country_id' => $development->country_id,
                        'frequency_id' => $development->frequency_id,
                        'type' => $development->type,
                        'value' => $development->value,
                        'status' => _P_,
                        'user_id' => $user_id,
                        'create_date' => $development->create_date,
                        'expiry_date' => $development->expire_date,
                        'file_name' => $development->filename,
                        'org_name' => str_replace('excels/', '', $development->org_name),
                        'indicator_type' => $development->indicator_type,
                        'col1' => $colname1,
                        'col2' => $colname2,
                        'col3' => $colname3
                    );
                } else {
                    $data = array(
                        'development_id' => $development->development_id,
                        'company_id' => $development->company_id,
                        'region_id' => $development->region_id,
                        'country_id' => $development->country_id,
                        'frequency_id' => $development->frequency_id,
                        'type' => $development->type,
                        'value' => $development->value,
                        'status' => _P_,
                        'user_id' => $user_id,
                        'create_date' => $development->create_date,
                        'expiry_date' => $development->expire_date,
                        'col1' => $colname1,
                        'col2' => $colname2,
                        'col3' => $colname3
                    );
                }

                $development_query = $this->tableGateway->insert($data);

                if ($development->indicator_type == _UPLOAD_) {
                    $development_id = $this->tableGateway->lastInsertValue;
                    if ($development_query == true) {
                        foreach ($emp_ids as $index => $name) {
                            if ($development->bulk_type == 1) {
                                $data1 = array(
                                    'employee_id' => $emp_ids[$index],
                                    'employee_name' => $names[$index],
                                    'employee_designation' => $designations[$index],
                                    'create_date' => $date[$index],
                                    'bulk_type' => $development->bulk_type,
                                    'parent_id' => $development_id,
                                    'section_name' => 'development',
                                    'col1' => $col1[$index],
                                    'col2' => $col2[$index],
                                    'col3' => $col3[$index],
                                    'col4' => $col4[$index],
                                    'col5' => $col5[$index],
                                    'col6' => $col6[$index],
                                    'col7' => $col7[$index],
                                    'col8' => $col8[$index],
                                    'col9' => $col9[$index],
                                    'col10' => $col10[$index],
                                );
                            } else if ($development->bulk_type == 2) {
                                $data1 = array(
                                    'employee_id' => $emp_ids[$index],
                                    'employee_name' => $names[$index],
                                    'employee_dob' => date('Y-m-d', strtotime($dobs[$index])),
                                    'employee_age' => date_diff(date_create($dobs[$index]), date_create('today'))->y,
                                    'employee_nationality' => $nationalities[$index],
                                    'gender' => $genders[$index],
                                    'employee_designation' => $designations[$index],
                                    'bulk_type' => $development->bulk_type,
                                    'parent_id' => $development_id,
                                    'section_name' => 'development',
                                    'col1' => $col1[$index],
                                    'col2' => $col2[$index],
                                    'col3' => $col3[$index],
                                    'col4' => $col4[$index],
                                    'col5' => $col5[$index],
                                    'col6' => $col6[$index],
                                    'col7' => $col7[$index],
                                    'col8' => $col8[$index],
                                    'col9' => $col9[$index],
                                    'col10' => $col10[$index],
                                );
                            } else if ($development->bulk_type == 3) {
                                $data1 = array(
                                    'employee_id' => $emp_ids[$index],
                                    'employee_name' => $names[$index],
                                    'travel_from' => $travel_from[$index],
                                    'travel_to' => $travel_to[$index],
                                    'travel_distance' => $travel_distance[$index],
                                    'travel_date' => date_format(date_create($travel_date[$index]), "Y-m-d"),
                                    'bulk_type' => $development->bulk_type,
                                    'parent_id' => $development_id,
                                    'section_name' => 'development',
                                    'col1' => $col1[$index],
                                    'col2' => $col2[$index],
                                    'col3' => $col3[$index],
                                    'col4' => $col4[$index],
                                    'col5' => $col5[$index],
                                    'col6' => $col6[$index],
                                    'col7' => $col7[$index],
                                    'col8' => $col8[$index],
                                    'col9' => $col9[$index],
                                    'col10' => $col10[$index],
                                );
                            }

                            $insert = $sql->insert('bulk_detail');
                            //  $insert->columns($columns);
                            $insert->values($data1);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }

                    }
                }
            } else {
                if ($this->getEnergy($id)) {
                    if ($user_type == _ADMIN_) {
                        $data = array(
                            /*'company_id' => $development->company_id,
                            'frequency_id' => $development->frequency_id,
                            'region_id'        => $development->region_id,
                            'country_id'        => $development->country_id,
                            'type'      => $development->type,*/
                            'value' => $development->value,
                            'status' => _A_,
                            'col1' => $colname1,
                            'col2' => $colname2,
                            'col3' => $colname3

                        );
                        if ($development->indicator_type == _UPLOAD_) {
                            // if file re uploaded
                            if ($development->emp_id != NULL) {
                                $delete_row = $sql->delete('bulk_detail');
                                $delete_row->where(array('section_name' => 'development', 'parent_id' => (int)$id));
                                $statement = $sql->prepareStatementForSqlObject($delete_row);
                                $results = $statement->execute();
                                foreach ($emp_ids as $index => $name) {
                                    if ($development->bulk_type == 1) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'employee_designation' => $designations[$index],
                                            'create_date' => $date[$index],
                                            'bulk_type' => $development->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'development',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    } else if ($development->bulk_type == 2) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'employee_dob' => date('Y-m-d', strtotime($dobs[$index])),
                                            'employee_age' => date_diff(date_create($dobs[$index]), date_create('today'))->y,
                                            'employee_nationality' => $nationalities[$index],
                                            'gender' => $genders[$index],
                                            'employee_designation' => $designations[$index],
                                            'bulk_type' => $development->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'development',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    } else if ($development->bulk_type == 3) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'travel_from' => $travel_from[$index],
                                            'travel_to' => $travel_to[$index],
                                            'travel_distance' => $travel_distance[$index],
                                            'travel_date' => date_format(date_create($travel_date[$index]), "Y-m-d"),
                                            'bulk_type' => $development->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'development',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    }
                                    $insert = $sql->insert('bulk_detail');
                                    //  $insert->columns($columns);
                                    $insert->values($data1);
                                    $statement = $sql->prepareStatementForSqlObject($insert);
                                    $results = $statement->execute();
                                }
                            }
                        }
                    }
                    if ($user_type == _CHAMPION_) {
                        $data = array(
                            /*  'company_id' => $development->company_id,
                              'region_id'        => $development->region_id,
                              'country_id'        => $development->country_id,
                              'frequency_id' => $development->frequency_id,
                              'type'      => $development->type,*/
                            'value' => $development->value,
                            'status' => _P_,
                            'col1' => $colname1,
                            'col2' => $colname2,
                            'col3' => $colname3
                        );
                        if ($development->indicator_type == _UPLOAD_) {
                            // if file re uploaded
                            if ($development->employee_id != NULL) {
                                $delete_row = $sql->delete('bulk_detail');
                                $delete_row->where(array('section_name' => 'development', 'parent_id' => (int)$id));
                                $statement = $sql->prepareStatementForSqlObject($delete_row);
                                $results = $statement->execute();
                                foreach ($emp_ids as $index => $name) {
                                    if ($development->bulk_type == 1) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'employee_designation' => $designations[$index],
                                            'create_date' => $date[$index],
                                            'bulk_type' => $development->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'development',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    } else if ($development->bulk_type == 2) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'employee_dob' => date('Y-m-d', strtotime($dobs[$index])),
                                            'employee_age' => date_diff(date_create($dobs[$index]), date_create('today'))->y,
                                            'employee_nationality' => $nationalities[$index],
                                            'gender' => $genders[$index],
                                            'employee_designation' => $designations[$index],
                                            'bulk_type' => $development->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'development',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    } else if ($development->bulk_type == 3) {
                                        $data1 = array(
                                            'employee_id' => $emp_ids[$index],
                                            'employee_name' => $names[$index],
                                            'travel_from' => $travel_from[$index],
                                            'travel_to' => $travel_to[$index],
                                            'travel_distance' => $travel_distance[$index],
                                            'travel_date' => date_format(date_create($travel_date[$index]), "Y-m-d"),
                                            'bulk_type' => $development->bulk_type,
                                            'parent_id' => $id,
                                            'section_name' => 'development',
                                            'col1' => $col1[$index],
                                            'col2' => $col2[$index],
                                            'col3' => $col3[$index],
                                            'col4' => $col4[$index],
                                            'col5' => $col5[$index],
                                            'col6' => $col6[$index],
                                            'col7' => $col7[$index],
                                            'col8' => $col8[$index],
                                            'col9' => $col9[$index],
                                            'col10' => $col10[$index],
                                        );
                                    }
                                    $insert = $sql->insert('bulk_detail');
                                    //  $insert->columns($columns);
                                    $insert->values($data1);
                                    $statement = $sql->prepareStatementForSqlObject($insert);
                                    $results = $statement->execute();
                                }
                            }
                        }
                    }
                    $this->tableGateway->update($data, array('development_id' => $id));
                } else {
                    throw new \Exception('Development id does not exist');
                }

            }
        }
    }

    public function deleteDevelopment($id, $adapter)
    {
        $sql = new Sql($adapter);
        $delete = $sql->delete();
        $delete->from('bulk_detail');
        $delete->where(array('section_name' => 'development', 'parent_id' => (int)$id));
        $statement = $sql->prepareStatementForSqlObject($delete);
        $results = $statement->execute();
        $this->tableGateway->delete(array('development_id' => (int)$id));

    }
}
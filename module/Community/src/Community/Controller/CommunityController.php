<?php
namespace Community\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Community\Form\CommunityForm;
use Community\Model\Community;
use Community\Model\CommunityTable;
use Zend\Session\Container;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class CommunityController extends AbstractActionController
{
    protected $CommunityTable;
    protected $PartnersTable;
    protected $authservice;
    protected $TrainingTable;
    protected $EventTable;

    public function getMenu()
    {
        if (!$this->MenuTable) {
            $sm = $this->getServiceLocator();
            $this->MenuTable = $sm->get('Application\Model\MenuTable');
        }
        return $this->MenuTable;
    }

    public function getCommunityTable()
    {
        if (!$this->CommunityTable) {
            $sm = $this->getServiceLocator();
            $this->CommunityTable = $sm->get('Community\Model\CommunityTable');
        }
        return $this->CommunityTable;
    }

    public function getPartnersTable()
    {
        if (!$this->PartnersTable) {
            $sm = $this->getServiceLocator();
            $this->PartnersTable = $sm->get('Community\Model\CommunityTable\Partners');
        }
        return $this->PartnersTable;
    }

    public function getEventTable()
    {
        if (!$this->EventTable) {
            $sm = $this->getServiceLocator();
            $this->EventTable = $sm->get('Event\Model\EventTable');
        }
        return $this->EventTable;
    }

    public function getTrainingTable()
    {
        if (!$this->TrainingTable) {
            $sm = $this->getServiceLocator();
            $this->TrainingTable = $sm->get('Training\Model\TrainingTable');
        }
        return $this->TrainingTable;
    }

    public function getAuthService()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }
        return $this->authservice;
    }

    public function indexAction()
    {

        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            //for session varibles
            $form = new CommunityForm();
            $form->get('submit')->setValue('Request Indicator');
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $config = $this->getServiceLocator()->get('Config');
            $dsn = $config['db']['dsn'];
            $username = $config['db']['username'];
            $password = $config['db']['password'];
            $controllerClass= __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
           // $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            //$Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            //$Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsIDByGroup($dbAdapter,$moduleNamespace);
            //$frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
           // $record_expiration = $this->getMenu()->RecordExpiration($dbAdapter,$moduleNamespace,$Indicators,$Companies,$Countries,$frequencies);

            if ($userSession->user_type == _ADMIN_) {
                // grab the paginator from the CommunityTable
                $paginator = $this->getMenu()->fetchAll_admin($dbAdapter, true,$moduleNamespace,$dsn,$username,$password,$Indicators);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('community/admin');
                return $view;
            }

            if ($userSession->user_type == _CHAMPION_) {
//                $form = new CommunityForm($dbAdapter);
//                $form->get('submit')->setValue('Save');
                /* $userSession = new Container('user');
                 $username = $userSession->username;*/
                $request = $this->getRequest();

                if ($request->isPost()) {
                    $community = new Community();
                    $id = (int)$this->params()->fromRoute('id', 0);
                    $form->setInputFilter($community->getInputFilter($id));
                    $form->setData($request->getPost());

                    if ($form->isValid()) {

                        //for list of community added

                        $community->exchangeArray($form->getData());
                        $this->getCommunityTable()->saveCommunity($community, null, $id);
                        // Redirect to list of community
                        return $this->redirect()->toRoute('community');
                    }
                }
                $paginator = $this->getMenu()->fetchAll_champion($dbAdapter, true,$moduleNamespace,$dsn,$username,$password,$Indicators);

                // $paginator = $this->getCommunityTable()->fetchAll_champion($dbAdapter, true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('community/index');
                return $view;
            }

        } else {
            return $this->redirect()->toRoute('login');
        }

    }

    public function addAction()
    {

        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            //for session varibles
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $community_partner_id = (int)$this->params()->fromRoute('community_partner_id', 0);
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $controllerClass = __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter, $moduleNamespace);
            $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter, $moduleNamespace);
            $DateField = $this->getMenu()->DateField();

            if ($userSession->user_type == _ADMIN_) {


                // grab the paginator from the CommunityTable
                $paginator = $this->getCommunityTable()->fetchAll_admin($dbAdapter, true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                ));
                $view->setTemplate('community/admin');
                return $view;
            }

            if ($userSession->user_type == _CHAMPION_) {
                //    $id = $this->UserAuthentication()->getIdentity()->getId();
                $form = new CommunityForm($Companies, $Regions, $Countries, $Indicators, $frequencies, $DateField);
                $form->get('submit')->setValue('Save');
                /* $userSession = new Container('user');
                 $username = $userSession->username;*/
                $request = $this->getRequest();
                if ($request->isPost()) {
                    $community = new Community();
                    $id = (int)$this->params()->fromRoute('id', 0);
                    $created_date = $this->getRequest()->getPost('create_date');
                    $indicator_id = $this->getRequest()->getPost('type');
                    $company_id = $this->getRequest()->getPost('company_id');
                    $country_id = $this->getRequest()->getPost('country_id');
                    $frequency_id = $this->getRequest()->getPost('frequency_id');
                    $date_validation = $this->getMenu()->DateFieldValidation($dbAdapter, $moduleNamespace, $created_date, $indicator_id, $company_id, $country_id, $frequency_id);
                    $form->setInputFilter($community->getInputFilter($date_validation));
                    $form->setData($request->getPost());

                    if ($form->isValid()) {

                        //for list of community added

                        $community->exchangeArray($form->getData());
                        $this->getMenu()->StatusChangeApproved($dbAdapter, $moduleNamespace, $indicator_id, $company_id, $country_id, $frequency_id);
                        $expire_date = $this->getMenu()->FrequencyChecker($frequency_id, $created_date);
                        $community->expire_date = $expire_date;
                        $this->getCommunityTable()->saveCommunity($community, null, $community_partner_id);
                        // Redirect to list of community
                        return $this->redirect()->toRoute('community');
                    }
                }
                //$this->getPartnersTable()->fetchAllPartners();
                return array('form' => $form, 'community_partner_id' => $community_partner_id, 'communities' => $this->getCommunityTable()->fetchAll_p1(), 'communities2' => $this->getCommunityTable()->fetchAll_p2(), 'communities3' => $this->getCommunityTable()->fetchAll_p3(), 'communities4' => $this->getCommunityTable()->fetchAll_p4(), 'communities5' => $this->getCommunityTable()->fetchAll_p5(), 'communities6' => $this->getCommunityTable()->fetchAll_p6(), 'partners' => $this->getPartnersTable()->fetchAllPartners());
            }
        } else {
            return $this->redirect()->toRoute('login');
        }

    }

    public function Graph1Action()
    {
        $company_id = '';
        $region_id = '';
        $country_id = '';
        $group = '';
        $frequency = '';
        $indicator = '';
        $toyear = '';
        $fromyear = '';
        if ($_REQUEST) {
            extract($_REQUEST);
            $company_id;
            $region_id;
            $country_id;
            $indicator;
            $toyear;
            $fromyear;
            $group;
            $frequency;
        }
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            //for session varibles
            $userSession = new Container('user');
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $controllerClass = __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            // $form = new MilestoneForm($dbAdapter);
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            if ($userSession->user_type == _ADMIN_) {
                // grab the paginator from the CommunityTable
                //   $paginator = $this->getMilestoneTable()->fetchAll_admin($dbAdapter,true);
                // set the current page to what has been passed in query string, or to 1 if none set
                //   $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                //   $paginator->setItemCountPerPage(10);
                //  $GData = $this->getMilestoneTable()->fetchGraphData($dbAdapter,$fromyear,$toyear,$company_id,$event_desc);
                $GData = $this->getMenu()->fetchGraphData($dbAdapter, $company_id, $group, $frequency, $indicator, $fromyear, $toyear, $country_id);

                $Groups = $this->getMenu()->fetchAllGroupForCommunity($dbAdapter);
                $Companies = $this->getMenu()->fetchCompany($dbAdapter);
                $Regions = $this->getMenu()->fetchRegion($dbAdapter);
                $Countries = $this->getMenu()->fetchCountry($dbAdapter);
                $Indicators = $this->getMenu()->fetchIndicator($dbAdapter);
                $Frequencies = $this->getMenu()->fetchFrequency($dbAdapter);
                $company_name = $this->getMenu()->fetchCompanyById($dbAdapter, $company_id);
                $country_name = $this->getMenu()->fetchCountryById($dbAdapter, $country_id);
                $region_name = $this->getMenu()->fetchRegionById($dbAdapter, $region_id);
                $group_name = $this->getMenu()->fetchGroupById($dbAdapter, $group);
                $view = new ViewModel(array(
                    'GData' => $GData,
                    'Groups' => $Groups,
                    'Companies' => $Companies,
                    'Regions' => $Regions,
                    'Countries' => $Countries,
                    'Indicators' => $Indicators,
                    'Frequencies' => $Frequencies,
                    'company_name' => $company_name,
                    'country_name' => $country_name,
                    'region_name' => $region_name,
                    'group_name' => $group_name,
                    'moduleNamespace' => $moduleNamespace

                ));

                $view->setTemplate('community/analytic');
                return $view;
            }
            if ($userSession->user_type == _CHAMPION_) {
                return $this->redirect()->toRoute('application');
            }
        } else {
            return $this->redirect()->toRoute('login');
        }

    }

    public function editAction()
    {

        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $id = (int)$this->params()->fromRoute('id', 0);
            $community_partner_id = (int)$this->params()->fromRoute('community_partner_id', 0);
            /*  if (!$id) {
                  return $this->redirect()->toRoute('album', array(
                      'action' => 'add'
                  ));
              }*/

            // Get the Community with the specified id.  An exception is thrown
            // if it cannot be found, in which case go to the index page.
            try {
                $community = $this->getCommunityTable()->getCommunity($id);
            } catch (\Exception $ex) {
                return $this->redirect()->toRoute('community', array(
                    'action' => 'index'
                ));
            }
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $controllerClass = __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter, $moduleNamespace);
            $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter, $moduleNamespace);
            $DateField = $this->getMenu()->DateField();
            $form = new CommunityForm($Companies, $Regions, $Countries, $Indicators, $frequencies, $DateField);
            $form->bind($community);
            //for session varibles
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            if ($userSession->user_type == _ADMIN_) {
                $form->get('submit')->setAttribute('value', 'Update & Approval');
            }
            if ($userSession->user_type == _CHAMPION_) {
                $form->get('submit')->setAttribute('value', 'Update');
            }
            $request = $this->getRequest();
            if ($request->isPost()) {
                $form->setInputFilter($community->getInputFilter(null));
                $form->setData($request->getPost());

                if ($form->isValid()) {
                    $this->getCommunityTable()->saveCommunity($community, $id, $community_partner_id);

                    // Redirect to list of Community
                    return $this->redirect()->toRoute('community');
                }
            }
            return array(
                'id' => $id,
                'community_partner_id' => $community_partner_id,
                'form' => $form,
                'partners' => $this->getPartnersTable()->fetchAllPartners()
            );
        } else {
            return $this->redirect()->toRoute('login');
        }
    }
    //for Admin User Dashboard
    /*  public function adminAction(){
          return array('communities' => $this->getCommunityTable()->fetchAll());
      }*/
    // Add content to the following method:
    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('community');
        }
        if ($id) {
            $this->getCommunityTable()->deleteCommunity($id);
            return $this->redirect()->toRoute('community');
        }

    }


}
<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 8/4/2015
 * Time: 12:13 PM
 */


namespace Community\Model;
// Add these import statements
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
class Community implements InputFilterAwareInterface
{


    public $company_id;
    public $region_id;
    public $country_id;
    public $frequency_id;
    public $type;
    public $value;
    public $inputFilter;
    public $id;
    public $create_date;





    public function exchangeArray($data)
    {
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->company_id           = (!empty($data['company_id'])) ? $data['company_id'] : null;
        $this->region_id           = (!empty($data['region_id'])) ? $data['region_id'] : null;
        $this->country_id           = (!empty($data['country_id'])) ? $data['country_id'] : null;
        $this->frequency_id         = (!empty($data['frequency_id'])) ? $data['frequency_id'] : null;
        $this->type                 = (!empty($data['type'])) ? $data['type'] : null;
        $this->value                = (!empty($data['value'])) ? $data['value'] : null;
        $this->create_date                   = (!empty($data['create_date'])) ? $data['create_date'] : null;
    }

// Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter($date_validation=null)
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
           if($date_validation != null){
               $inputFilter->add(
                   $date_validation
               );
           }
        $inputFilter->add(array(
                'name'     => 'id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'company_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                /*     'validators' => array(
                         array(
                             'name'    => 'StringLength',
                             'options' => array(
                                 'encoding' => 'UTF-8',
                                 'min'      => 1,
                                 'max'      => 100,
                             ),
                         ),
                     ),*/
            ));

            $inputFilter->add(array(
                'name'     => 'frequency_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                /* 'validators' => array(
                     array(
                         'name'    => 'StringLength',
                         'options' => array(
                             'encoding' => 'UTF-8',
                             'min'      => 1,
                             'max'      => 100,
                         ),
                     ),
                 ),*/
            ));

//            $inputFilter->add(array(
//                'name'     => 'type',
//                'required' => true,
//                'filters'  => array(
//                    array('name' => 'StripTags'),
//                    array('name' => 'StringTrim'),
//                ),
//                'validators' => array(
//                    array(
//                        'name'    => 'StringLength',
//                        'options' => array(
//                            'encoding' => 'UTF-8',
//                            'min'      => 1,
//                            'max'      => 100,
//                        ),
//                    ),
//                ),
//            ));

            $inputFilter->add(array(
                'name'     => 'type',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'value',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
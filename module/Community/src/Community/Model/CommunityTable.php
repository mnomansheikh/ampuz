<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 8/4/2015
 * Time: 12:19 PM
 */

namespace Community\Model;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Http\Header\Date;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
class CommunityTable
{


    protected $tableGateway;
    protected $adapter;


    public function __construct(tableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }


    public function fetchAll($adapter)
    {
    //for session varibles
    $userSession = new Container('user');
    //  echo 'Logged in as ' . $userSession->username;
    $user_id=  $userSession->user_id;
        //$resultSet = $this->tableGateway->select();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('community');
        $select->where(array('community.user_id' => $user_id,'community.status'=>_P_));
        $select->join('user_profile', 'user_profile.user_id = community.user_id','user_name');
        $select->join('company', 'company.company_id = community.company_id','company_name');
        $select->join('frequency', 'frequency.frequency_id = community.frequency_id','frequency_name');
        $select->join('indicator', 'indicator.id = community.type','name');
        $select->order('id DESC');


        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    public function fetchAll_admin($adapter,$paginated=false)
    {

        if ($paginated) {
            // create a new Select object for the table community
            $userSession = new Container('user');

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('community');
            $select->join('user_profile', 'user_profile.user_id = community.user_id',array('user_name','user_email'));
            $select->join('company', 'company.company_id = community.company_id', 'company_name');
            $select->join('country', 'country.id = community.country_id','country_name');
            $select->join('region', 'region.id = community.region_id','region_description');
           // $select->join('department', 'department.department_id = user_profile.department_id', 'department_name');
            $select->join('frequency', 'frequency.frequency_id = community.frequency_id', 'frequency_name');
            $select->join('indicator', 'indicator.id = community.type','name');
            $select->where(array('community.company_id'=>$userSession->user_companies,'community.country_id'=>$userSession->user_countries,'community.region_id'=>$userSession->user_regions));
            $select->order('community.id DESC');


            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Community());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

        public function fetchAll_champion($adapter,$paginated=false)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('community');
            $select->where(array('community.user_id' => $user_id));
            $select->join('user_profile', 'user_profile.user_id = community.user_id','user_name');
            $select->join('company', 'company.company_id = community.company_id','company_name');
           // $select->join('department', 'department.department_id = user_profile.department_id','department_name');
            $select->join('frequency', 'frequency.frequency_id = community.frequency_id','frequency_name');
            $select->join('indicator', 'indicator.id = community.type','name');
            $select->order('community.id DESC');


            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Community());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }



    /* $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('community');
        $select->join('user_profile', 'user_profile.user_id = community.user_id');
        $select->join('company', 'company.company_id = community.company_id');
        $select->join('department', 'department.department_id = user_profile.department_id');
        $select->join('frequency', 'frequency.frequency_id = community.frequency_id');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;*/
    }
    public function fetchAll_p1()
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        $resultSet = $this->tableGateway->select(array('user_id' => $user_id,'community_partner_id'=>1,'status'=>_P_));

        return $resultSet;
    }
    public function fetchAll_p2()
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        $resultSet = $this->tableGateway->select(array('user_id' => $user_id,'community_partner_id'=>2,'status'=>_P_));
        return $resultSet;
    }
    public function fetchAll_p3()
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        $resultSet = $this->tableGateway->select(array('user_id' => $user_id,'community_partner_id'=>3,'status'=>_P_));
        return $resultSet;
    }
    public function fetchAll_p4()
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        $resultSet = $this->tableGateway->select(array('user_id' => $user_id,'community_partner_id'=>4,'status'=>_P_));
        return $resultSet;
    }
    public function fetchAll_p5()
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        $resultSet = $this->tableGateway->select(array('user_id' => $user_id,'community_partner_id'=>5,'status'=>_P_));
        return $resultSet;
    }
    public function fetchAll_p6()
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        $resultSet = $this->tableGateway->select(array('user_id' => $user_id,'community_partner_id'=>6,'status'=>_P_));
        return $resultSet;
    }

   //Get ALL Community Partners
    public function fetchAllPartners()
    {

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }


    public function getCommunity($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }



    public function saveCommunity(Community $community,$id,$partner_id)
    {

        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $user_type = $userSession->user_type;
        if ($id == 0) {
            $data = array(
                'company_id' => $community->company_id,
                //  'frequency_id'  => $community->frequency_id,
                'frequency_id'  => $community->frequency_id,
                'region_id'        => $community->region_id,
                'country_id'        => $community->country_id,
                'frequency_id'      => $community->frequency_id,
                'type'              => $community->type,
                'value'             => $community->value,
                'status'            => _P_,
                'user_id'           => $user_id,
                'create_date'       => $community->create_date,
                'expiry_date'       => $community->expire_date,
                'community_partner_id' => $partner_id
            );
            $this->tableGateway->insert($data);
          } else {
               if ($this->getCommunity($id)) {
                   if($user_type == _ADMIN_) {
                       $data = array(
                           'value'         => $community->value,
                           'status'  => _A_
                       );
                   }
                   if($user_type == _CHAMPION_) {
                       $data = array(
                           'value'         => $community->value,
                           'status' => _P_,
                       );
                   }
                   $this->tableGateway->update($data, array('id' => $id));
               } else {
                   throw new \Exception('Community id does not exist');
               }
           }
    }

      public function deleteCommunity($id)
      {
          $this->tableGateway->delete(array('id' => (int) $id));
      }
}
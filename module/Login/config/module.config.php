<?php

/**
 * Generated by ZF2ModuleCreator
 */

return array(

    'view_manager' => array(
        'template_path_stack' => array(
            'Auth' =>    __DIR__ . '/../view',
        ),
    ),

    'controllers' => array(
        'invokables' => array(
            'Login\Controller\Auth' => 'Login\Controller\AuthController', 'Login\Controller\Success' => 'Login\Controller\SuccessController'
        ),
    ),

    'service_manager' => array(

    ),

    'router' => array(
// Uncomment below to add routes
//        'routes' => array(
//            'login' => array(
//                'type' => 'Literal',
//                'options' => array(
//                    'route' => '/login',
//                    'defaults' => array(
//                        '__NAMESPACE__' => 'Login\Controller',
//                        'controller' => 'Login',
//                        'action' => 'index',
//                    )
//                )
//            )
//        ),
//        'may_terminate' => true,
//        'child_routes' => array(
//            'default' => array(
//                'type' => 'Segment',
//                'options' => array(
//                    'route' => '/[:controller[/:action]]',
//                    'constraints' => array(
//                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
//                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
//                    )
//                )
//            )
//        )

        'routes' => array(

            'login' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/login[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Login\Controller',
                        'controller'    => 'Auth',
                        'action'        => 'login',
                        'id'     => '0',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'process' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:action]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
            'success' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/success',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Login\Controller',
                        'controller'    => 'Success',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:action]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),

        )
    )
);
<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/30/2016
 * Time: 11:47 AM
 */

namespace Login\Form;

use Zend\Form\Form;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Application\Model\MenuTable;
use Zend\Form\Element;

class UserProfileForm extends Form
{

    public function __construct(AdapterInterface $dbAdapter)
    {
        // we want to ignore the name passed
        $this->adapter = $dbAdapter;
        parent::__construct('login');
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from('menu');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->order('id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $main_menus = $statement->execute();
        $menus_arr = array();
        foreach ($main_menus as $main_menu) {
            $menus_arr[$main_menu['id']] = $main_menu['label'];
        }

        $select = $sql->select();
        $select->from('menu');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->order('id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $menus = $statement->execute();
        //  $menus_arr = $this->select(function (Select $select){
        //      $select->order(array('id asc'));
        //  });

        // $menus_arr = $results->toArray();
        $sub_menus_arr = array();
        foreach ($menus as $menu):
            $select = $sql->select();
            $select->from('sub_menu');
            $select->where(array('menu_id' => $menu['id']));
            $select->order('id asc');
            $statement = $sql->prepareStatementForSqlObject($select);
            $sub_menus = $statement->execute();
            $itr = 0 ;
            foreach ($sub_menus as $sub_menu) :
                $sub_menus_arr[$menu['label']]['label']= $menu['label'];
                $sub_menus_arr[$menu['label']]['data-menu-id'] = $menu['id'];
                $sub_menus_arr[$menu['label']]['options'][$itr]['value'] = $sub_menu['id'];
                $sub_menus_arr[$menu['label']]['options'][$itr]['label'] = $sub_menu['label'];
                $sub_menus_arr[$menu['label']]['options'][$itr]['attributes']['data-menu-id'] = $menu['id'];

                $itr++;
            endforeach;
        endforeach;

        $select = $sql->select();
        $select->from('city');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->where(array('is_active' => 1));
        $select->order('id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $cities = $statement->execute();
        $cities_arr = array(''=>'Please Select City');
        foreach ($cities as $city) {
            $cities_arr[$city['id']] = $city['city_name'];
        }

        $select = $sql->select();
        $select->from('region');
        $select->join('company_region', 'company_region.region_id = region.id', 'company_id');
        $select->where(array('region.is_active' => 1));
        $select->order('region.id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $regions = $statement->execute();
        $regions_arr = array();
        $itr = 0 ;
        foreach ($regions as $region) {
            //$regions_arr[$region['id']] = $region['region_description'];
            $regions_arr[$itr]['value'] = $region['id'];
            $regions_arr[$itr]['label'] = $region['region_description'];
            $regions_arr[$itr]['attributes']['data-company-id'] = $region['company_id'];
            $itr++;
        }


        $select = $sql->select();
        $select->from('country');
        $select->join('country_region', 'country_region.country_id = country.id', 'region_id');
        $select->join('company_country', 'company_country.country_id = country.id', 'company_id');
        $select->where(array('country.is_active' => 1));
        $select->order('country.id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $countries = $statement->execute();
        $countries_arr = array();
        $itr = 0 ;
        foreach ($countries as $country) {
            // $countries_arr[$country['id']] = $country['country_name'];
            $countries_arr[$itr]['value'] = $country['id'];
            $countries_arr[$itr]['label'] = $country['country_name'];
            $countries_arr[$itr]['attributes']['data-region-id'] = $country['region_id'];
            $countries_arr[$itr]['attributes']['data-company-id'] = $country['company_id'];
            $itr++;
        }
        $select = $sql->select();
        $select->from('indicator');
        $select->where(array('indicator.is_active' => 1));
        $select->order('indicator.id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $indicators = $statement->execute();
        $indicator_arr = array();
        $itr = 0 ;
        foreach ($indicators as $indicator) {
            $indicator_arr[$itr]['value'] = $indicator['id'];
            $indicator_arr[$itr]['label'] = $indicator['name'];
            $itr++;
        }
        $select = $sql->select();
        $select->from('gender');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->order('gender_id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $genders = $statement->execute();
        $genders_arr = array(''=>'Please Select Gender');
        foreach ($genders as $gender) {
            $genders_arr[$gender['gender_id']] = $gender['gender_description'];
        }

        $select = $sql->select();
        $select->from('roles');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->where(array('is_active' => 1));
        $select->order('role_id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $roles = $statement->execute();
        $roles_arr = array(''=>'Please Select Role');
        foreach ($roles as $role) {
            $roles_arr[$role['role_id']] = $role['role_name'];
        }

        $select = $sql->select();
        $select->from('types');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->where(array('is_active' => 1));
        $select->order('type_id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $types = $statement->execute();
        $types_arr = array(''=>'Please Select Type');
        foreach ($types as $type) {
            $types_arr[$type['type_id']] = $type['type_name'];
        }


        $select = $sql->select();
        $select->from('company');
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        $select->where(array('is_active' => 1));
        $select->order('company_id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $companies = $statement->execute();
        $companies_arr = array();
        foreach ($companies as $company) {
            $companies_arr[$company['company_id']] = $company['company_name'];
        }


        $select = $sql->select();
        $select->from('user_profile');
        $select->where(array('status' => _ACTIVE_));
        // $select->join('sub_menu', 'sub_menu.menu_id = menu.id','label','left');
        /*  $select->order('user_id asc');
          $statement = $sql->prepareStatementForSqlObject($select);
          $users = $statement->execute();
          $users_arr = array();
          foreach ($users as $user) {
              $users_arr[$user['user_id']] = $user['user_name'];
          }*/
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'menu_id',
            'options' => array(
                'label' => 'Menu Rights',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $menus_arr,
            ),
            'attributes' => array(
                // 'value' => '', //set selected to '1'
                /*'class' => 'form-control',*/
                'id' => 'menu_id',
                'multiple' => 'multiple',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'sub_menu_id',
            'options' => array(
                'label' => 'Menu Rights',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $sub_menus_arr,
            ),
            'attributes' => array(
                // 'value' => '', //set selected to '1'
               /* 'class' => 'form-control',*/
                'id' => 'sub_menu_id',
                'multiple' => 'multiple',
            )
        ));


        $this->add(array(
            'name' => 'user_id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'company_id',
            'options' => array(
                'label' => 'Company',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $companies_arr,
            ),
            'attributes' => array(
                // 'value' => '1', //set selected to '1'
                'id' => 'company_id',
                /*'class' => 'form-control',*/
                'multiple' => 'multiple',
            )
        ));
        $this->add(array(
            'name' => 'user_name',
            'attributes' => array(
                'id' => 'user_name',
                'type' => 'text',
                'required' => 'required',
                'class' => 'form-control',
            )
        ));
        $password = new Element\Password('password');
        $password
            ->setLabel('Password')
            ->setName('user_password')
            ->setAttributes(array(
                'id' => 'user_password',
                'type' => 'password',
                'required' => 'required',
                'class' => 'form-control',
            ));

        $this->add(
            $password
        );
        $confirm_password = new Element\Password('password');
        $confirm_password
            ->setLabel('Confirm Password')
            ->setName('confirm_password')
            ->setAttributes(array(
                'id' => 'confirm_password',
                'type' => 'password',
                'required' => 'required',
                'class' => 'form-control',
            ));

        $this->add(
            $confirm_password
        );

        /*  $this->add(array(
              'name' => 'user_password',
              'attributes' => array(
                  'id' => 'user_password',
                  'type' => 'password',
                  'required' => 'required',
                  'class' => 'form-control',
              ),
          ));*/
        $this->add(array(
            'name' => 'user_email',
            'attributes' => array(
                'id' => 'user_email',
                'type' => 'email',
                'required' => 'required',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'city_id',
            'options' => array(
                'label' => 'City',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $cities_arr,
            ),
            'attributes' => array(
                // 'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'city_id',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'region_id',
            'options' => array(
                'label' => 'Region',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $regions_arr,
            ),
            'attributes' => array(
                // 'value' => '', //set selected to '1'
              /*  'class' => 'form-control',*/
                'id' => 'region_id',
                'multiple' => 'multiple',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'user_role_id',
            'options' => array(
                'label' => 'User Role',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $roles_arr,
            ),
            'attributes' => array(
                'value' => '', //set selected to '1'
                'class' => 'form-control',
                'id' => 'user_role_id',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'user_type',
            'options' => array(
                'label' => 'User Type',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $types_arr,
            ),
            'attributes' => array(
                'value' => '', //set selected to '1'
                'class' => 'form-control',
                'id' => 'user_type_id',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country_id',
            'options' => array(
                'label' => 'Country',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $countries_arr,
            ),
            'attributes' => array(
                // 'value' => '', //set selected to '1'
               /* 'class' => 'form-control',*/
                'id' => 'country_id',
                'multiple' => 'multiple',

            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'indicator_id',
            'options' => array(
                'label' => 'Indicator',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $indicator_arr,
            ),
            'attributes' => array(
                // 'value' => '', //set selected to '1'
                /*'class' => 'form-control',*/
                'id' => 'indicator_id',
                'multiple' => 'multiple',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'gender_id',
            'options' => array(
                'label' => 'Gender',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $genders_arr,
            ),
            'attributes' => array(
                //'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'gender_id',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'status',
            'attributes' => array(
                'id' => 'myonoffswitch',
                'class' => 'onoffswitch-checkbox',
                'value' => 'A'
            ),
            'options' => array(
                'label' => 'Status',
                'use_hidden_element' => true,
                'checked_value' => 'A',
                'unchecked_value' => 'D'
            ),
        ));
        $this->add(array(
            'name' => 'expiry_date',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
                'class' => 'btn8 btn-8 btn-8f pull-right',
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'type' => 'button',
            'options' => array(
                'label' => 'Cancel'
            ),
            'attributes' => array(
                'onclick' => 'javascript:window.location.href = "/'._PROJECT_NAME_.'/public/login/list";',
                'class' => 'btn8 btn-8 btn-8f pull-right',
            ),
        ));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Noman sheikh
 * Date: 8/6/2015
 * Time: 12:41 PM
 */

//module/Login/src/Login/Model/User.php
namespace Login\Model;

use Zend\Form\Annotation;


/**
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("User")
 * @Annotation\Attributes({"class":"form-signin"})
 */
class User
{
    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"class":"form-control"})
     * @Annotation\Attributes({"placeholder":"Username"})
     */
    public $username;

    /**
     * @Annotation\Type("Zend\Form\Element\Password")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"class":"form-control"})
     * @Annotation\Attributes({"placeholder":"Password"})
     */
    public $password;

    /**
     * @Annotation\Type("Zend\Form\Element\Checkbox")
     * @Annotation\Options({"label":"Remember Me ?:"})
     */
    //public $rememberme;

    /**
     * @Annotation\Type("Zend\Form\Element\Submit")
     * @Annotation\Attributes({"value":"Submit"})
     * @Annotation\Attributes({"class":"btn btn-lg btn-primary btn-block"})
     */
    public $submit;
}
<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/30/2016
 * Time: 11:50 AM
 */

namespace Login\Model;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class UserProfile implements InputFilterAwareInterface
{
public $user_id;
public $user_name;
public $user_password;
public $user_email;
public $gender_id;
public $city_id;
public $company_id;
public $region_id;
public $country_id;
public $indicator_id;
public $user_role_id;
public $user_type;
public $menu_id;
public $sub_menu_id;
public $expiry_date;
public $status;
// public $user_id;
    public $inputFilter;

    protected $dbAdapter;

    public function setDbAdapter($dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }
    public function exchangeArray($data)
    {
        $this->user_id             = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->user_name             = (!empty($data['user_name'])) ? $data['user_name'] : null;
        $this->user_password    = (!empty($data['user_password'])) ? $data['user_password'] : null;
        $this->user_email            = (!empty($data['user_email'])) ? $data['user_email'] : null;
        $this->gender_id            = (!empty($data['gender_id'])) ? $data['gender_id'] : null;
        //$this->user_id              = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->city_id           = (!empty($data['city_id'])) ? $data['city_id'] : null;
        $this->company_id      = (!empty($data['company_id'])) ? $data['company_id'] : null;
        $this->region_id        = (!empty($data['region_id'])) ? $data['region_id'] : null;
        $this->country_id         = (!empty($data['country_id'])) ? $data['country_id'] : null;
        $this->indicator_id         = (!empty($data['indicator_id'])) ? $data['indicator_id'] : null;
        $this->user_role_id             = (isset($data['user_role_id']))  ? $data['user_role_id']     : null;
        $this->user_type             = (isset($data['user_type']))  ? $data['user_type']     : null;
        $this->menu_id          = (isset($data['menu_id']))  ? $data['menu_id']     : null;
        $this->sub_menu_id          = (isset($data['sub_menu_id']))  ? $data['sub_menu_id']     : null;
        $this->expiry_date                 = (isset($data['expiry_date']))  ? $data['expiry_date']     : null;
        $this->status           = (isset($data['status']))  ? $data['status']     : null;
    }
    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $inputFilter->add(array(
                'name'     => 'user_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
                /*  'validators' => array(
                     array(
                         'name'    => 'StringLength',
                         'options' => array(
                             'encoding' => 'UTF-8',
                             'min'      => 1,
                             'max'      => 100,
                         ),
                     ),
                 ),*/
            ));
            $inputFilter->add(array(
                'name' => 'user_name', // add second password field
                /* ... other params ... */
                'validators' => array(
                    array(
                        'name' => '\Zend\Validator\Db\NoRecordExists',
                        'options' => array(
                            'table' => 'user_profile',
                            'field' => 'user_name',
                            'adapter' => $this->dbAdapter,
                            'messages' => array(
                                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'Change Username already exist',
                            ),
                        ),
                ),
                ),
            ));
            $inputFilter->add(array(
                'name' => 'confirm_password', // add second password field
                /* ... other params ... */
                'validators' => array(
                    array(
                        'name' => 'Identical',
                        'options' => array(
                            'message' => 'Password and Re-type Password do not match',
                            'token' => 'user_password', // name of first password field
                        ),
                    ),
                ),
            ));
            $inputFilter->add(array(
                'name'     => 'expiry_date',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                /*  'validators' => array(
                      array(
                          'name'    => 'StringLength',
                          'options' => array(
                              'encoding' => 'UTF-8',
                              'min'      => 1,
                              'max'      => 100,
                          ),
                      ),
                  ),*/
            ));

            $this->inputFilter = $inputFilter;

        }

        return $this->inputFilter;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 8/7/2015
 * Time: 4:48 PM
 */
namespace Login\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class CurrentUser
{
    protected $tableGateway;

    public function __construct(tableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($adapter)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;

        $username = $userSession->username;
        $password = md5($userSession->password);
       // $resultSet = $this->tableGateway->select(array('user_name' => $username, 'user_pwd' => $password));
        $sql = new Sql($adapter);
        $select = $sql->select();
        //        $sql = new Sql($adapter);
        $select->from(array('u' => 'user_profile'));
        $select->join(array('uc' => 'user_company'), 'uc.user_id = u.user_id ', array('company_ids' => new Expression('GROUP_CONCAT(DISTINCT uc.company_id)')));
        $select->join(array('uco' => 'user_country'), 'uco.user_id = u.user_id ', array('country_ids' => new Expression('GROUP_CONCAT(DISTINCT uco.country_id)')));
        $select->join(array('ur' => 'user_region'), 'ur.user_id = u.user_id ', array('region_ids' => new Expression('GROUP_CONCAT(DISTINCT ur.region_id)')));
        $select->join(array('umr' => 'user_menu_rights'), 'umr.user_id = u.user_id ', array('sub_menu_ids' => new Expression('GROUP_CONCAT(DISTINCT umr.sub_menu_id)')));
        $select->join(array('ui' => 'user_indicator'), 'ui.user_id = u.user_id ', array('indicator_ids' => new Expression('GROUP_CONCAT(DISTINCT ui.indicator_id)')));
        $select->where (array('u.user_name' => $username, 'u.user_pwd' => $password));
        $select->group('u.user_id');
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        $resultSet = $resultSet->current();
        $company_ids = explode(',', $resultSet['company_ids']);
        $country_ids = explode(',', $resultSet['country_ids']);
        $region_ids = explode(',', $resultSet['region_ids']);
        $indicator_ids = explode(',', $resultSet['indicator_ids']);
        $resultSet['company_id'] = $company_ids;
        $resultSet['country_id'] = $country_ids;
        $resultSet['region_id'] = $region_ids;
        $resultSet['indicator_id'] = $indicator_ids;
        return $resultSet;
    }

    public function getUserProfile($id, $adapter)
    {
        $id = (int)$id;

        $rowset = $this->tableGateway->select(function (Select $select) use ($id) {
            $select
                ->columns(array(
                    '*'
                ))
                ->join(array('uc' => 'user_company'), 'uc.user_id = user_profile.user_id ', array('company_ids' => new Expression('GROUP_CONCAT(DISTINCT uc.company_id)')))
                ->join(array('uco' => 'user_country'), 'uco.user_id = user_profile.user_id ', array('country_ids' => new Expression('GROUP_CONCAT(DISTINCT uco.country_id)')))
                ->join(array('ur' => 'user_region'), 'ur.user_id = user_profile.user_id ', array('region_ids' => new Expression('GROUP_CONCAT(DISTINCT ur.region_id)')))
                ->join(array('umr' => 'user_menu_rights'), 'umr.user_id = user_profile.user_id ', array('menu_ids' => new Expression('GROUP_CONCAT(DISTINCT umr.menu_id)'),'sub_menu_ids' => new Expression('GROUP_CONCAT(DISTINCT umr.sub_menu_id)')))
                ->join(array('ui' => 'user_indicator'), 'ui.user_id = user_profile.user_id ', array('indicator_ids' => new Expression('GROUP_CONCAT(DISTINCT ui.indicator_id)')))
                ->where(array('user_profile.user_id ' => $id));
            // ->group('user_profile.userid');
        });


        $row = $rowset->current();
        $company_ids = explode(',', $row['company_ids']);
        $country_ids = explode(',', $row['country_ids']);
        $indicator_ids = explode(',', $row['indicator_ids']);
        $region_ids = explode(',', $row['region_ids']);
        $menu_ids = explode(',', $row['menu_ids']);
        $sub_menu_ids = explode(',', $row['sub_menu_ids']);
        $row['company_id'] = $company_ids;
        $row['country_id'] = $country_ids;
        $row['indicator_id'] = $indicator_ids;
        $row['region_id'] = $region_ids;
        $row['menu_id'] = $menu_ids;
        $row['sub_menu_id'] = $sub_menu_ids;
        $row['user_password'] = $row['user_pwd'];
        // $id  = (int) $id;
        // $rowset = $this->tableGateway->select(array('user_id' => $id));
        // $row = $rowset->current();


//$row = (object) $row;
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getUser($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(array('user_id' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveUserProfile(UserProfile $UserProfile, $adapter)
    {
        $sql = new Sql($adapter);
        $id = $UserProfile->user_id;
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $user_type = $userSession->user_type;


        if ($id == 0) {
            //status
            /*
                        if($UserProfile->status == 1 ){
                            $status  = _ACTIVE_;

                        }
                        else{
                            $status  = _DEACTIVE_;
                        }*/
            $data = array(
                'user_name' => $UserProfile->user_name,
                'user_pwd' => md5($UserProfile->user_password),
                'user_email' => $UserProfile->user_email,
                'gender_id' => $UserProfile->gender_id,
                'city_id' => $UserProfile->city_id,
                /* 'country_id' => $UserProfile->country_id,
                 'company_id' => $UserProfile->company_id,*/
                'user_role_id' => $UserProfile->user_role_id,
                'user_type' => $UserProfile->user_type,
                'status' => $UserProfile->status,
                'create_date' => date('Y-m-d'),
                'expiry_date' => $UserProfile->expiry_date,
                'create_by' => $user_id,

            );
            $UserProfile_query = $this->tableGateway->insert($data);
            $last_inserted_id = $this->tableGateway->lastInsertValue;
            if ($UserProfile_query == true) {
                /*insert companies*/
                foreach ($UserProfile->company_id as $company_id) {
                    $data_user_company = array(
                        'company_id' => $company_id,
                        'user_id' => $last_inserted_id,
                    );
                    $insert = $sql->insert('user_company');
                    $insert->values($data_user_company);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }
                /*insert companies*/
                /*insert regions*/
                foreach ($UserProfile->region_id as $region_id) {
                    $data_user_region = array(
                        'region_id' => $region_id,
                        'user_id' => $last_inserted_id,
                    );
                    $insert = $sql->insert('user_region');
                    $insert->values($data_user_region);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }
                /*insert regions*/
                /*insert countries*/
                foreach ($UserProfile->country_id as $country_id) {
                    $data_user_region = array(
                        'country_id' => $country_id,
                        'user_id' => $last_inserted_id,
                    );
                    $insert = $sql->insert('user_country');
                    $insert->values($data_user_region);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }
                /*insert countries*/
                /*insert indicators*/
                foreach ($UserProfile->indicator_id as $indicator_id) {
                    $data_user_indicator = array(
                        'indicator_id' => $indicator_id,
                        'user_id' => $last_inserted_id,
                    );
                    $insert = $sql->insert('user_indicator');
                    $insert->values($data_user_indicator);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }
                /*insert indicators*/
                /*insert user_menu_rights*/
                foreach ($UserProfile->menu_id as $menu_id) {
                    $data_user_menu = array(
                        'menu_id' => $menu_id,
                        'user_id' => $last_inserted_id,
                    );
                    $insert = $sql->insert('user_menu_rights');
                    $insert->values($data_user_menu);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }
                foreach ($UserProfile->sub_menu_id as $sub_menu_id) {
                    $data_user_menu = array(
                        'sub_menu_id' => $sub_menu_id,
                        'user_id' => $last_inserted_id,
                    );
                    $insert = $sql->insert('user_menu_rights');
                    $insert->values($data_user_menu);
                    $statement = $sql->prepareStatementForSqlObject($insert);
                    $results = $statement->execute();
                }
                /*insert user_menu_rights*/
            }

        } else {

            if ($this->getUser($id)) {
                    $data = array(
                       /* 'user_name' => $UserProfile->user_name,*/
                        /*'user_pwd' => md5($UserProfile->user_password),*/
                        'user_email' => $UserProfile->user_email,
                        'gender_id' => $UserProfile->gender_id,
                        'city_id' => $UserProfile->city_id,
                        /* 'country_id' => $UserProfile->country_id,
                         'company_id' => $UserProfile->company_id,*/
                        'user_role_id' => $UserProfile->user_role_id,
                        'user_type' => $UserProfile->user_type,
                        'status' => $UserProfile->status,
                        'modified_date' => date('Y-m-d'),
                        'expiry_date' => $UserProfile->expiry_date,
                        'create_by' => $user_id,

                    );
                    /*change password */
                    if($UserProfile->user_password != NULL){
                        $data[ 'user_pwd' ] =md5($UserProfile->user_password);
                    }
                    /*change password */
                    //status
                    /*
                                if($UserProfile->status == 1 ){
                                    $status  = _ACTIVE_;

                                }
                                else{
                                    $status  = _DEACTIVE_;
                                }*/

                    // if file re uploaded
                    if ($UserProfile->user_id != NULL) {

                        /*delete companies*/
                        $delete_row = $sql->delete('user_company');
                        $delete_row->where(array('user_id' => (int)$id));
                        $statement = $sql->prepareStatementForSqlObject($delete_row);
                        $statement->execute();
                        /*delete regions*/
                        $delete_row = $sql->delete('user_region');
                        $delete_row->where(array('user_id' => (int)$id));
                        $statement = $sql->prepareStatementForSqlObject($delete_row);
                        $statement->execute();
                        /*delete countries*/
                        $delete_row = $sql->delete('user_country');
                        $delete_row->where(array('user_id' => (int)$id));
                        $statement = $sql->prepareStatementForSqlObject($delete_row);
                        $statement->execute();
                        /*delete indicators*/
                        $delete_row = $sql->delete('user_indicator');
                        $delete_row->where(array('user_id' => (int)$id));
                        $statement = $sql->prepareStatementForSqlObject($delete_row);
                        $statement->execute();
                        /*delete user_menu_rights*/
                        $delete_row = $sql->delete('user_menu_rights');
                        $delete_row->where(array('user_id' => (int)$id));
                        $statement = $sql->prepareStatementForSqlObject($delete_row);
                        $statement->execute();

                        /*insert companies*/
                        foreach ($UserProfile->company_id as $company_id) {
                            $data_user_company = array(
                                'company_id' => $company_id,
                                'user_id' => $id,
                            );
                            $insert = $sql->insert('user_company');
                            $insert->values($data_user_company);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                        /*insert companies*/
                        /*insert regions*/
                        foreach ($UserProfile->region_id as $region_id) {
                            $data_user_region = array(
                                'region_id' => $region_id,
                                'user_id' => $id,
                            );
                            $insert = $sql->insert('user_region');
                            $insert->values($data_user_region);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                        /*insert regions*/
                        /*insert countries*/
                        foreach ($UserProfile->country_id as $country_id) {
                            $data_user_region = array(
                                'country_id' => $country_id,
                                'user_id' => $id,
                            );
                            $insert = $sql->insert('user_country');
                            $insert->values($data_user_region);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                        /*insert countries*/
                        /*insert indicators*/
                        foreach ($UserProfile->indicator_id as $indicator_id) {
                            $data_user_indicator = array(
                                'indicator_id' => $indicator_id,
                                'user_id' => $id,
                            );
                            $insert = $sql->insert('user_indicator');
                            $insert->values($data_user_indicator);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                        /*insert indicators*/
                        /*insert user_menu_rights*/
                        foreach ($UserProfile->menu_id as $menu_id) {
                            $data_user_menu = array(
                                'menu_id' => $menu_id,
                                'user_id' => $id,
                            );
                            $insert = $sql->insert('user_menu_rights');
                            $insert->values($data_user_menu);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                        foreach ($UserProfile->sub_menu_id as $sub_menu_id) {
                            $data_user_menu = array(
                                'sub_menu_id' => $sub_menu_id,
                                'user_id' => $id,
                            );
                            $insert = $sql->insert('user_menu_rights');
                            $insert->values($data_user_menu);
                            $statement = $sql->prepareStatementForSqlObject($insert);
                            $results = $statement->execute();
                        }
                        /*insert user_menu_rights*/

                    } else {

                    }
                    $this->tableGateway->update($data, array('user_id' => $id));


            } else {
                throw new \Exception('User id does not exist');
            }

        }
    }

    public function deleteMilestone($id, $adapter)
    {
        $this->tableGateway->delete(array('milestone_id' => (int)$id, 'status' => _P_));

        $sql = new Sql($adapter);
        $delete = $sql->delete();
        $delete->from('milestone_details');
        $delete->where(array('milestone_id' => (int)$id));
        $statement = $sql->prepareStatementForSqlObject($delete);
        $results = $statement->execute();
    }

    public function fetchAll_user($adapter, $paginated = false)
    {
        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            //        $sql = new Sql($adapter);
            $select->from(array('u' => 'user_profile'));
            $select->join(array('uc' => 'user_company'), 'uc.user_id = u.user_id ', array('company_ids' => new Expression('GROUP_CONCAT(DISTINCT uc.company_id)')));
            $select->join(array('uco' => 'user_country'), 'uco.user_id = u.user_id ', array('country_ids' => new Expression('GROUP_CONCAT(DISTINCT uco.country_id)')));
            $select->join(array('ur' => 'user_region'), 'ur.user_id = u.user_id ', array('region_ids' => new Expression('GROUP_CONCAT(DISTINCT ur.region_id)')));
            $select->join(array('umr' => 'user_menu_rights'), 'umr.user_id = u.user_id ', array('sub_menu_ids' => new Expression('GROUP_CONCAT(DISTINCT umr.sub_menu_id)')));
            $select->join(array('ui' => 'user_indicator'), 'ui.user_id = u.user_id ', array('indicator_ids' => new Expression('GROUP_CONCAT(DISTINCT ui.indicator_id)')));
            $select->group('u.user_id');
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            // create a new result set based on the User entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new UserProfile());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );

            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

}



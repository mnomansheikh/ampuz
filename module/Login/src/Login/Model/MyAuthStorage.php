<?php
/**
 * Created by PhpStorm.
 * User: Noman Sheikh
 * Date: 8/6/2015
 * Time: 12:43 PM
 */


//module/Login/src/Login/Model/MyAuthStorage.php
namespace Login\Model;


use Zend\Authentication\Storage;

class MyAuthStorage extends Storage\Session
{
    public function setRememberMe($rememberMe = 0, $time = 1209600)
    {
        if ($rememberMe == 1) {
            $this->session->getManager()->rememberMe($time);
        }
    }

    public function forgetMe()
    {
        $this->session->getManager()->forgetMe();
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Noman Sheikh
 * Date: 8/6/2015
 * Time: 12:53 PM
 */


//module/Login/src/Controller/AuthController.php
namespace Login\Controller;

use Zend\Console\Adapter\AdapterInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\ViewModel;
use Login\Model\User;
//use Login\Model\MyAuthStorage;
use Zend\Session\Container;
use Login\Model\CurrentUser;
use Login\Model\UserProfile;
use Login\Form\UserProfileForm;

class AuthController extends AbstractActionController
{
    protected $form;
    protected $userSession;
    protected $storage;
    protected $authservice;
    // protected $currentUserId;
    protected $CurrentUser;
    protected $UserProfileTable;
//    public function getUserProfileTable()
//    {
//        if (!$this->UserProfileTable) {
//            $sm = $this->getServiceLocator();
//            $this->UserProfileTable = $sm->get('Login\Model\UserProfileTable');
//        }
//        return $this->UserProfileTable;
//    }
    public function getCurrentUserTable()
    {
        if (!$this->CurrentUser) {
            $sm = $this->getServiceLocator();
            $this->CurrentUser = $sm->get('Login\Model\CurrentUser');
        }
        return $this->CurrentUser;
    }


    public function getAuthService()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authservice;
    }

    public function getCurrentuserTableGateway()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('CurrentuserTableGateway');
        }

        return $this->authservice;
    }

    public function getSessionStorage()
    {
        if (!$this->storage) {
            $this->storage = $this->getServiceLocator()
                ->get('Login\Model\MyAuthStorage');
        }

        return $this->storage;
    }

    public function getForm()
    {
        if (!$this->form) {
            $user = new User();
            $builder = new AnnotationBuilder();
            $this->form = $builder->createForm($user);
        }

        return $this->form;
    }

    public function loginAction()
    {
        //if already login, redirect to success page
        if ($this->getAuthService()->hasIdentity()) {
            return $this->redirect()->toRoute('application');
        }

        $form = $this->getForm();

        return array(
            'form' => $form,
            'messages' => $this->flashmessenger()->getMessages()
        );
    }

    public function authenticateAction()
    {
        $form = $this->getForm();
        $redirect = 'login';
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                //check authentication...
                $this->getAuthService()->getAdapter()
                    ->setIdentity($request->getPost('username'))
                    ->setCredential($request->getPost('password'));

                $result = $this->getAuthService()->authenticate();
                foreach ($result->getMessages() as $message) {
                    $code=    $result->getCode();
                    switch ($code) {

                        case $result::FAILURE_IDENTITY_NOT_FOUND:
                            /** do stuff for nonexistent identity **/
                            $message=  "Invalid Username Or Password";
                            break;

                        case $result::FAILURE_CREDENTIAL_INVALID:
                            /** do stuff for invalid credential **/
                            $message=  "Your Account has been disabled please contact your administrator";
                            break;

                        case $result::SUCCESS:
                            /** do stuff for successful authentication **/
                            $message= "Login Successfully";
                            break;

                        default:
                            /** do stuff for other failure **/
                            $message=  "Invalid Username Or Password";
                            break;
                    }


                    //save message temporary into flashmessenger
                    $this->flashmessenger()->addMessage($message);
                }

                if ($result->isValid()) {
                    $redirect = 'application';
                    // Store variables in session
                    $userSession = new Container('user');
                    $userSession->username = $request->getPost('username');
                    $userSession->password = $request->getPost('password');


                    //current login user data
                    $results = $this->getCurrentUserTable()->fetchAll($dbAdapter);
                   // foreach ($results as $result) {

                    $user_id = $results['user_id'];
                    $user_type = $results['user_role_id'];
                    $user_companies = $results['company_id'];
                    $user_regions = $results['region_id'];
                    $user_countries = $results['country_id'];
                    $user_indicators = $results['indicator_id'];
                    $userSession->user_id = $user_id;
                    $userSession->user_type = $user_type;
                    $userSession->user_companies = $user_companies;
                    $userSession->user_regions = $user_regions;
                    $userSession->user_countries = $user_countries;
                    $userSession->user_indicators = $user_indicators;

                  //  }

                    //check if it has rememberMe :


                    if ($request->getPost('rememberme') == 1) {
                        $this->getSessionStorage()
                            ->setRememberMe(1);
                        //set storage again
                        $this->getAuthService()->setStorage($this->getSessionStorage());
                    }
                    $this->getAuthService()->getStorage()->write($request->getPost('username'));
                }
            }
        }

        return $this->redirect()->toRoute($redirect);
    }

    public function logoutAction()
    {
        $this->getSessionStorage()->forgetMe();
        $this->getAuthService()->clearIdentity();
        $userSession = new Container('user');
        unset($_SESSION['user']);
        $this->flashmessenger()->addMessage("You've been logged out");
        return $this->redirect()->toRoute('login');
    }

    /*public function addAction()
    {
        if ($this->getAuthService()->hasIdentity()) {
            $userprofile = new UserProfile();
            $builder = new AnnotationBuilder();
            $form = $builder->createForm($userprofile);

            $request = $this->getRequest();
            if ($request->isPost()) {
                $form->bind($userprofile);
                $form->setData($request->getPost());
                if ($form->isValid()) {
                    print_r($form->getData());
                }
            }

            return array('form' => $form);
        }
        else{
            return $this->redirect()->toRoute('login');
        }
    }*/
    /*user profile*/
    public function ListAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $form = new UserProfileForm($dbAdapter);
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user

            // grab the paginator from the CommunityTable
            $paginator = $this->getCurrentUserTable()->fetchAll_user($dbAdapter, true);
            // set the current page to what has been passed in query string, or to 1 if none set
            $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
            // set the number of items per page to 10
            $paginator->setItemCountPerPage(10);

            $view = new ViewModel(array(
                'paginator' => $paginator,
                'form' => $form

            ));
            $view->setTemplate('login/auth/list');
            return $view;
        } /*   if ($userSession->user_type == _CHAMPION_) {
                   $form = new EnergyForm();
                   $form->get('submit')->setValue('Add');
                   $request = $this->getRequest();

                   if ($request->isPost()) {
                       $energy = new Energy();
                       $form->setInputFilter($energy->getInputFilter());
                       $form->setData($request->getPost());

                       if ($form->isValid()) {
                           $energy->exchangeArray($form->getData());
                           $this->getEnergyTable()->saveEnergy($energy, null);

                           // Redirect to list of albums
                           return $this->redirect()->toRoute('energy');
                       }
                   }
                   return array('form' => $form, 'energy' => $this->getEnergyTable()->fetchAll($dbAdapter));
               }
               */
        else {
            return $this->redirect()->toRoute('login');
        }


//        return new ViewModel(array(
//            'energy' => $this->getEnergyTable()->fetchAll(),
//
//        ));
        //    $form->setVariable('form', $form);

    }

    public function addAction()
    {

        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            if ($userSession->user_type == _ADMIN_) {
                $form = new UserProfileForm($dbAdapter);
                $form->get('submit')->setValue('Save');
                $form->get('cancel')->setValue('Cancel');
                $request = $this->getRequest();

                if ($request->isPost()) {
                    $user_profile = new UserProfile();
                    $user_profile->setDbAdapter($dbAdapter);
                    $form->setInputFilter($user_profile->getInputFilter());
                    $data = array_merge_recursive(
                        $this->getRequest()->getPost()->toArray(),
                        $this->getRequest()->getFiles()->toArray()
                    );

                    $form->setData($request->getPost());
                    $form->setData($data);

                    if ($form->isValid()) {
                        $user_profile->exchangeArray($form->getData());
                        $this->getCurrentUserTable()->saveUserProfile($user_profile, $dbAdapter);
                        // Redirect to list
                        return $this->redirect()->toRoute('login', array(
                            'action' => 'list'
                        ));
                    }
                }


                //return array('form' => $form, 'event' => $this->getUserProfileTable()->fetchAll($dbAdapter));
                $view = new ViewModel(array(
                    'form' => $form
                ));
                $view->setTemplate('login/auth/add');
                return $view;
            }
            if ($userSession->user_type == _CHAMPION_) {
                return $this->redirect()->toRoute('application');
            }
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function editAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $id = (int)$this->params()->fromRoute('id', 0);
            try {
                $UserProfileTable = $this->getCurrentUserTable()->getUserProfile($id, $dbAdapter);
            } catch (\Exception $ex) {
                return $this->redirect()->toRoute('login', array(
                    'action' => 'index'
                ));
            }
            $form = new UserProfileForm($dbAdapter);
            $UserProfile = new UserProfile();
            $form->bind($UserProfileTable);
            //for session varibles
            $form->get('submit')->setValue('Save');
            $form->get('cancel')->setValue('Cancel');
            $request = $this->getRequest();
            if ($request->isPost()) {
                $UserProfile->setDbAdapter($dbAdapter);
                $form->setInputFilter($UserProfile->getInputFilter());
                $form->setData($request->getPost());
                if ($id > 0) {
                    $form->getInputFilter()->get('user_name')->setRequired(false);
                    $form->getInputFilter()->get('user_password')->setRequired(false);
                    $form->getInputFilter()->get('confirm_password')->setRequired(false);
                }
                if ($form->isValid()) {
                    $UserProfile->exchangeArray($form->getData());
                    $this->getCurrentUserTable()->saveUserProfile($UserProfile, $dbAdapter);

                    // Redirect to list
                    return $this->redirect()->toRoute('login', array(
                        'action' => 'list'
                    ));
                }
            }
            //  $user = $this->getCurrentUserTable()->fetchAll_event($dbAdapter,$id);
            return array(
                'id' => $id,
                'form' => $form,
                //    'user' => $user,

            );
        } else {
            return $this->redirect()->toRoute('login');
        }

    }
    /*user profile*/

}
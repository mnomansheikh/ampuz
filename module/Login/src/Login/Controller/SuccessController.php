<?php
/**
 * Created by PhpStorm.
 * User: Noman Sheikh
 * Date: 8/6/2015
 * Time: 12:55 PM
 */

//module/SanAuth/src/SanAuth/Controller/SuccessController.php
namespace Login\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SuccessController extends AbstractActionController
{
    public function indexAction()
    {
        if (! $this->getServiceLocator()
            ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('login');
        }

        return new ViewModel();
    }
}
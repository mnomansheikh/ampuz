<?php
namespace Indicator\Model;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Indicator implements InputFilterAwareInterface
{
    public $user_id;
    public $id;
    public $name;
    public $measuring_unit_id;
    public $measuring_unit_value;
    public $menu_id;
    public $sub_menu_id;
    public $frequency_id;
    public $company_id;
    public $indicator_description;
    public $data_type;
    public $role;
    public $gender_required;
    public $is_attachment;
    public $is_uploading;
    public $bulk_type;
    public $last_updateon;
    public $last_updateby;
    public $create_date;
    public $create_by;
    public $is_active;
    public $inputFilter;


    public function exchangeArray($data)
    {
        $this->id                   = (!empty($data['id'])) ? $data['id'] : null;
        $this->menu_id              = (!empty($data['menu_id'])) ? $data['menu_id']         : null;
        $this->sub_menu_id          = (!empty($data['sub_menu_id'])) ? $data['sub_menu_id'] : null;
        $this->company_id           = (!empty($data['company_id'])) ? $data['company_id'] : null;
        $this->name                 = (!empty($data['name'])) ? $data['name'] : null;
        $this->measuring_unit_id    = (!empty($data['measuring_unit_id'])) ? $data['measuring_unit_id'] : null;
        $this->measuring_unit_value = (!empty($data['measuring_unit_value'])) ? $data['measuring_unit_value'] : null;
        $this->before_remainder     = (!empty($data['before_remainder'])) ? $data['before_remainder'] : null;
        $this->max_remainder        = (!empty($data['max_remainder'])) ? $data['max_remainder'] : null;
        $this->user_id              = (!empty($data['user_id'])) ? $data['user_id']     : null;
        $this->frequency_id         = (!empty($data['frequency_id'])) ? $data['frequency_id']       : null;




        if($data['is_attachment'] == 1){
            $this->role        = '1';
        }else if($data['is_uploading'] == 1){
            $this->role        = '2';
        }else{
            $this->role        = '3';
            //$this->is_attachment        = (isset($data['is_attachment']))  ? $data['is_attachment']     : null;
            //$this->is_uploading         = (isset($data['is_uploading']))  ? $data['is_uploading'] : null;
        }

        if($data['role'] == '1'){
            $this->is_attachment        = (isset($data['is_attachment']))  ? $data['is_attachment']     : null;
        }else{
            $this->is_attachment        = null;
        }
        if($data['role'] == '2'){
            $this->is_uploading        = (isset($data['is_uploading']))  ? $data['is_uploading']     : null;
        }else{
            $this->is_uploading        = null;
        }
        $this->bulk_type        = (isset($data['bulk_type']))  ? $data['bulk_type']     : null;
        $this->create_date          = (isset($data['create_date']))  ? $data['create_date']   : null;
        $this->create_by            = (isset($data['create_by']))  ? $data['create_by'] : null;
        $this->last_updateon        = (isset($data['last_updateon']))  ? $data['last_updateon']  : null;
        $this->last_updateby        = (isset($data['last_updateby']))  ? $data['last_updateby']  : null;
        $this->is_active            = (isset($data['is_active']))  ? $data['is_active']  : null;
    }
    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $inputFilter->add(array(
                'name'     => 'id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'menu_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
              /*  'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),*/
            ));
            $inputFilter->add(array(
                'name'     => 'sub_menu_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ));
            $inputFilter->add(array(
                'name'     => 'company_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ));

            $inputFilter->add(
                $factory->createInput(array(
                    'name'     => 'fileupload',
                    'required' => false,
                ))
            );

            $inputFilter->add(array(
                'name'     => 'measuring_unit_value',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'before_remainder',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'max_remainder',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
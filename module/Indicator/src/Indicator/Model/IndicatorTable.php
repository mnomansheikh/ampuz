<?php
namespace Indicator\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Filter\Null;
use Zend\Http\Header\Date;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
class IndicatorTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    public function fetchAll($adapter)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;
        // $resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('indicator');
        $select->join('user_profile', 'user_profile.user_id = indicator.user_id','user_name');
        $select->join('company', 'company.company_id = indicator.company_id','company_name');
        $select->join('menu', 'menu.id = indicator.menu_id',array('menu_label' => 'label'));
        $select->join('sub_menu', 'sub_menu.id = indicator.sub_menu_id',array('sub_menu_label' => 'label'));
        $select->join('frequency', 'frequency.frequency_id = indicator.frequency_id','frequency_name');
        $select->join('measure_unit', 'measure_unit.id = indicator.measuring_unit_id','unit_description');
        $select->order('id DESC');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function fetchAll_admin($adapter,$paginated=false)
    {

        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('indicator');
            $select->join('user_profile', 'user_profile.user_id = indicator.user_id',array('user_name','user_email'));
            //$select->join('company', 'company.company_id = indicator.company_id','company_name');
            $select->join('frequency', 'frequency.frequency_id = indicator.frequency_id','frequency_name');
            $select->join('menu', 'menu.id = indicator.menu_id',array('menu_label' => 'label'));
            $select->join('sub_menu', 'sub_menu.id = indicator.sub_menu_id',array('sub_menu_label' => 'label'));
            $select->order('id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Indicator());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function fetchAll_champion($adapter,$paginated=false)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('indicator');
            $select->where(array('indicator.user_id'=>$user_id));
            $select->join('user_profile', 'user_profile.user_id = indicator.user_id','user_name');
            //$select->join('company', 'company.company_id = indicator.company_id','company_name');
            $select->join('frequency', 'frequency.frequency_id = indicator.frequency_id','frequency_name');
            $select->join('menu', 'menu.id = indicator.menu_id',array('menu_label' => 'label'));
            $select->join('sub_menu', 'sub_menu.id = indicator.sub_menu_id',array('sub_menu_label' => 'label'));
            $select->order('id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Indicator());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function getIndicator($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveIndicator(Indicator $indicator)
    {
        {
            $id = $indicator->id;
            $fieldname1 = '';
            $fieldname2 = '';
            $fieldname3 = '';
            $fieldname4 = '';
            $fieldname5 = '';
            $fieldname6 = '';
            $fieldname7 = '';
            $fieldname8 = '';
            $fieldname9 = '';
            $fieldname10 = '';

            if($_POST['role'] == "1"){
                $is_attachment = 1;
            }else {
                $is_attachment = '';
            }
            if($_POST['role'] == "2"){
                $is_uploading = 1;
            }else {
                $is_uploading = '';
            }
            if($_POST['role'] == "3"){
                $is_manual = 1;
            }else {
                $is_manual = '';
            }

            $i = 1;
            foreach($indicator->field_name AS $fieldname){
                ${'fieldname'.$i} = strtoupper($fieldname);
                $i++;
            }
            $userSession = new Container('user');
            $user_id = $userSession->user_id;
            $user_type = $userSession->user_type;
            //$oneYearOn = date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day"));
            if($id == 0) {
                $data = array(
                    'company_id'      => $indicator->company_id,
                    'frequency_id'    => $indicator->frequency_id,
                    'name'            => $indicator->name,
                    'menu_id'         => $indicator->menu_id,
                    'sub_menu_id'     => $indicator->sub_menu_id,
                    'measuring_unit_id' => $indicator->measuring_unit_id,
                    'measuring_unit_value' => $indicator->measuring_unit_value,
                    'before_remainder' => $indicator->before_remainder,
                    'max_remainder' => $indicator->max_remainder,
                    'is_uploading'    => $is_uploading,
                    'is_attachment'   => $is_attachment,
                    'bulk_type'   => $indicator->bulk_type,
                    'is_active'   => $indicator->is_active,
                    'user_id'           => $user_id,
                    'create_date'       => date("Y-m-d"),
                    'is_col1'       => $fieldname1,
                    'is_col2'       => $fieldname2,
                    'is_col3'       => $fieldname3,
                    'is_col4'       => $fieldname4,
                    'is_col5'       => $fieldname5,
                    'is_col6'       => $fieldname6,
                    'is_col7'       => $fieldname7,
                    'is_col8'       => $fieldname8,
                    'is_col9'       => $fieldname9,
                    'is_col10'       => $fieldname10,
              //      'expiry_date'       => $oneYearOn,

                );
                $this->tableGateway->insert($data);
            }

            else {
                if ($this->getIndicator($id)) {
                    if($user_type == _ADMIN_) {
                        $data = array(
                            'company_id'      => $indicator->company_id,
                            'frequency_id'    => $indicator->frequency_id,
                            'menu_id'         => $indicator->menu_id,
                            'name'            => $indicator->name,
                            'sub_menu_id'     => $indicator->sub_menu_id,
                            'measuring_unit_id' => $indicator->measuring_unit_id,
                            'measuring_unit_value' => $indicator->measuring_unit_value,
                            'before_remainder' => $indicator->before_remainder,
                            'max_remainder' => $indicator->max_remainder,
                            'is_uploading'    => $is_uploading,
                            'is_attachment'   => $is_attachment,
                            'bulk_type'   => $indicator->bulk_type,
                            'is_active'   => $indicator->is_active,
                            'is_col1'       => $fieldname1,
                            'is_col2'       => $fieldname2,
                            'is_col3'       => $fieldname3,
                            'is_col4'       => $fieldname4,
                            'is_col5'       => $fieldname5,
                            'is_col6'       => $fieldname6,
                            'is_col7'       => $fieldname7,
                            'is_col8'       => $fieldname8,
                            'is_col9'       => $fieldname9,
                            'is_col10'       => $fieldname10,
                        );
                    }
                    if($user_type == _CHAMPION_) {
                        $data = array(
                            'company_id'      => $indicator->company_id,
                            'frequency_id'    => $indicator->frequency_id,
                            'menu_id'         => $indicator->menu_id,
                            'name'            => $indicator->name,
                            'sub_menu_id'     => $indicator->sub_menu_id,
                            'measuring_unit_id' => $indicator->measuring_unit_id,
                            'measuring_unit_value' => $indicator->measuring_unit_value,
                            'before_remainder' => $indicator->before_remainder,
                            'max_remainder' => $indicator->max_remainder,
                            'is_uploading'    => $is_uploading,
                            'is_attachment'   => $is_attachment,
                            'bulk_type'   => $indicator->bulk_type,
                            'is_active'   => $indicator->is_active,

                        );
                    }
                    $this->tableGateway->update($data, array('id' => $id));
                } else {
                    throw new \Exception('Indicator id does not exist');
                }

            }
        }
    }

    public function deleteIndicator($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 1/5/2016
 * Time: 3:52 PM
 */

namespace Event\Model;


use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Session\Container;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;


class Area
{

    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($adapter)
    {
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('area');
        $select->order('name DESC');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getEventByName($name)
    {
        $sql = new Sql($name);
        $select = $sql->select();
        $select->from('area');
        $select->where(array('area.name' => 'event'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results[0];
    }


}
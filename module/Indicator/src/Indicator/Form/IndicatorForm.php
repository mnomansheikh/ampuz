<?php
namespace Indicator\Form;

use Zend\Form\Form;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Sql;

class IndicatorForm extends Form
{

    public function __construct(AdapterInterface $dbAdapter)
    {
        $this->adapter = $dbAdapter;
        // we want to ignore the name passed
        parent::__construct('event');

        $sql = new Sql($dbAdapter);

        //Select Section from DB
        $select = $sql->select();
        $select->from('menu');
        $select->where(array('id' => array('2', '3', '4')));
        $statement = $sql->prepareStatementForSqlObject($select);
        $sections = $statement->execute();
        $sections_arr = array();
        $itr = 0 ;
        foreach ($sections as $section) {
            $sections_arr[$itr]['value'] = $section['id'];
            $sections_arr[$itr]['label'] = $section['label'];
            $itr++;
        }

        //Select Groups from DB
        $select = $sql->select();
        $select->from('sub_menu');
        $select->where(array('menu_id' => array('2', '3', '4')));
        $statement = $sql->prepareStatementForSqlObject($select);
        $groups = $statement->execute();
        $groups_arr = array();
        $itr = 0 ;
        $groups_arr[$itr]['value'] = '';
        $groups_arr[$itr]['label'] = '';
        $itr++ ;
        foreach ($groups as $group) {
            $groups_arr[$itr]['value'] = $group['id'];
            $groups_arr[$itr]['label'] = $group['label'];
            $groups_arr[$itr]['attributes']['data-menu-id'] = $group['menu_id'];
            $itr++;
        }

        //Select Companies from DB
        $select = $sql->select();
        $select->from('company');
        $select->where(array('company.is_active' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $companies = $statement->execute();
        $companies_arr = array();
        $itr = 0 ;
        foreach ($companies as $company) {
            $companies_arr[$itr]['value'] = $company['company_id'];
            $companies_arr[$itr]['label'] = $company['company_name'];
            $itr++;
        }

        //Select Frequencies from DB
        $select = $sql->select();
        $select->from('frequency');
        $statement = $sql->prepareStatementForSqlObject($select);
        $frequencies = $statement->execute();
        $frequencies_arr = array();
        $itr = 0 ;
        foreach ($frequencies as $frequency) {
            $frequencies_arr[$itr]['value'] = $frequency['frequency_id'];
            $frequencies_arr[$itr]['label'] = $frequency['frequency_name'];
            $itr++;
        }

        //Select Units from DB
        $select = $sql->select();
        $select->from('measure_unit');
        $statement = $sql->prepareStatementForSqlObject($select);
        $units = $statement->execute();
        $units_arr = array();
        $itr = 0 ;
        foreach ($units as $unit) {
            $units_arr[$itr]['value'] = $unit['id'];
            $units_arr[$itr]['label'] = $unit['unit_description'];
            $itr++;
        }


        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));

//        $this->add(array(
//            'name' => 'emp_id',
//            'type' => 'Hidden',
//        ));

//        $this->add(array(
//            'name' => 'indicator_description',
//            'attributes' => array(
//                'class' => 'form-control',
//                'required' =>    'required',
//            ),
//        ));

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'class' => 'form-control',
                'required' =>    'required',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'role',
            'options' => array(
                'label' => 'A Radio Button',
                'value_options' => array(
                    '1' => 'Manual With Evidence',
                    '2' => 'Bulk',
                    '3' => 'Manual Entry',
                ),
            ),
            'attributes' => array(
                'id' => 'role',
                'value' => '1'
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'bulk_type',
            'options' => array(
                'label' => 'Bulk Type',
                'value_options' => array(
                    '0' => '',
                    '1' => 'Event',
                    '2' => 'Diversity',
                    '3' => 'Transport',
                ),
            ),
            'attributes' => array(
                'value' => '0',
                'class' => 'form-control',
                'required' => 'required',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_active',
            'attributes' => array(
                'id' => 'myonoffswitch',
                'class' => 'onoffswitch-checkbox',
                'value' => '1'
            ),
            'options' => array(
                'label' => 'Status',
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0'
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'menu_id',
            'options' => array(
                'label' => 'Section',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $sections_arr,
            ),
            'attributes' => array(
                'id' => 'menu_id',
                'value' => '1', //set selected to '1'
                'class' => 'form-control',

            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'sub_menu_id',
            'options' => array(
                'label' => 'Group',
                'class' => 'control-label col-sm-2',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $groups_arr,
            ),
            'attributes' => array(
                'id' => 'sub_menu_id',
                'value' => '', //set selected to '1'
                'class' => 'form-control',

            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'frequency_id',
            'options' => array(
                'label' => 'Frequency',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $frequencies_arr,
            ),
            'attributes' => array(
                'value' => '1', //set selected to '1'
               'class' => 'form-control',

            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'measuring_unit_id',
            'options' => array(
                'label' => 'Unit',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $units_arr,
            ),
            'attributes' => array(
                'value' => '1', //set selected to '1'
                'class' => 'form-control',

            )
        ));

        $this->add(array(
            'name' => 'measuring_unit_value',
            'attributes' => array(
                'class' => 'form-control',
                'required' => 'required',
            ),
        ));

        $this->add(array(
            'name' => 'before_remainder',
            'attributes' => array(
                'class' => 'form-control',
                'required' => 'required',
            ),
        ));

        $this->add(array(
            'name' => 'max_remainder',
            'attributes' => array(
                'class' => 'form-control',
                'required' => 'required',
            ),
        ));
        $this->add(array(
            'name' => 'field_name[]',
            'options' => array(
                'label' => 'FieldName',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $companies_arr,
            ),
            'attributes' => array(
                'class' => 'form-control',
                'required' => 'required',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'company_id',
            'options' => array(
                'label' => 'Company',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $companies_arr,
            ),
            'attributes' => array(
                'value' => '1', //set selected to '1'
                'class' => 'form-control',

            )
        ));

        $this->add(array(
            'name' => 'user_name',
            'type' => 'Text',
            'options' => array(
                'label' => 'User Name',
            ),
        ));



        $this->add(array(
        'name' => 'submit',
        'type' => 'Submit',
        'attributes' => array(
            'value' => 'Go',
            'id' => 'submitbutton',
            'class' => 'btn8 btn-8 btn-8f pull-right',
        ),
    ));


        $this->add(array(
            'name' => 'cancel',
            'type' => 'button',
            'options'=>array(
                'label'=>'Cancel'
            ),
            'attributes' => array(
                'onclick' => 'javascript:history.back();',
                'class' => 'btn8 btn-8 btn-8f pull-right',
            ),

        ));

//        $this->setAttribute('method', 'post');
//        $this->setAttribute('enctype','multipart/form-data');

    }
}
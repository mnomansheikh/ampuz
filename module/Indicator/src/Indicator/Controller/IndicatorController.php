<?php
namespace Indicator\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Indicator\Model\IndicatorTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Indicator\Model\Indicator;
use Indicator\Form\IndicatorForm;
use Zend\Validator\File\Size;
use Zend\Session\Container;

class IndicatorController extends AbstractActionController
{
    protected $indicatorTable;
    protected $authservice;
    protected $IndicatorTable;


    public function getIndicatorTable()
    {
        if (!$this->IndicatorTable) {
            $sm = $this->getServiceLocator();
            $this->IndicatorTable = $sm->get('Indicator\Model\IndicatorTable');
        }
        return $this->IndicatorTable;
    }


    public function getAuthService()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authservice;
    }


    public function indexAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $form = new IndicatorForm($dbAdapter);
            $form->get('submit')->setValue('Request Indicator');
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user


            $_SESSION['category'] = "community";
            $_SESSION['area'] = "indicator";

            if ($userSession->user_type == _ADMIN_) {
                // grab the paginator from the CommunityTable
                $paginator = $this->getIndicatorTable()->fetchAll_admin($dbAdapter, true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('indicator/index');
                return $view;
            }
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function addAction()
    {

        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            if ($userSession->user_type == _ADMIN_) {
                $form = new IndicatorForm($dbAdapter);
                $form->get('submit')->setValue('Save');
                $form->get('cancel')->setValue('Cancel');
                $request = $this->getRequest();

                if ($request->isPost()) {
                    $indicator = new Indicator();
                    $form->setInputFilter($indicator->getInputFilter());

                    $data = array_merge_recursive(
                        $this->getRequest()->getPost()->toArray(),
                        $this->getRequest()->getFiles()->toArray()
                    );

                    foreach($data['field_name'] AS $fieldname){
                        $indicator->field_name[]          = (!empty($fieldname)) ? $fieldname       : null;
                    }

                    $form->setData($request->getPost());
                    $form->setData($data);

                    if ($form->isValid()) {
                        $indicator->exchangeArray($form->getData());
                        $this->getIndicatorTable()->saveIndicator($indicator, $dbAdapter);
                        // Redirect to list of albums
                        return $this->redirect()->toRoute('indicator', array(
                            'action' => 'index'
                        ));
                    }
                }


                //return array('form' => $form, 'indicator' => $this->getIndicatorTable()->fetchAll($dbAdapter));
                $view = new ViewModel(array(
                    'form' => $form, 'indicator' => $this->getIndicatorTable()->fetchAll($dbAdapter)
                ));
                $view->setTemplate('indicator/add');
                return $view;
            }
            if ($userSession->user_type == _CHAMPION_) {
                return $this->redirect()->toRoute('application');
            }
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function editAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $id = (int)$this->params()->fromRoute('id', 0);
            try {
                $indicator = $this->getIndicatorTable()->getIndicator($id);
            } catch (\Exception $ex) {
                return $this->redirect()->toRoute('indicator', array(
                    'action' => 'index'
                ));
            }
            $form = new IndicatorForm($dbAdapter);
            $form->bind($indicator);
            //for session varibles
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $form->get('cancel')->setValue('Cancel');
            if ($userSession->user_type == _ADMIN_) {
                $form->get('submit')->setAttribute('value', 'Update & Approval');
            }
            if ($userSession->user_type == _CHAMPION_) {
                $form->get('submit')->setAttribute('value', 'Update');
            }
            $request = $this->getRequest();
            if ($request->isPost()) {
                $form->setInputFilter($indicator->getInputFilter());
                $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(),
                    $this->getRequest()->getFiles()->toArray()
                );

                foreach($data['field_name'] AS $fieldname){
                    $indicator->field_name[]          = (!empty($fieldname)) ? $fieldname       : null;
                }

                $form->setData($request->getPost());
                $form->setData($data);

                if ($form->isValid()) {
                    $this->getIndicatorTable()->saveIndicator($indicator, $dbAdapter);

                    // Redirect to list of Community
                    return $this->redirect()->toRoute('indicator');
                }
            }
            $indicator_details = $this->getIndicatorTable()->fetchAll($dbAdapter, $id);
            $indicator = $this->getIndicatorTable()->fetchAll($dbAdapter, $id);

            return array(
                'id' => $id,
                'form' => $form,
                'indicator_details' => $indicator_details,
                'indicator' => $indicator,
            );
        } else {
            return $this->redirect()->toRoute('login');
        }

    }


    public function deleteAction()
    {
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('indicator');
        }
        if ($id) {
            $this->getIndicatorTable()->deleteIndicator($id, $dbAdapter);
            return $this->redirect()->toRoute('indicator');
        }
    }


    public function downloadAction()
    {

        $str = $this->params()->fromRoute('str', 0);
        $id = $this->params()->fromRoute('id', 0);
        $str = trim($str);
        if ($id == 0) {
            $file = 'sampleformat/'.$str;
        } else {
            $file = 'sampleformat/'.$str;
        }

        $response = new \Zend\Http\Response\Stream();
        $response->setStream(fopen($file, 'r'));
        $response->setStatusCode(200);
        $response->setStreamName(basename($file));
        $headers = new \Zend\Http\Headers();
        $headers->addHeaders(array(
            'Content-Disposition' => 'attachment; filename="' . basename($file) .'"',
            'Content-Type' => 'application/octet-stream',
            'Content-Length' => filesize($file),
            'Expires' => '@0', // @0, because zf2 parses date as string to \DateTime() object
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public'
        ));
        $response->setHeaders($headers);
        return $response;


    }

    public function Graph1Action()
    {
        $fromyear = '';
        $toyear = '';
        $indicator_desc = '';
        $indicator = '';
        $company_id = '';
        if ($_REQUEST) {
            extract($_REQUEST);
            $fromyear;
            $toyear;
            $indicator;
            $company_id;
        }
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            //for session varibles
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            if ($userSession->user_type == _ADMIN_) {
                // grab the paginator from the CommunityTable
                $paginator = $this->getIndicatorTable()->fetchAll_admin($dbAdapter, true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);
                $GData = $this->getIndicatorTable()->fetchGraphData($dbAdapter, $fromyear, $toyear, $company_id, $indicator_desc);

                //------------------------------ BAR Chart --------------------------------------
                // grab the paginator from the CommunityTable
                $principle = $this->getIndicatorTable()->fetchAll_principle($dbAdapter, true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $principle->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $principle->setItemCountPerPage(10);

                //------------------------------ PIE Chart --------------------------------------
                // grab the paginator from the CommunityTable
                $principle_pie = $this->getIndicatorTable()->fetchAll_principle($dbAdapter, true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $principle_pie->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $principle_pie->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'GData' => $GData,
                    'indicator' => $indicator,
                    'paginator' => $paginator,
                    'indicator_desc' => $indicator_desc,
                    'principle' => $principle,
                    'principle_pie' => $principle_pie,
                ));

                $view->setTemplate('indicator/analytic');
                return $view;
            }
            if ($userSession->user_type == _CHAMPION_) {
                return $this->redirect()->toRoute('application');
            }
        } else {
            return $this->redirect()->toRoute('login');
        }

    }
}
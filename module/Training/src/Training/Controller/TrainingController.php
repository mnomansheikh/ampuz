<?php
namespace Training\Controller;

use Zend\Http\Headers;
use Zend\Http\Response\Stream;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Training\Model\TrainingTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Training\Model\Training;
use Training\Form\TrainingForm;
use Zend\Validator\File\Size;
use Zend\Session\Container;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class TrainingController extends AbstractActionController
{
    protected $trainingTable;
    protected $authservice;
    protected $EventTable;

    public function getAuthService()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authservice;
    }
    public function getEventTable()
    {
        if (!$this->EventTable) {
            $sm = $this->getServiceLocator();
            $this->EventTable = $sm->get('Event\Model\EventTable');
        }
        return $this->EventTable;
    }
    public function getMenu()
    {
        if (!$this->MenuTable) {
            $sm = $this->getServiceLocator();
            $this->MenuTable = $sm->get('Application\Model\MenuTable');
        }
        return $this->MenuTable;
    }

    public function indexAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $form = new TrainingForm();
            $form->get('submit')->setValue('Request Indicator');
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $config = $this->getServiceLocator()->get('Config');
            $dsn = $config['db']['dsn'];
            $username = $config['db']['username'];
            $password = $config['db']['password'];
            $controllerClass= __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            //$Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            //$Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            //$Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsIDByGroup($dbAdapter,$moduleNamespace);
            //$frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
            //$record_expiration = $this->getMenu()->RecordExpiration($dbAdapter,$moduleNamespace,$Indicators,$Companies,$Countries,$frequencies);

            $_SESSION['category']   = "community";
            $_SESSION['area'] = "training";
            if ($userSession->user_type == _ADMIN_) {
                // grab the paginator from the CommunityTable
                $paginator = $this->getMenu()->fetchAll_admin($dbAdapter, true,$moduleNamespace,$dsn,$username,$password,$Indicators);
                //$paginator = $this->getTrainingTable()->fetchAll_admin($dbAdapter, true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('training/admin');
                return $view;
            }

            if ($userSession->user_type == _CHAMPION_) {
//                $form = new TrainingForm();
//                $form->get('submit')->setValue('Add');
                $request = $this->getRequest();

                if ($request->isPost()) {
                    $training = new Training();
                    $form->setInputFilter($training->getInputFilter());
//          File upload
                    $nonFile = $request->getPost()->toArray();
                    $File = $this->params()->fromFiles('fileupload');
                    $data = array_merge(
                        $nonFile, //POST
                        array('fileupload' => $File['name']) //FILE...
                    );

                    $data = array_merge_recursive(
                        $this->getRequest()->getPost()->toArray(),
                        $this->getRequest()->getFiles()->toArray()
                    );

                    $form->setData($request->getPost());
                    $form->setData($data);

                    if ($form->isValid()) {

                        $training->exchangeArray($form->getData());
                        $this->getTrainingTable()->saveTraining($training, NULL);
                        // Redirect to list of albums
                        return $this->redirect()->toRoute('training');

                        $size = new Size(array('min' => 2000000)); //minimum bytes filesize

                        $adapter = new \Zend\File\Transfer\Adapter\Http();
                        //validator can be more than one...
                        $adapter->setValidators(array($size), $File['name']);

                        if (!$adapter->isValid()) {
                            $dataError = $adapter->getMessages();
                            $error = array();
                            foreach ($dataError as $key => $row) {
                                $error[] = $row;
                            } //set formElementErrors
                            $form->setMessages(array('fileupload' => $error));
                        } else {
                            $adapter->setDestination(dirname(__DIR__) . '/assets');
                            if ($adapter->receive($File['name'])) {
                                $profile->exchangeArray($form->getData());
                                echo 'Profile Name ' . $profile->profilename . ' upload ' . $profile->fileupload;
                            }
                        }
                    }
                }
                // grab the paginator from the CommunityTable
               // $paginator = $this->getTrainingTable()->fetchAll_champion($dbAdapter, true);
                $paginator = $this->getMenu()->fetchAll_champion($dbAdapter, true,$moduleNamespace,$dsn,$username,$password,$Indicators);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                    'form' => $form
                ));
                $view->setTemplate('training/index');
                return $view;
                //  return array('form' => $form, 'training' => $this->getTrainingTable()->fetchAll($dbAdapter));
            }

        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function getTrainingTable()
    {
        if (!$this->trainingTable) {
            $sm = $this->getServiceLocator();
            $this->trainingTable = $sm->get('Training\Model\TrainingTable');
        }
        return $this->trainingTable;
    }

    public function uploadFormAction()
    {
        $form = new UploadForm('upload-form');
        $tempFile = null;

        $prg = $this->fileprg($form);
        if ($prg instanceof \Zend\Http\PhpEnvironment\Response) {
            return $prg; // Return PRG redirect response
        } elseif (is_array($prg)) {
            if ($form->isValid()) {
                $data = $form->getData();
                // Form is valid, save the form!
                return $this->redirect()->toRoute('upload-form/success');
            } else {
                // Form not valid, but file uploads might be valid...
                // Get the temporary file information to show the user in the view
                $fileErrors = $form->get('image-file')->getMessages();
                if (empty($fileErrors)) {
                    $tempFile = $form->get('image-file')->getValue();
                }
            }
        }

        return array(
            'form' => $form,

            'tempFile' => $tempFile,
        );
    }

    public function addAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            if ($userSession->user_type == _ADMIN_) {
                // grab the paginator from the CommunityTable
                $paginator = $this->getTrainingTable()->fetchAll_admin($dbAdapter, true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);

                $view = new ViewModel(array(
                    'paginator' => $paginator,
                ));
                $view->setTemplate('training/admin');
                return $view;
            }

            if ($userSession->user_type == _CHAMPION_) {
                $controllerClass= __NAMESPACE__;
                $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
                $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
                $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
                $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
                $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter,$moduleNamespace);
                $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
                $DateField = $this->getMenu()->DateField();
                $form = new TrainingForm($Companies,$Regions,$Countries,$Indicators,$frequencies,$DateField);
                $form->get('submit')->setValue('Save');
                $request = $this->getRequest();

                if ($request->isPost()) {
                    $training = new Training();
                    $created_date =$this->getRequest()->getPost('create_date');
                    $indicator_id =$this->getRequest()->getPost('type');
                    $company_id =$this->getRequest()->getPost('company_id');
                    $country_id =$this->getRequest()->getPost('country_id');
                    $frequency_id =$this->getRequest()->getPost('frequency_id');
                    $date_validation = $this->getMenu()->DateFieldValidation($dbAdapter,$moduleNamespace,$created_date,$indicator_id,$company_id,$country_id,$frequency_id);
                    $form->setInputFilter($training->getInputFilter($date_validation));
                    $form->setData($request->getPost());
//          File upload
                    $nonFile = $request->getPost()->toArray();
                    $File = $this->params()->fromFiles('fileupload');
                    $data = array_merge(
                        $nonFile, //POST
                        array('fileupload' => $File['name']) //FILE...
                    );

                    $data = array_merge_recursive(
                        $this->getRequest()->getPost()->toArray(),
                        $this->getRequest()->getFiles()->toArray()
                    );

                    $form->setData($request->getPost());
                    $form->setData($data);

                    if ($form->isValid()) {
                        // $handle = fopen($_FILES["fileupload"]["tmp_name"], 'r');
                        $file_name_tmp = $form->getData()["fileupload"]["tmp_name"];
                        $file_name =$form->getData()["fileupload"]["name"];
                        $temp = explode(".", $file_name);
                        $newfilename = $temp[0] . '.' . end($temp);
                        $add = "$newfilename"; // the path with the file name where the file will be stored
                        $upload = move_uploaded_file($file_name_tmp, $add);
                        $AttachedFile = $add;
                        echo "File Upload Successfully!";
                        $show = FALSE;
                        $training->exchangeArray($form->getData());
                        /* Expired entries  marked as normal if the desire entry has been made after intimation.*/
                        $this->getMenu()->StatusChangeApproved($dbAdapter,$moduleNamespace,$indicator_id,$company_id,$country_id,$frequency_id);
                        $expire_date = $this->getMenu()->FrequencyChecker($frequency_id,$created_date);
                        $training->expire_date =$expire_date;
                        $training-> file_name = $AttachedFile;
                       $this->getTrainingTable()->saveTraining($training, NULL);
                        // Redirect to list of albums
                        return $this->redirect()->toRoute('training');
                        //        $size = new Size(array('min' => 2000000)); //minimum bytes filesize

                        $adapter = new \Zend\File\Transfer\Adapter\Http();
                        //validator can be more than one...
                        $adapter->setValidators(array($size), $File['name']);

                        if (!$adapter->isValid()) {
                            $dataError = $adapter->getMessages();
                            $error = array();
                            foreach ($dataError as $key => $row) {
                                $error[] = $row;
                            } //set formElementErrors
                            $form->setMessages(array('fileupload' => $error));
                        } else {
                            $adapter->setDestination(dirname(__DIR__) . '/assets');
                            if ($adapter->receive($File['name'])) {
                                $profile->exchangeArray($form->getData());
                                echo 'Profile Name ' . $profile->profilename . ' upload ' . $profile->fileupload;
                            }
                        }
                    }
                }
                return array('form' => $form, 'training' => $this->getTrainingTable()->fetchAll($dbAdapter));
            }

        } else {
            return $this->redirect()->toRoute('login');
        }
    }


    public function editAction()
    {
        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            $id = (int)$this->params()->fromRoute('id', 0);
            try {
                $training = $this->getTrainingTable()->getTraining($id);
            } catch (\Exception $ex) {
                return $this->redirect()->toRoute('training', array(
                    'action' => 'index'
                ));
            }
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $controllerClass= __NAMESPACE__;
            $moduleNamespace = lcfirst(substr($controllerClass, 0, strpos($controllerClass, '\\')));
            $Companies = $this->getMenu()->fetchCompaniesByUser($dbAdapter);
            $Regions = $this->getMenu()->fetchRegionsByUser($dbAdapter);
            $Countries = $this->getMenu()->fetchCountriesByUser($dbAdapter);
            $Indicators = $this->getMenu()->fetchIndicatorsByGroup($dbAdapter,$moduleNamespace);
            $frequencies = $this->getMenu()->fetchFrequenciesByIndicator($dbAdapter,$moduleNamespace);
            $DateField = $this->getMenu()->DateField();

            $form = new TrainingForm($Companies,$Regions,$Countries,$Indicators,$frequencies,$DateField);
            $form->bind($training);
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            if ($userSession->user_type == _ADMIN_) {
                $form->get('submit')->setAttribute('value', 'Update & Approval');
            }
            if ($userSession->user_type == _CHAMPION_) {
                $form->get('submit')->setAttribute('value', 'Update');
            }
            $request = $this->getRequest();
            if ($request->isPost()) {
                $form->setInputFilter($training->getInputFilter());
                $form->setData($request->getPost());

                if ($form->isValid()) {
                    $this->getTrainingTable()->saveTraining($training, $id);

                    // Redirect to list of Community
                    return $this->redirect()->toRoute('training');
                }
            }
            return array(
                'id' => $id,
                'form' => $form,
            );
        } else {
            return $this->redirect()->toRoute('login');
        }
    }

    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('training');
        }
        //  $request = $this->getRequest();
//        if ($request->isPost()) {
//            $del = $request->getPost('del', 'No');
//            if ($del == 'Yes') {
//                $id = (int) $request->getPost('id');
//                $this->getTrainingTable()->deletetraining($id);
//            }
//
//            // Redirect to list of albums
//            return $this->redirect()->toRoute('training');
//        }
        if ($id) {
            $this->getTrainingTable()->deleteTraining($id);
            return $this->redirect()->toRoute('training');
        }

    }

   public function downloadAction()
    {

        $str = $this->params()->fromRoute('str', 0);
        $id = $this->params()->fromRoute('id', 0);
        $str = trim($str);


// first, get MIME information from the file
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime =  finfo_file($finfo, $str);
        finfo_close($finfo);

// send header information to browser
        header('Content-Type: '.$mime);
        header('Content-Disposition: attachment;  filename="'.$str.'"');
        header('Content-Length: ' . filesize($str));
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

//stream file
        ob_get_clean();
        echo file_get_contents($str);
        ob_end_flush();
    }

        public function Graph1Action()
        {
            $fromyear = '';
            $toyear = '';
            $indicator = '';
            $company_id = '';
            $training_pie= '';
            if ($_REQUEST) {
                extract($_REQUEST);
                $fromyear;
                $toyear;
                $indicator;
                $company_id;
            }


            //Check user is logged in
            if ($this->getAuthService()->hasIdentity()) {
                //for session varibles
                $userSession = new Container('user');
                // echo 'Logged in as ' . $userSession->user_type;
                //For admin user
                $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
                if ($userSession->user_type == _ADMIN_) {

                    $GData = $this->getTrainingTable()->fetchGraph1Data($dbAdapter, $fromyear, $toyear, $company_id,$training_pie);
                    // grab the paginator from the CommunityTable
                    $paginator = $this->getEventTable()->fetchAll_admin($dbAdapter,true);
                    // set the current page to what has been passed in query string, or to 1 if none set
                    $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                    // set the number of items per page to 10
                    $paginator->setItemCountPerPage(10);



                    //------------------------------ BAR Chart --------------------------------------
                    // grab the paginator from the CommunityTable
                    $principle = $this->getTrainingTable()->fetchAll_principle($dbAdapter,true);
                    // set the current page to what has been passed in query string, or to 1 if none set
                    $principle->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                    // set the number of items per page to 10
                    $principle->setItemCountPerPage(10);


                    //------------------------------ PIE Chart --------------------------------------
                    // grab the paginator from the CommunityTable
                    $principle_pie = $this->getTrainingTable()->fetchAll_principle($dbAdapter,true);
                    // set the current page to what has been passed in query string, or to 1 if none set
                    $principle_pie->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                    // set the number of items per page to 10
                    $principle_pie->setItemCountPerPage(10);



                    $view = new ViewModel(array(
                        'GData' => $GData,
                        'paginator' => $paginator,
                        'indicator' => $indicator,
                        'principle' => $principle,
                        'principle_pie' => $principle_pie,

                    ));
                    $view->setTemplate('training/analytic');
                    return $view;


                }
                if ($userSession->user_type == _CHAMPION_) {
                    return $this->redirect()->toRoute('application');
                }
            } else {
                return $this->redirect()->toRoute('login');
            }

        }

      function Graph2Action()
    {
        $fromyear = '';
        $toyear = '';
        $indicator = '';
        $num_training='';

        $company_id = '';
        if ($_REQUEST) {
            extract($_REQUEST);
            $fromyear;
            $toyear;
            $indicator;
            $company_id;
        }


        //Check user is logged in
        if ($this->getAuthService()->hasIdentity()) {
            //for session varibles
            $userSession = new Container('user');
            // echo 'Logged in as ' . $userSession->user_type;
            //For admin user
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            if ($userSession->user_type == _ADMIN_) {

                $GData = $this->getTrainingTable()->fetchGraph1Data($dbAdapter, $fromyear, $toyear, $company_id,$num_training);
                // grab the paginator from the CommunityTable
                $paginator = $this->getEventTable()->fetchAll_admin($dbAdapter,true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $paginator->setItemCountPerPage(10);



                //------------------------------ BAR Chart --------------------------------------
                // grab the paginator from the CommunityTable
                $principle = $this->getTrainingTable()->fetchAll_principle($dbAdapter,true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $principle->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $principle->setItemCountPerPage(10);


                //------------------------------ PIE Chart --------------------------------------
                // grab the paginator from the CommunityTable
                $principle_pie = $this->getTrainingTable()->fetchAll_principle($dbAdapter,true);
                // set the current page to what has been passed in query string, or to 1 if none set
                $principle_pie->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
                // set the number of items per page to 10
                $principle_pie->setItemCountPerPage(10);



                $view = new ViewModel(array(
                    'GData' => $GData,
                    'paginator' => $paginator,
                    'principle' => $principle,
                    'indicator' => $indicator,
                    'principle_pie' => $principle_pie,

                ));

                $view->setTemplate('training/analytic2');
                return $view;


            }
            if ($userSession->user_type == _CHAMPION_) {
                return $this->redirect()->toRoute('application');
            }
        } else {
            return $this->redirect()->toRoute('login');
        }

    }
}
<?php
namespace Training\Model;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Training implements InputFilterAwareInterface
{
    public $id;
    public $frequency_id;
    public $company_id;
    public $region_id;
    public $country_id;
    public $type;
    public $value;
    public $file_name;
    public $inputFilter;
    public $training_id;
    public $create_date;


    public function exchangeArray($data)
    {
        $this->training_id          = (!empty($data['training_id'])) ? $data['training_id'] : null;
        $this->company_id           = (!empty($data['company_id'])) ? $data['company_id'] : null;
        $this->region_id           = (!empty($data['region_id'])) ? $data['region_id'] : null;
        $this->country_id           = (!empty($data['country_id'])) ? $data['country_id'] : null;
        $this->frequency_id         = (!empty($data['frequency_id'])) ? $data['frequency_id'] : null;
        $this->type                 = (!empty($data['type'])) ? $data['type'] : null;
        $this->value                = (!empty($data['value'])) ? $data['value'] : null;
        $this->file_name            = (!empty($data['file_name'])) ? $data['file_name'] : null;
        $this->id                   = (!empty($data['id'])) ? $data['id'] : null;
        $this->create_date                   = (!empty($data['create_date'])) ? $data['create_date'] : null;


    }
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter($date_validation=null)
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            if($date_validation != null){
                $inputFilter->add(
                    $date_validation
                );
            }
//            $inputFilter->add(array(
//                'name'     => 'id',
//                'required' => false,
//                'filters'  => array(
//                    array('name' => 'Int'),
//                ),
//            ));

            $inputFilter->add(array(
                'name'     => 'company_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                /*'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),*/
            ));
            $inputFilter->add(array(
                'name'     => 'department_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
//                'validators' => array(
//                    array(
//                        'name'    => 'StringLength',
//                        'options' => array(
//                            'encoding' => 'UTF-8',
//                            'min'      => 1,
//                            'max'      => 100,
//                        ),
//                    ),
//                ),
            ));
            $inputFilter->add(array(
                'name'     => 'frequency_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
//                'validators' => array(
//                    array(
//                        'name'    => 'StringLength',
//                        'options' => array(
//                            'encoding' => 'UTF-8',
//                            'min'      => 1,
//                            'max'      => 100,
//                        ),
//                    ),
//                ),
            ));
            $inputFilter->add(array(
                'name'     => 'type',
                'required' => false,

                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
//                'validators' => array(
//                    array(
//                        'name'    => 'StringLength',
//                        'options' => array(
//                            'encoding' => 'UTF-8',
//                            'min'      => 1,
//                            'max'      => 100,
//                        ),
//                    ),
//                ),
            ));

            $inputFilter->add(array(
                'name'     => 'value',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));

            $inputFilter->add(
                $factory->createInput(array(
                    'name'     => 'fileupload',
                    'required' => false,
                ))
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
<?php
namespace Training\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Http\Header\Date;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
class TrainingTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;

    }

    public function fetchAll($adapter)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        //  $resultSet = $this->tableGateway->select(array('user_id' => $user_id,'status'=>_P_));

        // create a new Select object for the table community
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('training');
        $select->where(array('training.user_id' => $user_id,'training.status'=>_P_));
        $select->join('user_profile', 'user_profile.user_id = training.user_id', 'user_name');
        $select->join('company', 'company.company_id = training.company_id', 'company_name');
     /*   $select->join('department', 'department.department_id = training.department_id', 'department_name');*/
        $select->join('frequency', 'frequency.frequency_id = training.frequency_id', 'frequency_name');
        //$select->join('principle', 'principle.principle_id = training.principle_id', 'principle_name');
        $select->join('indicator', 'indicator.id = training.type', 'name');
        $select->order('training_id DESC');


        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }


    public function fetchAll_principle($adapter,$paginated=false)
    {
        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('indicator');
            $select->where(array('sub_menu_id' => 10));
            $select->order('principle_id DESC');
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function fetchAll_admin($adapter,$paginated=false)
    {

    if ($paginated) {
        // create a new Select object for the table community
        $userSession = new Container('user');
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('training');
        $select->join('user_profile', 'user_profile.user_id = training.user_id',array('user_name','user_email'));
        $select->join('company', 'company.company_id = training.company_id', 'company_name');
        $select->join('country', 'country.id = training.country_id','country_name');
        $select->join('region', 'region.id = training.region_id','region_description');
      /*  $select->join('department', 'department.department_id = training.department_id', 'department_name');*/
        $select->join('frequency', 'frequency.frequency_id = training.frequency_id', 'frequency_name');
        //$select->join('principle', 'principle.principle_id = training.principle_id', 'principle_name');
        $select->join('indicator', 'indicator.id = training.type', 'name');
        $select->where(array('training.company_id'=>$userSession->user_companies,'training.country_id'=>$userSession->user_countries,'training.region_id'=>$userSession->user_regions));
        $select->order('training_id DESC');

        // create a new result set based on the Community entity
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Training());
        // create a new pagination adapter object
        $paginatorAdapter = new DbSelect(
        // our configured select object
            $select,
            // the adapter to run it against
            $adapter
        // the result set to hydrate
        // $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
        }
    }

    public function fetchAll_champion($adapter,$paginated=false)
    {
        //for session varibles
        $userSession = new Container('user');
        //  echo 'Logged in as ' . $userSession->username;
        $user_id=  $userSession->user_id;

        if ($paginated) {
            // create a new Select object for the table community
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('training');
            $select->where(array('training.user_id' => $user_id));
            $select->join('user_profile', 'user_profile.user_id = training.user_id', 'user_name');
            $select->join('company', 'company.company_id = training.company_id', 'company_name');
          //  $select->join('department', 'department.department_id = training.department_id', 'department_name');
            $select->join('frequency', 'frequency.frequency_id = training.frequency_id', 'frequency_name');
            //$select->join('principle', 'principle.principle_id = training.principle_id', 'principle_name');
            $select->join('indicator', 'indicator.id = training.type', 'name');
            $select->order('training_id DESC');

            // create a new result set based on the Community entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Training());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $adapter
            // the result set to hydrate
            // $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
    }

    public function getTraining($id)
    {

        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('training_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveTraining(Training $training,$id)
    {
       // $id = (int) $training->training_id;
        $userSession = new Container('user');
        $user_id = $userSession->user_id;
        $user_type = $userSession->user_type;
        $oneYearOn = date('Y-m-d', strtotime(date("Y-m-d", time()) . " + 365 day"));

        if ($id == 0) {
            $data = array(
                'training_id' => $training->training_id,
                'company_id' => $training->company_id,
                'region_id'        => $training->region_id,
                'country_id'        => $training->country_id,
                'department_id' => $training->department_id,
                'frequency_id' => $training->frequency_id,
                'type' => $training->type,
                'value' => $training->value,
                'file_name' => $training->file_name,
                'user_id' => $user_id,
                'status' => _P_,
                'create_date'       => $training->create_date,
                'expiry_date'       => $training->expire_date
            );


            $this->tableGateway->insert($data);
        } else {
            if ($this->getTraining($id)) {
                if ($user_type == _ADMIN_) {
                    $data = array(
                       /* 'company_id' => $training->company_id,
                        'department_id' => $training->department_id,
                        'frequency_id' => $training->frequency_id,
                        'region_id'        => $training->region_id,
                        'country_id'        => $training->country_id,
                        'type' => $training->type,*/
                        'value' => $training->value,
                       'file_name' => $training->file_name,
                        'status' => _A_,
                    );
                }
                if ($user_type == _CHAMPION_) {
                    $data = array(
                     /*   'company_id' => $training->company_id,
                        'region_id'        => $training->region_id,
                        'country_id'        => $training->country_id,
                        'department_id' => $training->department_id,
                        'frequency_id' => $training->frequency_id,
                        'type' => $training->type,*/
                        'value' => $training->value,
                        'file_name' => $training->file_name,
                        'status' => _P_,
                    );
                }
             $this->tableGateway->update($data, array('training_id' => $id));
        }
            else {
        throw new \Exception('Training id does not exist');
            }
        }
    }
    public function deleteTraining($id)
    {
        $this->tableGateway->delete(array('training_id' => (int) $id));
    }

    public function fetchGraphData($adapter,$fromyear=NULL,$toyear=NULL,$company_id =NULL,$num_training = NULL,$training_pie=NULL)
    {
        $sql = new Sql($adapter);

        $select = $sql->select();
        $expression = new Expression("MONTH(Training.create_date)");
        $select->columns(array( new Expression("MONTH(Training.create_date) AS MONTH"), new Expression("SUM(no_of_training) AS no_student"),'principle_id'));
        $select->from('Training');
        $select->join('principle', 'principle.principle_id = training.principle_id', 'principle_name');
        $select->group(array(new Expression("MONTH(Training.create_date)"),'principle_id'));
        $select->order(array(new Expression("MONTH(Training.create_date)"),'principle_id'));
        if($fromyear== NULL){

            $fromyear = date('Y');
            $toyear = date('Y');
            $year1 =(int)$fromyear;
            $year2 =(int)$toyear;
        }
        else{

            $year1 = (int)$fromyear;
            $year2 = (int)$toyear;
        }

        $yearexp = (new Expression("MONTH(Training.create_date)"));
        // $subSelect = new Select('community_partnership');
        $select->where(array('Training.status'=>_A_));
        if($company_id != NULL){
            $company_id = (int)$company_id;
            $select->where(array('Training.status'=>_A_,'company_id'=>$company_id));
        }
        if($num_training != NULL){
            $select->where(array('Training.status'=>_A_,'training.no_of_training'=>$num_training));
        }
        if($training_pie != NULL){
            $select->where(array('Training.status'=>_A_,'training.principle_id'=>$training_pie));
        }
        else{

            $select->where(array('Training.status'=>_A_));
        }
        $select->where->between($yearexp, $year1, $year2);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }



    public function fetchGraph1Data($adapter,$fromyear=NULL,$toyear=NULL,$company_id =NULL,$num_training = NULL)
    {
        $sql = new Sql($adapter);

        $select = $sql->select();
        $expression = new Expression("MONTH(Training.create_date)");
        $select->columns(array( new Expression("MONTH(Training.create_date) AS MONTH"), new Expression("SUM(value) AS no_student")));
        $select->from('Training');
        $select->join('indicator', 'indicator.id = training.type', 'name');
        $select->group(array(new Expression("MONTH(Training.create_date)")));
        $select->order(array(new Expression("MONTH(Training.create_date)")));
        if($fromyear== NULL){
            $fromyear = date('Y');
            $toyear = date('Y');
            $year1 =(int)$fromyear;
            $year2 =(int)$toyear;
        }
        else{

            $year1 = (int)$fromyear;
            $year2 = (int)$toyear;
        }

        $yearexp = (new Expression("MONTH(Training.create_date)"));
        // $subSelect = new Select('community_partnership');
        $select->where(array('Training.status'=>_A_));
        if($company_id != NULL){
            $company_id = (int)$company_id;
            $select->where(array('Training.status'=>_A_,'company_id'=>$company_id));
        }
      //  echo $num_training;
        if($num_training != NULL){
            $select->where(array('Training.status'=>_A_,'training.type'=>$num_training));
        }
        else{

            $select->where(array('Training.status'=>_A_));
        }
        $select->where->between($yearexp, $year1, $year2);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
}
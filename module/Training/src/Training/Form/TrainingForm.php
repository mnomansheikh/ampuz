<?php
namespace Training\Form;
use Zend\InputFilter;
use Zend\Form\Element;
use Zend\Form\Form;

class TrainingForm extends Form
{

    public function addElements()
    {
        // File Input
        $file = new Element\File('image-file');
        $file->setLabel('Avatar Image Upload')
            ->setAttribute('id', 'image-file');
        $this->add($file);
    }
    public function addInputFilter()
    {
        $inputFilter = new InputFilter\InputFilter();

        // File Input
        $fileInput = new InputFilter\FileInput('image-file');
        $fileInput->setRequired(true);
        $fileInput->getFilterChain()->attachByName(
            'filerenameupload',
            array(
                'target'    => 'd:/',
                'randomize' => true,
            )
        );

        $inputFilter->add($fileInput);
        $this->setInputFilter($inputFilter);
    }

    public function __construct($Companies = null,$Regions=null,$Countries= null,$Indicators=null,$Frequencies = null,$DateField=null)

    {
        // we want to ignore the name passed
        parent::__construct('training');
        $this->addElements();
        $this->addInputFilter();

        $this->add(array(
            'name' => 'training_id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'category',
            'type' => 'text',
            'attributes' => array(
                'value' => 'environment'
            )
        ));

        $this->add(array(
            'name' => 'area',
            'type' => 'text',
            'attributes' => array(
                'value' => 'training'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'company_id',
            'options' => array(
                'label' => 'Company',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Companies,
            ),
            'attributes' => array(
               // 'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'company_form_id',

            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'region_id',
            'options' => array(
                'label' => 'Country',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Regions,
            ),
            'attributes' => array(
                // 'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'region_form_id',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country_id',
            'options' => array(
                'label' => 'Country',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Countries,
            ),
            'attributes' => array(
                // 'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'country_form_id',

            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'frequency_ids',
            'options' => array(
                'label' => 'Frequency',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Frequencies,
            ),
            'attributes' => array(
             //   'value' => '1', //set selected to '1'
                'class' => 'form-control',
                'id' => 'frequency_form_ids',
            )
        ));


        $this->add(array(
            'name' => 'frequency_name',
            'type' => 'text',
            'attributes' => array(
                'readonly' => 'readonly',
                 'class' => 'form-control',
                'id' => 'frequency_form_name',
            )
        ));

        $this->add(array(
            'name' => 'frequency_id',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'frequency_form_id',
            )
        ));
        if($DateField != null){
            $this->add(
                $DateField
            );
       }
        $this->add(array(
            'name' => 'month',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'month',
            )
        ));
        $this->add(array(
            'name' => 'year',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'year',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'type',
            'options' => array(
                'label' => 'Type',
                //'value_options' => $this->getOptionsForSelect(),
                'value_options' => $Indicators,
            ),
            'attributes' => array(
              /*  'value' => '1', //set selected to '1'*/
                'class' => 'form-control',
                'id' => 'indicator_form_id',
            )
        ));

        /*$this->add(array(
            'name' => 'no_of_training',
            'type' => 'Text',
            'options' => array(
                'label' => 'No of Training',
                'attributes' => array(
                    'class' => 'control-label col-sm-1 col-sm-2 col-md-2 col-lg-2 ',
                ),
            ),
            'attributes' => array(
                'class' => 'textbox_size',
                'required'  => 'required',
            ),
        ));

        $this->add(array(
            'name' => 'event_description',
            'attributes' => array(
                'class' => 'form-control',
                'required' =>    'required',
            ),
        ));*/


        $this->add(array(
            'name' => 'value',
            'attributes' => array(
                'class' => 'form-control',
                'required' =>    'required',
            ),
        ));



        $this->add(array(
            'name' => 'file_name',
            'type' => 'hidden',
            'options' => array(
                'label' => 'File Name',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
                'class' => 'btn8 btn-8f btn-8 pull-right',
            ),
        ));
        $this->add(array(
            'name' => 'upload',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'File Attachment',
                'id' => 'submitbutton1',
                'class' => 'btn btn-primary btn-xs',
            ),
        ));
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');

        $this->add(array(
            'name' => 'fileupload',
            'attributes' => array(
                'type'  => 'file',
                'required'  => 'required',
            ),
            'options' => array(
                'label' => '',
            ),
        ));

        $this->add(array(
            'name' => 'cancel',
            'type' => 'button',
            'options'=>array(
                'label'=>'Cancel'
            ),
            'attributes' => array(
                'onclick' => 'javascript:window.location.href = "/'._PROJECT_NAME_.'/public/training";',
                'class' => 'btn8 btn-8f btn-8 pull-right',
            ),

        ));

    }
}
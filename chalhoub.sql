/*
SQLyog Trial v12.13 (64 bit)
MySQL - 5.6.17 : Database - chalhoub
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`chalhoub` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `chalhoub`;

/*Table structure for table `city` */

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(40) DEFAULT NULL,
  KEY `city_id` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `city` */

insert  into `city`(`city_id`,`city_name`) values (1,'Karachi'),(2,'Beijing');

/*Table structure for table `community_partner` */

DROP TABLE IF EXISTS `community_partner`;

CREATE TABLE `community_partner` (
  `community_partner_id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_name` varchar(40) DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `community_partner_id` (`community_partner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `community_partner` */

insert  into `community_partner`(`community_partner_id`,`partner_name`,`status`,`create_date`,`modify_date`) values (1,'Friends of Cancer Patients','A','2015-08-10 10:51:08','0000-00-00 00:00:00'),(2,'Dubai Cares',NULL,'2015-08-10 10:51:28','0000-00-00 00:00:00'),(3,'Jusoor','A','2015-08-10 10:51:55','0000-00-00 00:00:00'),(4,'Emirates Marine Group','A','2015-08-10 10:52:37','0000-00-00 00:00:00'),(5,'Start','A','2015-08-10 10:52:41','0000-00-00 00:00:00'),(6,'Give A Ghaf Program','A','2015-08-10 10:53:21','0000-00-00 00:00:00');

/*Table structure for table `community_partnership` */

DROP TABLE IF EXISTS `community_partnership`;

CREATE TABLE `community_partnership` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `frequency_id` int(11) DEFAULT NULL,
  `community_partner_id` int(11) DEFAULT NULL,
  `no_pins` int(11) DEFAULT NULL,
  `no_employee_engage` int(11) DEFAULT NULL,
  `no_employee_trained` int(11) DEFAULT NULL,
  `no_student_literacy_center` int(11) DEFAULT NULL,
  `no_student_scholar` int(11) DEFAULT NULL,
  `no_employee_engage_beachcleanup` int(11) DEFAULT NULL,
  `no_workshop` int(11) DEFAULT NULL,
  `no_tree_planted` int(11) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expiry_date` datetime DEFAULT NULL,
  `status_approval` varchar(40) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

/*Data for the table `community_partnership` */

insert  into `community_partnership`(`id`,`company_id`,`frequency_id`,`community_partner_id`,`no_pins`,`no_employee_engage`,`no_employee_trained`,`no_student_literacy_center`,`no_student_scholar`,`no_employee_engage_beachcleanup`,`no_workshop`,`no_tree_planted`,`create_date`,`expiry_date`,`status_approval`,`user_id`,`modify_date`) values (1,1,2,1,1,1,1,NULL,NULL,NULL,NULL,NULL,'2015-08-12 16:36:45','2016-08-11 00:00:00','Approved',1,'0000-00-00 00:00:00'),(2,1,2,1,2,3,6,NULL,NULL,NULL,NULL,NULL,'2015-08-12 17:16:03','2016-08-11 00:00:00','Approved',1,'0000-00-00 00:00:00'),(3,1,2,1,1,2,4,NULL,NULL,NULL,NULL,NULL,'2015-08-12 17:20:20','2016-08-11 00:00:00','Approved',1,'0000-00-00 00:00:00'),(4,1,2,1,11111,888,2222,NULL,NULL,NULL,NULL,NULL,'2015-08-13 16:12:25','2016-08-11 00:00:00','Approved',1,'0000-00-00 00:00:00'),(7,1,2,2,1,200000,3,NULL,NULL,NULL,NULL,NULL,'2015-08-13 19:36:21','2016-08-12 00:00:00','Approved',1,'0000-00-00 00:00:00'),(8,1,2,2,45,1000065466,3,NULL,NULL,NULL,NULL,NULL,'2015-08-13 19:36:51','2016-08-12 00:00:00','Approved',1,'0000-00-00 00:00:00'),(9,1,2,3,NULL,NULL,NULL,45,30,NULL,NULL,NULL,'2015-08-13 19:59:06','2016-08-12 00:00:00','Approved',1,'0000-00-00 00:00:00'),(12,1,2,4,NULL,NULL,NULL,NULL,NULL,125555555,NULL,NULL,'2015-08-13 00:00:00','2016-08-12 00:00:00','Pending',1,'0000-00-00 00:00:00'),(13,1,2,4,NULL,NULL,NULL,NULL,NULL,123456789,NULL,NULL,'2015-08-13 00:00:00','2016-08-12 00:00:00','Pending',1,'0000-00-00 00:00:00'),(16,1,2,2,10,45,32,NULL,NULL,NULL,NULL,NULL,'2015-08-13 00:00:00','2016-08-12 00:00:00','Pending',1,'0000-00-00 00:00:00'),(17,1,2,5,NULL,12,NULL,NULL,NULL,NULL,5667,NULL,'2015-08-13 20:00:01','2016-08-12 00:00:00','Approved',1,'0000-00-00 00:00:00'),(19,1,2,5,NULL,3333,NULL,NULL,NULL,NULL,777,NULL,'2015-08-13 00:00:00','2016-08-12 00:00:00','Pending',1,'0000-00-00 00:00:00'),(20,1,2,2,12,23,45,NULL,NULL,NULL,NULL,NULL,'2015-08-13 00:00:00','2016-08-12 00:00:00','Pending',1,'0000-00-00 00:00:00'),(24,1,2,2,1,2,3,NULL,NULL,NULL,NULL,NULL,'2015-08-13 00:00:00','2016-08-12 00:00:00','Pending',2,'0000-00-00 00:00:00'),(29,1,2,1,1,3,4,NULL,NULL,NULL,NULL,NULL,'2015-08-13 00:00:00','2016-08-12 00:00:00','Pending',2,'0000-00-00 00:00:00'),(30,1,2,1,3424,12341,34134134,NULL,NULL,NULL,NULL,NULL,'2015-08-13 00:00:00','2016-08-12 00:00:00','Pending',1,'0000-00-00 00:00:00'),(31,1,2,1,4534,45343,787852,NULL,NULL,NULL,NULL,NULL,'2015-08-13 19:59:43','2016-08-12 00:00:00','Approved',1,'0000-00-00 00:00:00'),(32,1,2,1,2121,777,533,NULL,NULL,NULL,NULL,NULL,'2015-08-13 00:00:00','2016-08-12 00:00:00','Pending',1,'0000-00-00 00:00:00'),(49,1,2,1,1,3,5,NULL,NULL,NULL,NULL,NULL,'2015-08-13 00:00:00','2016-08-12 00:00:00','Pending',1,'0000-00-00 00:00:00'),(50,1,2,2,3,5,6,NULL,NULL,NULL,NULL,NULL,'2015-08-13 00:00:00','2016-08-12 00:00:00','Pending',1,'0000-00-00 00:00:00'),(51,1,2,1,1,2,3,NULL,NULL,NULL,NULL,NULL,'2015-08-15 00:00:00','2016-08-14 00:00:00','Pending',1,'0000-00-00 00:00:00');

/*Table structure for table `company` */

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(60) NOT NULL,
  `company_address` varchar(120) DEFAULT NULL,
  `company_email` varchar(40) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(40) DEFAULT NULL,
  `modified_by` varchar(40) DEFAULT NULL,
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `company` */

insert  into `company`(`company_id`,`company_name`,`company_address`,`company_email`,`country_id`,`city_id`,`status`,`create_date`,`modify_date`,`created_by`,`modified_by`) values (1,'MCT FZE','46/51 street','mct@yahoo.com',1,1,'active','2015-08-04 11:20:29','0000-00-00 00:00:00',NULL,NULL),(2,'Alied Enterprise','F/51 street','alied@yahoo.com',2,2,'active','2015-08-04 11:20:29','0000-00-00 00:00:00',NULL,NULL);

/*Table structure for table `country` */

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(40) DEFAULT NULL,
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `country` */

insert  into `country`(`country_id`,`country_name`) values (1,'Pakistan'),(2,'China');

/*Table structure for table `department` */

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(40) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(40) DEFAULT NULL,
  KEY `department_id` (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `department` */

insert  into `department`(`department_id`,`department_name`,`company_id`,`create_date`,`modify_date`,`created_by`,`modified_by`,`status`) values (1,'HR',1,'2015-08-04 11:05:35','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),(2,'Finance',1,'2015-08-04 11:05:35','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL);

/*Table structure for table `education` */

DROP TABLE IF EXISTS `education`;

CREATE TABLE `education` (
  `education_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `frequency_id` int(11) DEFAULT NULL,
  `no_of_scholarship` int(11) DEFAULT NULL,
  `edu_ex_students` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expiry_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `education_id` (`education_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `education` */

insert  into `education`(`education_id`,`company_id`,`frequency_id`,`no_of_scholarship`,`edu_ex_students`,`user_id`,`status`,`create_date`,`expiry_date`,`modify_date`) values (3,1,2,2,2,1,'Approved','2015-08-15 13:16:37','2016-08-14 00:00:00','0000-00-00 00:00:00'),(5,1,2,1,2,1,'Pending','2015-08-15 00:00:00','2016-08-14 00:00:00','0000-00-00 00:00:00'),(6,1,2,1,2,1,'Pending','2015-08-15 00:00:00','2016-08-14 00:00:00','0000-00-00 00:00:00'),(7,1,2,3,4,1,'Pending','2015-08-15 00:00:00','2016-08-14 00:00:00','0000-00-00 00:00:00'),(8,1,2,1,1,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00','0000-00-00 00:00:00');

/*Table structure for table `event` */

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_description` varchar(40) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modifity_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(40) DEFAULT NULL,
  `event_atten_designation` varchar(120) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `frequency_id` int(11) DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  KEY `event_id` (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

/*Data for the table `event` */

insert  into `event`(`event_id`,`event_description`,`create_date`,`modifity_date`,`name`,`event_atten_designation`,`user_id`,`company_id`,`department_id`,`frequency_id`,`status`) values (32,'Beach','2015-08-14 03:24:41','0000-00-00 00:00:00','uzair','Manager',1,1,1,1,'Approved'),(33,'Beach','2015-08-14 03:25:12','0000-00-00 00:00:00','Ali','Manager',1,2,2,1,'Approved'),(37,'Beach','2001-01-15 00:00:00','0000-00-00 00:00:00','Jamil','Manager',1,1,1,1,'Pending'),(38,'Beach','2001-01-15 00:00:00','0000-00-00 00:00:00','uzair','Manager',1,1,1,1,'Pending'),(39,'Beach','2001-01-15 00:00:00','0000-00-00 00:00:00','uzair','Manager',1,1,1,1,'Pending');

/*Table structure for table `frequency` */

DROP TABLE IF EXISTS `frequency`;

CREATE TABLE `frequency` (
  `frequency_id` int(11) NOT NULL AUTO_INCREMENT,
  `frequency_name` varchar(40) DEFAULT NULL,
  KEY `frequency_id` (`frequency_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `frequency` */

insert  into `frequency`(`frequency_id`,`frequency_name`) values (1,'Monthly'),(2,'Yearly');

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(40) NOT NULL,
  `status` varchar(40) DEFAULT NULL,
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `menu` */

/*Table structure for table `principle` */

DROP TABLE IF EXISTS `principle`;

CREATE TABLE `principle` (
  `principle_id` int(11) NOT NULL AUTO_INCREMENT,
  `principle_name` varchar(40) DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `principle_id` (`principle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `principle` */

insert  into `principle`(`principle_id`,`principle_name`,`status`,`create_date`) values (1,'enviornment','active','2015-08-04 11:26:22'),(2,'Anit corruption','active','2015-08-04 11:26:47');

/*Table structure for table `sub_menu` */

DROP TABLE IF EXISTS `sub_menu`;

CREATE TABLE `sub_menu` (
  `sub_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_menu_name` varchar(50) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  KEY `sub_menu_id` (`sub_menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sub_menu` */

/*Table structure for table `training` */

DROP TABLE IF EXISTS `training`;

CREATE TABLE `training` (
  `training_id` int(11) NOT NULL AUTO_INCREMENT,
  `frequency_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `principle_id` int(11) DEFAULT NULL,
  `no_of_training` int(11) DEFAULT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  PRIMARY KEY (`training_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

/*Data for the table `training` */

insert  into `training`(`training_id`,`frequency_id`,`company_id`,`department_id`,`principle_id`,`no_of_training`,`file_name`,`user_id`,`status`,`create_date`,`expiry_date`) values (6,2,1,1,1,20053,NULL,1,'Approved','2015-08-16 00:00:00','2016-08-15 00:00:00'),(9,2,1,1,1,1111,NULL,1,'Approved','2015-08-16 00:00:00','2016-08-15 00:00:00'),(10,2,1,1,1,1,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(11,2,1,1,1,1,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(12,2,1,1,1,1,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(13,2,1,1,1,22,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(15,NULL,NULL,NULL,NULL,NULL,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(16,2,1,1,1,2,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(17,NULL,NULL,NULL,NULL,NULL,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(18,NULL,NULL,NULL,NULL,NULL,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(19,NULL,NULL,NULL,NULL,NULL,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(20,NULL,NULL,NULL,NULL,NULL,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(21,2,2,2,2,200,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(22,NULL,NULL,NULL,NULL,NULL,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(23,NULL,NULL,NULL,NULL,NULL,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(24,NULL,NULL,NULL,NULL,NULL,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(25,2,1,1,1,2,'1.xlsx',1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(26,2,1,1,1,2,NULL,3,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(27,2,1,1,1,2,NULL,3,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(28,2,2,2,2,33,NULL,3,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(29,2,2,2,2,33,NULL,3,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(30,2,2,2,2,33,NULL,3,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(31,2,2,2,2,33,NULL,3,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(32,2,1,1,1,2006,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(33,2,1,1,1,12,NULL,1,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00'),(34,2,1,1,1,2009,NULL,3,'Pending','2015-08-16 00:00:00','2016-08-15 00:00:00');

/*Table structure for table `user_profile` */

DROP TABLE IF EXISTS `user_profile`;

CREATE TABLE `user_profile` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(40) NOT NULL,
  `user_pwd` varchar(40) NOT NULL,
  `user_email` varchar(40) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `department_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `user_type` varchar(40) NOT NULL,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `user_profile` */

insert  into `user_profile`(`user_id`,`user_name`,`user_pwd`,`user_email`,`city_id`,`country_id`,`status`,`create_date`,`modified_date`,`department_id`,`company_id`,`user_type`) values (1,'champion1','202cb962ac59075b964b07152d234b70','noman@gmail.com',2,1,'A','2015-08-13 10:05:38','0000-00-00 00:00:00',1,1,'champion'),(2,'champion2','202cb962ac59075b964b07152d234b70','uzair@gmail.com',2,2,'A','2015-08-13 10:05:44','0000-00-00 00:00:00',2,2,'champion'),(3,'admin','202cb962ac59075b964b07152d234b70','admin@gmail.com',2,2,'A','2015-08-10 17:31:59','0000-00-00 00:00:00',2,2,'admin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

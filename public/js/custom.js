/**
 * Created by Admin on 8/6/2015.
 */

/*menu handler*/
$(document).ready(function() {
    $(function () {
        function stripTrailingSlash(str) {
            if (str.substr(-1) == '/') {
                return str.substr(0, str.length - 1);
            }
            return str;
        }
        var url1 = window.location.pathname;
        var activePage1 = stripTrailingSlash(url1);
        $('.sidebar-menu li a').each(function () {

            var currentPage1 = stripTrailingSlash($(this).attr('href'));
            //console.log(activePage1 );
            if (activePage1 == currentPage1) {
                $(this).parent().addClass('active ');
                $(this).parent().parent().css('display', 'block');
                $(this).parent().parent().parent().addClass('active');
            }
        });

    });
});


$(function(){
    function stripTrailingSlash(str) {
        if(str.substr(-1) == '/') {
            return str.substr(0, str.length - 1);
        }
        return str;
    }

    var url = window.location.pathname;
    var activePage = stripTrailingSlash(url);

    $('.second_menu li a').each(function(){
        var currentPage = stripTrailingSlash($(this).attr('href'));

        if (activePage == currentPage) {
            $(this).parent().addClass('active');
        }
    });

});

/* ================== tABLE ==================== */
$(document).ready(function () {

    var url = window.location.pathname;
   // var activePage = stripTrailingSlash(url);


     $('table').dataTable({
          "bPaginate": false,
          "dom": '<""f>rt<"bottom"ip>',
		  "scrollX": true,
		  initComplete: function () {
             select_html = '<label>&nbsp;&nbsp;Status:&nbsp;&nbsp;<select id="DataTables_Table_status_filter" class="status_filter"><option value=""></option><option value="approved">Approved</option><option value="pending">Pending</option><option value="expired">Expired</option></select></label>';
            	
             $("#DataTables_Table_0_filter").append(select_html);
         

        }


      });
      $('table').removeClass('display').addClass('table table-striped table-bordered');
      $('.dataTables_filter input[type="search"]').attr({'placeholder': 'Search','id':'s_bar'}).css({
          'width': '250px',
          'display': 'inline-block'


      });

    var table = $('table').DataTable();
    var lastColumnIndex = $('table thead tr th').length / 2 - 1;

	$('#DataTables_Table_status_filter').on("change", function() {
		  table
        .columns( lastColumnIndex )
        .search( this.value )
        .draw();
    });


  });

/* ================== aDD mINUS ==================== */

(function($) {
    "use strict"
    // Accordion Toggle Items
    var iconOpen = 'glyphicon glyphicon-minus',
        iconClose = 'glyphicon glyphicon-plus';

    $(document).on('show.bs.collapse hide.bs.collapse', '.accordion', function (e) {
        var $target = $(e.target)
        $target.siblings('.accordion-heading')
            .find('i').toggleClass(iconOpen + ' ' + iconClose);
        if(e.type == 'show')
            $target.prev('.accordion-heading').find('.accordion-toggle').addClass('active');
        if(e.type == 'hide')
            $(this).find('.accordion-toggle').not($target).removeClass('active');
    });

})(jQuery);


(function ($) {
    "use strict";
    $(document).ready(function () {
        /*==Left Navigation Accordion ==*/
        if ($.fn.dcAccordion) {
            $('#nav-accordion').dcAccordion({
                eventType: 'click',
                autoClose: true,
                saveState: true,
                disableLink: true,
                speed: 'slow',
                showCount: false,
                autoExpand: true,
                classExpand: 'dcjq-current-parent'
            });
        }
        /*==Slim Scroll ==*/
        if ($.fn.slimScroll) {
            $('.event-list').slimscroll({
                height: '305px',
                wheelStep: 20
            });
            $('.conversation-list').slimscroll({
                height: '360px',
                wheelStep: 35
            });
            $('.to-do-list').slimscroll({
                height: '300px',
                wheelStep: 35
            });
        }
        /*==Nice Scroll ==*/
        if ($.fn.niceScroll) {


            $(".leftside-navigation").niceScroll({
                cursorcolor: "#1FB5AD",
                cursorborder: "0px solid #fff",
                cursorborderradius: "0px",
                cursorwidth: "3px"
            });

            $(".leftside-navigation").getNiceScroll().resize();
            if ($('#sidebar').hasClass('hide-left-bar')) {
                $(".leftside-navigation").getNiceScroll().hide();
            }
            $(".leftside-navigation").getNiceScroll().show();

            $(".right-stat-bar").niceScroll({
                cursorcolor: "#1FB5AD",
                cursorborder: "0px solid #fff",
                cursorborderradius: "0px",
                cursorwidth: "3px"
            });

        }


        /*==Collapsible==*/
        $('.widget-head').click(function (e) {
            var widgetElem = $(this).children('.widget-collapse').children('i');

            $(this)
                .next('.widget-container')
                .slideToggle('slow');
            if ($(widgetElem).hasClass('ico-minus')) {
                $(widgetElem).removeClass('ico-minus');
                $(widgetElem).addClass('ico-plus');
            } else {
                $(widgetElem).removeClass('ico-plus');
                $(widgetElem).addClass('ico-minus');
            }
            e.preventDefault();
        });




        /*==Sidebar Toggle==*/

        $(".leftside-navigation .sub-menu > a").click(function () {
            var o = ($(this).offset());
            var diff = 80 - o.top;
            if (diff > 0)
                $(".leftside-navigation").scrollTo("-=" + Math.abs(diff), 500);
            else
                $(".leftside-navigation").scrollTo("+=" + Math.abs(diff), 500);
        });



        $('.toggler').click(function (e) {

            $(".leftside-navigation").niceScroll({
                cursorcolor: "#1FB5AD",
                cursorborder: "0px solid #fff",
                cursorborderradius: "0px",
                cursorwidth: "3px"
            });

            $('#sidebar').toggleClass('hide-left-bar');
            if ($('#sidebar').hasClass('hide-left-bar')) {
                $(".leftside-navigation").getNiceScroll().hide();
              $(this).children('span').removeClass('fa-chevron-left');
                $(this).children('span').addClass('fa-chevron-right');
            }
            else{
                $(this).children('span').removeClass('fa-chevron-right');
                $(this).children('span').addClass('fa-chevron-left');
            }
            $(".leftside-navigation").getNiceScroll().show();
            $('#main-content').toggleClass('merge-left');
            e.stopPropagation();
            if ($('#container').hasClass('open-right-panel')) {
                $('#container').removeClass('open-right-panel')
            }
            if ($('.right-sidebar').hasClass('open-right-bar')) {
                $('.right-sidebar').removeClass('open-right-bar')
            }

            if ($('.header').hasClass('merge-header')) {
                $('.header').removeClass('merge-header')
            }


        });
        $('.toggle-right-box').click(function (e) {
            $('#container').toggleClass('open-right-panel');
            $('.right-sidebar').toggleClass('open-right-bar');
            $('.header').toggleClass('merge-header');

            e.stopPropagation();
        });

        $('.header,#main-content,#sidebar').click(function () {
            if ($('#container').hasClass('open-right-panel')) {
                $('#container').removeClass('open-right-panel')
            }
            if ($('.right-sidebar').hasClass('open-right-bar')) {
                $('.right-sidebar').removeClass('open-right-bar')
            }

            if ($('.header').hasClass('merge-header')) {
                $('.header').removeClass('merge-header')
            }


        });


        $('.panel .tools .fa').click(function () {
            var el = $(this).parents(".panel").children(".panel-body");
            if ($(this).hasClass("fa-chevron-down")) {
                $(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
                el.slideUp(200);
            } else {
                $(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
                el.slideDown(200); }
        });



        $('.panel .tools .fa-times').click(function () {
            $(this).parents(".panel").parent().remove();
        });

        // tool tips

        $('.tooltips').tooltip();

        // popovers

        $('.popovers').popover();


    });
$(document).ready(function(){
    //Hide all of Select region
    var indicator = $("#indicator_form_id");
    var frequencies = $("#frequency_form_ids");
    var frequency_id = $("#frequency_form_id");
    var frequency_name = $("#frequency_form_name");
    var frequency_opt = $("#frequency_form_ids option");
    var region = $("#region_form_id");
    var company = $("#company_form_id");
    var country = $("#country_form_id");
    var country_opt = $('#country_form_id option');
    var region_opt = $('#region_form_id option');

    // On load Dropdown values in Company, Region, Country all are set add or edit section
    var count = 0;
    var count_country = 0;
    $("select[name='company_id'] > option").each(function () {
        if(this.value == $("#company_form_id").val()){
            var company_id = $("#company_form_id").val();
            var region_repeat = {};
            $("select[name='region_id'] > option").each(function () {
                var id = $(this).val();
                count++;
                if ($(this).data('options') == undefined) {
                    /*Taking an array of all options-2 and kind of embedding it on the select1*/
                    $(this).data('options', region_opt.clone());
                }
                var options_region = $(this).data('options').filter('[data-company-id=' + company_id + ']');
                if(count == 1){
                    console.log(count + '' + region);
                    region.empty();
                    region.append(options_region);
                }

                if (region_repeat[this.text]) {
                    //alert(region_repeat[this.text]);
                    $(this).remove();
                } else {
                    region_repeat[this.text] = this.value;
                }
                if($("select[name='region_id'] > option:selected").val()){
                    if(this.value == $("select[name='region_id'] > option:selected").val()) {
                        var country_repeat = {};
                        $("select[name='country_id'] > option").each(function () {
                            count_country++;
                            if ($(this).data('options') == undefined) {
                                /*Taking an array of all options-2 and kind of embedding it on the select1*/
                                $(this).data('options', country_opt.clone());
                            }
                            var options_country = $(this).data('options').filter('[data-region-id=' + id + '][data-company-id=' + company_id + ']');
                            if(count_country == 1){
                                country.empty();
                                country.append(options_country);
                            }
                            if (country_repeat[this.text]) {
                                $(this).remove();
                            } else {
                                country_repeat[this.text] = this.value;
                            }
                        });
                    }
                }else{
                    country.empty();
                }
            });
        }
    });
    //$("#created_date").attr('readonly',true);
    //$("#company_form_id").children('option:not(:selected)').attr('disabled',true);
    //$("#indicator_form_id").children('option:not(:selected)').attr('disabled',true);
    //$("#country_form_id").children('option:not(:selected)').attr('disabled',true);
    //$("#region_form_id").children('option:not(:selected)').attr('disabled',true);
    // On load Dropdown values in Company, Region, Country all are set add or edit section

    company.bind('change', function() {
        region.children('option').show();

        if ($(this).data('options') == undefined) {
            /*Taking an array of all options-2 and kind of embedding it on the select1*/
            $(this).data('options', region_opt.clone());
        }
        var id = $(this).val();
        var options_region = $(this).data('options').filter('[data-company-id=' +id+']');
        region.empty();
        country.empty();
        region.append('<option value=""></option>');
        region.append(options_region);
        /*remove repeated option of region*/
        var region_repeat = {};
        $("select[name='region_id'] > option").each(function () {
            if(region_repeat[this.text]) {
                $(this).remove();
            } else {
                region_repeat[this.text] = this.value;
            }
        });
        $("#region_form_id").prop('selectedIndex', 0);
        $("#country_form_id").prop('selectedIndex', 0);
    });

    //Hide all of Select region

    region.on('change', function() {
        country.children('option').show();

        // console.log(filter.join(","));
        if ($(this).data('options') == undefined) {
            /*Taking an array of all options-2 and kind of embedding it on the select1*/
            $(this).data('options', country_opt.clone());
        }
        var id = $(this).val();
        var company_id = ($("#company_form_id").val());
        var options_country = $(this).data('options').filter('[data-region-id=' + id +'][data-company-id=' + company_id +']');
        country.empty();
        country.append('<option value=""></option>');
        country.append(options_country);
        /*remove repeated option of country*/
        var country_repeat = {};
        $("select[name='country_id'] > option").each(function () {
            if(country_repeat[this.text]) {
                $(this).remove();
            } else {
                country_repeat[this.text] = this.value;
            }
        });
    });
    //Hide all of Select frequencies by indicators

    indicator.on('change', function() {
        // console.log(filter.join(","));
        if ($(this).data('options') == undefined) {
            /*Taking an array of all options-2 and kind of embedding it on the select1*/
            $(this).data('options', frequency_opt.clone());
        }
        var id = $(this).val();
        var options_frequency = $(this).data('options').filter('[data-indicator-id=' + id +']');

        frequencies.empty();
        frequencies.append(options_frequency);
       // frequency.val(options_frequency);
        frequency_name.val($("#frequency_form_ids option:first").html());
        frequency_id.val($("#frequency_form_ids option:first").val());
    });
$('#frequencies_div').hide();
if(frequency_id.val() != ''){

    frequency_name.val($("#frequency_form_ids  option[value="+frequency_id.val()+"]").html());
}
    $('#create_date').datetimepicker({
        format: 'YYYY-MM-DD',
        maxDate : 'now',
        viewMode: 'years'
    });
/*collapse panel*/
    $(document).on('click', '.panel-heading span.clickable', function(e){
        var $this = $(this);
        if(!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp().removeClass('hide');
            //$this.parents('.panel').find('.panel-body').removeClass('hide');
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown().addClass('hide');
           // $this.parents('.panel').find('.panel-body').addClass('hide');
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
        }
    });
/*collapse panel*/
    /*multiselect*/
    $("select[multiple]").multipleSelect({
        width: '100%'
    });
   /* $("select[multiple] >option").each(function() {
    //  $(this).css('background','red');
       // console.log( $(this));
        $(this).prepend("<span>aaa</span>");
    });*/
    /*multiselect*/
});
})(jQuery);